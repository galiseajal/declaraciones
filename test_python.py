# -- coding: utf-8 --
import requests
import time
import pytest
import random
import string
import pprint
import json

#pytest.host = "http://189.213.134.151:8080/api/"
#pytest.host = "http://54.173.200.251/api/"
pytest.host = "http://ec2-3-235-133-6.compute-1.amazonaws.com/api/"
pytest.tokenbearer = "BUP4MkZLQoDQKMzJvNigSK1zMAvf9S"

@pytest.fixture
def gettoken():
    data = {
        #"grant_type": "authorization_code",
        #"code": "1xWpREheoai3frZDE6vaWHNp7FYkLu",
        "grant_type": "password",
        "client_id":"client_id",
        "client_secret":"client_secret",
        "scope":"read write",
        "username":"plataforma",
        "password":"digitalnacional"
    }
    response = requests.post(
            url= "http://ec2-3-235-133-6.compute-1.amazonaws.com/o/token/",
        data=data
    ).json()
    
    tokenbearer = response["access_token"]
    pytest.tokenbearer = tokenbearer

@pytest.mark.test_1
def test01(gettoken):
    data = {
        #"grant_type": "authorization_code",
        #"code": "1xWpREheoai3frZDE6vaWHNp7FYkLu",
        "grant_type": "password",
        "client_id":"client_id",
        "client_secret":"client_secret",
        "scope":"read write",
        "username":"plataforma",
        "password":"digitalnacional"
    }
    response = requests.post(
        url= "http://ec2-3-235-133-6.compute-1.amazonaws.com/o/token/",
        data=data
    ).json()

    
    assert "refresh_token" in response
    assert "access_token" in response
    assert "token_type" in response
    assert "expires_in" in response



@pytest.mark.test_3
def test_03():
    data = {
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    app_json = json.dumps(response, sort_keys=True)
    f= open("guru99.json","w+")
    f.write(str(app_json))
    f.close()
    '''assert "pagination" in response
    pagination = response["pagination"]
    assert "pageSize" in pagination
    assert "page" in pagination
    assert "totalRows" in pagination
    assert "hasNextPage" in pagination
    assert "results" in response
    results = response["results"]
    assert type(results) == list
    assert len(response["results"]) > 0
    result = response["results"][0]
    assert "id" in result
    assert "metadata" in result
    assert "declaracion" in result'''


@pytest.mark.test_4
def test_04():
    """ 
    1 .- This depends on the loaded data...
    2 .- size of the page... if only the first
    10th elemnts are not of that type this 
    would fail...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    # Declaraciones.objects.filter(cat_tipos_declaracion__codigo="MODIFICACIÓN")
    Declaraciones.objects.filter(cat_tipos_declaracion__codigo="Modificada")
    # Crear una...
    CatTiposDeclaracion.objects.get(codigo="Modificada") 
    """
    data = {}
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    for result in response["results"]:
        if "tipo" in result["metadata"]:
            pass
        pass
    pass


@pytest.mark.test_5
def test_05():
    """ 
    1 .- This depends on the loaded data...
    2 .- size of the page... if only the first
    10th elemnts are not of that type this 
    would fail...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    # Declaraciones.objects.filter(cat_tipos_declaracion__codigo="MODIFICACIÓN")
    Declaraciones.objects.filter(cat_tipos_declaracion__codigo="Modificada")
    # Crear una...
    CatTiposDeclaracion.objects.get(codigo="Conclusión") 
    """
    data = {}
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    for result in response["results"]:
        if "tipo" in result["metadata"]:
            pass
        pass
    pass


@pytest.mark.test_6
def test_06():
    """ 
    send "a next page"

    """
    data = {}
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert "pagination" in response
    pagination = response["pagination"]

    assert "totalRows" in pagination
    total_rows = int(pagination["totalRows"])

    if total_rows > 10:
        page_calculado = (total_rows/10) + 1
    else:
        page_calculado = 1

    data = {
        "query":{
            "page": page_calculado
        }
    }
    print(data)

    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    pagination2 = response2["pagination"]
    print(pagination2)
    assert(pagination2["page"] == page_calculado)



@pytest.mark.test_7
def test_07():
    """ 
    send "diff page sizes"

    """
    data = {
        "pageSize": 1
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    data = {
        "pageSize": 15
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    data = {
        "pageSize": 30
    }
    response3 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert "pagination" in response
    pagination =  response["pagination"]
    assert "pagination" in response2
    pagination2 = response2["pagination"]
    assert "pagination" in response3
    pagination3 = response3["pagination"]

    assert pagination["pageSize"] == 1
    assert pagination2["pageSize"] == 15
    assert pagination3["pageSize"] == 30
    

@pytest.mark.test_8
def test_08():
    """ Filter by folio
    """
    data = {
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert "pagination" in response
    pagination =  response["pagination"]
    
    random_result = random.choice(
        response["results"]
    )
    result_id = random_result["id"]
    data = {
        "query":{
            "id": result_id
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) == 1
    


@pytest.mark.test_9
def test_09():
    """ Filter by folio (id)
    """
    data = {
        "query":{
            "id": "910761fd-0134-43a4-9752-eb1a06683091"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) == 0
    pass

    
@pytest.mark.test_10
def test_10():
    """ Filter by folio (id)
    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    

    """
    data = {
        "query": {
            "nombres": "PEDRO"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) > 0
    data = {
        "query": {
            "nombres": "asdadasdasd"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) == 0
    pass

@pytest.mark.test_11
def test_11():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    

    """
    data = {
        "query": {
            "nombres": "PED"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) > 0
    data = {
        "query": {
            "nombres": "asdadasdasd"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) == 0
    pass


@pytest.mark.test_13
def test_13():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    [ dec.info_personal_fija.nombres for dec in Declaraciones.objects.a
   ...: ll()]  

    """
    data = {
        "query": {
            "nombres": "PÉDRO"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    data = {
        "query": {
            "nombres": "PEDRO"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert len(response2["results"]) == len(response["results"])
    pass

@pytest.mark.test_14
def test_14():
    """ Filter by primerapellido (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    [ dec.info_personal_fija.primer_apellido for dec in Declaraciones.objects.a
   ...: ll()]  
    """
    data = {
        "query": {
            "primerApellido": "PERAFAN"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) > 0
    data = {
        "query": {
            "primerApellido": "asdadasdasd"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) == 0


@pytest.mark.test_15
def test_15():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion
    """
    data = {
        "query": {
            "primerApellido": "PERA"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) > 0
    data = {
        "query": {
            "primerApellido": "asdadasdasd"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) == 0
    pass

@pytest.mark.test_16
def test_16():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    [ dec.info_personal_fija.nombres for dec in Declaraciones.objects.a
   ...: ll()]  

    """
    data = {
        "query": {
            "primerApellido": "PerAFAN"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0
    data = {
        "query": {
            "primerApellido": "PERAFAN"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert len(response2["results"]) == len(response["results"])
    pass


@pytest.mark.test_17
def test_17():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    [ dec.info_personal_fija.nombres for dec in Declaraciones.objects.a
   ...: ll()]  

    """
    #REGISTRO GUARDADO <<<<SIN ACENTO>>>>
    data = {
        "query": {
            "primerApellido": "VILLÁREAL"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    data = {
        "query": {
            "primerApellido": "VILLAREAL"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert len(response["results"]) == len(response2["results"])



@pytest.mark.test_18
def test_18():
    """ Filter by primerapellido (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    [ dec.info_personal_fija.apellido1 for dec in Declaraciones.objects.all: ll()]  
    """
    data = {
        "query": {
            "segundoApellido": "TORRES"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) > 0
    data = {
        "query": {
            "segundoApellido": "asdadasdasd"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) == 0


@pytest.mark.test_19
def test_19():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion
    """
    data = {
        "query": {
            "segundoApellido": "TORRES"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0
    data = {
        "query": {
            "segundoApellido": "asdadasdasd"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0
    pass


@pytest.mark.test_20
def test_20():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    [ dec.info_personal_fija.nombres for dec in Declaraciones.objects.all()]  

    """
    data = {
        "query": {
            "segundoApellido": "ToRRES"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0

    data = {
        "query": {
            "segundoApellido": "TORRES"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert len(response2["results"]) == len(response["results"])
    pass


@pytest.mark.test_21
def test_21():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    [ dec.info_personal_fija.nombres for dec in Declaraciones.objects.a
   ...: ll()]  

    """
    data = {
        "query": {
            "segundoApellido": "ESCÓBAR"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    total_rows1 = response["pagination"]["totalRows"]

    data = {
        "query": {
            "segundoApellido": "ESCOBAR"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    total_rows2 = response2["pagination"]["totalRows"]
    assert total_rows1 == total_rows2


@pytest.mark.test_22
def test_22():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ (dec.info_personal_fija.nombres, dec.info_personal_fija.apellido1) for dec in Declaraciones.objects.all()]  

    """
    data = {}
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    data2 = {
        "query": {
            "nombre": "RICARDO RAUL",
            "primerApellido": "MONTES"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data2,
        headers=headers
    ).json()

    result = response2["results"][0]
    assert result in (response2["results"]) and result in (response["results"])


@pytest.mark.test_23
def test_23():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ (dec.info_personal_fija.nombres, dec.info_personal_fija.apellido1) for dec in Declaraciones.objects.all()]  

    """
    data = {
        "query": {
            "nombre": "PEDRO"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0

    data = {
        "query": {
            "segundoApellido": "TORRES"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    data = {
        "query": {
            "nombre": "PEDRO",
            "segundoApellido": "TORRES"
        }
    }
    response3 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    result = response3["results"][0]
    assert result in (response2["results"]) and result in (response["results"])


@pytest.mark.test_24
def test_24():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ (dec.info_personal_fija.nombres, dec.info_personal_fija.apellido1) for dec in Declaraciones.objects.all()]  

    """
    data = {
        "query": {
            "primerApellido": "CARRILLO"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    #assert len(response["results"]) > 0
    data = {
        "query": {
            "segundoApellido": "LOMA"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    data = {
        "query": {
            "primerApellido": "CARRILLO",
            "segundoApellido": "LOMA"
        }
    }
    response3 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    #result = response3["results"][0]
    #assert result in (response2["results"]) and result in (response["results"])


@pytest.mark.test_25
def test_25():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ (dec.info_personal_fija.nombres, dec.info_personal_fija.apellido1) for dec in Declaraciones.objects.all()]  

    """
    data = {
        "query": {
            "primerApellido": "PERAFAN"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0
    data = {
        "query": {
            "segundoApellido": "TORRES"
        }
    }
    data = {
        "query": {
            "nombres": "Pédro"
        }
    }
    response1 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0
    data = {
        "query": {
            "segundoApellido": "TORRES"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    data = {
        "query": {
            "nombres": "Pédro",
            "primerApellido": "PERAFAN",
            "segundoApellido": "TORRES"
        }
    }
    response3 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    result = response3["results"][0]
    assert (
        result in (response2["results"]) and
        result in (response1["results"]) and
        result in (response["results"]) 
    )


@pytest.mark.test_26
def test_26():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ dc.cat_grados_academicos.grado_academico for dc in dec.datoscurriculares_set.all()] for dec in Declaraciones.objects.all()]  

    """
    data = {
        "query": {
            "escolaridadNivel": "CARRERA TÉCNICA O COMERCIAL"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0

@pytest.mark.test_27
def test_27():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ dc.cat_grados_academicos.grado_academico for dc in dec.datoscurriculares_set.all()] for dec in Declaraciones.objects.all()]  

    """
    data = {
        "query": {
            "escolaridadNivel": "CLAVE PRUEBA"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_28
def test_28():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ encargo.cat_entes_publicos.ente_publico for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "nombreEntePublico": "Contraloría General"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_29
def test_29():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ encargo.cat_entes_publicos.ente_publico for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "nombreEntePublico": "Contraloría"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_30
def test_30():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ encargo.cat_entes_publicos.ente_publico for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "nombreEntePublico": "Contraloría"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_31
def test_31():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ encargo.cat_entes_publicos.ente_publico for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "nombreEntePublico": "CONTraloría"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_32
def test_32():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [Declaraciones.objects.first().info_personal_fija.cat_entidades_federativas.entidad_federativa for dec in Declaraciones.objects.all()]
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "entidadFederativa": "Jalisco"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0


@pytest.mark.test_33
def test_33():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ encargo.cat_entes_publicos.ente_publico for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "entidadFederativa": "“entidadFederativaPrueba”"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) == 0


@pytest.mark.test_34
def test_34():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    encargos__domicilios__municipio__valor__icontains
    [ [ encargo.domicilios.municipio.valor for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "municipioAlcaldia": "Atenguillo"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_35
def test_35():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ encargo.cat_entes_publicos.ente_publico for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "municipioAlcaldia": "municipioAlcaldiatest"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_35_2
def test_35_2():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    encargos__empleo_cargo_comision__icontains
    [ [ encargo.empleo_cargo_comision for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "empleoCargoComision": "Jefe de departamento"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_36
def test_36():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    encargos__empleo_cargo_comision__icontains
    [ [ encargo.empleo_cargo_comision for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "empleoCargoComision": "Jefe de DEPARTAMENTO"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_37
def test_37():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    encargos__empleo_cargo_comision__icontains
    [ [ encargo.empleo_cargo_comision for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "empleoCargoComision": "JefÉ dÉ DEPARTAMENTO"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_38
def test_38():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    encargos__empleo_cargo_comision__icontains
    [ [ encargo.empleo_cargo_comision for encargo in dec.encargos_set.all() if encargo.cat_entes_publicos] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "empleoCargoComision": "JEFE DE DEPARTAMENTO"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    if len(response["results"]) > 0:
        print(len(response["results"]))
    else:
        print({})
    #assert len(response["results"]) > 0


@pytest.mark.test_39
def test_39():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    encargos__cat_ordenes_gobierno__orden_gobierno__icontains
    [ [ encargo.cat_ordenes_gobierno.orden_gobierno for encargo in dec.encargos_set.all() if encargo.cat_ordenes_gobierno] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "nivelOrdenGobierno": "Federal"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0


@pytest.mark.test_40
def test_40():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ encargo.nivel_encargo for encargo in dec.encargos_set.all()] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "nivelEmpleoCargoComision": "de departamento"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert len(response["results"]) > 0


@pytest.mark.test_41
def test_41():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ encargo.nivel_encargo for encargo in dec.encargos_set.all()] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "nivelEmpleoCargoComision": "Jefatura de"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert len(response["results"]) > 0


@pytest.mark.test_42
def test_42():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ encargo.nivel_encargo for encargo in dec.encargos_set.all()] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "datosEmpleoCargoComision":
                {   
                    "nivelEmpleoCargoComision": "Jefatura de DEPARTAMENTO"
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert len(response["results"]) > 0

@pytest.mark.test_43
def test_43():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    # bienesinmuebles__superficie_construccion__gte
    [  [bi.superficie_construccion for bi dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    min_val = 510
    max_val = 600
    data = {
        "query": {
            "bienesInmuebles":
                {   
                    "superficieConstruccion":{
                        "min": min_val
                    }
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert len(response["results"]) > 0

    data = {
        "query": {
            "bienesInmuebles":
                {   
                    "superficieConstruccion":{
                        "max": max_val
                    }
                }
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response2["results"]) > 0

    data = {
        "query": {
            "bienesInmuebles":
                {   
                    "superficieConstruccion":{
                        "min": min_val,
                        "max": max_val,
                    }
                }
        }
    }
    response3 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    """
    for x in response["results"]:
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            pprint.pprint(y["superficieConstruccion"])
    print("-----")
    
    for x in response2["results"]:
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            pprint.pprint(y["superficieConstruccion"])
    
    print("-----")
    for x in response3["results"]:
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            pprint.pprint(y["superficieConstruccion"])
    """
    for r in response3["results"] :
        assert r in response["results"]
        assert r in response2["results"]

@pytest.mark.test_44
def test_44():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    
    [  [bi.superficie_terreno for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    min_val = 250
    max_val = 500
    data = {
        "query": {
            "bienesInmuebles":
                {   
                    "superficieTerreno":{
                        "min": min_val
                    }
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0

    data = {
        "query": {
            "bienesInmuebles":
                {   
                    "superficieTerreno":{
                        "max": max_val
                    }
                }
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0

    data = {
        "query": {
            "bienesInmuebles":
                {   
                    "superficieTerreno":{
                        "min": min_val,
                        "max": max_val,
                    }
                }
        }
    }
    response3 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    for r in response3["results"]:
        assert r in response["results"]
        assert r in response2["results"]
    


@pytest.mark.test_45
def test_45():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.precio_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    min_val = 510
    max_val = 600
    data = {
        "query": {
            "bienesInmuebles":
                {   
                    "valorAdquisicion":{
                        "min": min_val
                    }
                }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0
    data = {
        "query": {
            "bienesInmuebles":
                {   
                    "valorAdquisicion":{
                        "max": max_val
                    }
                }
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) > 0
    data = {
        "query": {
            "bienesInmuebles":
                {   
                    "valorAdquisicion":{
                        "min": min_val,
                        "max": max_val,
                    }
                }
        }
    }
    response3 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    

@pytest.mark.test_46
def test_46():
    pass




@pytest.mark.test_47
def test_47():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "bienesInmuebles":{
                "formaAdquisicion": "Donación"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    if len(response["results"]) == 0:
        print({})
    else:
        print(len(response["results"]))


    #assert len(response["results"]) > 0


@pytest.mark.test_48
def test_48():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "query": {
            "bienesInmuebles":{
                "formaAdquisicion": "DonaciónNNEXISTENT"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    assert len(response["results"]) == 0


@pytest.mark.test_49
def test_49():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
            "nombres": "asc"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["nombre"] \
        for res in response["results"] 
    ])
    data = {
        "sort": {
            "nombres": "desc"
        }
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["nombre"] \
        for res in response["results"] 
    ])


@pytest.mark.test_50
def test_50():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
            "primerApellido": "asc"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["primerApellido"] \
        for res in response["results"] 
    ])
    data = {
        "sort": {
            "primerApellido": "desc"
        }
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["primerApellido"] \
        for res in response["results"] 
    ])

@pytest.mark.test_51
def test_51():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
            "segundoApellido": "asc"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["segundoApellido"] \
        for res in response["results"] 
    ])
    data = {
        "sort": {
            "segundoApellido": "desc"
        }
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["segundoApellido"] \
        for res in response["results"] 
    ])

@pytest.mark.test_52
def test_52():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
            "escolaridadNivel": "asc"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["segundoApellido"] \
        for res in response["results"] 
    ])

    data = {
        "sort": {
            "escolaridadNivel": "desc"
        }
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["segundoApellido"] \
        for res in response["results"] 
    ])


@pytest.mark.test_53
def test_53():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
            "datosEmpleoCargoComision":
            {
                "nombreEntePublico": "asc"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["segundoApellido"] \
        for res in response["results"] 
    ])
    data = {
        "sort": {
            "nombreEntePublico": "desc"
        }
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["segundoApellido"] \
        for res in response["results"] 
    ])


@pytest.mark.test_54
def test_54():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
        "datosEmpleoCargoComision":
            {
                "municipioAlcaldia": "asc"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    print([ 
        res["declaracion"]["situacionPatrimonial"]["domicilioDeclarante"]["domicilioMexico"]["municipioAlcaldia"]["valor"] \
        for res in response["results"] 
    ])
    data = {
        "sort": {
            "municipioAlcaldia": "desc"
        }
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    print([ 
        res["declaracion"]["situacionPatrimonial"]["domicilioDeclarante"]["domicilioMexico"]["municipioAlcaldia"]["valor"] \
        for res in response["results"] 
    ])

@pytest.mark.test_55
def test_55():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
            "datosEmpleoCargoComision":
            {
                "empleoCargoComision": "asc"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["nombre"] \
        for res in response["results"] 
    ])
    data = {
        "sort": {
            "empleoCargoComision": "desc"
        }
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["nombre"] \
        for res in response["results"] 
    ])

@pytest.mark.test_56
def test_56():
    """ Filter by sort nivelEmpleoCargoComision

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
            "datosEmpleoCargoComision":
            {
                "nivelEmpleoCargoComision": "asc"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["segundoApellido"] \
        for res in response["results"] 
    ])
    data = {
        "sort": {
            "nivelEmpleoCargoComision": "desc"
        }
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["segundoApellido"] \
        for res in response["results"] 
    ])

@pytest.mark.test_57
def test_57():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
            "empleoCargoComision": "asc"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["segundoApellido"] \
        for res in response["results"] 
    ])
    data = {
        "sort": {
            "empleoCargoComision": "desc"
        }
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    print([ 
        res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["segundoApellido"] \
        for res in response["results"] 
    ])


@pytest.mark.test_58
def test_58():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [ [ encargo.cat_ordenes_gobierno.orden_gobierno for encargo in dec.encargos_set.all() if encargo.cat_ordenes_gobierno] for dec in Declaraciones.objects.all()]
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data1 = {
        "sort": {
            "datosEmpleoCargoComision":
            {
                "nivelOrdenGobierno": "asc"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data1,
        headers=headers
    ).json()
   
    data2 = {
        "sort": {
            "datosEmpleoCargoComision":
            {
                "nivelOrdenGobierno": "desc"
            }
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data2,
        headers=headers
    ).json()
    res1 =[]
    res2 =[]
    for res in response["results"]:
        if "datosEmpleoCargoComision" in res["declaracion"]["situacionPatrimonial"]:
            res1.append(res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["nombre"]+": "+res["declaracion"]["situacionPatrimonial"]["datosEmpleoCargoComision"]["nivelOrdenGobierno"])
    for res in response2["results"]:
        if "datosEmpleoCargoComision" in res["declaracion"]["situacionPatrimonial"]:
            res2.append(res["declaracion"]["situacionPatrimonial"]["datosGenerales"]["nombre"]+": "+res["declaracion"]["situacionPatrimonial"]["datosEmpleoCargoComision"]["nivelOrdenGobierno"])

    
@pytest.mark.test_59
def test_59():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data1 = {
        "sort": {
            "totalIngresosMensualesNetos": "asc"
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data1,
        headers=headers
    ).json()
    
    data2 = {
        "sort": {
            "totalIngresosMensualesNetos": "desc"
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data2,
        headers=headers
    ).json()
    
    res1 =[]
    res2 =[]
    for res in response["results"]:
        if "ingresos" in res["declaracion"]["situacionPatrimonial"]:
            if "totalIngresosMensualesNetos" in  res["declaracion"]["situacionPatrimonial"]["ingresos"]:
                res1.append(res["declaracion"]["situacionPatrimonial"]["ingresos"]["totalIngresosMensualesNetos"]["valor"])
    for res in response2["results"]:
        if "ingresos" in res["declaracion"]["situacionPatrimonial"]:
            if "totalIngresosMensualesNetos" in res["declaracion"]["situacionPatrimonial"]["ingresos"]:
                res2.append(res["declaracion"]["situacionPatrimonial"]["ingresos"]["totalIngresosMensualesNetos"]["valor"])
    

@pytest.mark.test_60
def test_60():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
            "bienesInmuebles": {
                "superficieConstruccion": "asc"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    data = {
        "sort": {
            "bienesInmuebles": {
                "superficieConstruccion": "desc"
            }
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    for x in response["results"]:
        print("xxxx")
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            pprint.pprint(y["superficieConstruccion"])
    print("----")
    for x in response2["results"]:
        print("xxxx")
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            pprint.pprint(y["superficieConstruccion"])


@pytest.mark.test_61
def test_61():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data = {
        "sort": {
            "bienesInmuebles": {
                "superficieTerreno": "asc"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    
    data = {
        "sort": {
            "bienesInmuebles": {
                "superficieTerreno": "desc"
            }
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()
    for x in response["results"]:
        print("xxxx")
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            pprint.pprint(y["superficieTerreno"])
    print("----")
    for x in response2["results"]:
        print("xxxx")
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            pprint.pprint(y["superficieTerreno"])


@pytest.mark.test_62
def test_62():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data1 = {
        "sort": {
            "bienesInmuebles": {
                "formaAdquisicion": "asc"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data1,
        headers=headers
    ).json()
    
    data2 = {
        "sort": {
            "bienesInmuebles": {
                "formaAdquisicion": "desc"
            }
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data2,
        headers=headers
    ).json()

    for x in response["results"]:
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            print(x["declaracion"]["situacionPatrimonial"]["datosGenerales"]["nombre"])
            pprint.pprint(y["formaAdquisicion"])
    
    for x in response2["results"]:
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            print(x["declaracion"]["situacionPatrimonial"]["datosGenerales"]["nombre"])
            pprint.pprint(y["formaAdquisicion"])


@pytest.mark.test_63
def test_63():
    """ Filter by folio (id)

    List names...
    from declaracion.models.informacion_personal import Declaraciones
    [  [bi.cat_formas_adquisiciones.forma_adquisicion for bi in dec.bienesinmuebles_set.all()] for dec in Declaraciones.objects.all()] 
    query data:
    Must be a child element of "datosEmpleoCargoComision"
    """
    data1 = {
        "sort": {
            "bienesInmuebles": {
                "valorAdquisicion": "asc"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data1,
        headers=headers
    ).json()
    
    data2 = {
        "sort": {
            "bienesInmuebles": {
                "valorAdquisicion": "desc"
            }
        }
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data2,
        headers=headers
    ).json()

    for x in response["results"]:
        print(x["declaracion"]["situacionPatrimonial"]["datosGenerales"]["nombre"])
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            pprint.pprint(y["valorAdquisicion"])

    for x in response2["results"]:
        print(x["declaracion"]["situacionPatrimonial"]["datosGenerales"]["nombre"])
        for y in x["declaracion"]["situacionPatrimonial"]["bienesInmuebles"]["bienInmueble"]:
            pprint.pprint(y["valorAdquisicion"])

