# -- coding: utf-8 --
import requests
import time
import pytest
import random
import string
import pprint
import json

pytest.host = "http://189.213.134.151:8080/api/"
#pytest.host = "http://localhost:8080/api/"
#pytest.host = "http://54.173.200.251/api/"
#pytest.host = "http://ec2-3-235-133-6.compute-1.amazonaws.com/api/"

pytest.u_token = "http://189.213.134.151:8080/o/token/"
#pytest.u_token = "http://localhost:8080/o/token/"
#pytest.u_token = "http://localhost:8080/o/token/"
#pytest.u_token = "http://ec2-3-235-133-6.compute-1.amazonaws.com/o/token/"


@pytest.mark.test_2
def test_02():
    # parte 1
    #headers = {
    #    "Authorization": "Bearer {}".format(local_tokenbearer)
    #}
    data = {
        "username": "plataforma",
        "password": "digitalnacional",
        "grant_type": "password",
        "client_id": "client_id",
        "client_secret": "client_secret",
        "scope": "read write",
    }
    response1 = requests.post(
        url= pytest.u_token,
        data=data, 
    #    headers=headers
    ).json()
    
    local_tokenbearer = response1["access_token"]

    # parte 2
    expires_in = int(response1["expires_in"])
    expires_in_3 = (expires_in / 3) 

    # parte 3
    for x in range(1,2):
        time.sleep(expires_in_3)
        for_headers = {
            "Authorization": "Bearer {}".format(local_tokenbearer)
        }
        data = {}
        response = requests.post(
            url= pytest.host + "v2/declaraciones/",
            json= data,
            headers= for_headers
        ).json()

        assert "pagination" in response
        pagination = response["pagination"]
        assert "pageSize" in pagination
        assert "page" in pagination
        assert "totalRows" in pagination
        assert "hasNextPage" in pagination

        assert "results" in response
        results = response["results"]
        #assert "id" in results
        #assert "metadata" in results
        for declaracion in results:
            assert "declaracion" in declaracion    
            assert "id" in declaracion
            assert "metadata" in declaracion
        # ?
        #declaracion = results["declaracion"]
        
    time.sleep(5) 
    headers = {
            "Authorization": "Bearer {}".format(local_tokenbearer)
        }
    data = {}
    response4 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert "code" in response4
    assert "message" in response4
    print("******respuesta final de token caducado****->  " , response4["message"])
    assert response4["message"] == "acceso no autorizado" 