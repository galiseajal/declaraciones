# -*- coding: UTF-8 -*-
from __future__ import unicode_literals
import urllib
import uuid
# from django.core.files.storage import default_storage
from django.contrib import messages
from django.contrib.auth import login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordResetView
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.utils.http import urlsafe_base64_decode
from django.views.generic import (TemplateView, FormView, RedirectView,
                                  ListView)
from django.views import View
from django.conf import settings
from django.utils.encoding import force_text
from sitio.util import account_activation_token
from sitio.forms import PasswordResetForm
from sitio.forms import LoginForm
from declaracion.models import (Declaraciones, InfoPersonalVar,
                                InfoPersonalFija, DatosCurriculares, Encargos,
                                ExperienciaLaboral, ConyugeDependientes,
                                Observaciones, SeccionDeclaracion, Secciones, IngresosDeclaracion, 
                                MueblesNoRegistrables, BienesInmuebles, ActivosBienes, BienesPersonas, 
                                BienesMuebles,SociosComerciales,Membresias, Apoyos,ClientesPrincipales,
                                BeneficiosGratuitos, Inversiones, DeudasOtros, PrestamoComodato, DeclaracionFiscal)
from sitio.models import sitio_personalizacion
from declaracion.forms import ConfirmacionForm
from django.contrib.auth.models import User

from declaracion.views.utils import campos_configuracion_todos,declaracion_datos
from django.core import serializers
import json
from datetime import datetime, date
from django.db.models import Q

from weasyprint import HTML
from weasyprint.fonts import FontConfiguration
from django.http import HttpResponse
from django.template.loader import render_to_string
from itertools import chain
from .models import declaracion_faqs as faqs, sitio_personalizacion as personalizacion
from declaracion.views.utils import campos_configuracion_todos


class call_baseCSS(TemplateView):
    template_name = 'layout/base.css'


class IndexView(ListView):
    model = personalizacion
    template_name = "sitio/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["body_bg"] = "white";
        
        try:
            context['personalizacion'] = personalizacion.objects.all()[0]
        except Exception as e:
            pass
            
        return context


class FAQsView(TemplateView):
    template_name = "sitio/faqs.html"

    def get_context_data(self, **kwargs):
        context = super(FAQsView, self).get_context_data(**kwargs)
        queryset = faqs.objects.all().order_by("orden").exclude(orden=0)
        context.update({'questions': queryset})
        return context


class InformacionView(TemplateView):
    template_name = "sitio/informacion.html"


class DeclaracionesPreviasView(ListView):
    template_name = "sitio/declaraciones-previas.html"
    context_object_name = "declaraciones"

    def get_queryset(self):
        queryset = Declaraciones.objects.filter(
            cat_estatus_id = 4,
            info_personal_fija__usuario=self.request.user
            )
        return queryset

def get_context_InformacionPersonal(folio_declaracion, usuario):
    """
    Function get_context_InformacionPersonal
    ----------
    Se obtiene información de subsecciones

    Parameters
    ----------
    folio_declaracion: str
        Cadena de texto correspondiente al folio de la declaración
    usuario: queryset
        Información del usuario logeado


    Return
    ------
    context: dict

    """
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion), info_personal_fija__usuario=usuario).all()[0]
    except Exception as e:
        folio_declaracion = None

    if folio_declaracion:
        
        info_personal_var = InfoPersonalVar.objects.filter(declaraciones=declaracion).first()
        info_personal_fija = InfoPersonalFija.objects.filter(declaraciones=declaracion).first()
        datos_curriculares = DatosCurriculares.objects.filter(declaraciones=declaracion)
        encargos = Encargos.objects.filter(declaraciones=declaracion).first()
        experiecia_laboral = ExperienciaLaboral.objects.filter(declaraciones=declaracion)
        conyuge_dependientes = ConyugeDependientes.objects.filter(declaraciones=declaracion, es_pareja=True).first()
        otros_dependientes = ConyugeDependientes.objects.filter(declaraciones=declaracion, es_pareja=False)
        seccion_id = Secciones.objects.filter(url='informacion-personal-observaciones').first()
        seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
        if seccion:
            observaciones = seccion.observaciones
        else:
            observaciones = ''
    else:
        info_personal_var = {}
        info_personal_fija = {}
        datos_curriculares = {}
        encargos = {}
        experiecia_laboral = {}
        conyuge_dependientes = {}
        otros_dependientes = {}
        observaciones = {}

    context = {
        'declaracion': declaracion,
        'info_personal_var': info_personal_var,
        'info_personal_fija': info_personal_fija,
        'datos_curriculares': datos_curriculares,
        'encargos': encargos,
        'experiecia_laboral': experiecia_laboral,
        'conyuge_dependientes': conyuge_dependientes,
        'otros_dependientes': otros_dependientes,
        'observaciones': observaciones,
        'folio_declaracion': folio_declaracion,
        'avance':declaracion.avance
    }

    return context


def get_context_Intereses(folio_declaracion, usuario):
    """
    Function get_context_Intereses
    ----------
    Se obtiene información de subsecciones

    Parameters
    ----------
    folio_declaracion: str
        Cadena de texto correspondiente al folio de la declaración
    usuario: queryset
        Información del usuario logeado


    Return
    ------
    context: dict

    """
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
        activas = declaracion.representaciones_set.filter(es_representacion_activa=True).all()
        pasivas = declaracion.representaciones_set.filter(es_representacion_activa=False).all()
        socio = SociosComerciales.objects.filter(declaraciones=declaracion) # old_v = False
        membresia = Membresias.objects.filter(declaraciones=declaracion) # old_v = False
        apoyo = Apoyos.objects.filter(declaraciones=declaracion) # old_v = False
        clientes = ClientesPrincipales.objects.filter(declaraciones=declaracion)
        beneficios = BeneficiosGratuitos.objects.filter(declaraciones=declaracion)

        seccion_id = Secciones.objects.filter(url='intereses-observaciones').first()
        seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
        if seccion:
            observaciones = seccion.observaciones
        else:
            observaciones = ''

    except Exception as e:
        folio_declaracion = ''
        declaracion = {}

    context = {
        'declaracion': declaracion,
        'activas': activas,
        'clientes': clientes,
        'beneficios': beneficios,
        'socios': socio,
        'membresias': membresia,
        'apoyos': apoyo,
        'folio_declaracion': folio_declaracion,
        'avance':declaracion.avance,
        'observaciones': observaciones,
    }

    return context



def get_context_pasivos(folio_declaracion, usuario):
    """
    Function get_context_Intereses
    ----------
    Se obtiene información de subsecciones

    Parameters
    ----------
    folio_declaracion: str
        Cadena de texto correspondiente al folio de la declaración
    usuario: queryset
        Información del usuario logeado


    Return
    ------
    context: dict

    """
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
        seccion_id = Secciones.objects.filter(url='pasivos-observaciones').first()
        seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
        
        if seccion:
            observaciones = seccion.observaciones
        else:
            observaciones = ''

    except Exception as e:
        folio_declaracion = ''
        declaracion = {}

    confirmacion = ConfirmacionForm()

    context = {
        'declaracion': declaracion,
        'folio_declaracion': folio_declaracion,
        'observaciones': observaciones,
        'confirmacion': confirmacion,
        'avance':declaracion.avance
    }

    return context


def get_context_ingresos(folio_declaracion, usuario):
    """
    Function get_context_Intereses
    ----------
    Se obtiene información de subsecciones

    Parameters
    ----------
    folio_declaracion: str
        Cadena de texto correspondiente al folio de la declaración
    usuario: queryset
        Información del usuario logeado


    Return
    ------
    context: dict

    """
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
        IngresosNetos = IngresosDeclaracion.objects.filter(tipo_ingreso=1, declaraciones=declaracion).first()
        IngresosNetos_anterior = IngresosDeclaracion.objects.filter(tipo_ingreso=0, declaraciones=declaracion).first()

    except Exception as e:
        IngresosNetos = {}

    context = {
        'IngresosNetos': IngresosNetos,
        'IngresosNetos_anterior':IngresosNetos_anterior
    }

    return context


def get_inmuebles(dictionary, objs, name, title=''):
    datos = []
    #print(objs)
    obj = objs[0] # aqui te quedaste
    for i_, info in enumerate(obj):
        dictionary.append('<div class="col-12">')
        if i_ is 0: dictionary.append('<h6> '+title+' # '+str(i_+1)+'</h6>')
        dictionary.append(' '+name+ "<br>")
        for i, field in enumerate(model_to_dict(info)): # got only key names
            value = datos[i]
            verbose = obj.model._meta.get_field(field).verbose_name
            if verbose is not 'ID':
                dictionary.append('<dl class="p_opciones col-12"><dt>')
                if value is None: value = ''
                dictionary.append(verbose +'</dt><dd class="text-black_opciones">'+ str(value)+"</dd></dl>")
                #print(dictionary)
        dictionary.append('</div>')
    return dictionary


def get_context_activos(folio_declaracion, usuario):
    """
    Function get_context_activos
    ----------
    Se obtiene información de subsecciones

    Parameters
    ----------
    folio_declaracion: str
        Cadena de texto correspondiente al folio de la declaración
    usuario: queryset
        Información del usuario logeado


    Return
    ------
    context: dict

    """
    context = {}
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion), info_personal_fija__usuario=usuario).all()[0]

        activos_bienes = ActivosBienes.objects.filter(declaraciones=declaracion, cat_activo_bien_id=ActivosBienes.BIENES_INMUEBLES).first() # toma todos los bienes inmuebles de esa declaracion
        Inmueble_declarante = BienesPersonas.objects.filter(activos_bienes=activos_bienes, cat_tipo_participacion_id=BienesPersonas.DECLARANTE)
        Inmuebles_propAnterior = BienesPersonas.objects.filter(activos_bienes=activos_bienes, cat_tipo_participacion_id=BienesPersonas.PROPIETARIO_ANTERIOR)
        

        kwargs = { 'folio_declaracion':folio_declaracion }
        agregar, editar_id, bienes_inmuebles_data, informacion_registrada = ( declaracion_datos(kwargs, BienesInmuebles, declaracion))
        
        inmueble = sorted(chain(informacion_registrada, Inmueble_declarante, Inmuebles_propAnterior), key=lambda instance: instance.created_at)
        vehiculos = MueblesNoRegistrables.objects.filter(declaraciones=declaracion)
        muebles = BienesMuebles.objects.filter(declaraciones=declaracion)
        error = {}
    
    except Exception as e:
        #exc_type, exc_obj, exc_tb = sys.exc_info()
        #fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(e)
        declaracion = {}
        inmueble = {}
        vehiculos = {}
        muebles = {}
        error = e

    context.update({
        'declaracion': declaracion,
        'inmuebles': inmueble,
        'vehiculos': vehiculos,
        'muebles': muebles, 
        'error': error,
    })

    return context

class DescargarReportesView(View):
    template_name = 'sitio/reportes_main.html'

    def get(self, request, *args, **kwargs):
        tipo_declaracion = self.kwargs['tipo']

        if tipo_declaracion:
            resumen = {'activos':[],'baja':[],'iniciales':[],'conclusion':[],'abiertas':[],'cerradas':[],'usuarios_activos_d_inicial':[],'usuarios_activos_d_pendiente':[]}
            usuarios = User.objects.filter(is_superuser=0)
            declaraciones =Declaraciones.objects.extra(
                select={
                    'patrimonial': 'SELECT concat(num,"/",max) FROM declaracion_secciondeclaracion WHERE  declaracion_secciondeclaracion.seccion_id = 1 AND declaracion_secciondeclaracion.declaraciones_id = declaracion_declaraciones.id',
                    'intereses': 'SELECT concat(num,"/",max) FROM declaracion_secciondeclaracion WHERE  declaracion_secciondeclaracion.seccion_id = 17 AND declaracion_secciondeclaracion.declaraciones_id = declaracion_declaraciones.id',
                    'fiscal': 'SELECT concat(num,"/",max) FROM declaracion_secciondeclaracion WHERE  declaracion_secciondeclaracion.seccion_id = 25 AND declaracion_secciondeclaracion.declaraciones_id = declaracion_declaraciones.id',
                },
            )

            for usuario in usuarios:
                if usuario.is_active == True:
                   resumen['activos'].append(usuario)
                   #Separa aquellos usarios que ya tiene una declaración y los que faltan
                   usuario_declaraciones = declaraciones.filter(info_personal_fija__usuario=usuario, cat_estatus=1)
                   if usuario_declaraciones:
                      resumen['usuarios_activos_d_inicial'].append(usuario)
                   else:
                      resumen['usuarios_activos_d_pendiente'].append(usuario)
                else:
                    resumen['baja'].append(usuario)

            for declaracion in declaraciones:
                #Separa por tipo de declaración
                if declaracion.cat_tipos_declaracion.pk == 1:
                    resumen['iniciales'].append(declaracion)
                if declaracion.cat_tipos_declaracion.pk == 3:
                    resumen['conclusion'].append(declaracion)

                #Separa por estatus de declaración
                if declaracion.cat_estatus_id == 1:
                    resumen['abiertas'].append(declaracion)
                if declaracion.cat_estatus_id == 4:
                    resumen['cerradas'].append(declaracion)
            
            context = {
                'declaraciones': declaraciones,
                'tipo_declaracion': tipo_declaracion,
                'resumen': resumen
            }
            
            response = HttpResponse(content_type="application/pdf")
            response['Content-Disposition'] = "inline; filename=donation-receipt.pdf"
            html = render_to_string(self.template_name, context)

            font_config = FontConfiguration()
            HTML(string=html,base_url=request.build_absolute_uri()).write_pdf(response, font_config=font_config)
            return response

class DeclaracionesPreviasDescargarView(View):
    template_name = "sitio/descargar.html"
    def get(self, request, *args, **kwargs):
        usuario = request.user

        try:
            folio_declaracion = self.kwargs['folio']
            declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion)).first()
            if 'user' in request:
                usuario = request.user
            else:
                usuario = declaracion.info_personal_fija.usuario.pk
        except Exception as e:
            folio_declaracion = None

        context = {}
        
        context.update(get_context_InformacionPersonal(folio_declaracion, usuario))
        context.update(get_context_Intereses(folio_declaracion, usuario))
        context.update(get_context_ingresos(folio_declaracion, usuario))
        context.update(get_context_activos(folio_declaracion, usuario))
        context.update(get_context_pasivos(folio_declaracion, usuario))

        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion), info_personal_fija__usuario=usuario).all()[0]
        vehiculos = MueblesNoRegistrables.objects.filter(declaraciones=declaracion)
        inversiones = Inversiones.objects.filter(declaraciones=declaracion)
        adeudos = DeudasOtros.objects.filter(declaraciones=declaracion)
        prestamos = PrestamoComodato.objects.filter(declaraciones=declaracion)
        fiscal = DeclaracionFiscal.objects.filter(declaraciones=declaracion)
        context.update({"vehiculos": vehiculos})
        context.update({"inversiones": inversiones})
        context.update({"adeudos": adeudos})
        context.update({"prestamos": prestamos})
        context.update({"fiscal": fiscal})
        
        if declaracion.datos_publicos == False:
           context.update({"campos_privados": campos_configuracion_todos('p')})

        institucion = sitio_personalizacion.objects.all().first()
        context.update({"institucion": institucion})

        response = HttpResponse(content_type="application/pdf")
        response['Content-Disposition'] = "inline; filename=donation-receipt.pdf"
        html = render_to_string(self.template_name, context)

        font_config = FontConfiguration()
        HTML(string=html,base_url=request.build_absolute_uri()).write_pdf(response, font_config=font_config)
        return response


class LoginView(FormView):
    form_class = AuthenticationForm
    template_name = "sitio/login.html"
    success_url =  reverse_lazy("declaracion:perfil")

    def get(self, request, *args, **kwargs):

        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        else:
            context = {'form': LoginForm()}
            try:
                context['personalizacion'] = personalizacion.objects.all()[0]
            except Exception as e:
                pass
            return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        username = request.POST.get('username')

        try:
            user = User.objects.get(username=username)
            if user.password == '':
                url = reverse('password_reset')
                return HttpResponseRedirect(url + "?rfc=%s" % (username))
        except:
            pass

        form = LoginForm(request.POST)
        if form.is_valid():
            login(request,user, backend='django.contrib.auth.backends.ModelBackend')
            return HttpResponseRedirect(self.get_success_url())
        else:
            context = {'form': form}
            try:
                context['personalizacion'] = personalizacion.objects.all()[0]
            except Exception as e:
                pass
            return render(request,self.template_name, context)

        return super(LoginView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user(), backend='django.contrib.auth.backends.ModelBackend')
        return super(LoginView, self).form_valid(form)
        

class LogoutView(RedirectView):
    pattern_name = 'login'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class PasswordResetRFCView(PasswordResetView):
    from_email=settings.EMAIL_SENDER
    form_class = PasswordResetForm
    def get(self,request,*args,**kwargs):
        rfc = request.GET.get('rfc')

        try:
            obj = User.objects.get(username=rfc,password='')
            form = PasswordResetForm(initial={'rfc':obj.username})
            return render(request,self.template_name,{'form':form})
        except Exception as e:

            return render(request, self.template_name, {'form': PasswordResetForm()})

    def form_valid(self, form):
        opts = {
            'use_https': self.request.is_secure(),
            'token_generator': self.token_generator,
            'from_email': self.from_email,
            'email_template_name': self.email_template_name,
            'subject_template_name': self.subject_template_name,
            'request': self.request,
            'html_email_template_name': self.html_email_template_name,
            'extra_email_context': self.extra_email_context,
        }
        if form.save(**opts):
            return HttpResponseRedirect(self.get_success_url())
        else:
            return HttpResponse(content="",status=500)


class CambioPasswordView(View):
    template_name = 'sitio/cambio-password.html'

    @method_decorator(login_required(login_url='/login'))
    def get(self, request, *args, **kwargs):
        form = PasswordChangeForm(request.user)

        return render(request, self.template_name, {
            'form': form
        })

    @method_decorator(login_required(login_url='/login'))
    def post(self, request, *args, **kwargs):
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            if request.user.check_password(form.cleaned_data['new_password2']):
                messages.error(request,"Tu nueva contraseña es idéntica a la actual")
                return render(request, self.template_name, {
                    'form': form
                })

            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, '¡Tu contraseña a sido cambiada!')

            return redirect('declaracion:perfil')

        return render(request, self.template_name, {
                'form': form
        })

def activar(request, uidb64, token):
    """
    Function activar se encarga de activar al usuario en respuesta a la confirmación de su correo
    """
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        messages.success(request, '¡Tu cuenta a sido activada. ¡Cambia tu contraseña!')
        return redirect('cambiar')
    else:
        return HttpResponse('¡El enlace es invalido!')
