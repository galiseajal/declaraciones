from django.contrib import admin
from .models import sitio_personalizacion, declaracion_faqs


class declaracion_faqs_admin(admin.ModelAdmin):
	pass

class sitio_personalizacion_admin(admin.ModelAdmin):
	pass

admin.site.register(declaracion_faqs, declaracion_faqs_admin)
admin.site.register(sitio_personalizacion, sitio_personalizacion_admin)
