from django.db import models
from colorfield.fields import ColorField

class sitio_personalizacion(models.Model):
	"""
	Class modelo sitio_personalizacion integrado para realizar cambios en el diseño del sistema como logo y colores
	"""
	nombre_institucion = models.CharField("Nombre de la institución", max_length=300)
	siglas_sistema = models.CharField("Siglas del sistema", max_length=30)
	bg_color = models.CharField("Color de fondo (default=white)", max_length=50, default="white")
	font_color_primary = models.CharField("Color de texto Primario (default=#061d26)", max_length=50, default="#061d26")
	color_primary = models.CharField("Color Primario (default=#96f164)", max_length=50, default="#96f164")
	color_secondary = models.CharField("Color Secundario (default=#727176)", max_length=50, default="#727176")
	color_success = models.CharField("Color de conseguido (default=##5dffff)", max_length=50, default="##5dffff")
	color_info = models.CharField("Color de información (default=#96f164)", max_length=50, default="#96f164")
	color_warning = models.CharField("Color de advertencia (default=#727176)", max_length=50, default="#727176")
	color_danger = models.CharField("Color de peligro (default=#5d5d5e)", max_length=50, default="#5d5d5e")
	color_light = models.CharField("Color luz (default=#015F78)", max_length=50, default="#015F78")
	color_dark = models.CharField("Color obscuro (default=#4c4e4f)", max_length=50, default="#4c4e4f")


	class Meta:
		verbose_name = 'personalización del sitio'
		verbose_name_plural = 'personalización del sitio'

	def __str__(self):
		return self.nombre_institucion


class declaracion_faqs(models.Model):
	orden = models.IntegerField("Orden (si tiene cero es omitido)", blank=False, default=0)
	pregunta = models.CharField("Pregunta", max_length=3000, default=0)
	respuesta = models.CharField("Respuesta", max_length=3000, default=0)

	class Meta:
		verbose_name = "Preguntas Frecuentes sobre declaraciones"
		verbose_name_plural = "Preguntas Frecuentes sobre declaraciones"
