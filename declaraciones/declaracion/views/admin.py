import uuid

from django.http import JsonResponse
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.core.paginator import Paginator
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.views import View
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.contrib import messages

from declaracion.forms import BusquedaDeclaranteForm, BusquedaDeclaracionForm, BusquedaUsuariosForm,RegistroUsuarioForm,BusquedaGraficasForm
from declaracion.models import (Declaraciones, InfoPersonalVar,
                                InfoPersonalFija, DatosCurriculares, Encargos,
                                ExperienciaLaboral, ConyugeDependientes,
                                Observaciones, SeccionDeclaracion, Secciones, IngresosDeclaracion, 
                                MueblesNoRegistrables, BienesInmuebles, ActivosBienes, BienesPersonas, 
                                BienesMuebles,SociosComerciales,Membresias, Apoyos,ClientesPrincipales,
                                BeneficiosGratuitos, Inversiones, DeudasOtros, PrestamoComodato, Fideicomisos, DeclaracionFiscal,
                                CatTiposDeclaracion)
from declaracion.views import RegistroView
from declaracion.views.confirmacion import (get_context_InformacionPersonal,get_context_Intereses,get_context_pasivos,
                                            get_context_ingresos,get_inmuebles,get_context_activos)
from sitio.util import account_activation_token

from datetime import datetime, date
from rest_framework.views import APIView 
from rest_framework.response import Response

from weasyprint import HTML
from weasyprint.fonts import FontConfiguration
from django.http import HttpResponse
from django.template.loader import render_to_string

class BusquedaDeclarantesFormView(View):
    template_name="declaracion/admin/busqueda-declarantes.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):

        if request.user.is_staff:
            return render(request,self.template_name,{'form':BusquedaDeclaranteForm()})
        else:
            return redirect('login')

    @method_decorator(login_required(login_url='/login'))
    def post(self,request,*args,**kwargs):
        if request.user.is_staff:
            request_post = request.POST
            form = BusquedaDeclaranteForm(request_post)

            if form.is_valid():
                result = InfoPersonalFija.objects.filter(usuario__is_staff=False)
                page = form.cleaned_data.get('page')
                page_size =form.cleaned_data.get('page_size')
                nombre = form.cleaned_data.get('nombre')
                estatus = form.cleaned_data.get('estatus')
              
                if nombre and not nombre=="":
                    result = result.filter( nombres__icontains=nombre)
                apellido1 = form.cleaned_data.get('apellido1')
                if apellido1 and not apellido1=="":
                    result = result.filter( apellido1__icontains=apellido1)
                apellido2 = form.cleaned_data.get('apellido2')
                if apellido2 and not apellido2=="":
                    result = result.filter( apellido2__icontains=apellido2)
                rfc = form.cleaned_data.get('rfc')
                if rfc and not rfc=="":
                    result = result.filter( rfc__icontains=rfc)
                curp = form.cleaned_data.get('curp')
                if curp and not curp=="":
                    result = result.filter(curp__icontains=curp)
                if estatus:
                    result = result.filter(usuario__is_active = estatus)
                fecha_inicio = date(int(request_post.get('fecha_inicio_year')),int(request_post.get('fecha_inicio_month')),int(request_post.get('fecha_inicio_day')))
                fecha_fin = date(int(request_post.get('fecha_fin_year')),int(request_post.get('fecha_fin_month')),int(request_post.get('fecha_fin_day')))
                tipo_fecha = request_post.get('nacimiento')
                if fecha_inicio and fecha_fin:
                    if tipo_fecha == 'nacimiento':
                        result = result.filter(fecha_nacimiento__range=[fecha_inicio,fecha_fin])
                    elif tipo_fecha == 'ingreso':
                        result = result.filter(fecha_inicio__range=[fecha_inicio,fecha_fin])

                if page and page.isdigit():
                    page = int(page)
                else:
                    page=1
                if page_size and page_size.isdigit():
                    page_size = int(page_size)
                else:
                    page_size=10

                paginator = Paginator(result, page_size)
                result = paginator.get_page(page)

            return render(request,self.template_name,{'form':form,'result':result,'paginas': range(1, paginator.num_pages + 1)})
        else:
            return redirect('declaracion:index')


class BusquedaUsuariosFormView(View):
    template_name="declaracion/admin/busqueda-usuarios.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):

        if request.user.is_staff:
            return render(request,self.template_name,{'form':BusquedaUsuariosForm()})
        else:
            return redirect('login')

    @method_decorator(login_required(login_url='/login'))
    def post(self,request,*args,**kwargs):
        if request.user.is_staff:
            form = BusquedaUsuariosForm(request.POST )

            if form.is_valid():
                result = InfoPersonalFija.objects.filter(usuario__is_staff=True)
                page = form.cleaned_data.get('page')
                page_size =form.cleaned_data.get('page_size')
                nombre = form.cleaned_data.get('nombre')
                estatus = form.cleaned_data.get('estatus')

                if nombre and not nombre=="":
                    result = result.filter( nombres__icontains=nombre)
                apellido1 = form.cleaned_data.get('apellido1')
                if apellido1 and not apellido1=="":
                    result = result.filter( apellido1__icontains=apellido1)
                apellido2 = form.cleaned_data.get('apellido2')
                if apellido2 and not apellido2=="":
                    result = result.filter( apellido2__icontains=apellido2)
                if estatus:
                    result = result.filter( usuario__is_active = estatus)
                rfc = form.cleaned_data.get('rfc')
                if rfc and not rfc=="":
                    result = result.filter(rfc__icontains=rfc)

                if page and page.isdigit():
                    page = int(page)
                else:
                    page=1
                if page_size and page_size.isdigit():
                    page_size = int(page_size)
                else:
                    page_size=10

                paginator = Paginator(result, page_size)
                result = paginator.get_page(page)

            return render(request,self.template_name,{'form':form,'result':result,'paginas': range(1, paginator.num_pages + 1)})
        else:
            return redirect('declaracion:index')


class BusquedaDeclaracionesFormView(View):
    template_name="declaracion/admin/busqueda-declaraciones.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):

        if request.user.is_staff:
            return render(request,self.template_name,{'form':BusquedaDeclaracionForm()})
        else:
            return redirect('login')

    @method_decorator(login_required(login_url='/login'))
    def post(self,request,*args,**kwargs):
        if request.user.is_staff:
            request_post = request.POST
            form = BusquedaDeclaracionForm(request_post)

            if form.is_valid():
                try:
                    result = Declaraciones.objects.all()
                    page = form.cleaned_data.get('page')
                    page_size =form.cleaned_data.get('page_size')
                    folio = form.cleaned_data.get('folio')

                    if folio and not folio=="":
                        result = result.filter(folio=uuid.UUID(folio))
                    tipo = form.cleaned_data.get('tipo')
                    if tipo :
                        result = result.filter(cat_tipos_declaracion=tipo)
                    estatus = form.cleaned_data.get('estatus')
                    if estatus:
                        result = result.filter(cat_estatus=estatus)
                    fecha_inicio = date(int(request_post.get('fecha_inicio_year')),int(request_post.get('fecha_inicio_month')),int(request_post.get('fecha_inicio_day')))
                    fecha_fin = date(int(request_post.get('fecha_fin_year')),int(request_post.get('fecha_fin_month')),int(request_post.get('fecha_fin_day')))
                    if fecha_inicio and fecha_fin:
                        result = result.filter(fecha_declaracion__range=[fecha_inicio,fecha_fin])

                    if page and page.isdigit():
                        page = int(page)
                    else:
                        page=1
                    if page_size and page_size.isdigit():
                        page_size = int(page_size)
                    else:
                        page_size=10

                    paginator = Paginator(result, page_size)
                    result = paginator.get_page(page)
                except Exception as e:
                    print (e)
                    messages.warning(request, u"Para buscar por folio este debe ser la cadena completa del mismo. NO SE ENCONTRARON RESULTADOS EN EL PERIODO DE FECHAS DADAS")
                    return redirect('declaracion:busqueda-declaraciones')

            return render(request,self.template_name,{'form':form,'result':result,'paginas': range(1, paginator.num_pages + 1)})
        else:
            return redirect('declaracion:index')


class InfoDeclarantesFormView(View):
    template_name="declaracion/admin/info-declarante.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):
        usuario = self.kwargs['pk']
        context = {}

        if request.user.is_staff:
            result = InfoPersonalFija.objects.get(pk=usuario)
            context.update({"result": result})

            declaraciones = Declaraciones.objects.filter(info_personal_fija=result)

            if declaraciones:
                context.update({"declaraciones": declaraciones})
                
                cargo = Encargos.objects.filter(declaraciones=declaraciones[0].pk).first()
                if cargo:
                    context.update({"cargo": cargo})

            return render(request,self.template_name,context)
        else:
            return redirect('login')


class InfoUsuarioFormView(View):
    template_name="declaracion/admin/info-usuario.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):

        if request.user.is_superuser:
            result = InfoPersonalFija.objects.get(usuario__pk=self.kwargs['pk'])
            return render(request,self.template_name,{'info':result})
        else:
            return redirect('login')


class InfoDeclaracionFormView(View):
    template_name="declaracion/admin/info-declaracion.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):
        context = {}

        if request.user.is_staff:
            declaracion = Declaraciones.objects.get(pk=self.kwargs['pk'])
            folio_declaracion = str(declaracion.folio)
            context.update(get_context_InformacionPersonal(folio_declaracion, declaracion.info_personal_fija.usuario.id))
            context.update(get_context_Intereses(folio_declaracion, declaracion.info_personal_fija.usuario.id))
            context.update(get_context_ingresos(folio_declaracion, declaracion.info_personal_fija.usuario.id))
            context.update(get_context_activos(folio_declaracion, declaracion.info_personal_fija.usuario.id))
            context.update(get_context_pasivos(folio_declaracion, declaracion.info_personal_fija.usuario.id))

            vehiculos = MueblesNoRegistrables.objects.filter(declaraciones=declaracion)
            inversiones = Inversiones.objects.filter(declaraciones=declaracion)
            adeudos = DeudasOtros.objects.filter(declaraciones=declaracion)
            prestamos = PrestamoComodato.objects.filter(declaraciones=declaracion)
            fideicomisos = Fideicomisos.objects.filter(declaraciones=declaracion)
            context.update({"vehiculos": vehiculos})
            context.update({"inversiones": inversiones})
            context.update({"adeudos": adeudos})
            context.update({"prestamos": prestamos})
            context.update({"fideicomisos": fideicomisos})
            context.update({"tipo": self.kwargs['tipo']})

            try:
                fiscal = DeclaracionFiscal.objects.filter(declaraciones=declaracion).first()
                context.update({"fiscal": fiscal})
            except Exception as e:
                return u""

            return render(request,self.template_name,context)
        else:
            return redirect('login')


class EliminarUsuarioFormView(View):
    @method_decorator(login_required(login_url='/login'))
    def post(self,request,*args,**kwargs):

        if request.user.is_superuser:
            user = User.objects.get(pk=self.kwargs['pk'])
            user.is_active=False
            user.save()
            return HttpResponse("",status=200)
        else:
            return HttpResponse("", status=500)


class NuevoUsuariosFormView(View):
    template_name = 'declaracion/admin/registro.html'
    template_redirect='declaracion/admin/busqueda-usuarios.html'
    form_redirect = BusquedaUsuariosForm
    is_staff = True
    def get(self, request, *args, **kwargs):

        if not request.user.is_authenticated or not request.user.is_superuser :
            raise Http404()

        return render(request, self.template_name, {
            'form': RegistroUsuarioForm(),
            'is_staff': self.is_staff
        })


    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated or not request.user.is_superuser :
            raise Http404()

        registro = RegistroUsuarioForm(request.POST)

        if registro.is_valid():
            email = registro.cleaned_data.get('email')
            rfc = registro.cleaned_data.get('rfc')
            rfc = rfc.upper()


            password = User.objects.make_random_password()

            nombre = registro.cleaned_data.get("nombres")
            rol = registro.cleaned_data.get("rol")
            apellidos = registro.cleaned_data.get("apellido1")+" "+registro.cleaned_data.get("apellido2")

            user = User.objects.create_user(username=rfc,
                                            email=email,
                                            password=password,
                                            first_name=nombre,
                                            last_name=apellidos

                                            )
            user.is_superuser = registro.cleaned_data.get("rol")
            user.is_staff = True
            user.is_superuser=rol

            user.is_active=False

            user.save()

            datos = InfoPersonalFija(
                nombres=nombre,
                apellido1=registro.cleaned_data.get("apellido1"),
                apellido2=registro.cleaned_data.get("apellido2"),
                rfc=rfc,
                curp=registro.cleaned_data.get("rfc"),
                usuario=user,
                cat_ente_publico=registro.cleaned_data.get("nombre_ente_publico"),
                telefono=registro.cleaned_data.get('telefono'),
                puesto=registro.cleaned_data.get('puesto')
            )
            datos.save()

            current_site = get_current_site(request)
            mail_subject = 'Activación de cuenta'
            message = render_to_string('declaracion/admin/acc_active_email_admin.html', {
                'user': user,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)),
                'token':account_activation_token.make_token(user),
                'protocol': request.scheme,
                'password': password
            })
            to_email = email
            email = EmailMultiAlternatives(
                        mail_subject, message, to=[to_email],from_email=settings.EMAIL_SENDER
            )
            email.attach_alternative(message, "text/html")
            email.send()
            if self.form_redirect:
                return render(request,self.template_redirect,{'form':self.form_redirect(),'msg':True,'infopersonalfija':datos,'is_staff':self.is_staff})
            else:
                return render(request, self.template_redirect,
                              {'form': None, 'msg': True, 'infopersonalfija': datos,
                               'is_staff': self.is_staff})


        return render(request, self.template_name, {
            'form': registro,
            'is_staff':self.is_staff
        })


class EditarUsuarioFormView(View):
    template_name = 'declaracion/admin/registro.html'
    template_redirect='declaracion/admin/busqueda-usuarios.html'
    form_redirect = BusquedaUsuariosForm
    is_staff = True
    def get(self, request, *args, **kwargs):

        if request.user.is_authenticated and not self.is_staff:

            return redirect('logout')
        info = InfoPersonalFija.objects.get(usuario__pk=self.kwargs['pk'])
        initial = {
            'nombres':info.nombres,
            'apellido1':info.apellido1,
            'apellido2':info.apellido2,
            'nombre_ente_publico':info.nombre_ente_publico,
            'telefono':info.telefono,
            'rfc':info.rfc,
            'puesto':info.puesto,
            'email':info.usuario.email,
            'rol':info.usuario.is_superuser,
            'estatus':info.usuario.is_active,
            'id':info.usuario_id,
        }


        return render(request, self.template_name, {
            'form': RegistroUsuarioForm(initial=initial),
            'is_staff': self.is_staff,
            'editar':True,
            'info':info
        })


    def post(self, request, *args, **kwargs):
        registro = RegistroUsuarioForm(request.POST)

        if registro.is_valid():

            id = registro.cleaned_data.get('id')
            user = User.objects.get(pk=id)

            email = registro.cleaned_data.get('email')
            rfc = registro.cleaned_data.get('rfc')
            rfc = rfc.upper()
            nombre = registro.cleaned_data.get("nombres")
            apellidos = registro.cleaned_data.get("apellido1")+" "+registro.cleaned_data.get("apellido2")

            user.username=rfc
            user.email=email
            user.first_name=nombre
            user.last_name=apellidos

            user.is_superuser = registro.cleaned_data.get("rol")
            user.is_staff = True

            user.is_active=registro.cleaned_data.get("estatus")

            user.save()

            datos = InfoPersonalFija.objects.get(usuario__pk=id)

            datos.nombres=registro.cleaned_data.get("nombres")
            datos.apellido1=registro.cleaned_data.get("apellido1")
            datos.apellido2=registro.cleaned_data.get("apellido2")
            datos.rfc=rfc
            datos.curp=registro.cleaned_data.get("rfc")
            datos.nombre_ente_publico=registro.cleaned_data.get("nombre_ente_publico")
            datos.telefono=registro.cleaned_data.get('telefono')
            datos.puesto=registro.cleaned_data.get('puesto')
            datos.save()

            if self.form_redirect:
                return render(request,self.template_redirect,{'form':self.form_redirect(),'msg':False,'infopersonalfija':datos,'is_staff':self.is_staff,'editar':True})
            else:
                return render(request, self.template_redirect,
                              {'form': None, 'msg': False, 'infopersonalfija': datos,
                               'is_staff': self.is_staff,'editar':True})


        return render(request, self.template_name, {
            'form': registro,
            'is_staff':self.is_staff,
            'editar':True,

        })


class DescargarReportesView(View):
    template_name="declaracion/admin/reportes_main.html"

    def get(self, request, *args, **kwargs):
        tipo_declaracion = self.kwargs['tipo']
        request_get = request.GET
        resumen = {'activos':[],'baja':[],'iniciales':[],'conclusion':[],'abiertas':[],'cerradas':[],'usuarios_activos_d_inicial':[],'usuarios_activos_d_pendiente':[]}
        form = BusquedaDeclaracionForm(request_get)

        if tipo_declaracion:
            usuarios = User.objects.filter(is_superuser=0)
            declaraciones =Declaraciones.objects.extra(
                select={
                    'patrimonial': 'SELECT concat(num,"/",max) FROM declaracion_secciondeclaracion WHERE  declaracion_secciondeclaracion.seccion_id = 1 AND declaracion_secciondeclaracion.declaraciones_id = declaracion_declaraciones.id',
                    'intereses': 'SELECT concat(num,"/",max) FROM declaracion_secciondeclaracion WHERE  declaracion_secciondeclaracion.seccion_id = 17 AND declaracion_secciondeclaracion.declaraciones_id = declaracion_declaraciones.id',
                    'fiscal': 'SELECT concat(num,"/",max) FROM declaracion_secciondeclaracion WHERE  declaracion_secciondeclaracion.seccion_id = 25 AND declaracion_secciondeclaracion.declaraciones_id = declaracion_declaraciones.id',
                }
            )

            #Se realiza el filtro de acuerdo a los parametros recibidos
            if form.is_valid():
                folio = form.cleaned_data.get('folio')
                if folio and not folio=="":
                    declaraciones = declaraciones.filter(folio=uuid.UUID(folio))
                tipo = form.cleaned_data.get('tipo')
                if tipo :
                    declaraciones = declaraciones.filter(cat_tipos_declaracion=tipo)
                estatus = form.cleaned_data.get('estatus')
                if estatus:
                    declaraciones = declaraciones.filter(cat_estatus=estatus)
                fecha_inicio = date(int(request_get.get('fecha_inicio_year')),int(request_get.get('fecha_inicio_month')),int(request_get.get('fecha_inicio_day')))
                fecha_fin = date(int(request_get.get('fecha_fin_year')),int(request_get.get('fecha_fin_month')),int(request_get.get('fecha_fin_day')))
                declaraciones = declaraciones.filter(fecha_declaracion__range=[fecha_inicio,fecha_fin])

            for usuario in usuarios:
                if usuario.is_active == True:
                   resumen['activos'].append(usuario)

                   #Separa aquellos usuarios que ya tiene una declaración y los que faltan
                   usuario_declaraciones = declaraciones.filter(info_personal_fija__usuario=usuario, cat_estatus=1)
                   if usuario_declaraciones:
                      resumen['usuarios_activos_d_inicial'].append(usuario)
                   else:
                      resumen['usuarios_activos_d_pendiente'].append(usuario)
                else:
                    resumen['baja'].append(usuario)

            for declaracion in declaraciones:
                #Separa por tipo de declaración
                if declaracion.cat_tipos_declaracion.pk == 1:
                    resumen['iniciales'].append(declaracion)
                if declaracion.cat_tipos_declaracion.pk == 3:
                    resumen['conclusion'].append(declaracion)

                #Separa por estatus de declaración
                if declaracion.cat_estatus_id == 1:
                    resumen['abiertas'].append(declaracion)
                if declaracion.cat_estatus_id == 4:
                    resumen['cerradas'].append(declaracion)
            
            context = {
                'declaraciones': declaraciones,
                'tipo_declaracion': tipo_declaracion,
                'resumen': resumen
            }
            
            response = HttpResponse(content_type="application/pdf")
            response['Content-Disposition'] = "inline; filename=donation-receipt.pdf"
            html = render_to_string(self.template_name, context)

            font_config = FontConfiguration()
            HTML(string=html,base_url=request.build_absolute_uri()).write_pdf(response, font_config=font_config)
            return response

class DeclaracionesGraficas(View):
    template_name="declaracion/admin/reportes_graficas.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):
        usuarios = User.objects.all()
        usuarios_activos = usuarios.filter(is_active=1)
        usuarios_baja = usuarios.filter(is_active=0)

        tipos_declaracion = ['Sin declaración']
        for tipo in CatTiposDeclaracion.objects.all():
            tipos_declaracion.append(tipo.codigo)

        context = {
           "total_usuario_activos":len(usuarios_activos),
           "total_usuario_baja":len(usuarios_baja),
           "tipos_declaracion":tipos_declaracion,
           "form":BusquedaGraficasForm(),
           "extra_params": date.today().year
        }

        return render(request,self.template_name,context)

    @method_decorator(login_required(login_url='/login'))
    def post(self,request,*args,**kwargs):
        return render(request,self.template_name)


class DeclaracionesGraficasData(APIView): 
    authentication_classes = [] 
    permission_classes = [] 
   
    def get(self, request, format = None):
        declaraciones_por_usuario = {'sin_declaracion':0}
        tipos_declaracion = ['Sin declaración']
        request_get = request.GET
        fecha_inicio = date(date.today().year,1,1)
        fecha_fin = date(date.today().year,12,31)
        
        if request_get.get('anio') is not None:
            fecha_inicio = date(int(request_get.get('anio')),1,1)
            fecha_fin = date(int(request_get.get('anio')),12,31)
        
        #Información que mostrará el promedio de las declaraciones creadas por la cantidad de usuario existentes
        for tipo in CatTiposDeclaracion.objects.all():
            tipos_declaracion.append(tipo.codigo)
            declaraciones_por_usuario.update({tipo.codigo: 0})

        usuarios = User.objects.all()
        usuarios_activos = usuarios.filter(is_active=1,date_joined__range=[fecha_inicio,fecha_fin])
        usuarios_baja = usuarios.filter(is_active=0,date_joined__range=[fecha_inicio,fecha_fin])

        declaraciones = Declaraciones.objects.all()
        declaraciones_por_anio = declaraciones.filter(fecha_declaracion__range=[fecha_inicio,fecha_fin])

        for usuario in usuarios_activos:
            usuario_declaraciones = declaraciones_por_anio.filter(info_personal_fija__usuario=usuario)
            if usuario_declaraciones.exists():
               for usu_dec in usuario_declaraciones:
                  declaraciones_por_usuario[usu_dec.cat_tipos_declaracion.codigo]+=1;
            else:
                declaraciones_por_usuario['sin_declaracion']+=1;

        chartdata = declaraciones_por_usuario.values()

        #Información que mostrara la cantidad de usuarios y declaraciónes creados por año
        meses = ['Enero', 'Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
        if len(usuarios_activos) > len(declaraciones):
            total_usuarios_declaraciones = len(usuarios_activos)
        else:
            total_usuarios_declaraciones = len(declaraciones)

        usuarios_por_anio = usuarios.filter(date_joined__range=[fecha_inicio,fecha_fin])
        declaraciones_por_mes={}
        usuarios_por_mes={}

        for mes in meses:
            mes_index = meses.index(mes)+1
            declaraciones_mes = declaraciones_por_anio.filter(fecha_declaracion__month=mes_index)
            if declaraciones_mes:
                declaraciones_por_mes.update({mes: len(declaraciones_mes)})
            else:
                declaraciones_por_mes.update({mes:0})

            usuarios_mes = usuarios_por_anio.filter(date_joined__month=mes_index)
            if usuarios_mes:
               usuarios_por_mes.update({mes:len(usuarios_mes)})
            else:
               usuarios_por_mes.update({mes:0})

        data = {
            "labels":tipos_declaracion,
            "lables_meses": meses,
            "chartLabel":"Declaraciones",
            "chartLabel_usuario":"Usuarios",
            "chartdata":chartdata,
            "total_usuario_activos": len(usuarios_activos),
            "total_usuario_baja": len(usuarios_baja),
            "total_usuarios_declaraciones": total_usuarios_declaraciones,
            "chartdata_datos_anuales_declaraciones": declaraciones_por_mes.values(),
            "chartdata_datos_anuales_usuarios": usuarios_por_mes.values(),
            "extra_params": [request_get.get('anio')]
        }
        return Response(data)