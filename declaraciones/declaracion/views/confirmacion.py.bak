import sys, os
import uuid
import base64
import requests
from datetime import datetime
from django.views import View
from django.shortcuts import render, redirect
from django.http import Http404
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from declaracion.models import (Declaraciones, InfoPersonalVar,
                                InfoPersonalFija, DatosCurriculares, Encargos,
                                ExperienciaLaboral, ConyugeDependientes,
                                Observaciones, SeccionDeclaracion, Secciones, IngresosDeclaracion, 
                                MueblesNoRegistrables, BienesInmuebles, ActivosBienes, BienesPersonas, 
                                BienesMuebles,SociosComerciales,Membresias, Apoyos)
from declaracion.forms import ConfirmacionForm
from .utils import (validar_declaracion, declaracion_datos)
from django.conf import settings
from django.forms.models import model_to_dict
from itertools import chain


def get_context_InformacionPersonal(folio_declaracion, usuario):
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion), info_personal_fija__usuario=usuario).all()[0]
    except Exception as e:
        folio_declaracion = None

    if folio_declaracion:
        
        info_personal_var = InfoPersonalVar.objects.filter(declaraciones=declaracion).first()
        info_personal_fija = InfoPersonalFija.objects.filter(declaraciones=declaracion).first()
        datos_curriculares = DatosCurriculares.objects.filter(declaraciones=declaracion)
        encargos = Encargos.objects.filter(declaraciones=declaracion).first()
        experiecia_laboral = ExperienciaLaboral.objects.filter(declaraciones=declaracion)
        conyuge_dependientes = ConyugeDependientes.objects.filter(declaraciones=declaracion, es_pareja=True).first()
        otros_dependientes = ConyugeDependientes.objects.filter(declaraciones=declaracion, es_pareja=False)
        seccion_id = Secciones.objects.filter(url='informacion-personal-observaciones').first()
        seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
        if seccion:
            observaciones = seccion.observaciones
        else:
            observaciones = ''
    else:
        declaracion = {}
        info_personal_var = {}
        info_personal_fija = {}
        datos_curriculares = {}
        encargos = {}
        experiecia_laboral = {}
        conyuge_dependientes = {}

    context = {
        'declaracion': declaracion,
        'info_personal_var': info_personal_var,
        'info_personal_fija': info_personal_fija,
        'datos_curriculares': datos_curriculares,
        'encargos': encargos,
        'experiecia_laboral': experiecia_laboral,
        'conyuge_dependientes': conyuge_dependientes,
        'otros_dependientes': otros_dependientes,
        'observaciones': observaciones,
        'folio_declaracion': folio_declaracion,
        'avance':declaracion.avance
    }

    return context


def get_context_Intereses(folio_declaracion, usuario):
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
        activas = declaracion.representaciones_set.filter(es_representacion_activa=True).all()
        pasivas = declaracion.representaciones_set.filter(es_representacion_activa=False).all()
        socio = SociosComerciales.objects.filter(declaraciones=declaracion) # old_v = False
        membresia = Membresias.objects.filter(declaraciones=declaracion) # old_v = False
        apoyo = Apoyos.objects.filter(declaraciones=declaracion) # old_v = False



        seccion_id = Secciones.objects.filter(url='intereses-observaciones').first()
        seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
        if seccion:
            observaciones = seccion.observaciones
        else:
            observaciones = ''

    except Exception as e:
        folio_declaracion = ''
        declaracion = {}

    context = {
        'declaracion': declaracion,
        'activas': activas,
        'socios': socio,
        'membresias': membresia,
        'apoyos': apoyo,
        'pasivas': pasivas,
        'folio_declaracion': folio_declaracion,
        'avance':declaracion.avance,
        'observaciones': observaciones,
    }

    return context



def get_context_pasivos(folio_declaracion, usuario):
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
        seccion_id = Secciones.objects.filter(url='pasivos-observaciones').first()
        seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
        
        if seccion:
            observaciones = seccion.observaciones
        else:
            observaciones = ''

    except Exception as e:
        folio_declaracion = ''
        declaracion = {}

    confirmacion = ConfirmacionForm()

    context = {
        'declaracion': declaracion,
        'folio_declaracion': folio_declaracion,
        'observaciones': observaciones,
        'confirmacion': confirmacion,
        'avance':declaracion.avance
    }

    return context


def get_context_ingresos(folio_declaracion, usuario, old_v=True):
    if old_v:
        try:
            declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
            sueldos = declaracion.ingresosvarios_set.filter(cat_tipos_ingresos_varios_id=1).all()
            profesional = declaracion.ingresosvarios_set.filter(cat_tipos_ingresos_varios_id=2).all()
            empresarial = declaracion.ingresosvarios_set.filter(cat_tipos_ingresos_varios_id=3).all()
            menor = declaracion.ingresosvarios_set.filter(cat_tipos_ingresos_varios_id=4).all()
            arrendamiento = declaracion.ingresosvarios_set.filter(cat_tipos_ingresos_varios_id=5).all()
            intereses = declaracion.ingresosvarios_set.filter(cat_tipos_ingresos_varios_id=6).all()
            premios = declaracion.ingresosvarios_set.filter(cat_tipos_ingresos_varios_id=7).all()
            bienes = declaracion.ingresosvarios_set.filter(cat_tipos_ingresos_varios_id=8).all()
            otros = declaracion.ingresosvarios_set.filter(cat_tipos_ingresos_varios_id=9).all()

            seccion_id = Secciones.objects.filter(url='ingresos-observaciones').first()
            seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
            if seccion:
                observaciones = seccion.observaciones
            else:
                observaciones = ''
            avance = declaracion.avance

        except Exception as e:
            folio_declaracion = ''
            declaracion = {}
            sueldos = {}
            profesional = {}
            empresarial = {}
            menor = {}
            arrendamiento = {}
            intereses = {}
            premios = {}
            bienes = {}
            otros = {}
            observaciones = ''
            avance = {}

        context = {
            'declaracion': declaracion,
            'folio_declaracion': folio_declaracion,
            'sueldos': sueldos,
            'profesional': profesional,
            'empresarial': empresarial,
            'menor': menor,
            'arrendamiento': arrendamiento,
            'intereses': intereses,
            'premios': premios,
            'bienes': bienes,
            'otros': otros,
            'observaciones': observaciones,
            'avance':avance
        }
    else:
        try:
            declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
            IngresosNetos = IngresosDeclaracion.objects.filter(tipo_ingreso=1, declaraciones=declaracion).first()
            IngresosNetos_anterior = IngresosDeclaracion.objects.filter(tipo_ingreso=0, declaraciones=declaracion).first()

        except Exception as e:
            IngresosNetos = {}

        context = {
            'IngresosNetos': IngresosNetos,
            'IngresosNetos_anterior':IngresosNetos_anterior
        }
    return context


def get_inmuebles(dictionary, objs, name, title=''):
    datos = []
    #print(objs)
    obj = objs[0] # aqui te quedaste
    for i_, info in enumerate(obj):
        dictionary.append('<div class="col-12">')
        if i_ is 0: dictionary.append('<h6> '+title+' # '+str(i_+1)+'</h6>')
        dictionary.append(' '+name+ "<br>")
        for i, field in enumerate(model_to_dict(info)): # got only key names
            value = datos[i]
            verbose = obj.model._meta.get_field(field).verbose_name
            if verbose is not 'ID':
                dictionary.append('<dl class="p_opciones col-12"><dt>')
                if value is None: value = ''
                dictionary.append(verbose +'</dt><dd class="text-black_opciones">'+ str(value)+"</dd></dl>")
                #print(dictionary)
        dictionary.append('</div>')
    return dictionary


def get_context_activos(folio_declaracion, usuario, old_v=True):
    if old_v:
        try:
            declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
            seccion_id = Secciones.objects.filter(url='activos-observaciones').first()
            seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
            if seccion:
                observaciones = seccion.observaciones
            else:
                observaciones = ''
            avance = declaracion.avance

        except Exception as e:
            folio_declaracion = ''
            declaracion = {}
            observaciones = ''
            avance = ''

        context = {
            'declaracion': declaracion,
            'observaciones': observaciones,
            'folio_declaracion': folio_declaracion,
            'avance':avance
        }
    else:
        context = {}
        try:
            declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion), info_personal_fija__usuario=usuario).all()[0]

            activos_bienes = ActivosBienes.objects.filter(declaraciones=declaracion, cat_activo_bien_id=ActivosBienes.BIENES_INMUEBLES).first() # toma todos los bienes inmuebles de esa declaracion
            Inmueble_declarante = BienesPersonas.objects.filter(activos_bienes=activos_bienes, cat_tipo_participacion_id=BienesPersonas.DECLARANTE)
            # Inmueble_copropietario = BienesPersonas.objects.filter(activos_bienes=activos_bienes, cat_tipo_participacion_id=BienesPersonas.COPROPIETARIO)
            Inmuebles_propAnterior = BienesPersonas.objects.filter(activos_bienes=activos_bienes, cat_tipo_participacion_id=BienesPersonas.PROPIETARIO_ANTERIOR)
            

            kwargs = { 'folio_declaracion':folio_declaracion }
            agregar, editar_id, bienes_inmuebles_data, informacion_registrada = ( declaracion_datos(kwargs, BienesInmuebles, declaracion))
            
            #inmueble = sorted(chain(informacion_registrada, Inmueble_declarante, Inmuebles_propAnterior), key=lambda instance: instance.date_created)
            
            '''
            Inmuebles = {}
            temp = {}
            for idx, item in enumerate(informacion_registrada):
                Inmuebles.update({(str(idx)):{
                    (str(idx)+'_inmueble_informacion_registrada') : informacion_registrada.values()[idx],
                    (str(idx)+'_inmueble_declarante') : Inmueble_declarante.values()[idx],
                    (str(idx)+'_inmueble_copropietario') : Inmueble_copropietario.values()[idx],
                    (str(idx)+'_inmueble_propAnterior') : Inmuebles_propAnterior.values()[idx]
                }})
            '''
                #context.update({
                #    (str(idx)+'_inmueble_informacion_registrada'): informacion_registrada.values()[idx],
                #    (str(idx)+'_inmueble_declarante'): Inmueble_declarante.values()[idx],
                #    (str(idx)+'_inmueble_copropietario'): Inmueble_copropietario.values()[idx],
                #    (str(idx)+'_inmueble_propAnterior'): Inmuebles_propAnterior.values()[idx]
                #})     
            

            #inmueble = {}
            #obj = Inmueble_declarante
            #inmueble.update(get_inmuebles(inmueble, obj, "Declarante"))
            #obj = Inmueble_copropietario
            #get_inmuebles(inmueble, obj, "Copropietario")
            #obj = Inmuebles_propAnterior
            #inmueble.update(get_inmuebles(inmueble, obj, "Propietario Anterior"))

            #context.update({"inmueble": inmueble})
            #dato = Inmueble_copropietario.model._meta.get_field('info_personal_var')
            #context.update({"verbose": dato.verbose_name })
            #context.update({"attributename": dato.attname })
            #context.update({"value": Inmueble_copropietario.values("info_personal_var")[0] })

            
            #Inmuebles = {
            #    'informacion_registrada': informacion_registrada,
            #    'inmueble_declarante': Inmueble_declarante,
            #    'inmueble_copropietario': Inmueble_copropietario,
            #    'nmuebles_propAnterior': Inmuebles_propAnterior
            #}
            
            #Inmuebles = BienesInmuebles.objects.filter(declaraciones=declaracion)
            # Inmuebles = InfoPersonalVar.objects.filter(declaraciones=declaracion, activos_bienes__cat_activo_bien__pk=1).first().activos_bienes.all()
            
            inmueble = sorted(chain(informacion_registrada, Inmueble_declarante, Inmuebles_propAnterior), key=lambda instance: instance.created_at)
            vehiculos = MueblesNoRegistrables.objects.filter(declaraciones=declaracion)
            muebles = BienesMuebles.objects.filter(declaraciones=declaracion)
            error = {}
        
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(e, exc_type, fname, exc_tb.tb_lineno)
            declaracion = {}
            inmueble = {}
            vehiculos = {}
            muebles = {}
            error = e

        #inmueble = []
        #objs = []
        #objs.append(informacion_registrada)
        #objs.append(Inmueble_declarante)
        #objs.append(Inmuebles_propAnterior)

        #get_inmuebles(inmueble, objs, "Información ", "INMUEBLE")
        #for idx, info in enumerate(informacion_registrada):
        #    inmueble_ = sorted(chain(informacion_registrada, Inmueble_declarante, Inmuebles_propAnterior), key=lambda instance: instance.created_at)
        #    inmueble = chain(inmueble, inmueble_)

        context.update({
            'declaracion': declaracion,
            'inmuebles': inmueble,
            'vehiculos': vehiculos,
            'muebles': muebles, 
            'error': error,
        })

    return context


class ConfirmacionAllinOne(View):
    template_name = "declaracion/confirmacion/all.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self, request, *args, **kwargs):

        context = {}
        folio_declaracion = self.kwargs['folio']
        usuario = request.user

        context.update(get_context_InformacionPersonal(folio_declaracion, usuario))
        context.update(get_context_Intereses(folio_declaracion, usuario))
        context.update(get_context_ingresos(folio_declaracion, usuario, False))
        context.update(get_context_activos(folio_declaracion, usuario))
        context.update(get_context_pasivos(folio_declaracion, usuario))

        return render(request, self.template_name, context)


class ConfimacionInformacionPersonalView(View):
    template_name = "declaracion/confirmacion/informacion_personal.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):

        context = get_context_InformacionPersonal(self.kwargs['folio'], request.user)
        
        return render(request ,self.template_name, context)


class ConfimacionInteresesView(View):
    template_name = "declaracion/confirmacion/intereses.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):
        usuario = request.user

        context = get_context_Intereses(self.kwargs['folio'], usuario)

        return render(request,self.template_name, context)


class ConfimacionPasivosView(View):
    template_name = "declaracion/confirmacion/pasivos.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):
        usuario = request.user

        context = get_context_pasivos(self.kwargs['folio'], usuario)

        return render(request, self.template_name, context)


class ConfimacionIngresosView(View):
    template_name = "declaracion/confirmacion/ingresos.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):
        usuario = request.user

        context = get_context_ingresos(self.kwargs['folio'], usuario)

        return render(request,self.template_name, context)


class ConfimacionActivosView(View):
    template_name = "declaracion/confirmacion/activos.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self,request,*args,**kwargs):
        usuario = request.user

        context = get_context_activos(self.kwargs['folio'], usuario)

        return render(request,self.template_name, context)


class ConfirmarDeclaracionView(View):
    def get(self, request, *args, **kwargs):
        raise Http404()

    @method_decorator(login_required(login_url='/login'))
    def post(self, request, *args, **kwargs):
        
        try:
            folio_declaracion = self.kwargs['folio']
            declaracion = validar_declaracion(request, folio_declaracion)
        except Exception as e:
            raise Http404()

        try:
            confirmacion = ConfirmacionForm(request.POST, request.FILES)
            if confirmacion.is_valid():
                byteKey = base64.b64encode(
                    request.FILES['key'].read()).decode('utf-8')
                bytecer = base64.b64encode(
                    request.FILES['cer'].read()).decode('utf-8')
                password = base64.b64encode(
                    request.POST['password'].encode('utf-8')).decode('utf-8')

                firma = {
                  "security": {
                    "tokenId": settings.TOKEN_ID
                  },
                  "data": {
                    "password": password,
                    "cadena": str(declaracion.folio),
                    "byteKey": byteKey,
                    "bytecer": bytecer
                  }
                }
                url = settings.FIRMA_URL

                r = requests.post(url, json=firma)
                response = r.json()
                if response['error']['code'] == 0:
                    declaracion.cat_estatus_id = 4
                    declaracion.sello = response['data']['sello']
                    declaracion.fecha_recepcion = response['data']['fechaFirma']
                    declaracion.save()
                    return redirect('declaraciones-previas')
                else:
                    messages.warning(request, u"Error de autenticación")
                    return redirect('declaracion:confirmacion-pasivos', folio=declaracion.folio)
            else:
                for key, error in confirmacion.errors.items():
                    messages.warning(request, error)
            return redirect('declaracion:confirmacion-pasivos', folio=declaracion.folio)
        except Exception as e:
            print (e)
            return redirect('declaracion:confirmacion-pasivos', folio=declaracion.folio)
