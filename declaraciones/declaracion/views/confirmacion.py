import sys, os
import uuid
import base64
import requests
from datetime import datetime
from django.views import View
from django.shortcuts import render, redirect
from django.http import Http404
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from declaracion.models import (Declaraciones, InfoPersonalVar,
                                InfoPersonalFija, DatosCurriculares, Encargos,
                                ExperienciaLaboral, ConyugeDependientes,
                                Observaciones, SeccionDeclaracion, Secciones, IngresosDeclaracion, 
                                MueblesNoRegistrables, BienesInmuebles, ActivosBienes, BienesPersonas, 
                                BienesMuebles,SociosComerciales,Membresias, Apoyos,ClientesPrincipales,
                                BeneficiosGratuitos, Inversiones, DeudasOtros, PrestamoComodato, Fideicomisos)
from declaracion.forms import ConfirmacionForm
from .utils import (validar_declaracion, declaracion_datos)
from django.conf import settings
from django.forms.models import model_to_dict
from itertools import chain
from django.conf import settings	
from django.views.decorators.cache import cache_page

CACHE_TTL = getattr(settings, 'CACHE_TTL', 60*1)


def get_context_InformacionPersonal(folio_declaracion, usuario):
    """
    Function get_context_InformacionPersonal
    ----------
    Se obtiene información de subsecciones

    Parameters
    ----------
    folio_declaracion: str
        Cadena de texto correspondiente al folio de la declaración
    usuario: queryset
        Información del usuario logeado


    Return
    ------
    context: dict

    """
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion), info_personal_fija__usuario=usuario).all()[0]
    except Exception as e:
        folio_declaracion = None

    if folio_declaracion:
        
        info_personal_var = InfoPersonalVar.objects.filter(declaraciones=declaracion).first()
        info_personal_fija = InfoPersonalFija.objects.filter(declaraciones=declaracion).first()
        datos_curriculares = DatosCurriculares.objects.filter(declaraciones=declaracion)
        encargos = Encargos.objects.filter(declaraciones=declaracion).first()
        experiecia_laboral = ExperienciaLaboral.objects.filter(declaraciones=declaracion)
        conyuge_dependientes = ConyugeDependientes.objects.filter(declaraciones=declaracion, es_pareja=True).first()
        otros_dependientes = ConyugeDependientes.objects.filter(declaraciones=declaracion, es_pareja=False)
        seccion_id = Secciones.objects.filter(url='informacion-personal-observaciones').first()
        seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
        if seccion:
            observaciones = seccion.observaciones
        else:
            observaciones = ''
    else:
        declaracion = {}
        info_personal_var = {}
        info_personal_fija = {}
        datos_curriculares = {}
        encargos = {}
        experiecia_laboral = {}
        conyuge_dependientes = {}

    context = {
        'declaracion': declaracion,
        'info_personal_var': info_personal_var,
        'info_personal_fija': info_personal_fija,
        'datos_curriculares': datos_curriculares,
        'encargos': encargos,
        'experiecia_laboral': experiecia_laboral,
        'conyuge_dependientes': conyuge_dependientes,
        'otros_dependientes': otros_dependientes,
        'observaciones': observaciones,
        'folio_declaracion': folio_declaracion,
        'avance':declaracion.avance
    }

    return context


def get_context_Intereses(folio_declaracion, usuario):
    """
    Function get_context_Intereses
    ----------
    Se obtiene información de subsecciones

    Parameters
    ----------
    folio_declaracion: str
        Cadena de texto correspondiente al folio de la declaración
    usuario: queryset
        Información del usuario logeado


    Return
    ------
    context: dict

    """
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
        activas = declaracion.representaciones_set.filter(es_representacion_activa=True).all()
        pasivas = declaracion.representaciones_set.filter(es_representacion_activa=False).all()
        socio = SociosComerciales.objects.filter(declaraciones=declaracion) # old_v = False
        membresia = Membresias.objects.filter(declaraciones=declaracion) # old_v = False
        apoyo = Apoyos.objects.filter(declaraciones=declaracion) # old_v = False
        clientes = ClientesPrincipales.objects.filter(declaraciones=declaracion)
        beneficios = BeneficiosGratuitos.objects.filter(declaraciones=declaracion)

        seccion_id = Secciones.objects.filter(url='intereses-observaciones').first()
        seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
        if seccion:
            observaciones = seccion.observaciones
        else:
            observaciones = ''

    except Exception as e:
        folio_declaracion = ''
        declaracion = {}

    context = {
        'declaracion': declaracion,
        'activas': activas,
        'clientes': clientes,
        'beneficios': beneficios,
        'socios': socio,
        'membresias': membresia,
        'apoyos': apoyo,
        'folio_declaracion': folio_declaracion,
        'avance':declaracion.avance,
        'observaciones': observaciones,
    }

    return context



def get_context_pasivos(folio_declaracion, usuario):
    """
    Function get_context_pasivos
    ----------
    Se obtiene información de subsecciones

    Parameters
    ----------
    folio_declaracion: str
        Cadena de texto correspondiente al folio de la declaración
    usuario: queryset
        Información del usuario logeado


    Return
    ------
    context: dict

    """
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
        seccion_id = Secciones.objects.filter(url='pasivos-observaciones').first()
        seccion = SeccionDeclaracion.objects.filter(declaraciones=declaracion, seccion=seccion_id).first()
        
        if seccion:
            observaciones = seccion.observaciones
        else:
            observaciones = ''

    except Exception as e:
        folio_declaracion = ''
        declaracion = {}

    confirmacion = ConfirmacionForm()

    context = {
        'declaracion': declaracion,
        'folio_declaracion': folio_declaracion,
        'observaciones': observaciones,
        'confirmacion': confirmacion,
        'avance':declaracion.avance
    }

    return context


def get_context_ingresos(folio_declaracion, usuario):
    """
    Function get_context_ingresos
    ----------
    Se obtiene información de subsecciones

    Parameters
    ----------
    folio_declaracion: str
        Cadena de texto correspondiente al folio de la declaración
    usuario: queryset
        Información del usuario logeado


    Return
    ------
    context: dict

    """
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion),info_personal_fija__usuario=usuario).all()[0]
        IngresosNetos = IngresosDeclaracion.objects.filter(tipo_ingreso=1, declaraciones=declaracion).first()
        IngresosNetos_anterior = IngresosDeclaracion.objects.filter(tipo_ingreso=0, declaraciones=declaracion).first()

    except Exception as e:
        IngresosNetos = {}

    context = {
        'IngresosNetos': IngresosNetos,
        'IngresosNetos_anterior':IngresosNetos_anterior
    }

    return context


def get_inmuebles(dictionary, objs, name, title=''):
    datos = []
    obj = objs[0]
    for i_, info in enumerate(obj):
        dictionary.append('<div class="col-12">')
        if i_ is 0: dictionary.append('<h6> '+title+' # '+str(i_+1)+'</h6>')
        dictionary.append(' '+name+ "<br>")
        for i, field in enumerate(model_to_dict(info)): # got only key names
            value = datos[i]
            verbose = obj.model._meta.get_field(field).verbose_name
            if verbose is not 'ID':
                dictionary.append('<dl class="p_opciones col-12"><dt>')
                if value is None: value = ''
                dictionary.append(verbose +'</dt><dd class="text-black_opciones">'+ str(value)+"</dd></dl>")
        dictionary.append('</div>')
    return dictionary


def get_context_activos(folio_declaracion, usuario):
    """
    Function get_context_activos
    ----------
    Se obtiene información de subsecciones

    Parameters
    ----------
    folio_declaracion: str
        Cadena de texto correspondiente al folio de la declaración
    usuario: queryset
        Información del usuario logeado


    Return
    ------
    context: dict

    """
    context = {}
    try:
        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion), info_personal_fija__usuario=usuario).all()[0]

        activos_bienes = ActivosBienes.objects.filter(declaraciones=declaracion, cat_activo_bien_id=ActivosBienes.BIENES_INMUEBLES).first() # toma todos los bienes inmuebles de esa declaracion
        Inmueble_declarante = BienesPersonas.objects.filter(activos_bienes=activos_bienes, cat_tipo_participacion_id=BienesPersonas.DECLARANTE)
        Inmuebles_propAnterior = BienesPersonas.objects.filter(activos_bienes=activos_bienes, cat_tipo_participacion_id=BienesPersonas.PROPIETARIO_ANTERIOR)
        

        kwargs = { 'folio_declaracion':folio_declaracion }
        agregar, editar_id, bienes_inmuebles_data, informacion_registrada = ( declaracion_datos(kwargs, BienesInmuebles, declaracion))
        
        inmueble = sorted(chain(informacion_registrada, Inmueble_declarante, Inmuebles_propAnterior), key=lambda instance: instance.created_at)
        vehiculos = MueblesNoRegistrables.objects.filter(declaraciones=declaracion)
        muebles = BienesMuebles.objects.filter(declaraciones=declaracion)
        error = {}
    
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(e, exc_type, fname, exc_tb.tb_lineno)
        declaracion = {}
        inmueble = {}
        vehiculos = {}
        muebles = {}
        error = e

    context.update({
        'declaracion': declaracion,
        'inmuebles': inmueble,
        'vehiculos': vehiculos,
        'muebles': muebles, 
        'error': error,
    })

    return context

#@cache_page(CACHE_TTL)
class ConfirmacionAllinOne(View):
    """
    Class ConfirmacionAllinOne vista basada en clases muestra información de todas las secciones de la declaración
    """
    template_name = "declaracion/confirmacion/all.html"

    @method_decorator(login_required(login_url='/login'))
    def get(self, request, *args, **kwargs):
        """
        Function get obtiene la información de todas las secciones de declaración y la envia 
        a un template que será presentado al usuario
        """
        context = {}
        folio_declaracion = self.kwargs['folio']
        usuario = request.user

        context.update(get_context_InformacionPersonal(folio_declaracion, usuario))
        context.update(get_context_Intereses(folio_declaracion, usuario))
        context.update(get_context_ingresos(folio_declaracion, usuario))
        context.update(get_context_activos(folio_declaracion, usuario))
        context.update(get_context_pasivos(folio_declaracion, usuario))

        declaracion = Declaraciones.objects.filter(folio=uuid.UUID(folio_declaracion), info_personal_fija__usuario=usuario).all()[0]
        vehiculos = MueblesNoRegistrables.objects.filter(declaraciones=declaracion)
        inversiones = Inversiones.objects.filter(declaraciones=declaracion)
        adeudos = DeudasOtros.objects.filter(declaraciones=declaracion)
        prestamos = PrestamoComodato.objects.filter(declaraciones=declaracion)
        fideicomisos = Fideicomisos.objects.filter(declaraciones=declaracion)
        context.update({"vehiculos": vehiculos})
        context.update({"inversiones": inversiones})
        context.update({"adeudos": adeudos})
        context.update({"prestamos": prestamos})
        context.update({"fideicomisos": fideicomisos})

        return render(request, self.template_name, context)


class ConfirmarDeclaracionView(View):
    """
    Class ConfirmarDeclaración se encarga de realiar el cierre de la declaración
    """
    def get(self, request, *args, **kwargs):
        raise Http404()

    @method_decorator(login_required(login_url='/login'))
    def post(self, request, *args, **kwargs):
        """
        Function post recibe y guarda información de la declaración
        --------
        Una vez guardad el usuario ya no podrá relizar modificaciones
        --------
        Se le solicita la usuario que indique si los campos serán públicos o no
        """
        try:
            folio_declaracion = self.kwargs['folio']
            declaracion = validar_declaracion(request, folio_declaracion)
        except Exception as e:
            raise Http404()

        try:
            confirmacion = ConfirmacionForm(request.POST)
            if confirmacion.is_valid():
                declaracion.cat_estatus_id = 4
                declaracion.fecha_recepcion = datetime.today()
                declaracion.datos_publicos = bool(request.POST.get('datos_publicos'))
                declaracion.save()
                return redirect('declaraciones-previas')
            else:
                messages.warning(request, u"Debe indicar si los datos serán publicos")
                return redirect('declaracion:confirmar-allinone', folio=declaracion.folio)
        except Exception as e:
            print (e)
            return redirect('declaracion:confirmar-allinone', folio=declaracion.folio)