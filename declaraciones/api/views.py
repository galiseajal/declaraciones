import json

from django.http import HttpResponse
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view, \
permission_classes, authentication_classes
from django.views.decorators.csrf import csrf_exempt

from declaracion.models import Declaraciones
from django.http import JsonResponse
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication
from .utils import *
from .serialize_functions import serialize_declaracion, serialize_response_entry
from .validator import get_token_from_request, token_not_expired

from rest_framework import status, permissions, serializers, generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated

from oauth2_provider.views.generic import ProtectedResourceView
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from declaraciones.settings import EXPIRES_IN_N_MINUTES
from datetime import timedelta
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.generics import RetrieveAPIView
from rest_framework_simplejwt import views as jwt_views

from decimal import Decimal

default_page = 1
default_size = 10
max_page_size = 200
empty_json_error_auth = '{ "detail": "Credenciales no autorizadas." }'
empty_json_error_auth = json.loads(empty_json_error_auth)


class OauthDeclaraciones(ProtectedResourceView):

    def dispatch(self, request, *args, **kwargs):
        # let preflight OPTIONS requests pass
        if request.method.upper() == "OPTIONS":
            return super().dispatch(request, *args, **kwargs)

        # check if the request is valid and the protected resource may be accessed
        valid, r = self.verify_request(request)
        if valid:
            request.resource_owner = r.user
            return super().dispatch(request, *args, **kwargs)
        else:
            return JsonResponse({"codigo": "700", "descripcion":"token expirado"})

    def post(self, request, *args, **kwargs):

        declaraciones = Declaraciones.objects.filter(cat_estatus=4)

        try:
            json_data = json.loads(request.body)
        except Exception as e:
            return JsonResponse(res_400_error())

        #t_expired= False
        #if t_expired :
        #   response=json.loads('{"code":"700", "description":"Token expirado"}')
        #   return JsonResponse(response)


        page_number = int(json_data.get('page', default_page))

        page_size = clean_integer_value(
            json_data.get('pageSize', default_size),
            exception_type_default=default_size,
            min_value=1, max_value=max_page_size
        )

        query_filter = api_query_filter(
            json_data.get("query", {}), query_structure
        )
        
        if query_filter:
            declaraciones = declaraciones.filter(**query_filter)
            
            if "datosEmpleoCargoComision" in json_data.get("query", {}):
                declaraciones = api_query_filter_datosempleocargocomision(declaraciones,json_data.get("query", {}))

        sort_by = sanitize_sort_parameters(
            sort_structure, json_data.get("sort", {})
        )

        if sort_by:
            if "datosEmpleoCargoComision" in json_data.get("sort", {}):
                declaraciones = api_query_filter_datosempleocargocomision(declaraciones,json_data.get("query", {}))
            if "totalIngresosNetos" in json_data.get("sort", {}):
                declaraciones = api_query_filter_ingresos(declaraciones)

            declaraciones = declaraciones.order_by(*sort_by)


        pagination_data = {}
        declaraciones = get_page(
            declaraciones.all(),
            page_number=page_number,
            page_size=page_size,
            pagination_data=pagination_data
        )
        response = {
            "pagination": {
                "pageSize": pagination_data["pageSize"],
                "totalRows": pagination_data["totalRows"],
                "hasNextPage": pagination_data["hasNextPage"],
                "page": pagination_data["page"]
            },
            "results": [ serialize_response_entry(declaracion) for declaracion in declaraciones ]
        }
        return JsonResponse(response)


@api_view(['GET', 'POST'])
#@permission_classes([permissions.IsAuthenticated, TokenHasReadWriteScope])
class ApiSimpleExample(generics.ListAPIView):
    serializer_class = Declaraciones

    def get(self, request, format=None):
        try:
            results = Declaraciones.objects.filter(cat_estatus=4)
        except Exception as e:
            results = None
        
        return results


class LoginView(jwt_views.TokenObtainPairView):

    def post(self, request, *args, **kwargs):
        try:
            json_data = json.loads(request.body)
            _username = json_data.get('username')
            user = User.objects.get(username=_username)
            if not user.groups.filter(name='api').exists():
                return Response(empty_json_error_auth)
        except Exception as e:
            return Response(empty_json_error_auth)
        data = super().post(request, *args, **kwargs)
        data.data["token_type"] =  "Bearer"
        data.data["expires_in"] = EXPIRES_IN_N_MINUTES * 60
        return data



@api_view(['POST'])
#@authentication_classes([JWTTokenUserAuthentication, SessionAuthentication, BasicAuthentication])
#@permission_classes([IsAuthenticated])
def test_protected_view(request):
    """ View to test different status code responses.
    """
    # Test status code 400
    # return Response(res_400_error(), status=status.HTTP_400_BAD_REQUEST)
    # Test status code 401
    # return Response(res_401_error(), status=status.HTTP_401_UNAUTHORIZED)
    # Test status code default
    # VerifyJSONWebTokenSerializer
    # valid_data = VerifyJSONWebTokenSerializer().validate(request)
    # return Response(res_default_error(), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return JsonResponse({"hello": "world"})



#@authentication_classes([SessionAuthentication, BasicAuthentication])
#@authentication_classes([JWTTokenUserAuthentication])
#@permission_classes([IsAuthenticated])
@api_view(['GET', 'POST'])
def declaraciones_api_view(request):
    """ Api declaraciones endpoint.
    """
    ###
    # Validation in order to raise a 400 response
    ###
    declaraciones = Declaraciones.objects.filter(cat_estatus=4) 

    try:
        json_data = json.loads(request.body)
    except Exception as e:
        return Response(res_400_error(), status=status.HTTP_400_BAD_REQUEST)
        # Log exception
        json_data = {}

    # Page number based on results pagination.
    page_number = clean_integer_value(
        json_data.get('page', default_page),
        exception_type_default=default_page,
        min_value=1
    )

    page_size = clean_integer_value(
        json_data.get('pageSize', default_size),
        exception_type_default=default_size,
        min_value=1, max_value=max_page_size
    )

    ###
    # Query:
    ###
    query_filter = api_query_filter(
        json_data.get("query", {}), query_structure
    )
    if query_filter:
        declaraciones = declaraciones.filter(**query_filter)

    ###
    # Sort
    ###
    sort_by = sanitize_sort_parameters(
        sort_structure, json_data.get('sort', {})
    )
    declaraciones = declaraciones.order_by(*sort_by)

    ###
    # Slice (Paginate)
    ###
    pagination_data = {}
    declaraciones = get_page(
        declaraciones.all(),
        page_number=page_number,
        page_size=page_size,
        pagination_data=pagination_data
    )
    return JsonResponse({
        "pagination": {
            "pageSize": pagination_data["pageSize"],
            "totalRows": pagination_data["totalRows"],
            "hasNextPage": pagination_data["hasNextPage"],
            "page": pagination_data["page"]
        },
        "results": [ serialize_response_entry(declaracion) for declaracion in declaraciones ]
    })



def res_error(code, message):
    """ Unexpected Error response
    """
    return {
        "code": code,
        "message": message
    }


def res_default_error(code=500, message="Unexpected Error response"):
    """ Unexpected Error response
    """
    # code: TBD
    return res_error(code, message)


def res_400_error(code=400, message="Página inválida (bad request)"):
    """ Bad request custom response
    """
    return res_error(code, message)


def res_401_error(code=401, message="acceso no autorizado"):
    """ Auth error response
    """
    return res_error(code, message)
