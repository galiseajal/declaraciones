# -- coding: utf-8 --
import requests
import time
import pytest
import random
import string
import pprint
import json

#host = "http://189.213.134.151:8080/api/"
pytest.host = "http://189.213.134.151:8080/api/"
pytest.tokenbearer = "BUP4MkZLQoDQKMzJvNigSK1zMAvf9S"

@pytest.fixture
def gettoken():
    data = {
        #"grant_type": "authorization_code",
        #"code": "1xWpREheoai3frZDE6vaWHNp7FYkLu",
        "grant_type": "password",
        "client_id":"client_id",
        "client_secret":"client_secret",
        "scope":"read write",
        "username":"plataforma",
        "password":"digitalnacional"
    }
    response = requests.post(
        url= "http://189.213.134.151:8080/o/token/",
        data=data
    ).json()
    
    tokenbearer = response["access_token"]
    pytest.tokenbearer = tokenbearer

@pytest.mark.test_1
def test01(gettoken):
    data = {
        #"grant_type": "authorization_code",
        #"code": "1xWpREheoai3frZDE6vaWHNp7FYkLu",
        "grant_type": "password",
        "client_id":"client_id",
        "client_secret":"client_secret",
        "scope":"read write",
        "username":"plataforma",
        "password":"digitalnacional"
    }
    response = requests.post(
        url= "http://189.213.134.151:8080/o/token/",
        data=data
    ).json()
    print(response["access_token"])


@pytest.mark.test_55
def test_55():
    data = {
        "sort": {
        "datosEmpleoCargoComision":
            {
                "entidadFederativa": "asc"
            }
        }
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    data = {
        "sort": {
        "datosEmpleoCargoComision":
            {
                "entidadFederativa": "desc"
            }
        }
    }

    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    print([ 
        res["declaracion"]["situacionPatrimonial"]["domicilioDeclarante"]["domicilioMexico"]["entidadFederativa"] \
        for res in response["results"] 
    ])