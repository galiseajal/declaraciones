-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: declaracionesU
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=377 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add activos bienes',7,'add_activosbienes'),(26,'Can change activos bienes',7,'change_activosbienes'),(27,'Can delete activos bienes',7,'delete_activosbienes'),(28,'Can view activos bienes',7,'view_activosbienes'),(29,'Can add cat activo bien',8,'add_catactivobien'),(30,'Can change cat activo bien',8,'change_catactivobien'),(31,'Can delete cat activo bien',8,'delete_catactivobien'),(32,'Can view cat activo bien',8,'view_catactivobien'),(33,'Can add cat ambitos laborales',9,'add_catambitoslaborales'),(34,'Can change cat ambitos laborales',9,'change_catambitoslaborales'),(35,'Can delete cat ambitos laborales',9,'delete_catambitoslaborales'),(36,'Can view cat ambitos laborales',9,'view_catambitoslaborales'),(37,'Can add cat documentos obtenidos',10,'add_catdocumentosobtenidos'),(38,'Can change cat documentos obtenidos',10,'change_catdocumentosobtenidos'),(39,'Can delete cat documentos obtenidos',10,'delete_catdocumentosobtenidos'),(40,'Can view cat documentos obtenidos',10,'view_catdocumentosobtenidos'),(41,'Can add cat entes publicos',11,'add_catentespublicos'),(42,'Can change cat entes publicos',11,'change_catentespublicos'),(43,'Can delete cat entes publicos',11,'delete_catentespublicos'),(44,'Can view cat entes publicos',11,'view_catentespublicos'),(45,'Can add cat entidades federativas',12,'add_catentidadesfederativas'),(46,'Can change cat entidades federativas',12,'change_catentidadesfederativas'),(47,'Can delete cat entidades federativas',12,'delete_catentidadesfederativas'),(48,'Can view cat entidades federativas',12,'view_catentidadesfederativas'),(49,'Can add cat estados civiles',13,'add_catestadosciviles'),(50,'Can change cat estados civiles',13,'change_catestadosciviles'),(51,'Can delete cat estados civiles',13,'delete_catestadosciviles'),(52,'Can view cat estados civiles',13,'view_catestadosciviles'),(53,'Can add cat estatus declaracion',14,'add_catestatusdeclaracion'),(54,'Can change cat estatus declaracion',14,'change_catestatusdeclaracion'),(55,'Can delete cat estatus declaracion',14,'delete_catestatusdeclaracion'),(56,'Can view cat estatus declaracion',14,'view_catestatusdeclaracion'),(57,'Can add cat estatus estudios',15,'add_catestatusestudios'),(58,'Can change cat estatus estudios',15,'change_catestatusestudios'),(59,'Can delete cat estatus estudios',15,'delete_catestatusestudios'),(60,'Can view cat estatus estudios',15,'view_catestatusestudios'),(61,'Can add cat formas adquisiciones',16,'add_catformasadquisiciones'),(62,'Can change cat formas adquisiciones',16,'change_catformasadquisiciones'),(63,'Can delete cat formas adquisiciones',16,'delete_catformasadquisiciones'),(64,'Can view cat formas adquisiciones',16,'view_catformasadquisiciones'),(65,'Can add cat funciones principales',17,'add_catfuncionesprincipales'),(66,'Can change cat funciones principales',17,'change_catfuncionesprincipales'),(67,'Can delete cat funciones principales',17,'delete_catfuncionesprincipales'),(68,'Can view cat funciones principales',17,'view_catfuncionesprincipales'),(69,'Can add cat grados academicos',18,'add_catgradosacademicos'),(70,'Can change cat grados academicos',18,'change_catgradosacademicos'),(71,'Can delete cat grados academicos',18,'delete_catgradosacademicos'),(72,'Can view cat grados academicos',18,'view_catgradosacademicos'),(73,'Can add cat monedas',19,'add_catmonedas'),(74,'Can change cat monedas',19,'change_catmonedas'),(75,'Can delete cat monedas',19,'delete_catmonedas'),(76,'Can view cat monedas',19,'view_catmonedas'),(77,'Can add cat naturaleza membresia',20,'add_catnaturalezamembresia'),(78,'Can change cat naturaleza membresia',20,'change_catnaturalezamembresia'),(79,'Can delete cat naturaleza membresia',20,'delete_catnaturalezamembresia'),(80,'Can view cat naturaleza membresia',20,'view_catnaturalezamembresia'),(81,'Can add cat ordenes gobierno',21,'add_catordenesgobierno'),(82,'Can change cat ordenes gobierno',21,'change_catordenesgobierno'),(83,'Can delete cat ordenes gobierno',21,'delete_catordenesgobierno'),(84,'Can view cat ordenes gobierno',21,'view_catordenesgobierno'),(85,'Can add cat paises',22,'add_catpaises'),(86,'Can change cat paises',22,'change_catpaises'),(87,'Can delete cat paises',22,'delete_catpaises'),(88,'Can view cat paises',22,'view_catpaises'),(89,'Can add cat poderes',23,'add_catpoderes'),(90,'Can change cat poderes',23,'change_catpoderes'),(91,'Can delete cat poderes',23,'delete_catpoderes'),(92,'Can view cat poderes',23,'view_catpoderes'),(93,'Can add cat regimenes matrimoniales',24,'add_catregimenesmatrimoniales'),(94,'Can change cat regimenes matrimoniales',24,'change_catregimenesmatrimoniales'),(95,'Can delete cat regimenes matrimoniales',24,'delete_catregimenesmatrimoniales'),(96,'Can view cat regimenes matrimoniales',24,'view_catregimenesmatrimoniales'),(97,'Can add cat sectores industria',25,'add_catsectoresindustria'),(98,'Can change cat sectores industria',25,'change_catsectoresindustria'),(99,'Can delete cat sectores industria',25,'delete_catsectoresindustria'),(100,'Can view cat sectores industria',25,'view_catsectoresindustria'),(101,'Can add cat tipo participacion',26,'add_cattipoparticipacion'),(102,'Can change cat tipo participacion',26,'change_cattipoparticipacion'),(103,'Can delete cat tipo participacion',26,'delete_cattipoparticipacion'),(104,'Can view cat tipo participacion',26,'view_cattipoparticipacion'),(105,'Can add cat tipo persona',27,'add_cattipopersona'),(106,'Can change cat tipo persona',27,'change_cattipopersona'),(107,'Can delete cat tipo persona',27,'delete_cattipopersona'),(108,'Can view cat tipo persona',27,'view_cattipopersona'),(109,'Can add cat tipos acreedores',28,'add_cattiposacreedores'),(110,'Can change cat tipos acreedores',28,'change_cattiposacreedores'),(111,'Can delete cat tipos acreedores',28,'delete_cattiposacreedores'),(112,'Can view cat tipos acreedores',28,'view_cattiposacreedores'),(113,'Can add cat tipos actividad',29,'add_cattiposactividad'),(114,'Can change cat tipos actividad',29,'change_cattiposactividad'),(115,'Can delete cat tipos actividad',29,'delete_cattiposactividad'),(116,'Can view cat tipos actividad',29,'view_cattiposactividad'),(117,'Can add cat tipos adeudos',30,'add_cattiposadeudos'),(118,'Can change cat tipos adeudos',30,'change_cattiposadeudos'),(119,'Can delete cat tipos adeudos',30,'delete_cattiposadeudos'),(120,'Can view cat tipos adeudos',30,'view_cattiposadeudos'),(121,'Can add cat tipos apoyos',31,'add_cattiposapoyos'),(122,'Can change cat tipos apoyos',31,'change_cattiposapoyos'),(123,'Can delete cat tipos apoyos',31,'delete_cattiposapoyos'),(124,'Can view cat tipos apoyos',31,'view_cattiposapoyos'),(125,'Can add cat tipos beneficios',32,'add_cattiposbeneficios'),(126,'Can change cat tipos beneficios',32,'change_cattiposbeneficios'),(127,'Can delete cat tipos beneficios',32,'delete_cattiposbeneficios'),(128,'Can view cat tipos beneficios',32,'view_cattiposbeneficios'),(129,'Can add cat tipos bienes',33,'add_cattiposbienes'),(130,'Can change cat tipos bienes',33,'change_cattiposbienes'),(131,'Can delete cat tipos bienes',33,'delete_cattiposbienes'),(132,'Can view cat tipos bienes',33,'view_cattiposbienes'),(133,'Can add cat tipos declaracion',34,'add_cattiposdeclaracion'),(134,'Can change cat tipos declaracion',34,'change_cattiposdeclaracion'),(135,'Can delete cat tipos declaracion',34,'delete_cattiposdeclaracion'),(136,'Can view cat tipos declaracion',34,'view_cattiposdeclaracion'),(137,'Can add cat tipos especificos inversiones',35,'add_cattiposespecificosinversiones'),(138,'Can change cat tipos especificos inversiones',35,'change_cattiposespecificosinversiones'),(139,'Can delete cat tipos especificos inversiones',35,'delete_cattiposespecificosinversiones'),(140,'Can view cat tipos especificos inversiones',35,'view_cattiposespecificosinversiones'),(141,'Can add cat tipos fideicomisos',36,'add_cattiposfideicomisos'),(142,'Can change cat tipos fideicomisos',36,'change_cattiposfideicomisos'),(143,'Can delete cat tipos fideicomisos',36,'delete_cattiposfideicomisos'),(144,'Can view cat tipos fideicomisos',36,'view_cattiposfideicomisos'),(145,'Can add cat tipos ingresos varios',37,'add_cattiposingresosvarios'),(146,'Can change cat tipos ingresos varios',37,'change_cattiposingresosvarios'),(147,'Can delete cat tipos ingresos varios',37,'delete_cattiposingresosvarios'),(148,'Can view cat tipos ingresos varios',37,'view_cattiposingresosvarios'),(149,'Can add cat tipos inmuebles',38,'add_cattiposinmuebles'),(150,'Can change cat tipos inmuebles',38,'change_cattiposinmuebles'),(151,'Can delete cat tipos inmuebles',38,'delete_cattiposinmuebles'),(152,'Can view cat tipos inmuebles',38,'view_cattiposinmuebles'),(153,'Can add cat tipos instituciones',39,'add_cattiposinstituciones'),(154,'Can change cat tipos instituciones',39,'change_cattiposinstituciones'),(155,'Can delete cat tipos instituciones',39,'delete_cattiposinstituciones'),(156,'Can view cat tipos instituciones',39,'view_cattiposinstituciones'),(157,'Can add cat tipos inversiones',40,'add_cattiposinversiones'),(158,'Can change cat tipos inversiones',40,'change_cattiposinversiones'),(159,'Can delete cat tipos inversiones',40,'delete_cattiposinversiones'),(160,'Can view cat tipos inversiones',40,'view_cattiposinversiones'),(161,'Can add cat tipos metales',41,'add_cattiposmetales'),(162,'Can change cat tipos metales',41,'change_cattiposmetales'),(163,'Can delete cat tipos metales',41,'delete_cattiposmetales'),(164,'Can view cat tipos metales',41,'view_cattiposmetales'),(165,'Can add cat tipos muebles',42,'add_cattiposmuebles'),(166,'Can change cat tipos muebles',42,'change_cattiposmuebles'),(167,'Can delete cat tipos muebles',42,'delete_cattiposmuebles'),(168,'Can view cat tipos muebles',42,'view_cattiposmuebles'),(169,'Can add cat tipos operaciones',43,'add_cattiposoperaciones'),(170,'Can change cat tipos operaciones',43,'change_cattiposoperaciones'),(171,'Can delete cat tipos operaciones',43,'delete_cattiposoperaciones'),(172,'Can view cat tipos operaciones',43,'view_cattiposoperaciones'),(173,'Can add cat tipos pasivos',44,'add_cattipospasivos'),(174,'Can change cat tipos pasivos',44,'change_cattipospasivos'),(175,'Can delete cat tipos pasivos',44,'delete_cattipospasivos'),(176,'Can view cat tipos pasivos',44,'view_cattipospasivos'),(177,'Can add cat tipos relaciones personales',45,'add_cattiposrelacionespersonales'),(178,'Can change cat tipos relaciones personales',45,'change_cattiposrelacionespersonales'),(179,'Can delete cat tipos relaciones personales',45,'delete_cattiposrelacionespersonales'),(180,'Can view cat tipos relaciones personales',45,'view_cattiposrelacionespersonales'),(181,'Can add cat tipos representaciones',46,'add_cattiposrepresentaciones'),(182,'Can change cat tipos representaciones',46,'change_cattiposrepresentaciones'),(183,'Can delete cat tipos representaciones',46,'delete_cattiposrepresentaciones'),(184,'Can view cat tipos representaciones',46,'view_cattiposrepresentaciones'),(185,'Can add cat tipos titulares',47,'add_cattipostitulares'),(186,'Can change cat tipos titulares',47,'change_cattipostitulares'),(187,'Can delete cat tipos titulares',47,'delete_cattipostitulares'),(188,'Can view cat tipos titulares',47,'view_cattipostitulares'),(189,'Can add cat tipo via',48,'add_cattipovia'),(190,'Can change cat tipo via',48,'change_cattipovia'),(191,'Can delete cat tipo via',48,'delete_cattipovia'),(192,'Can view cat tipo via',48,'view_cattipovia'),(193,'Can add cat titular tipos relaciones',49,'add_cattitulartiposrelaciones'),(194,'Can change cat titular tipos relaciones',49,'change_cattitulartiposrelaciones'),(195,'Can delete cat titular tipos relaciones',49,'delete_cattitulartiposrelaciones'),(196,'Can view cat titular tipos relaciones',49,'view_cattitulartiposrelaciones'),(197,'Can add cat unidades temporales',50,'add_catunidadestemporales'),(198,'Can change cat unidades temporales',50,'change_catunidadestemporales'),(199,'Can delete cat unidades temporales',50,'delete_catunidadestemporales'),(200,'Can view cat unidades temporales',50,'view_catunidadestemporales'),(201,'Can add declaraciones',51,'add_declaraciones'),(202,'Can change declaraciones',51,'change_declaraciones'),(203,'Can delete declaraciones',51,'delete_declaraciones'),(204,'Can view declaraciones',51,'view_declaraciones'),(205,'Can add domicilios',52,'add_domicilios'),(206,'Can change domicilios',52,'change_domicilios'),(207,'Can delete domicilios',52,'delete_domicilios'),(208,'Can view domicilios',52,'view_domicilios'),(209,'Can add info personal var',53,'add_infopersonalvar'),(210,'Can change info personal var',53,'change_infopersonalvar'),(211,'Can delete info personal var',53,'delete_infopersonalvar'),(212,'Can view info personal var',53,'view_infopersonalvar'),(213,'Can add observaciones',54,'add_observaciones'),(214,'Can change observaciones',54,'change_observaciones'),(215,'Can delete observaciones',54,'delete_observaciones'),(216,'Can view observaciones',54,'view_observaciones'),(217,'Can add sueldos publicos',55,'add_sueldospublicos'),(218,'Can change sueldos publicos',55,'change_sueldospublicos'),(219,'Can delete sueldos publicos',55,'delete_sueldospublicos'),(220,'Can view sueldos publicos',55,'view_sueldospublicos'),(221,'Can add socios comerciales',56,'add_socioscomerciales'),(222,'Can change socios comerciales',56,'change_socioscomerciales'),(223,'Can delete socios comerciales',56,'delete_socioscomerciales'),(224,'Can view socios comerciales',56,'view_socioscomerciales'),(225,'Can add secciones',57,'add_secciones'),(226,'Can change secciones',57,'change_secciones'),(227,'Can delete secciones',57,'delete_secciones'),(228,'Can view secciones',57,'view_secciones'),(229,'Can add seccion declaracion',58,'add_secciondeclaracion'),(230,'Can change seccion declaracion',58,'change_secciondeclaracion'),(231,'Can delete seccion declaracion',58,'delete_secciondeclaracion'),(232,'Can view seccion declaracion',58,'view_secciondeclaracion'),(233,'Can add representaciones',59,'add_representaciones'),(234,'Can change representaciones',59,'change_representaciones'),(235,'Can delete representaciones',59,'delete_representaciones'),(236,'Can view representaciones',59,'view_representaciones'),(237,'Can add otras partes',60,'add_otraspartes'),(238,'Can change otras partes',60,'change_otraspartes'),(239,'Can delete otras partes',60,'delete_otraspartes'),(240,'Can view otras partes',60,'view_otraspartes'),(241,'Can add nacionalidades',61,'add_nacionalidades'),(242,'Can change nacionalidades',61,'change_nacionalidades'),(243,'Can delete nacionalidades',61,'delete_nacionalidades'),(244,'Can view nacionalidades',61,'view_nacionalidades'),(245,'Can add muebles no registrables',62,'add_mueblesnoregistrables'),(246,'Can change muebles no registrables',62,'change_mueblesnoregistrables'),(247,'Can delete muebles no registrables',62,'delete_mueblesnoregistrables'),(248,'Can view muebles no registrables',62,'view_mueblesnoregistrables'),(249,'Can add membresias',63,'add_membresias'),(250,'Can change membresias',63,'change_membresias'),(251,'Can delete membresias',63,'delete_membresias'),(252,'Can view membresias',63,'view_membresias'),(253,'Can add inversiones',64,'add_inversiones'),(254,'Can change inversiones',64,'change_inversiones'),(255,'Can delete inversiones',64,'delete_inversiones'),(256,'Can view inversiones',64,'view_inversiones'),(257,'Can add ingresos varios',65,'add_ingresosvarios'),(258,'Can change ingresos varios',65,'change_ingresosvarios'),(259,'Can delete ingresos varios',65,'delete_ingresosvarios'),(260,'Can view ingresos varios',65,'view_ingresosvarios'),(261,'Can add info personal fija',66,'add_infopersonalfija'),(262,'Can change info personal fija',66,'change_infopersonalfija'),(263,'Can delete info personal fija',66,'delete_infopersonalfija'),(264,'Can view info personal fija',66,'view_infopersonalfija'),(265,'Can add fideicomisos',67,'add_fideicomisos'),(266,'Can change fideicomisos',67,'change_fideicomisos'),(267,'Can delete fideicomisos',67,'delete_fideicomisos'),(268,'Can view fideicomisos',67,'view_fideicomisos'),(269,'Can add experiencia laboral',68,'add_experiencialaboral'),(270,'Can change experiencia laboral',68,'change_experiencialaboral'),(271,'Can delete experiencia laboral',68,'delete_experiencialaboral'),(272,'Can view experiencia laboral',68,'view_experiencialaboral'),(273,'Can add encargos',69,'add_encargos'),(274,'Can change encargos',69,'change_encargos'),(275,'Can delete encargos',69,'delete_encargos'),(276,'Can view encargos',69,'view_encargos'),(277,'Can add empresas sociedades',70,'add_empresassociedades'),(278,'Can change empresas sociedades',70,'change_empresassociedades'),(279,'Can delete empresas sociedades',70,'delete_empresassociedades'),(280,'Can view empresas sociedades',70,'view_empresassociedades'),(281,'Can add efectivo metales',71,'add_efectivometales'),(282,'Can change efectivo metales',71,'change_efectivometales'),(283,'Can delete efectivo metales',71,'delete_efectivometales'),(284,'Can view efectivo metales',71,'view_efectivometales'),(285,'Can add deudas otros',72,'add_deudasotros'),(286,'Can change deudas otros',72,'change_deudasotros'),(287,'Can delete deudas otros',72,'delete_deudasotros'),(288,'Can view deudas otros',72,'view_deudasotros'),(289,'Can add datos curriculares',73,'add_datoscurriculares'),(290,'Can change datos curriculares',73,'change_datoscurriculares'),(291,'Can delete datos curriculares',73,'delete_datoscurriculares'),(292,'Can view datos curriculares',73,'view_datoscurriculares'),(293,'Can add cuentas por cobrar',74,'add_cuentasporcobrar'),(294,'Can change cuentas por cobrar',74,'change_cuentasporcobrar'),(295,'Can delete cuentas por cobrar',74,'delete_cuentasporcobrar'),(296,'Can view cuentas por cobrar',74,'view_cuentasporcobrar'),(297,'Can add conyuge dependientes',75,'add_conyugedependientes'),(298,'Can change conyuge dependientes',75,'change_conyugedependientes'),(299,'Can delete conyuge dependientes',75,'delete_conyugedependientes'),(300,'Can view conyuge dependientes',75,'view_conyugedependientes'),(301,'Can add clientes principales',76,'add_clientesprincipales'),(302,'Can change clientes principales',76,'change_clientesprincipales'),(303,'Can delete clientes principales',76,'delete_clientesprincipales'),(304,'Can view clientes principales',76,'view_clientesprincipales'),(305,'Can add bienes personas',77,'add_bienespersonas'),(306,'Can change bienes personas',77,'change_bienespersonas'),(307,'Can delete bienes personas',77,'delete_bienespersonas'),(308,'Can view bienes personas',77,'view_bienespersonas'),(309,'Can add bienes muebles',78,'add_bienesmuebles'),(310,'Can change bienes muebles',78,'change_bienesmuebles'),(311,'Can delete bienes muebles',78,'delete_bienesmuebles'),(312,'Can view bienes muebles',78,'view_bienesmuebles'),(313,'Can add bienes intangibles',79,'add_bienesintangibles'),(314,'Can change bienes intangibles',79,'change_bienesintangibles'),(315,'Can delete bienes intangibles',79,'delete_bienesintangibles'),(316,'Can view bienes intangibles',79,'view_bienesintangibles'),(317,'Can add bienes inmuebles',80,'add_bienesinmuebles'),(318,'Can change bienes inmuebles',80,'change_bienesinmuebles'),(319,'Can delete bienes inmuebles',80,'delete_bienesinmuebles'),(320,'Can view bienes inmuebles',80,'view_bienesinmuebles'),(321,'Can add beneficios gratuitos',81,'add_beneficiosgratuitos'),(322,'Can change beneficios gratuitos',81,'change_beneficiosgratuitos'),(323,'Can delete beneficios gratuitos',81,'delete_beneficiosgratuitos'),(324,'Can view beneficios gratuitos',81,'view_beneficiosgratuitos'),(325,'Can add beneficios especie',82,'add_beneficiosespecie'),(326,'Can change beneficios especie',82,'change_beneficiosespecie'),(327,'Can delete beneficios especie',82,'delete_beneficiosespecie'),(328,'Can view beneficios especie',82,'view_beneficiosespecie'),(329,'Can add apoyos',83,'add_apoyos'),(330,'Can change apoyos',83,'change_apoyos'),(331,'Can delete apoyos',83,'delete_apoyos'),(332,'Can view apoyos',83,'view_apoyos'),(333,'Can add cat campos obligatorios',84,'add_catcamposobligatorios'),(334,'Can change cat campos obligatorios',84,'change_catcamposobligatorios'),(335,'Can delete cat campos obligatorios',84,'delete_catcamposobligatorios'),(336,'Can view cat campos obligatorios',84,'view_catcamposobligatorios'),(337,'Can add cat municipios',85,'add_catmunicipios'),(338,'Can change cat municipios',85,'change_catmunicipios'),(339,'Can delete cat municipios',85,'delete_catmunicipios'),(340,'Can view cat municipios',85,'view_catmunicipios'),(341,'Can add declaracion fiscal',86,'add_declaracionfiscal'),(342,'Can change declaracion fiscal',86,'change_declaracionfiscal'),(343,'Can delete declaracion fiscal',86,'delete_declaracionfiscal'),(344,'Can view declaracion fiscal',86,'view_declaracionfiscal'),(345,'Can add cat unidades medida',87,'add_catunidadesmedida'),(346,'Can change cat unidades medida',87,'change_catunidadesmedida'),(347,'Can delete cat unidades medida',87,'delete_catunidadesmedida'),(348,'Can view cat unidades medida',87,'view_catunidadesmedida'),(349,'Can add cat tipos instrumentos',88,'add_cattiposinstrumentos'),(350,'Can change cat tipos instrumentos',88,'change_cattiposinstrumentos'),(351,'Can delete cat tipos instrumentos',88,'delete_cattiposinstrumentos'),(352,'Can view cat tipos instrumentos',88,'view_cattiposinstrumentos'),(353,'Can add ingresos declaracion',89,'add_ingresosdeclaracion'),(354,'Can change ingresos declaracion',89,'change_ingresosdeclaracion'),(355,'Can delete ingresos declaracion',89,'delete_ingresosdeclaracion'),(356,'Can view ingresos declaracion',89,'view_ingresosdeclaracion'),(357,'Can add cat motivo baja',90,'add_catmotivobaja'),(358,'Can change cat motivo baja',90,'change_catmotivobaja'),(359,'Can delete cat motivo baja',90,'delete_catmotivobaja'),(360,'Can view cat motivo baja',90,'view_catmotivobaja'),(361,'Can add prestamo comodato',91,'add_prestamocomodato'),(362,'Can change prestamo comodato',91,'change_prestamocomodato'),(363,'Can delete prestamo comodato',91,'delete_prestamocomodato'),(364,'Can view prestamo comodato',91,'view_prestamocomodato'),(365,'Can add cat puestos',92,'add_catpuestos'),(366,'Can change cat puestos',92,'change_catpuestos'),(367,'Can delete cat puestos',92,'delete_catpuestos'),(368,'Can view cat puestos',92,'view_catpuestos'),(369,'Can add Preguntas Frecuentes sobre declaraciones',93,'add_declaracion_faqs'),(370,'Can change Preguntas Frecuentes sobre declaraciones',93,'change_declaracion_faqs'),(371,'Can delete Preguntas Frecuentes sobre declaraciones',93,'delete_declaracion_faqs'),(372,'Can view Preguntas Frecuentes sobre declaraciones',93,'view_declaracion_faqs'),(373,'Can add personalización del sitio',94,'add_sitio_personalizacion'),(374,'Can change personalización del sitio',94,'change_sitio_personalizacion'),(375,'Can delete personalización del sitio',94,'delete_sitio_personalizacion'),(376,'Can view personalización del sitio',94,'view_sitio_personalizacion');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$150000$EOk6EEwtOGNg$f0OJ9o62OJLCKvGOl/x9I+i6DlkxWFIesDaOxK1vyb8=','2020-02-26 22:33:04.727009',0,'GEER940421300','GALILEA','GRANADOS ESCOBAR','galileaseajal19@gmail.com',0,1,'2019-12-06 18:29:33.447899'),(2,'pbkdf2_sha256$150000$kHhKDBmCOeZH$4DZrCqzUg8AxpzFPNGe8AotHUUDtU5XhxwrZCVy3r+g=','2020-03-18 16:53:51.400190',0,'DEAF010101256','DANIEL','AVALOS TORRES','daniel.avalos@sesaj.org',1,1,'2019-12-14 23:22:05.000000'),(3,'pbkdf2_sha256$150000$OVYxgmTpEKRJ$BwdV7tQMbZ7b1nH+ap/xcUWIya+GLhQ6UZfkppXs8VI=','2020-01-30 20:46:51.616473',0,'SAAL121212689','SALVADOR','MORALES MORA','salvadorsesaj@zoho.com',0,1,'2019-12-15 23:03:35.889329'),(4,'pbkdf2_sha256$150000$u7hzSbvDK0TE$UjLHV+EE/RhkZKXvOiEe4jtH3TAbKjrMwDFJYqrMDXE=','2020-03-03 20:21:26.174420',0,'RUAT010101596','RAUL','TORRES SILVA','raul@seajal.org',0,1,'2019-12-17 15:38:57.068537'),(5,'pbkdf2_sha256$150000$qoAaHlkqJnA8$njTnQIbVg1tuICJxVEBHxvNgY9jvEWXcd2nS8xRKqa8=','2020-01-30 20:47:03.577031',0,'MOUR010101458','MONICA','ROSALES UREÑA','daniel@sesaj.org',0,1,'2019-12-17 16:05:27.342905'),(6,'pbkdf2_sha256$150000$EOk6EEwtOGNg$f0OJ9o62OJLCKvGOl/x9I+i6DlkxWFIesDaOxK1vyb8=','2020-03-03 16:37:13.304791',0,'JOID010101589','JOSE LUIS','ISIDRO LOPEZ','jose@seajal.org',0,1,'2019-12-17 16:08:13.816015'),(7,'pbkdf2_sha256$150000$xS8ApMNtVWs6$qmwChgiRBch59WhhHH1XAK0c/ic5+fd0hxQikw1Scr0=','2020-02-18 17:03:26.499887',0,'PEOD010101589','PEDRO','PARAFAN IRAIS','pedro@seajal.org',0,1,'2019-12-17 16:10:37.975111'),(8,'pbkdf2_sha256$150000$pD9Ebr3MnqNX$yyRARnUoRwNecZGBGnqLUuEA3bdOQZY1uWjhskVZ3yg=','2020-01-30 20:48:22.608643',0,'SIAM010101018','ISMAEL','PRADO HUERTA','ismael@seajal.org',0,1,'2019-12-17 16:21:26.964226'),(9,'pbkdf2_sha256$150000$FdFlXwyU5ubD$AlSrNmnr3D2sWziZWdoO9JLNHBYG9WqRN0LAdOg62iY=','2020-03-04 15:49:16.534233',0,'LOAS010101589','LORENA','SUAREZ MONTANEGRO','lorena@seajal.com',0,1,'2019-12-17 16:24:23.854631'),(10,'pbkdf2_sha256$150000$OXhwqe9SGX1h$4JHrCBc2b1c/xZyOgnO3/PK1fJPf9qNEc9pV4gPp2Q4=','2020-03-17 18:31:17.606911',0,'ARDB010101589','ARMANDO','DE LA BARRERA CHAVEZ','armando@seajal.com',0,1,'2019-12-17 16:33:22.466904'),(11,'pbkdf2_sha256$150000$YYe5xGNhT3FL$mQp2fCx/FJE3hOJ96CCkJ8MQTl+o118uR2k5m77A+Hc=','2020-03-12 22:30:03.309594',1,'admin','','','',1,1,'2019-12-26 23:15:19.053043'),(13,'pbkdf2_sha256$150000$yDqohk1waDT3$y+e2teti8N8NdawtQjKEd8jyfCq57aDDLKdd/7ZKtwI=','2020-02-26 22:34:25.142421',0,'ZAER010101023','Zara','Remo','zara.remo@gmail.com',0,1,'2019-12-30 17:04:48.175381'),(14,'pbkdf2_sha256$150000$eyogLOiGZHdy$qMoslSdvsZymFgOA/9A38NC4uC+iPV6tz828aNpnVWw=','2020-01-24 17:41:54.397156',0,'TEER010101589','TERESA','CHAVEZ GUTIERREZ','teresa.chavez@gmail.com',0,1,'2019-12-30 17:24:15.789016'),(15,'pbkdf2_sha256$150000$6ioDHqvhEZTN$qRcofqZ3/7Xiv14R9sVrw0iYMdRL+o+D47JhxIyoW6w=','2020-01-29 17:18:33.863635',0,'MIAL010101892','SEBASTIA','MIRAMAR ALETA','sebastian.miramar@seajal.org',0,1,'2019-12-30 17:52:51.798231'),(16,'pbkdf2_sha256$150000$JmpshE68r3Ae$wd9j7alzscLvytQ2icPfHrpYDJ+dbm7IwiQQ2+rOM0Q=','2020-03-18 16:50:05.653081',0,'LIAS010101256','LISA','SIMPSOM SIMPSON','lisa.simpson@seajal.org',0,1,'2019-12-30 17:55:53.613771'),(18,'pbkdf2_sha256$150000$Cm9lzz4M4Wrz$4D5jQsjVDSOrk6vOn+ZWNh6QOZVg/dVKLeARa8ThVQs=','2020-01-24 17:41:31.381938',0,'ZAER010101123','ZARA','MORALES MORALES','galileagranados19@gmail.com',0,1,'2019-12-30 20:51:15.888414'),(20,'pbkdf2_sha256$150000$3folgGitjKD3$q9X/rFmFpevBUJglTnU7/a6Mb4dTIh2OCAb8ugLmA+c=','2020-02-06 18:18:02.008550',0,'PUER010101123','PRUEBA 1','PRUEBA 1','prueba@seajal.org',0,1,'2020-02-06 17:13:54.959572'),(22,'',NULL,0,'FAEZ010101123','','Flores Flores','carlos@seajal.org',0,0,'2020-02-06 18:47:12.946865'),(24,'pbkdf2_sha256$150000$QK3BsAA8xnWa$YhGBDbRpO9LKG0PS2lm+P0yQqIczJLFJ3wSJTsHg7ag=','2020-02-27 18:13:45.521622',0,'PUER010101321','PRUEBA 1','PRUEBA 1','prueba@seajal.org',0,1,'2020-02-14 23:40:05.850344'),(26,'pbkdf2_sha256$150000$Qvzymh4XuN3o$bAJWZIZWU5tfLl1LcFELL1VjqpALucIruTuDJKiemLw=','2020-02-25 23:00:24.213285',0,'PUER010101852','PRUEBA 3','PRUEBA 3','prueba3@gmail.com',0,1,'2020-02-17 15:39:12.629171'),(28,'pbkdf2_sha256$150000$JvbNlYmFAmyE$TERdAY4ovP02fKCHFToAGt//uWeK4vQXov8wvRndPqU=','2020-03-03 20:32:27.829510',0,'PUER010101569','','PRUEBA G','pruebag@gmail.com',0,1,'2020-02-17 17:30:11.428773');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_activosbienes`
--

DROP TABLE IF EXISTS `declaracion_activosbienes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_activosbienes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_activobien` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_activo_bien_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_activosb_cat_activo_bien_id_cff5986d_fk_declaraci` (`cat_activo_bien_id`),
  KEY `declaracion_activosb_declaraciones_id_a7d9ae97_fk_declaraci` (`declaraciones_id`),
  CONSTRAINT `declaracion_activosb_cat_activo_bien_id_cff5986d_fk_declaraci` FOREIGN KEY (`cat_activo_bien_id`) REFERENCES `declaracion_catactivobien` (`id`),
  CONSTRAINT `declaracion_activosb_declaraciones_id_a7d9ae97_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_activosbienes`
--

LOCK TABLES `declaracion_activosbienes` WRITE;
/*!40000 ALTER TABLE `declaracion_activosbienes` DISABLE KEYS */;
INSERT INTO `declaracion_activosbienes` VALUES (1,1,'2019-12-31 19:16:52.971643','2020-02-26 22:26:03.713144',4,1),(2,2,'2020-01-31 22:05:00.097308','2020-01-31 22:33:23.976584',3,1),(3,1,'2020-01-31 22:23:11.801928','2020-02-26 22:25:32.091134',1,1),(4,1,'2020-02-04 00:25:22.381510','2020-02-04 00:27:25.130163',5,1),(5,2,'2020-02-04 01:38:23.606848','2020-02-04 01:38:25.515468',1,9),(6,3,'2020-02-04 01:55:39.762454','2020-02-04 02:48:21.171623',4,9),(7,2,'2020-02-04 14:29:29.745948','2020-02-04 15:05:24.059710',5,9),(8,9,'2020-02-04 16:12:46.692811','2020-02-04 22:36:02.788751',1,10),(9,5,'2020-02-04 16:15:57.775457','2020-02-21 19:33:53.425508',5,10),(10,4,'2020-02-04 18:31:41.488002','2020-02-04 18:31:41.526705',4,10),(11,10,'2020-02-04 22:40:02.574084','2020-02-06 22:12:51.000709',1,2),(12,5,'2020-02-06 23:01:37.519295','2020-02-06 23:01:37.676029',4,2),(13,3,'2020-02-07 03:13:09.323845','2020-02-07 03:13:09.450315',3,2),(14,3,'2020-02-10 20:07:35.826384','2020-02-10 22:46:22.415402',5,2),(15,4,'2020-02-18 22:54:26.099062','2020-02-18 22:54:26.496344',3,11),(16,5,'2020-02-21 22:11:27.182531','2020-02-21 22:11:27.221394',3,10),(17,11,'2020-02-25 18:11:51.054936','2020-02-25 23:03:22.064258',1,11);
/*!40000 ALTER TABLE `declaracion_activosbienes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_apoyos`
--

DROP TABLE IF EXISTS `declaracion_apoyos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_apoyos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_programa` varchar(255) NOT NULL,
  `institucion_otorgante` varchar(255) NOT NULL,
  `otro_apoyo` varchar(255) NOT NULL,
  `valor_anual` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `beneficiario_infopersonalvar_id` int(11) NOT NULL,
  `cat_ordenes_gobierno_id` int(11) DEFAULT NULL,
  `cat_tipos_apoyos_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `forma_recepcion` varchar(20) DEFAULT NULL,
  `monto_apoyo_mensual` int(11) DEFAULT NULL,
  `especifiqueApoyo` varchar(255) DEFAULT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_apoyos_beneficiario_infoper_732a138e_fk_declaraci` (`beneficiario_infopersonalvar_id`),
  KEY `declaracion_apoyos_cat_ordenes_gobierno_df1ffade_fk_declaraci` (`cat_ordenes_gobierno_id`),
  KEY `declaracion_apoyos_cat_tipos_apoyos_id_313b18d0_fk_declaraci` (`cat_tipos_apoyos_id`),
  KEY `declaracion_apoyos_observaciones_id_7ce16aaf_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_apoyos_declaraciones_id_e29aa2b1_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_apoyos_moneda_id_5033d60c_fk_declaraci` (`moneda_id`),
  KEY `declaracion_apoyos_cat_tipos_operacione_8cce0a3b_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_apoyos_cat_tipos_relaciones_512ddfd2_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  CONSTRAINT `declaracion_apoyos_beneficiario_infoper_732a138e_fk_declaraci` FOREIGN KEY (`beneficiario_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_apoyos_cat_ordenes_gobierno_df1ffade_fk_declaraci` FOREIGN KEY (`cat_ordenes_gobierno_id`) REFERENCES `declaracion_catordenesgobierno` (`id`),
  CONSTRAINT `declaracion_apoyos_cat_tipos_apoyos_id_313b18d0_fk_declaraci` FOREIGN KEY (`cat_tipos_apoyos_id`) REFERENCES `declaracion_cattiposapoyos` (`id`),
  CONSTRAINT `declaracion_apoyos_cat_tipos_operacione_8cce0a3b_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_apoyos_cat_tipos_relaciones_512ddfd2_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_apoyos_declaraciones_id_e29aa2b1_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_apoyos_moneda_id_5033d60c_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_apoyos_observaciones_id_7ce16aaf_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_apoyos`
--

LOCK TABLES `declaracion_apoyos` WRITE;
/*!40000 ALTER TABLE `declaracion_apoyos` DISABLE KEYS */;
INSERT INTO `declaracion_apoyos` VALUES (1,'Fuerza mujer','Emprendimiento','',NULL,'2019-12-09 16:32:45.347951','2020-02-26 22:23:01.417113',1,1,1,13,1,'MONETARIO',5000,NULL,101,2,NULL),(2,'Teleton','test','',NULL,'2019-12-24 19:13:02.813076','2019-12-24 19:13:02.813130',61,1,1,130,3,'MONETARIO',8569,'No hay apoyo, solo es una prueba',101,1,NULL),(4,'Fuerza mujer','Emprendimiento','',NULL,'2020-02-04 05:50:24.896590','2020-02-04 05:50:24.896625',82,1,3,289,9,'MONETARIO',5250,'sdfaf',43,3,NULL),(5,'Fuerza mujer','Municipal','',NULL,'2020-02-10 20:01:45.811127','2020-02-10 21:08:17.818743',54,2,2,337,2,'MONETARIO',6500,'adsafds',101,2,42),(6,'Fuerza mujer','Gubernamental','',NULL,'2020-02-21 22:26:32.159207','2020-02-21 22:26:32.159292',84,NULL,NULL,389,10,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `declaracion_apoyos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_beneficiosespecie`
--

DROP TABLE IF EXISTS `declaracion_beneficiosespecie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_beneficiosespecie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_bien_servicio` varchar(255) NOT NULL,
  `valor_mercado` decimal(12,2) DEFAULT NULL,
  `otro_familiar` varchar(255) NOT NULL,
  `otra_relacion` varchar(255) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `domicilios_id` int(11) NOT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `otra_relacion_familiar` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_benefici_cat_sectores_industr_8d6070b3_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_benefici_cat_tipos_relaciones_41650aa6_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  KEY `declaracion_benefici_declaraciones_id_a63b7067_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_benefici_domicilios_id_14a9c6ef_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_benefici_info_personal_var_id_ec165393_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_benefici_observaciones_id_338025f3_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_benefici_cat_sectores_industr_8d6070b3_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_benefici_cat_tipos_relaciones_41650aa6_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_benefici_declaraciones_id_a63b7067_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_benefici_domicilios_id_14a9c6ef_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_benefici_info_personal_var_id_ec165393_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_benefici_observaciones_id_338025f3_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_beneficiosespecie`
--

LOCK TABLES `declaracion_beneficiosespecie` WRITE;
/*!40000 ALTER TABLE `declaracion_beneficiosespecie` DISABLE KEYS */;
/*!40000 ALTER TABLE `declaracion_beneficiosespecie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_beneficiosgratuitos`
--

DROP TABLE IF EXISTS `declaracion_beneficiosgratuitos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_beneficiosgratuitos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otros_beneficios` varchar(255) NOT NULL,
  `origen_beneficio` varchar(255) NOT NULL,
  `otro_sector` varchar(255) NOT NULL,
  `valor_beneficio` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `cat_tipos_beneficios_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  `razon_social_otorgante` varchar(255) DEFAULT NULL,
  `rfc_otorgante` varchar(255) DEFAULT NULL,
  `tipo_persona` varchar(255) DEFAULT NULL,
  `especifiqueBeneficio` varchar(255) DEFAULT NULL,
  `forma_recepcion` varchar(20) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `otorgante_infopersonalVar_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_benefici_cat_sectores_industr_cb9102ff_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_benefici_cat_tipos_beneficios_f60c0e0a_fk_declaraci` (`cat_tipos_beneficios_id`),
  KEY `declaracion_benefici_declaraciones_id_141bcf3e_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_benefici_observaciones_id_8a095534_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_benefici_moneda_id_16bdaed8_fk_declaraci` (`moneda_id`),
  KEY `declaracion_benefici_cat_tipos_relaciones_ba3381be_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  KEY `declaracion_benefici_cat_tipos_operacione_03b8d265_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_benefici_otorgante_infoperson_6da3d0c9_fk_declaraci` (`otorgante_infopersonalVar_id`),
  CONSTRAINT `declaracion_benefici_cat_sectores_industr_cb9102ff_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_benefici_cat_tipos_beneficios_f60c0e0a_fk_declaraci` FOREIGN KEY (`cat_tipos_beneficios_id`) REFERENCES `declaracion_cattiposbeneficios` (`id`),
  CONSTRAINT `declaracion_benefici_cat_tipos_operacione_03b8d265_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_benefici_cat_tipos_relaciones_ba3381be_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_benefici_declaraciones_id_141bcf3e_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_benefici_moneda_id_16bdaed8_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_benefici_observaciones_id_8a095534_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_benefici_otorgante_infoperson_6da3d0c9_fk_declaraci` FOREIGN KEY (`otorgante_infopersonalVar_id`) REFERENCES `declaracion_infopersonalvar` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_beneficiosgratuitos`
--

LOCK TABLES `declaracion_beneficiosgratuitos` WRITE;
/*!40000 ALTER TABLE `declaracion_beneficiosgratuitos` DISABLE KEYS */;
INSERT INTO `declaracion_beneficiosgratuitos` VALUES (1,'','','',200,'2019-12-12 19:07:56.051724','2020-02-19 17:38:43.401325',31,3,1,35,31,12,NULL,NULL,NULL,'Se ha especificado la especie','ESPECIE',NULL,NULL),(2,'','Gubernamental','',8596,'2019-12-24 19:26:46.334841','2019-12-24 19:26:46.334902',32,1,3,134,32,1,'FUEM010101569',NULL,'FISICA','Se ha especificado la especie','MONETARIO',NULL,NULL),(3,'','','',NULL,'2020-01-22 19:01:35.823732','2020-01-22 19:04:15.956000',NULL,NULL,9,221,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'','','',1000,'2020-02-10 20:04:28.685493','2020-02-10 20:05:35.396651',25,2,2,339,101,1,'FUEM010101569',NULL,'FISICA','SADF','MONETARIO',NULL,NULL),(5,'','','',50000,'2020-02-19 17:43:03.082764','2020-02-19 17:45:02.948402',34,3,10,374,101,12,NULL,NULL,NULL,'Se ha especificado el BENEFICIO','MONETARIO',NULL,164),(6,'','','',6320,'2020-02-19 17:47:27.845870','2020-02-19 17:48:41.217843',21,3,10,375,101,7,NULL,NULL,NULL,'Se de por situación de crisis','MONETARIO',NULL,165),(7,'','','',8956,'2020-02-25 18:05:10.573340','2020-02-25 18:05:10.573404',22,1,11,397,101,12,NULL,NULL,NULL,'Se ha especificado la especie','MONETARIO',NULL,182),(10,'','','',8900,'2020-03-06 22:47:25.055548','2020-03-06 22:47:25.055610',27,2,12,431,101,7,NULL,NULL,NULL,'Se ha especificado la especie E23','MONETARIO',1,193);
/*!40000 ALTER TABLE `declaracion_beneficiosgratuitos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_bienesinmuebles`
--

DROP TABLE IF EXISTS `declaracion_bienesinmuebles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_bienesinmuebles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `superficie_terreno` decimal(12,2) DEFAULT NULL,
  `superficie_construccion` decimal(12,2) DEFAULT NULL,
  `otro_titular` varchar(255) NOT NULL,
  `num_escritura_publica` varchar(255) NOT NULL,
  `num_registro_publico` varchar(255) NOT NULL,
  `folio_real` varchar(255) NOT NULL,
  `fecha_contrato_compra` date DEFAULT NULL,
  `otra_forma` varchar(255) NOT NULL,
  `fecha_adquisicion` date DEFAULT NULL,
  `precio_adquisicion` decimal(12,2) DEFAULT NULL,
  `valor_catastral` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otra_operacion` varchar(255) DEFAULT NULL,
  `otro_inmueble` varchar(255) DEFAULT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_formas_adquisiciones_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_tipos_inmuebles_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_titulares_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `domicilios_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `forma_pago` varchar(255) DEFAULT NULL,
  `valor_conforme_a` varchar(255) DEFAULT NULL,
  `cat_motivo_baja_id` int(11) DEFAULT NULL,
  `otro_motivo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_bienesin_activos_bienes_id_4ce7b103_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_bienesin_cat_formas_adquisici_0f66ea20_fk_declaraci` (`cat_formas_adquisiciones_id`),
  KEY `declaracion_bienesin_cat_monedas_id_74348361_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_bienesin_cat_tipos_inmuebles__e7713b8e_fk_declaraci` (`cat_tipos_inmuebles_id`),
  KEY `declaracion_bienesin_cat_tipos_operacione_5251e561_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_bienesin_cat_tipos_titulares__000810d0_fk_declaraci` (`cat_tipos_titulares_id`),
  KEY `declaracion_bienesin_declaraciones_id_d3bd4a00_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_bienesin_domicilios_id_50855758_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_bienesin_observaciones_id_8deaca1c_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_bienesin_cat_motivo_baja_id_71062e70_fk_declaraci` (`cat_motivo_baja_id`),
  CONSTRAINT `declaracion_bienesin_activos_bienes_id_4ce7b103_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_formas_adquisici_0f66ea20_fk_declaraci` FOREIGN KEY (`cat_formas_adquisiciones_id`) REFERENCES `declaracion_catformasadquisiciones` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_monedas_id_74348361_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_motivo_baja_id_71062e70_fk_declaraci` FOREIGN KEY (`cat_motivo_baja_id`) REFERENCES `declaracion_catmotivobaja` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_tipos_inmuebles__e7713b8e_fk_declaraci` FOREIGN KEY (`cat_tipos_inmuebles_id`) REFERENCES `declaracion_cattiposinmuebles` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_tipos_operacione_5251e561_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_tipos_titulares__000810d0_fk_declaraci` FOREIGN KEY (`cat_tipos_titulares_id`) REFERENCES `declaracion_cattipostitulares` (`id`),
  CONSTRAINT `declaracion_bienesin_declaraciones_id_d3bd4a00_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_bienesin_domicilios_id_50855758_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_bienesin_observaciones_id_8deaca1c_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_bienesinmuebles`
--

LOCK TABLES `declaracion_bienesinmuebles` WRITE;
/*!40000 ALTER TABLE `declaracion_bienesinmuebles` DISABLE KEYS */;
INSERT INTO `declaracion_bienesinmuebles` VALUES (1,120.00,520.00,'','','sdfcsdf','',NULL,'','2020-01-02',123.00,NULL,'2020-01-31 22:23:11.938070','2020-02-26 22:25:32.081326',NULL,NULL,3,3,101,2,2,6,1,93,272,'CREDITO',NULL,NULL,''),(2,230.00,52.00,'','25165464','654654564','2421425','2019-03-29','','2020-01-27',2560.00,5230.00,'2020-02-04 01:38:25.478067','2020-02-04 01:38:25.478131',NULL,NULL,5,3,101,3,3,2,9,103,285,NULL,NULL,NULL,''),(9,200.00,200.00,'','','esta es su identificación','',NULL,'','2018-02-18',1250.00,NULL,'2020-02-04 22:36:02.786474','2020-02-04 22:36:02.786499',NULL,NULL,8,6,101,6,4,4,10,127,312,'CONTADO','ESCRITURA PÚBLICA',NULL,''),(10,255.00,255.00,'','','AD44555','',NULL,'','2016-05-18',80000000.00,NULL,'2020-02-04 22:40:02.582741','2020-02-06 22:12:50.892923',NULL,NULL,11,6,101,5,4,4,2,128,314,'CRÉDITO','ESCRITURA PÚBLICA',NULL,''),(11,456.00,250.00,'','','011254555S','',NULL,'','2019-08-29',58993.00,NULL,'2020-02-25 18:11:51.068219','2020-02-25 23:03:21.948569',NULL,NULL,17,2,101,1,1,1,11,171,400,'CONTADO','ESCRITURA PÚBLICA',1,'');
/*!40000 ALTER TABLE `declaracion_bienesinmuebles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_bienesintangibles`
--

DROP TABLE IF EXISTS `declaracion_bienesintangibles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_bienesintangibles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `otra_dependencia` varchar(255) NOT NULL,
  `num_registro` varchar(255) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `otro_sector` varchar(255) NOT NULL,
  `precio_adquisicion` decimal(12,2) DEFAULT NULL,
  `otra_forma` varchar(255) NOT NULL,
  `fecha_vencimiento` date DEFAULT NULL,
  `precio_total_adquisicion` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otro_ente` varchar(255) DEFAULT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_entes_publicos_id` int(11) DEFAULT NULL,
  `cat_formas_adquisiciones_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `nombre_ente_publico` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_bienesin_activos_bienes_id_051668f2_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_bienesin_cat_entes_publicos_i_c0279e45_fk_declaraci` (`cat_entes_publicos_id`),
  KEY `declaracion_bienesin_cat_formas_adquisici_73dc58d4_fk_declaraci` (`cat_formas_adquisiciones_id`),
  KEY `declaracion_bienesin_cat_monedas_id_38b20714_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_bienesin_cat_sectores_industr_b97eaee2_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_bienesin_cat_tipos_operacione_eac4529a_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_bienesin_declaraciones_id_98c638c2_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_bienesin_observaciones_id_7af289f1_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_bienesin_activos_bienes_id_051668f2_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_entes_publicos_i_c0279e45_fk_declaraci` FOREIGN KEY (`cat_entes_publicos_id`) REFERENCES `declaracion_catentespublicos` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_formas_adquisici_73dc58d4_fk_declaraci` FOREIGN KEY (`cat_formas_adquisiciones_id`) REFERENCES `declaracion_catformasadquisiciones` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_monedas_id_38b20714_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_sectores_industr_b97eaee2_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_tipos_operacione_eac4529a_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_bienesin_declaraciones_id_98c638c2_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_bienesin_observaciones_id_7af289f1_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_bienesintangibles`
--

LOCK TABLES `declaracion_bienesintangibles` WRITE;
/*!40000 ALTER TABLE `declaracion_bienesintangibles` DISABLE KEYS */;
/*!40000 ALTER TABLE `declaracion_bienesintangibles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_bienesmuebles`
--

DROP TABLE IF EXISTS `declaracion_bienesmuebles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_bienesmuebles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otro_tipo_mueble` varchar(255) NOT NULL,
  `otro_titular` varchar(255) NOT NULL,
  `otra_forma` varchar(255) NOT NULL,
  `fecha_adquisicion` date DEFAULT NULL,
  `precio_adquisicion` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  `cat_formas_adquisiciones_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipos_muebles_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_titulares_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `forma_pago` varchar(255) NOT NULL,
  `cat_motivo_baja_id` int(11) DEFAULT NULL,
  `descripcion_bien` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_bienesmu_activos_bienes_id_d26b3ee2_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_bienesmu_cat_entidades_federa_eb2a8206_fk_declaraci` (`cat_entidades_federativas_id`),
  KEY `declaracion_bienesmu_cat_formas_adquisici_fcd98d03_fk_declaraci` (`cat_formas_adquisiciones_id`),
  KEY `declaracion_bienesmu_cat_monedas_id_139584b5_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_bienesmu_cat_paises_id_3a63262a_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_bienesmu_cat_tipos_muebles_id_80b2d144_fk_declaraci` (`cat_tipos_muebles_id`),
  KEY `declaracion_bienesmu_cat_tipos_operacione_716f92b4_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_bienesmu_cat_tipos_titulares__8d6bb54f_fk_declaraci` (`cat_tipos_titulares_id`),
  KEY `declaracion_bienesmu_declaraciones_id_4ca011d8_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_bienesmu_observaciones_id_be05200e_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_bienesmu_cat_motivo_baja_id_edfecb9a_fk_declaraci` (`cat_motivo_baja_id`),
  CONSTRAINT `declaracion_bienesmu_activos_bienes_id_d26b3ee2_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_entidades_federa_eb2a8206_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_formas_adquisici_fcd98d03_fk_declaraci` FOREIGN KEY (`cat_formas_adquisiciones_id`) REFERENCES `declaracion_catformasadquisiciones` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_monedas_id_139584b5_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_motivo_baja_id_edfecb9a_fk_declaraci` FOREIGN KEY (`cat_motivo_baja_id`) REFERENCES `declaracion_catmotivobaja` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_paises_id_3a63262a_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_tipos_muebles_id_80b2d144_fk_declaraci` FOREIGN KEY (`cat_tipos_muebles_id`) REFERENCES `declaracion_cattiposmuebles` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_tipos_operacione_716f92b4_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_tipos_titulares__8d6bb54f_fk_declaraci` FOREIGN KEY (`cat_tipos_titulares_id`) REFERENCES `declaracion_cattipostitulares` (`id`),
  CONSTRAINT `declaracion_bienesmu_declaraciones_id_4ca011d8_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_bienesmu_observaciones_id_be05200e_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_bienesmuebles`
--

LOCK TABLES `declaracion_bienesmuebles` WRITE;
/*!40000 ALTER TABLE `declaracion_bienesmuebles` DISABLE KEYS */;
INSERT INTO `declaracion_bienesmuebles` VALUES (1,'','','','','2019-12-31',1000.00,'2020-01-31 22:05:00.386169','2020-01-31 22:05:00.386220',2,NULL,1,101,NULL,5,1,1,1,270,'',NULL,''),(2,'','','','','2020-01-02',5000.00,'2020-01-31 22:11:50.100502','2020-01-31 22:33:23.973175',2,NULL,3,101,NULL,5,3,2,1,271,'',NULL,''),(3,'','','','','2017-04-18',1233.00,'2020-02-07 03:13:09.445763','2020-02-07 03:13:09.445834',13,NULL,3,101,NULL,5,3,2,2,323,'CRÉDITO',3,''),(4,'','','','','2018-03-06',NULL,'2020-02-18 22:54:26.431989','2020-02-18 22:54:26.432051',15,NULL,NULL,NULL,NULL,5,3,2,11,369,'NO APLICA',2,'Esta es una descripción del bien'),(5,'','','','','1911-05-02',5801.00,'2020-02-21 22:11:27.213806','2020-02-21 22:11:27.213860',16,NULL,2,101,NULL,6,2,2,10,383,'CREDITO',2,'Esta es una descripción del bien');
/*!40000 ALTER TABLE `declaracion_bienesmuebles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_bienespersonas`
--

DROP TABLE IF EXISTS `declaracion_bienespersonas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_bienespersonas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `porcentaje` decimal(5,2) DEFAULT NULL,
  `es_propietario` tinyint(1) DEFAULT NULL,
  `precio_adquision` decimal(13,2) DEFAULT NULL,
  `el_adquirio` tinyint(1) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otra_relacion` varchar(255) DEFAULT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_tipo_participacion_id` int(11) NOT NULL,
  `otra_persona_id` int(11) DEFAULT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  `tipo_relacion_id` int(11) DEFAULT NULL,
  `otra_relacion_familiar` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_bienespe_tipo_relacion_id_5daea845_fk_declaraci` (`tipo_relacion_id`),
  KEY `declaracion_bienespe_cat_tipo_participaci_51722f94_fk_declaraci` (`cat_tipo_participacion_id`),
  KEY `declaracion_bienespe_activos_bienes_id_50697569_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_bienespe_info_personal_var_id_5e966265_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_bienespe_otra_persona_id_6a48ced9_fk_declaraci` (`otra_persona_id`),
  CONSTRAINT `declaracion_bienespe_activos_bienes_id_50697569_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_bienespe_cat_tipo_participaci_51722f94_fk_declaraci` FOREIGN KEY (`cat_tipo_participacion_id`) REFERENCES `declaracion_cattipoparticipacion` (`id`),
  CONSTRAINT `declaracion_bienespe_info_personal_var_id_5e966265_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_bienespe_otra_persona_id_6a48ced9_fk_declaraci` FOREIGN KEY (`otra_persona_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_bienespe_tipo_relacion_id_5daea845_fk_declaraci` FOREIGN KEY (`tipo_relacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_bienespersonas`
--

LOCK TABLES `declaracion_bienespersonas` WRITE;
/*!40000 ALTER TABLE `declaracion_bienespersonas` DISABLE KEYS */;
INSERT INTO `declaracion_bienespersonas` VALUES (1,NULL,NULL,NULL,NULL,'2019-12-31 19:16:53.176150','2020-02-26 22:26:03.716297',NULL,1,7,NULL,1,15,''),(2,NULL,NULL,NULL,NULL,'2019-12-31 19:16:53.216707','2020-02-26 22:26:03.726349',NULL,1,9,76,1,NULL,''),(3,NULL,NULL,NULL,NULL,'2019-12-31 19:16:53.248904','2020-02-26 22:26:03.734295',NULL,1,8,75,1,NULL,''),(4,10.00,NULL,NULL,NULL,'2020-01-31 22:05:00.416858','2020-01-31 22:33:23.979383',NULL,2,7,NULL,1,15,''),(5,NULL,NULL,NULL,NULL,'2020-01-31 22:05:00.468950','2020-01-31 22:33:23.986513',NULL,2,9,91,1,NULL,''),(6,NULL,NULL,NULL,NULL,'2020-01-31 22:05:00.502330','2020-01-31 22:33:23.996921',NULL,2,8,92,1,NULL,''),(7,100.00,NULL,NULL,NULL,'2020-01-31 22:11:50.117104','2020-01-31 22:11:50.117141',NULL,2,7,NULL,1,10,''),(8,NULL,NULL,NULL,NULL,'2020-01-31 22:11:50.129122','2020-01-31 22:11:50.129173',NULL,2,9,93,1,NULL,''),(9,NULL,NULL,NULL,NULL,'2020-01-31 22:11:50.138117','2020-01-31 22:11:50.138153',NULL,2,8,94,1,NULL,''),(10,100.00,NULL,NULL,NULL,'2020-01-31 22:23:11.991628','2020-02-26 22:25:32.095092',NULL,3,7,NULL,1,NULL,''),(11,100.00,NULL,NULL,NULL,'2020-01-31 22:23:11.995447','2020-02-26 22:25:32.104070',NULL,3,9,96,1,NULL,''),(12,100.00,NULL,NULL,NULL,'2020-01-31 22:23:12.004890','2020-02-26 22:25:32.114133',NULL,3,8,95,1,NULL,''),(13,NULL,NULL,NULL,NULL,'2020-02-04 00:25:22.398037','2020-02-04 00:27:25.142648',NULL,4,7,NULL,1,NULL,''),(14,NULL,NULL,NULL,NULL,'2020-02-04 00:25:22.405408','2020-02-04 00:27:25.153195',NULL,4,4,99,1,NULL,''),(15,NULL,NULL,NULL,NULL,'2020-02-04 00:25:22.413806','2020-02-04 00:27:25.166248',NULL,4,3,100,1,NULL,''),(16,NULL,NULL,NULL,NULL,'2020-02-04 00:25:22.429475','2020-02-04 00:27:25.180382',NULL,4,5,101,1,NULL,''),(17,100.00,NULL,NULL,NULL,'2020-02-04 01:38:25.519345','2020-02-04 01:38:25.519382',NULL,5,7,NULL,82,17,''),(18,NULL,NULL,NULL,NULL,'2020-02-04 01:38:25.527958','2020-02-04 01:38:25.527997',NULL,5,9,105,82,NULL,''),(19,NULL,NULL,NULL,NULL,'2020-02-04 01:38:25.538412','2020-02-04 01:38:25.538451',NULL,5,8,104,82,NULL,''),(20,NULL,NULL,NULL,NULL,'2020-02-04 01:55:39.797860','2020-02-04 02:48:21.174508',NULL,6,7,NULL,82,9,''),(21,NULL,NULL,NULL,NULL,'2020-02-04 01:55:39.804807','2020-02-04 02:48:21.189958',NULL,6,9,107,82,NULL,''),(22,NULL,NULL,NULL,NULL,'2020-02-04 01:55:39.943257','2020-02-04 02:48:21.198392',NULL,6,8,106,82,NULL,''),(23,100.00,NULL,NULL,NULL,'2020-02-04 01:57:01.232618','2020-02-04 01:57:01.232649',NULL,6,7,NULL,82,17,''),(24,NULL,NULL,NULL,NULL,'2020-02-04 01:57:01.243003','2020-02-04 01:57:01.243038',NULL,6,9,109,82,NULL,''),(25,NULL,NULL,NULL,NULL,'2020-02-04 01:57:01.249823','2020-02-04 01:57:01.249856',NULL,6,8,108,82,NULL,''),(26,NULL,NULL,NULL,NULL,'2020-02-04 14:29:29.754404','2020-02-04 15:05:24.081476',NULL,7,7,NULL,82,NULL,''),(27,NULL,NULL,NULL,NULL,'2020-02-04 14:29:29.761602','2020-02-04 15:05:24.122390',NULL,7,4,112,82,NULL,''),(28,NULL,NULL,NULL,NULL,'2020-02-04 14:29:29.770568','2020-02-04 15:05:24.148652',NULL,7,3,113,82,NULL,''),(29,NULL,NULL,NULL,NULL,'2020-02-04 14:29:29.775542','2020-02-04 15:05:24.186004',NULL,7,5,114,82,NULL,''),(56,85.00,NULL,NULL,NULL,'2020-02-04 22:40:02.714325','2020-02-06 22:12:51.121173',NULL,11,7,NULL,54,11,''),(57,100.00,NULL,NULL,NULL,'2020-02-04 22:40:02.722437','2020-02-06 22:12:51.206217',NULL,11,9,137,54,NULL,''),(58,100.00,NULL,NULL,NULL,'2020-02-04 22:40:02.729169','2020-02-06 22:12:51.246348',NULL,11,8,136,54,NULL,''),(59,NULL,NULL,NULL,NULL,'2020-02-06 23:01:37.685340','2020-02-06 23:01:37.685405',NULL,12,7,NULL,54,17,''),(60,NULL,NULL,NULL,NULL,'2020-02-06 23:01:37.714895','2020-02-06 23:01:37.714962',NULL,12,9,140,54,NULL,''),(61,NULL,NULL,NULL,NULL,'2020-02-06 23:01:37.730754','2020-02-06 23:01:37.730823',NULL,12,8,139,54,NULL,''),(62,NULL,NULL,NULL,NULL,'2020-02-07 03:13:09.460868','2020-02-07 03:13:09.460947',NULL,13,7,NULL,54,14,''),(63,NULL,NULL,NULL,NULL,'2020-02-07 03:13:09.491595','2020-02-07 03:13:09.491648',NULL,13,9,141,54,NULL,''),(64,NULL,NULL,NULL,NULL,'2020-02-07 03:13:09.499986','2020-02-07 03:13:09.500044',NULL,13,8,142,54,NULL,''),(65,NULL,NULL,NULL,NULL,'2020-02-10 20:07:35.956828','2020-02-10 22:46:22.430321',NULL,14,7,NULL,54,NULL,''),(66,NULL,NULL,NULL,NULL,'2020-02-10 20:07:35.963450','2020-02-10 22:46:22.439790',NULL,14,4,152,54,NULL,''),(67,NULL,NULL,NULL,NULL,'2020-02-10 20:07:35.971044','2020-02-10 22:46:22.459789',NULL,14,3,153,54,NULL,''),(68,NULL,NULL,NULL,NULL,'2020-02-10 20:07:35.976889','2020-02-10 22:46:22.474738',NULL,14,5,154,54,NULL,''),(69,NULL,NULL,NULL,NULL,'2020-02-18 22:54:26.518834','2020-02-18 22:54:26.518905',NULL,15,7,NULL,155,NULL,''),(70,NULL,NULL,NULL,NULL,'2020-02-18 22:54:26.602975','2020-02-18 22:54:26.603047',NULL,15,9,159,155,NULL,''),(71,NULL,NULL,NULL,NULL,'2020-02-18 22:54:26.623009','2020-02-18 22:54:26.623079',NULL,15,8,160,155,NULL,''),(75,NULL,NULL,NULL,NULL,'2020-02-21 17:44:38.626854','2020-02-21 19:33:53.454684',NULL,9,4,169,84,NULL,''),(76,NULL,NULL,NULL,NULL,'2020-02-21 17:44:38.638586','2020-02-21 19:33:53.459500',NULL,9,3,170,84,NULL,''),(77,NULL,NULL,NULL,NULL,'2020-02-21 17:44:38.643899','2020-02-21 19:33:53.463968',NULL,9,5,171,84,NULL,''),(78,NULL,NULL,NULL,NULL,'2020-02-21 19:33:53.434319','2020-02-21 19:33:53.434349',NULL,9,7,NULL,84,NULL,''),(79,NULL,NULL,NULL,NULL,'2020-02-21 22:11:27.224303','2020-02-21 22:11:27.224337',NULL,16,7,NULL,84,6,''),(80,NULL,NULL,NULL,NULL,'2020-02-21 22:11:27.228930','2020-02-21 22:11:27.228961',NULL,16,9,172,84,NULL,''),(81,NULL,NULL,NULL,NULL,'2020-02-21 22:11:27.240325','2020-02-21 22:11:27.240355',NULL,16,8,173,84,NULL,''),(82,100.00,NULL,NULL,NULL,'2020-02-25 18:11:51.165806','2020-02-25 23:03:22.125108',NULL,17,7,NULL,155,6,''),(83,100.00,NULL,NULL,NULL,'2020-02-25 18:11:51.229626','2020-02-25 23:03:22.154958',NULL,17,9,184,155,NULL,''),(84,100.00,NULL,NULL,NULL,'2020-02-25 18:11:51.244993','2020-02-25 23:03:22.160350',NULL,17,8,183,155,NULL,'');
/*!40000 ALTER TABLE `declaracion_bienespersonas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catactivobien`
--

DROP TABLE IF EXISTS `declaracion_catactivobien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catactivobien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activo_bien` varchar(45) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catactivobien`
--

LOCK TABLES `declaracion_catactivobien` WRITE;
/*!40000 ALTER TABLE `declaracion_catactivobien` DISABLE KEYS */;
INSERT INTO `declaracion_catactivobien` VALUES (1,'bienes_inmubles','2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',0,1,''),(2,'bienes_intangibles','2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',0,1,''),(3,'bienes_muebles','2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',0,1,''),(4,'muebles_no_registrables','2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',0,1,''),(5,'Fideicomiso','2019-04-04 04:34:58.000000','2019-04-04 04:34:58.000000',0,1,''),(6,'cuentas_por_cobrar','2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',0,1,'');
/*!40000 ALTER TABLE `declaracion_catactivobien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catambitoslaborales`
--

DROP TABLE IF EXISTS `declaracion_catambitoslaborales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catambitoslaborales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ambito_laboral` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catambitoslaborales`
--

LOCK TABLES `declaracion_catambitoslaborales` WRITE;
/*!40000 ALTER TABLE `declaracion_catambitoslaborales` DISABLE KEYS */;
INSERT INTO `declaracion_catambitoslaborales` VALUES (1,'Público','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'PUB'),(2,'Privado','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'PRV'),(3,'Ninguno','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'NIN'),(4,'Otro','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',1,1,'OTR');
/*!40000 ALTER TABLE `declaracion_catambitoslaborales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catcamposobligatorios`
--

DROP TABLE IF EXISTS `declaracion_catcamposobligatorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catcamposobligatorios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_tabla` varchar(255) DEFAULT NULL,
  `nombre_columna` varchar(255) DEFAULT NULL,
  `es_obligatorio` int(11) DEFAULT NULL,
  `seccion_id` int(11) DEFAULT NULL,
  `es_principal` tinyint(1) NOT NULL,
  `tipo` int(11) NOT NULL,
  `esta_pantalla` tinyint(1) NOT NULL,
  `es_privado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_catcampo_seccion_id_2408ef6d_fk_declaraci` (`seccion_id`),
  CONSTRAINT `declaracion_catcampo_seccion_id_2408ef6d_fk_declaraci` FOREIGN KEY (`seccion_id`) REFERENCES `declaracion_secciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=897 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catcamposobligatorios`
--

LOCK TABLES `declaracion_catcamposobligatorios` WRITE;
/*!40000 ALTER TABLE `declaracion_catcamposobligatorios` DISABLE KEYS */;
INSERT INTO `declaracion_catcamposobligatorios` VALUES (129,'declaracion_infopersonalfija','nombres',1,2,1,0,1,0),(130,'declaracion_infopersonalfija','apellido1',1,2,1,0,1,0),(131,'declaracion_infopersonalfija','apellido2',0,2,1,0,1,0),(132,'declaracion_infopersonalfija','homoclave',1,2,0,0,1,1),(133,'declaracion_infopersonalfija','curp',0,2,1,0,1,1),(134,'declaracion_infopersonalfija','rfc',1,2,1,0,1,1),(135,'declaracion_infopersonalvar','email_personal',0,2,1,0,1,1),(136,'declaracion_infopersonalvar','tel_particular',0,2,1,0,1,1),(137,'declaracion_infopersonalvar','tel_movil',0,2,1,0,1,1),(138,'declaracion_infopersonalvar','cat_estados_civiles_id',0,2,1,0,1,1),(139,'declaracion_infopersonalvar','cat_regimenes_matrimoniales',0,2,1,0,1,1),(140,'declaracion_infopersonalvar','observaciones',0,2,1,0,1,1),(141,'declaracion_infopersonalvar','nacionalidades',0,2,1,0,1,1),(142,'declaracion_infopersonalvar','cat_pais_id',0,2,1,0,1,1),(202,'declaracion_domicilios','nombre_via',1,3,0,0,1,1),(203,'declaracion_domicilios','cat_entidades_federativas',0,3,1,0,1,1),(204,'declaracion_domicilios','num_exterior',1,3,0,0,1,1),(205,'declaracion_domicilios','num_interior',0,3,0,0,1,1),(206,'declaracion_domicilios','colonia',0,3,0,0,1,1),(207,'declaracion_domicilios','municipio',0,3,0,0,1,1),(208,'declaracion_domicilios','cp',0,3,0,0,1,1),(209,'declaracion_domicilios','observaciones',0,3,1,0,0,1),(225,'declaracion_datoscurriculares','cat_grados_academicos',1,4,1,0,1,0),(226,'declaracion_datoscurriculares','institucion_educativa',1,4,1,0,1,0),(227,'declaracion_datoscurriculares','cat_estatus_estudios_id',1,4,1,0,1,0),(228,'declaracion_datoscurriculares','observaciones',0,4,0,0,1,1),(250,'declaracion_encargos','cat_poderes',1,5,1,0,1,0),(251,'declaracion_encargos','cat_puestos',1,5,1,0,1,0),(252,'declaracion_encargos','nombre_ente_publico',1,5,1,0,1,0),(253,'declaracion_encargos','observaciones',0,5,1,0,1,1),(278,'declaracion_experiencialaboral','nombre_institucion',1,6,1,0,1,0),(279,'declaracion_experiencialaboral','unidad_area_administrativa',1,6,1,0,1,0),(280,'declaracion_experiencialaboral','rfc',1,6,1,0,1,0),(281,'declaracion_experiencialaboral','observaciones',0,6,1,0,1,1),(299,'declaracion_conyugedependientes','cat_tipos_relaciones_personales',0,7,1,0,1,1),(300,'declaracion_infopersonalvar','nombres',1,7,0,0,1,1),(301,'declaracion_infopersonalvar','apellido1',1,7,0,0,1,1),(302,'declaracion_infopersonalvar','apellido2',0,7,0,0,1,1),(303,'declaracion_infopersonalvar','fecha_nacimiento',0,7,0,0,1,1),(304,'declaracion_infopersonalvar','rfc',1,7,1,0,1,1),(305,'declaracion_infopersonalvar','curp',1,7,0,0,1,1),(306,'declaracion_conyugedependientes','habita_domicilio',0,7,1,0,1,1),(307,'declaracion_infopersonalvar','nacionalidad',0,7,0,0,0,1),(308,'declaracion_domicilios','cat_pais_id',0,7,0,0,1,1),(309,'declaracion_domicilios','nombre_via',0,7,0,0,1,1),(310,'declaracion_infopersonalvar','cat_entidades_federativas_id',0,7,1,0,1,1),(311,'declaracion_domicilios','num_exterior',0,7,0,0,1,1),(312,'declaracion_domicilios','num_interior',0,7,0,0,1,1),(313,'declaracion_domicilios','colonia',0,7,0,0,1,1),(314,'declaracion_domicilios','municipio',0,7,0,0,1,1),(315,'declaracion_domicilios','cp',0,7,0,0,1,1),(316,'declaracion_conyugedependientes','ingresos_propios',0,7,1,0,1,1),(317,'declaracion_infopersonalvar','ocupacion_girocomercial',0,7,0,0,1,1),(318,'declaracion_infopersonalvar','cat_sectores_industria_id',0,7,0,0,1,1),(319,'declaracion_encargos','actividadLaboral',0,7,0,0,1,1),(320,'declaracion_encargos','cat_ordenes_gobierno',0,7,0,0,1,1),(321,'declaracion_encargos','cat_poderes',0,7,0,0,1,1),(322,'declaracion_encargos','empleo_cargo_comision',0,7,0,0,1,1),(323,'declaracion_encargos','cat_funciones_principales',0,7,0,0,1,1),(324,'declaracion_encargos','salarioMensualNeto',0,7,0,0,1,1),(325,'declaracion_encargos','moneda',0,7,0,0,1,1),(326,'declaracion_encargos','posesion_inicio',0,7,0,0,1,1),(327,'declaracion_encargos','nombreEmpresaSociedadAsociacion',0,7,0,0,1,1),(328,'declaracion_encargos','rfc',0,7,0,0,1,1),(329,'declaracion_encargos','proveedor_contratista',0,7,0,0,1,1),(330,'declaracion_conyugedependientes','es_extranjero',0,7,0,0,1,1),(331,'declaracion_encargos','nombre_ente_publico',0,7,0,0,1,1),(332,'declaracion_encargos','area_adscripcion',0,7,0,0,1,1),(333,'declaracion_conyugedependientes','cat_tipos_relaciones_personales',0,8,1,0,1,1),(334,'declaracion_infopersonalvar','nombres',1,8,0,0,1,1),(335,'declaracion_infopersonalvar','apellido1',1,8,0,0,1,1),(336,'declaracion_infopersonalvar','apellido2',0,8,0,0,1,1),(337,'declaracion_infopersonalvar','fecha_nacimiento',0,8,0,0,1,1),(338,'declaracion_infopersonalvar','rfc',1,8,1,0,1,1),(339,'declaracion_infopersonalvar','curp',1,8,0,0,1,1),(340,'declaracion_conyugedependientes','habita_domicilio',0,8,1,0,1,1),(341,'declaracion_domicilios','cat_pais_id',0,8,0,0,1,1),(342,'declaracion_domicilios','nombre_via',0,8,0,0,1,1),(343,'declaracion_infopersonalvar','cat_entidades_federativas_id',0,8,1,0,1,1),(344,'declaracion_domicilios','num_exterior',0,8,0,0,1,1),(345,'declaracion_domicilios','num_interior',0,8,0,0,1,1),(346,'declaracion_domicilios','colonia',0,8,0,0,1,1),(347,'declaracion_domicilios','municipio',0,8,0,0,1,1),(348,'declaracion_domicilios','cp',0,8,0,0,1,1),(349,'declaracion_conyugedependientes','ingresos_propios',0,8,1,0,1,1),(350,'declaracion_infopersonalvar','ocupacion_girocomercial',0,8,0,0,1,1),(351,'declaracion_infopersonalvar','cat_sectores_industria_id',0,8,0,0,1,1),(352,'declaracion_encargos','actividadLaboral',0,8,0,0,1,1),(353,'declaracion_encargos','cat_ordenes_gobierno',0,8,0,0,1,1),(354,'declaracion_encargos','cat_poderes',0,8,0,0,1,1),(355,'declaracion_encargos','empleo_cargo_comision',0,8,0,0,1,1),(356,'declaracion_encargos','cat_funciones_principales',0,8,0,0,1,1),(357,'declaracion_encargos','salarioMensualNeto',0,8,0,0,1,1),(358,'declaracion_encargos','moneda',0,8,0,0,1,1),(359,'declaracion_encargos','posesion_inicio',0,8,0,0,1,1),(360,'declaracion_encargos','nombreEmpresaSociedadAsociacion',0,8,0,0,1,1),(361,'declaracion_encargos','rfc',0,8,0,0,1,1),(362,'declaracion_encargos','proveedor_contratista',0,8,0,0,1,1),(363,'declaracion_conyugedependientes','es_extranjero',0,8,0,0,1,1),(364,'declaracion_encargos','nombre_ente_publico',0,8,0,0,1,1),(365,'declaracion_encargos','area_adscripcion',0,8,0,0,1,1),(366,'declaracion_encargos','observaciones',0,8,0,0,1,1),(452,'declaracion_bienesinmuebles','cat_tipos_inmuebles_id',0,11,1,0,1,0),(453,'declaracion_bienespersonas','tipo_relacion',0,11,0,7,1,1),(454,'declaracion_bienesinmuebles','cat_tipos_titulares_id',0,11,1,0,1,0),(455,'declaracion_infopersonalvar','razon_social',0,11,0,12,1,1),(456,'declaracion_infopersonalvar','rfc',0,11,0,12,1,1),(457,'declaracion_domicilios','nombre_via',0,11,0,0,1,1),(458,'declaracion_domicilios','cat_entidades_federativas',0,11,1,0,1,1),(459,'declaracion_domicilios','num_exterior',0,11,0,0,1,1),(460,'declaracion_domicilios','num_interior',0,11,0,0,1,1),(461,'declaracion_domicilios','colonia',0,11,0,0,1,1),(462,'declaracion_domicilios','municipio',0,11,0,0,1,1),(463,'declaracion_domicilios','cp',0,11,0,0,1,1),(464,'declaracion_bienesinmuebles','observaciones',0,11,1,0,1,1),(465,'declaracion_bienesinmuebles','forma_pago',1,11,1,0,1,0),(467,'declaracion_bienesinmuebles','fecha_adquisicion',1,11,1,0,1,0),(468,'declaracion_bienesinmuebles','cat_tipos_titulares',0,11,1,0,1,1),(469,'declaracion_bienesinmuebles','num_registro_publico',1,11,1,0,1,1),(470,'declaracion_infopersonalvar','nombres',0,11,1,0,1,1),(471,'declaracion_infopersonalvar','apellido1',0,11,1,0,1,1),(474,'declaracion_bienesmuebles','cat_tipos_titulares_id',0,12,1,0,1,1),(475,'declaracion_infopersonalvar','nombres',0,12,0,12,1,1),(476,'declaracion_infopersonalvar','apellido1',0,12,0,12,1,1),(477,'declaracion_infopersonalvar','razon_social',0,12,0,12,1,1),(478,'declaracion_infopersonalvar','rfc',0,12,0,12,1,1),(479,'declaracion_bienespersonas','tipo_relacion_id',0,12,0,7,1,1),(480,'declaracion_bienesmuebles','marca',1,12,1,0,1,0),(481,'declaracion_bienesmuebles','modelo',1,12,1,0,1,0),(482,'declaracion_bienesmuebles','anio',1,12,1,0,1,0),(483,'declaracion_bienesmuebles','num_serie',0,12,1,0,1,1),(484,'declaracion_bienesmuebles','forma_pago',1,12,1,0,1,0),(486,'declaracion_bienesmuebles','fecha_adquisicion',1,12,1,0,1,0),(487,'declaracion_bienesmuebles','observaciones',0,12,1,0,1,1),(488,'declaracion_infopersonalvar','apellido1',0,12,1,0,1,1),(526,'declaracion_bienesmuebles','cat_tipos_titulares_id',0,13,1,0,1,1),(527,'declaracion_bienespersonas','tipo_relacion_id',0,13,0,7,1,1),(528,'declaracion_bienesmuebles','precio_adquisicion',1,13,1,0,1,0),(529,'declaracion_bienesmuebles','fecha_adquisicion',1,13,1,0,1,0),(530,'declaracion_bienesmuebles','cat_monedas_id',1,13,1,0,1,0),(531,'declaracion_bienesmuebles','observaciones',0,13,1,0,1,1),(532,'declaracion_bienesmuebles','forma_pago',1,13,1,0,1,0),(575,'declaracion_inversiones','cat_tipos_titulares',1,14,1,0,1,1),(576,'declaracion_inversiones','saldo_actual',0,14,1,0,1,1),(577,'declaracion_inversiones','cat_monedas',0,14,1,0,1,1),(578,'declaracion_infopersonalvar','razon_social',0,14,0,0,1,1),(579,'declaracion_infopersonalvar','rfc',0,14,0,0,1,1),(580,'declaracion_inversiones','num_cuenta',0,14,1,0,1,1),(581,'declaracion_inversiones','cat_monedas_id',0,14,1,0,1,1),(582,'declaracion_inversiones','observaciones',0,14,0,0,1,1),(583,'declaracion_infopersonalvar','nombres',0,14,0,0,1,1),(584,'declaracion_infopersonalvar','apellido1',0,14,0,0,1,1),(600,'declaracion_deudasotros','cat_tipos_titulares',0,15,1,0,1,1),(601,'declaracion_deudasotros','numero_cuenta',1,15,1,0,1,1),(602,'declaracion_deudasotros','monto_original',1,15,1,0,1,0),(603,'declaracion_deudasotros','cat_monedas_id',1,15,1,0,1,0),(604,'declaracion_deudasotros','saldo_pendiente',1,15,1,0,1,1),(605,'declaracion_deudasotros','observaciones',0,15,1,0,1,1),(606,'declaracion_infopersonalvar','es_fisica',0,15,0,0,1,1),(607,'declaracion_infopersonalvar','rfc',0,15,0,0,1,1),(608,'declaracion_infopersonalvar','acreedor_nombre',0,15,0,0,1,1),(609,'declaracion_infopersonalvar','acreedor_rfc',0,15,0,0,1,1),(610,'declaracion_infopersonalvar','nombres',0,15,0,0,1,1),(611,'declaracion_infopersonalvar','apellido1',0,15,0,0,1,1),(633,'declaracion_infopersonalvar','docimilios',1,NULL,1,0,0,0),(649,'declaracion_deudasotros','cat_tipos_inmueble_id',1,16,1,0,1,0),(650,'declaracion_deudasotros','observaciones',0,16,1,0,1,0),(651,'declaracion_deudasotros','mueble_num_serie',0,16,1,0,1,1),(652,'declaracion_domicilios','nombre_via',1,16,0,0,1,1),(653,'declaracion_domicilios','cat_entidades_federativas',1,16,1,0,1,1),(654,'declaracion_domicilios','num_exterior',1,16,0,0,1,1),(655,'declaracion_domicilios','num_interior',0,16,0,0,1,1),(656,'declaracion_domicilios','colonia',0,16,0,0,1,1),(657,'declaracion_domicilios','municipio',0,16,0,0,1,1),(658,'declaracion_domicilios','cp',0,16,0,0,1,1),(659,'declaracion_infopersonalvar','nombres',0,16,0,0,1,1),(660,'declaracion_infopersonalvar','apellido1',0,16,0,0,1,1),(661,'declaracion_infopersonalvar','rfc',0,16,0,0,1,1),(662,'declaracion_infopersonalvar','razon_social',0,16,0,0,1,1),(700,'declaracion_socioscomerciales','actividad_vinculante',1,18,1,0,1,0),(701,'declaracion_socioscomerciales','rfc_entidad_vinculante',1,18,1,0,1,0),(702,'declaracion_socioscomerciales','porcentaje_participacion',1,18,1,0,1,0),(703,'declaracion_socioscomerciales','observaciones',0,18,1,0,1,1),(725,'declaracion_membresias','nombre_institucion',1,19,1,0,1,1),(726,'declaracion_membresias','puesto_rol',1,19,1,0,1,0),(727,'declaracion_membresias','rfc',1,19,1,0,1,1),(728,'declaracion_membresias','observaciones',0,19,1,0,1,1),(750,'declaracion_representaciones','cat_tipos_relaciones_personales_id',0,20,1,0,1,1),(751,'declaracion_representaciones','nombre_programa',1,20,1,0,1,0),(752,'declaracion_representaciones','institucion_otorgante',1,20,1,0,1,0),(753,'declaracion_representaciones','observaciones',0,20,1,0,1,1),(760,'declaracion_apoyos','cat_tipos_relaciones_personales_id',1,21,1,0,1,0),(761,'declaracion_apoyos','cat_tipos_representaciones',1,21,1,0,1,1),(762,'declaracion_apoyos','rfc',1,21,1,0,1,1),(763,'declaracion_apoyos','nombres',0,21,1,0,1,1),(764,'declaracion_apoyos','apellido1',0,21,1,0,1,1),(765,'declaracion_apoyos','razon_social',0,21,1,0,1,1),(766,'declaracion_apoyos','observaciones',0,21,1,0,1,1),(775,'declaracion_infopersonalvar','nombre_negocio',1,22,0,0,1,0),(776,'declaracion_infopersonalvar','rfc_negocio',1,22,0,0,1,1),(778,'declaracion_infopersonalvar','nombres',0,22,0,0,1,1),(779,'declaracion_infopersonalvar','apellido1',0,22,0,0,1,1),(780,'declaracion_infopersonalvar','razon_social',0,22,0,0,1,1),(781,'declaracion_clientesprincipales','observaciones',0,22,1,0,1,1),(782,'declaracion_clientesprincipales','monto',1,22,1,0,1,0),(800,'declaracion_beneficiosgratuitos','cat_tipos_relaciones_personales',0,23,1,0,1,1),(801,'declaracion_infopersonalvar','nombres',0,23,1,0,1,1),(802,'declaracion_infopersonalvar','apellido1',0,23,1,0,1,1),(803,'declaracion_infopersonalvar','razon_social',0,23,1,0,1,1),(804,'declaracion_infopersonalvar','rfc',0,23,1,0,1,1),(805,'declaracion_beneficiosgratuitos','valor_beneficio',1,23,1,0,1,0),(806,'declaracion_beneficiosgratuitos','moneda',1,23,1,0,1,0),(807,'declaracion_beneficiosgratuitos','observaciones',0,23,1,0,1,1),(824,'declaracion_infopersonalvar','rfc_fideicomisario',0,24,1,0,1,1),(825,'declaracion_infopersonalvar','rfc_fideicomitente',1,24,1,0,1,1),(826,'declaracion_infopersonalvar','fideicomitente',1,24,1,0,1,1),(827,'declaracion_infopersonalvar','fideicomisario',0,24,1,0,1,1),(828,'declaracion_fideicomisos','observaciones',0,24,1,0,1,1),(834,'declaracion_declaracionfiscal','archivo_pdf',1,26,1,0,1,0),(871,'declaracion_ingresosdeclaracion','ingreso_mensual_cargo',1,9,1,0,1,0),(872,'declaracion_ingresosdeclaracion','ingreso_mensual_neto',1,9,1,0,1,0),(873,'declaracion_ingresosdeclaracion','ingreso_mensual_total',1,9,1,0,1,0),(874,'declaracion_ingresosdeclaracion','ingreso_mensual_pareja_dependientes',0,9,1,0,1,1),(875,'declaracion_ingresosdeclaracion','cat_moneda_cargo_id',1,9,1,0,1,0),(876,'declaracion_ingresosdeclaracion','cat_moneda_neto_id',1,9,1,0,1,0),(877,'declaracion_observaciones','observaciones',0,9,1,0,1,1),(878,'declaracion_ingresosdeclaracion','cat_moneda_total_id',1,9,1,0,1,0),(879,'declaracion_observaciones','observaciones',0,9,1,0,1,1),(890,'declaracion_ingresosdeclaracion','ingreso_anio_anterior',1,10,1,0,1,1),(891,'declaracion_ingresosdeclaracion','ingreso_mensual_neto',1,10,1,0,1,1),(892,'declaracion_ingresosdeclaracion','ingreso_mensual_cargo',1,10,1,0,1,0),(893,'declaracion_ingresosdeclaracion','cat_moneda_neto_id',1,10,1,0,1,0),(894,'declaracion_ingresosdeclaracion','cat_moneda_cargo_id',1,10,1,0,1,0),(895,'declaracion_ingresosdeclaracion','observaciones',0,10,1,0,1,1),(896,'declaracion_conyugedependientes','observacion',0,7,1,0,1,1);
/*!40000 ALTER TABLE `declaracion_catcamposobligatorios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catdocumentosobtenidos`
--

DROP TABLE IF EXISTS `declaracion_catdocumentosobtenidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catdocumentosobtenidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documento_obtenido` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catdocumentosobtenidos`
--

LOCK TABLES `declaracion_catdocumentosobtenidos` WRITE;
/*!40000 ALTER TABLE `declaracion_catdocumentosobtenidos` DISABLE KEYS */;
INSERT INTO `declaracion_catdocumentosobtenidos` VALUES (1,'BOLETA','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,1,'BOL'),(2,'CERTIFICADO','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,1,'CERT'),(3,'CONSTANCIA','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,1,'CONST'),(4,'TITULO','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,1,'TIT');
/*!40000 ALTER TABLE `declaracion_catdocumentosobtenidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catentespublicos`
--

DROP TABLE IF EXISTS `declaracion_catentespublicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catentespublicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ente_publico` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=430 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catentespublicos`
--

LOCK TABLES `declaracion_catentespublicos` WRITE;
/*!40000 ALTER TABLE `declaracion_catentespublicos` DISABLE KEYS */;
INSERT INTO `declaracion_catentespublicos` VALUES (1,'Consejería Jurídica y de Servicios Legales','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(2,'Contraloría General','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(3,'Jefatura de Gobierno','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(4,'Oficialía Mayor','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(5,'Procuraduría General de Justicia del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(6,'Secretaría de Cultura','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(7,'Secretaría de Desarrollo Económico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(8,'Secretaría de Desarrollo Rural y Equidad para las Comunidades','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(9,'Secretaría de Desarrollo Social','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(10,'Secretaría de Desarrollo Urbano y Vivienda','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(11,'Secretaría de Educación','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(12,'Secretaría de Finanzas','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(13,'Secretaría de Gobierno','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(14,'Secretaría del Medio Ambiente','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(15,'Secretaría de Obras y Servicios','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(16,'Secretaría de Protección Civil','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(17,'Secretaría de Salud','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(18,'Secretaría de Seguridad Pública','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(19,'Secretaría de Trabajo y Fomento al Empleo','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(20,'Secretaría de Movilidad','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(21,'Secretaría de Turismo','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(22,'Caja de Previsión de la Policía Auxiliar del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(23,'Caja de Previsión de la Policía Preventiva del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(24,'Caja de Previsión para Trabajadores a Lista de Raya del Gobierno del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(25,'Consejo de Evaluación del Desarrollo Social del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(26,'Corporación Mexicana de Impresión S.A. de C.V.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(27,'Fideicomiso Centro Histórico de la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(28,'Fideicomiso de Recuperación Crediticia del Distrito Federal (FIDERE III)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(29,'Fideicomiso Educación Garantizada Del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(30,'Fideicomiso Museo de Arte Popular Mexicano','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(31,'Fideicomiso Museo del Estanquillo','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(33,'Fideicomiso Público Complejo Ambiental Xochimilco','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(34,'Fondo de Desarrollo Económico del Distrito Federal (FONDECO-DF)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(35,'Fondo Mixto de Promoción Turística','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(36,'Fondo para el Desarrollo Social de la Ciudad de México (FONDESO)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(37,'Heroico Cuerpo de Bomberos del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(38,'Secretaría de Ciencia y Tecnología e Inovación','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(39,'Instituto de Educación Media Superior del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(40,'Instituto de la Juventud del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(41,'Instituto de las Mujeres del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(42,'Instituto de Vivienda del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(43,'Metrobús','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(44,'Procuraduría Ambiental y del Ordenamiento Territorial del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(45,'Procuraduría Social del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(46,'Red de Transporte de Pasajeros del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(47,'Servicio de Transportes Eléctricos del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(48,'Servicios de Salud Pública del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(49,'Servicios Metropolitanos S.A. de C.V.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(50,'Sistema de Transporte Colectivo','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(51,'Sistema para el Desarrollo Integral de la Familia del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(52,'Delegación Álvaro Obregón','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(53,'Delegación Azcapotzalco','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(54,'Delegación Benito Juárez','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(55,'Delegación Coyoacán','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(56,'Delegación Cuajimalpa de Morelos','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(57,'Delegación Cuauhtémoc','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(58,'Delegación Gustavo A. Madero','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(59,'Delegación Iztacalco','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(60,'Delegación Iztapalapa','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(61,'Delegación La Magdalena Contreras','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(62,'Delegación Miguel Hidalgo','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(63,'Delegación Milpa Alta','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(64,'Delegación Tláhuac','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(65,'Delegación Tlalpan','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(66,'Delegación Venustiano Carranza','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(67,'Delegación Xochimilco','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(69,'Comisión de Filmaciones de la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(70,'Sistema de Radio y Televisión Digital','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(71,'Planta de Asfalto','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(73,'Instituto Tecnico de Formacion policial','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(74,'Centro de Atención a Emergencias','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(75,'Escuela de Administración Pública del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(76,'Instituto del Deporte del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(77,'Instituto de Asistencia e Integración Social','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(78,'Proyecto Metro','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(79,'Instituto para la Atención de los Adultos Mayores','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(80,'Sistema de Aguas de la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(81,'Junta de Asistencia Privada','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(83,'Instituto para la Atención y Prevención de las Adicciones de la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(84,'Instituto Local de la Infraestructura Física y Educativa','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(85,'Instituto de Verificación Administrativa del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(86,'Autoridad del Centro Histórico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(87,'Policía Bancaria Industrial','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(88,'Mecanismo de Seguimiento y Evaluación del Programa de Derechos Humanos del D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(89,'Autoridad de Espacios Públicos','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(90,'Fondo Ambiental Público del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(91,'Instituto para la Seguridad de las Construcciones en el Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(92,'Consejo para Prevenir y Eliminar la Discriminación de la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(93,'Instituto para la Integracion al Desarrollo de las Personas con Discapacidad','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(94,'Fideicomiso del Fondo para el Desarrollo Económico y Social','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(95,'Policia Auxiliar','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(96,'Coordinación de los Centros de Transferencia Modal del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(97,'Instituto de Formación Profesional','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(98,'Fondo para la Ateción y apoyo a las Víctimas del Delito','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(99,'Fideicomiso Público del Fondo de Apoyo a la Procuracón de Justicia del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(100,'Asamblea Legislativa del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(101,'Contaduría Mayor de Hacieda de la Asamblea Legislativa del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(102,'Tribunal Superior de Justicia del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(103,'Consejo De La Judicatura Del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(104,'Tribunal de lo Contencioso Administrativo del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(105,'Junta Local de Conciliación y Arbitraje del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(106,'Comisión De Derechos Humanos Del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(107,'Instituto Electoral del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(108,'Agencia de Protección Sanitaria del Gobierno del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(109,'Tribunal Electoral del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(110,'Universidad Autónoma de la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(111,'Fideicomiso Para la Promoción y Desarrollo del Cine Mexicano del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(112,'Instituto de Acceso a la Información Pública y Protección de Datos Personales del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(113,'Autoridad del Espacio Público del Distrito Federal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(141,'Autoridad de la Zona Patrimonio Mundial, Natural y Cultural de la Humanidad de Xochimilco, Tlahuac y Milpa Alta en la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(143,'Fideicomiso para el Fondo de Promoción para el Financiamiento del Transporte Público','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(145,'Fideicomiso Central de Abasto','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(147,'Dirección General de Asuntos Agrarios','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(148,'Instituto de Promoción Turistica','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(150,'Procuraduría de la Defensa del Trabajo','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(151,'Servicio Público de Localización Teléfonica \'LOCATEL\'','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(153,'FIDEICOMISO PÚBLICO INOVA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(154,'Agencia de Gestión Urbana de la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(155,'Consejo Económico de la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(156,'Instituto de Capacitación Para el Trabajo del D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(157,'PROCDMX, S.A de C.V.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(158,'Instituto Técnico de Formación Policial de la Secretaria de Seguridad Pública.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(160,'Auditoria Superior de la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(169,'Agencia de Promoción Inversión y Desarrollo Para la Ciudad de México','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(170,'NULL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(171,'DESCONOCIDA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(172,'DIRECCION GENERAL SECTORIAL PROG-PRESUP. DE DESARROLLO SUSTENTABLE Y DELEGACIONES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(173,'DIRECCION GENERAL SECTORIAL PROG-PRESUP. DE PROGRESO CON JUST., GOB., SEG. PUB., ADMON. Y FINANZAS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(174,'SUBSECRETARIA DE ATENCION CIUDADANA Y DESARROLLO POLICIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(175,'DIRECCION GENERAL DE TRANSPORTE','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(176,'DIRECCION GENERAL DE MEDICAMENTOS E INSUMOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(177,'DIRECCION GENERAL DE LA POLICIA METROPOLITANA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(178,'DIRECCION GENERAL DE LA POLICIA SECTORIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(179,'DIRECCION GENERAL DE ZOOLOGICOS DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(180,'JEFATURA GOBIERNO DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(181,'DIRECCION EJECUTIVA DE PLANEACION Y DESARROLLO TURISTICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(182,'DIRECCION GENERAL DE PLANEACION Y VIALIDAD','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(183,'DIRECCION GENERAL DE POLITICA LABORAL Y SERVICIO PUBLICO DE CARRERA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(184,'DIRECCION GENERAL DE POLITICA PRESUPUESTAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(185,'DIRECCION GENERAL DE GESTION AMBIENTAL DEL AIRE','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(186,'DIRECCION GENERAL DEL REGISTRO CIVIL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(187,'DIRECCION GENERAL DE REGULACION Y FOMENTO ECONOMICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(188,'DIRECCION GENERAL DE REGULACION Y GESTION AMBIENTAL DE AGUA, SUELO Y RESIDUOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(189,'DIRECCION GENERAL DE REGULACION AL TRANSPORTE','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(190,'DIRECCION GENERAL DE PROGRAMAS DELEGACIONALES Y REORDENAMIENTO DE LA VIA PUBLICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(191,'DIRECCION GENERAL DE SERVICIOS MEDICOS Y URGENCIAS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(192,'DIRECCION EJECUTIVA DE COORDINACION DE CONTRALORIAS INTERNAS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(193,'DIRECCION GENERAL DE COMUNICACION  SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(194,'SUBSECRETARIA DE GOBIERNO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(195,'DIRECCION GENERAL DE GOBIERNO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(196,'DIRECCION GENERAL DE REGULARIZACION TERRITORIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(197,'DIRECCION GENERAL DE PROTECCION CIVIL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(198,'SUBSECRETARIA DE TRABAJO Y PREVISION SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(199,'DIRECCION GENERAL DE TRABAJO Y PREVISION SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(200,'DIRECCION GENERAL DE EMPLEO Y CAPACITACION','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(201,'DIRECCION GENERAL DE DESARROLLO URBANO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(202,'DIRECCION GENERAL DE ADMINISTRACION URBANA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(203,'DIRECCION GENERAL DE ABASTO, COMERCIO Y DISTRIBUCION','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(204,'DIRECCION GENERAL DE PROGRAMAS ESTRATEGICOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(205,'DIRECCION GENERAL DE OBRAS PUBLICAS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(206,'DIRECCION GENERAL DE SERVICIOS URBANOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(207,'OFICINA DEL C. SECRETARIO DE CULTURA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(208,'DIRECCION GENERAL DE EQUIDAD Y DESARROLLO SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(209,'OFICINA DE LA C. SECRETARIA DE SALUD PUBLICA DEL D. F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(210,'TESORERIA DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(211,'SUBTESORERIA DE POLITICA FISCAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(212,'SUBTESORERIA DE ADMINISTRACION  TRIBUTARIA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(213,'SUBTESORERIA DE FISCALIZACION','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(214,'SUBTESORERIA DE CATASTRO Y PADRON TERRITORIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(215,'DIRECCION GENERAL DE ADMINISTRACION FINANCIERA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(216,'PROCURADURIA FISCAL DEL  DISTRITO  FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(217,'SUBPROCURADURIA DE LEGISLACION Y CONSULTA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(218,'SUBPROCURADURIA DE LO CONTENCIOSO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(219,'SUBPROCURADURIA DE RECURSOS ADMINISTRATIVOS Y AUTORIZACIONES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(220,'SUBPROCURADURIA DE ASUNTOS PENALES Y JUICIOS SOBRE INGRESOS COORDINADOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(221,'SUBSECRETARIA DE EGRESOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(222,'DIRECCION GENERAL DE CONTROL Y EVALUACION','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(223,'OFICINA DEL C. SECRETARIO DE TRANSPORTES Y VIALIDAD','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(224,'OFICINA DEL C. OFICIAL MAYOR','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(225,'DIRECCION GENERAL DE CONTRALORIAS INTERNAS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(226,'OFICINA DE LA C. CONSEJERA JURIDICA Y DE SERVICIOS LEGALES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(227,'DIRECCION GENERAL DE COMUNICACIONES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(228,'DIRECCION GENERAL DE POLICIA SECTORIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(229,'DIRECCION GENERAL DE LAS FUERZAS ESPECIALES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(230,'DIRECCION GENERAL DE AGRUPAMIENTOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(231,'DIRECCION GENERAL DE ASUNTOS INTERNOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(232,'DIRECCION GENERAL DE SEGURIDAD VIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(233,'SUBSECRETARIA DE APOYO INSTITUCIONAL Y POLICIAS COMPLEMENTARIAS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(234,'DIRECCION EJECUTIVA DEL REGISTRO Y CONTROL DE EMPRESAS DE SEGURIDAD PRIVADA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(235,'DIRECCION GENERAL DE APOYO INSTITUCIONAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(236,'DIRECCION GENERAL DE POLICIAS COMPLEMENTARIAS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(237,'SUBSECRETARIA DE PARTICIPACION CIUDADANA Y PREVENCION DEL DELITO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(238,'DIRECCION GENERAL DE PREVENCION DEL DELITO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(239,'COORDINACION GENERAL DE ADMINISTRACION Y APOYO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(240,'COORDINACION GENERAL DE PROGRAMAS METROPOLITANOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(241,'DIRECCION GENERAL DE PARTICIPACION CIUDADANA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(242,'DIRECCION GENERAL DE ADMINISTRACION DE PERSONAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(243,'DIRECCION GENERAL DE ASUNTOS EDUCATIVOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(244,'SECRETARIA DE EDUCACION, SALUD Y DESARROLLO SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(245,'OFICINA DE LA C. SECRETARIA DE DESARROLO SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(246,'DIRECCION GENERAL DE PLANEACION Y COORDINACION SECTORIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(247,'DIRECCION EJECUTIVA DE MICROCREDITOS DEL D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(248,'DIRECCION EJECUTIVA DE COORDINACION INSTITUCIONAL E INTEGRACION DE POLITICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(249,'UNIDAD DE BOSQUES URBANOS Y EDUCACION AMBIENTAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(250,'OFICINA DEL C. SECRETARIO DE FINANZAS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(251,'DIRECCION GENERAL DE INFORMATICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(252,'DIRECCION GENERAL DE CONCERTACION POLITICA Y ATENCION CIUDADANA Y CIUDADANA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(253,'DIRECCION GENERAL DEL FIDEICOMISO PARA EL MEJORAMIENTO DE LAS VIAS DE COMUNICACIÓN DEL D. F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(254,'OFICINA DE LA C. CONTRALORA GENERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(255,'DIRECCION GENERAL DE RECLUSORIOS Y CENTROS DE READAPTACION SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(256,'SUBSECRETARIA DE ASUNTOS JURIDICOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(257,'DIRECCION GENERAL DE SERVICIOS LEGALES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(258,'DIRECCION GENERAL DEL REGISTRO PUBLICO DE LA PROPIEDAD Y DE COMERCIO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(259,'DIRECCION GENERAL JURIDICA Y DE ESTUDIOS LEGISLATIVOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(260,'DIRECCION GENERAL DE TRABAJO NO ASALARIADO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(261,'SUBSECRETARIA DE COORDINACION DELEGACIONAL Y METROPOLITANA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(262,'DIRECCION GENERAL DE COORDINACION  METROPOLITANA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(263,'COORDINACION DE PARTICIPACION CIUDADANA PARA LA PREVENCION DEL DELITO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(264,'DIRECCION GENERAL DE POLITICA, SUPERVISION Y EVALUACION','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(265,'DIRECCION GENERAL DE PREVENCION DEL DELITO Y CAPACITACION','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(266,'DIRECCION GENERAL DE POLITICA Y NORMATIVIDAD ECONOMICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(267,'DIRECCION GENERAL DE PROMOCION E INVERSIONES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(268,'DIRECCION GENERAL DE DESARROLLO RURAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(269,'DIRECCION GENERAL DE IMAGEN Y COMERCIALIZACION TURISTICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(270,'DIRECCION GENERAL DE SERVICIOS TURISTICOS Y FOMENTO EMPRESARIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(271,'DIRECCION GENERAL DE INVESTIGACION Y REG. DE  SERVICIOS TURISTICOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(272,'DIRECCION GENERAL DE SERVICIOS TURISTICOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(273,'DIRECCION GENERAL DE PREVENCION Y CONTROL DE LA CONTAMINACION','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(274,'DIRECCION GENERAL DE PROYECTOS AMBIENTALES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(275,'DIRECCION GENERAL DE CONSTRUCCION Y OPERACION HIDRAULICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(276,'DIRECCION GENERAL DE CONSTRUCCION DE OBRAS DEL S.T.C.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(277,'COMISION DE AGUAS  DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(278,'DIRECCION GENERAL DE PROMOCION DEPORTIVA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(279,'DIRECCION GENERAL DE POLITICA SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(280,'INSTITUTO DE SERVICIOS DE SALUD DEL D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(281,'INSTITUTO DE CULTURA DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(282,'DIRECCION GENERAL DE PROGRAMACION','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(283,'DIRECCION GENERAL DE PRESUPUESTO Y CUENTA PUBLICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(284,'DIRECCION GENERAL DE PLANEACION Y PROY. DE TRANSPORTE Y VIALIDAD','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(285,'DIRECCION GENERAL DE NORMATIVIDAD Y EVALUACION DEL TRANSPORTE Y VIALIDAD','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(286,'DIRECCION GENERAL DE SERVICIOS AL TRANSPORTE','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(287,'AUTOTRANSPORTES  URBANOS  DE PASAJEROS R-100','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(288,'DIRECCION GENERAL DE CONTROL METROPOLITANO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(289,'DIRECCION GENERAL DE CONTROL REGIONAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(290,'DIRECCION GENERAL DE SERVICIOS DE  APOYO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(291,'POLICIA AUXILIAR DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(292,'OFICINA DEL C. SECRETARIO DE SEGURIDAD PUBLICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(293,'DIRECCION GENERAL DE ADMINISTRACION Y DESARROLLO DE PERSONAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(294,'DIRECCION GENERAL DE RECURSOS MATERIALES Y SERVICIOS GENERALES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(295,'DIRECCION GENERAL DE MODERNIZACION ADMINISTRATIVA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(296,'DIRECCION GENERAL DEL PATRIMONIO INMOBILIARIO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(297,'COMISION DE AVALUOS DE BIENES DEL D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(298,'DIRECCION GENERAL DE AUDITORIA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(299,'DIRECCION GENERAL DE LEGALIDAD Y RESPONSABILIDADES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(300,'DIRECCION GENERAL DE EVALUACION Y DIAGNOSTICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(301,'DIRECCION GENERAL DE COMISARIOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(302,'SUBPROCURADURIA DE AVERIGUACIONES PREVIAS CENTRALES (PGJDF)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(303,'SUBPROCURADURIA DE AVERIGUACIONES PREVIAS DESCONCENTRADAS (PGJDF)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(304,'SUBPROCURADURIA DE PROCESOS (PGJDF)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(305,'SUBPROCURADURIA JURIDICA Y DE DERECHOS HUMANOS (PGJDF)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(306,'PREVISIONES SALARIALES Y ECONOMICAS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(307,'COOPERACIONES Y SEGURIDAD SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(308,'DEUDA PUBLICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(309,'SUBSECRETARIA DE SEGURIDAD PUBLICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(310,'DIRECCION GENERAL DE COORDINACION DELEGACIONAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(311,'CONTRALORIAS INTERNAS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(312,'DELEGACIONES DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(313,'DIRECCION GENERAL DE OPERACION Y CONTROL DE PROYECTOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(314,'DIRECCION EJECUTIVA DE DESARROLLO INFORMATICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(315,'DIRECCION EJECUTIVA DE JUSTICIA CIVICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(316,'REGISTRO CIVIL DEL D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(317,'DIRECCION GENERAL DE EQUIPAMIENTO URBANO Y PROYECTOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(318,'FIDEICOMISO DE ESTUDIOS ESTRATEGICOS DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(319,'FIDEICOMISO DE LAS INSTITUCIONES DE LOS NIÑOS CALLE','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(320,'DIRECCION GENERAL DE SERVICIOS COMUNITARIOS INTEGRADOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(321,'DIRECCION GENERAL DE EDUCACION AMBIENTAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(322,'DIRECCION GENERAL DE BOSQUES URBANOS Y EDUCACION AMBIENTAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(323,'DIRECCION GENERAL DE ACCION SOCIAL, CIVICA Y CULTURAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(324,'INSTITUTO DEL TAXI DEL D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(325,'COMISION PARA LA PROTECCION DE INSUMOS PARA SALUD','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(326,'DIRECCION GENERAL DE REGULACION SANITARIA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(327,'DIRECCION GENERAL DE PROGRAMACION Y PRESUPUESTO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(328,'DIRECCION GENERAL DE CONTROL OPERATIVO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(329,'DIRECCION GENERAL DE CONTROL DE TRANSITO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(330,'COORDINACION EJECUTIVA DE DESARROLLO INFORMATICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(331,'PROCURADURIA AMBIENTAL Y DEL ORDENAMIENTO TERRITORIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(332,'DIRECCION DE LA OFICINA CENTRAL DEL REGISTRO CIVIL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(333,'SECRETARIA DE DESARROLLO URBANO Y VIVIENDA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(334,'COMISIÓN DE RECURSOS NATURALES Y DESARROLLO RURAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(335,'SECRETARIA DEL MEDIO AMBIENTE','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(336,'COMISIÓN DE RECURSOS NATURALES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(337,'SECRETARIA DE OBRAS Y SERVICIOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(338,'SECRETARIA DE SALUD DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(339,'SECRETARÍA DE CULTURA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(340,'COMISIÓN DE ASUNTOS AGRARIOS DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(341,'CONSEJERÍA JURÍDICA Y DE SERVICIOS LEGALES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(342,'SECRETARIA DE GOBIERNO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(343,'SECRETARIA DE DESARROLLO ECONOMICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(344,'SECRETARIA DE DESARROLLO SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(345,'JUNTA DE ASISTENCIA PRIVADA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(346,'INSTITUTO DE LA MUJER DEL D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(347,'INSTITUTO DE LAS MUJERES DEL D. F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(348,'DIRECCION GENERAL DE SERVICIOS DE TRANSPORTE PUBLICO INDIVIDUAL DE PASAJEROS DEL D. F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(349,'INSTITUTO DE PROMOCION TURISTICA DEL D. F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(350,'SECRETARÍA DE TURISMO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(351,'DIRECCION GENERAL DE PREVENCION Y READAPTACION SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(352,'INSTITUTO DEL DEPORTE DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(353,'FIDEICOMISO DE RECUPERACION CREDITICIA DE LA VIVIENDA POPULAR (FIDERE)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(354,'FONDO DE DESARROLLO ECONOMICO DEL D.F. (FONDECO)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(355,'FONDO PARA EL DESARROLLO SOCIAL DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(356,'SERVICIO PUBLICO DE LOCALIZACION TELEFONICA (LOCATEL)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(357,'CORPORACIÓN MEXICANA DE IMPRESIÓN, S.A. DE C.V. (COMISA)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(358,'SERVICIOS METROPOLITANOS, S.A. DE C.V.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(359,'CAJA DE PREVISION DE LA POLICIA AUXILIAR DEL D. F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(360,'FIDEICOMISO CENTRAL DE ABASTO DE CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(361,'PROCURADURIA SOCIAL DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(362,'FIDEICOMISO DE VIVIENDA, DESARROLLO SOCIAL Y URBANO (FIVIDESU)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(363,'FIDEICOMISO PROGRAMA CASA PROPIA (FICAPRO)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(364,'PROCURADURIA DE LA DEFENSA DEL TRABAJO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(365,'DIRECCION GENERAL DEL HEROICO CUERPO DE BOMBEROS DEL D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(366,'INSTITUTO DE LA JUVENTUD DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(367,'INSTITUTO DE ASISTENCIA E INTEGRACION SOCIAL DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(368,'SISTEMA PARA EL DESARROLLO INTEGRAL DE LA FAMILIA DEL D. F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(369,'SERVICIOS DE SALUD PUBLICA DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(370,'RED DE TRANSPORTE DE PASAJEROS DEL D.F. ','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(371,'FIDEICOMISO DE EMERGENCIA 080 DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(372,'FIDEICOMISO CENTRO HISTÓRICO DE LA CIUDAD DE MÉXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(373,'FIDEICOMISO PARA EL MEJORAMIENTO DE LAS VIAS DE COMUNICACION DEL D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(374,'FONDO MIXTO DE PROMOCION TURISTICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(375,'INSTITUTO DE EDUCACION MEDIA SUPERIOR DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(376,'PROCURADURIA AMBIENTAL Y DEL ORDENAMIENTO TERRITORIAL DEL D. F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(377,'METROBUS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(378,'FIDEICOMISO MUSEO DE ARTE POPULAR MEXICANO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(379,'FIDEICOMISO MUSEO DEL ESTANQUILLO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(380,'INSTITUTO DE CIENCIA Y TECNOLOGÍA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(381,'SECRETARÍA DE PROTECCIÓN CIVIL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(382,'SECRETARÍA DE EDUCACIÓN PÚBLICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(383,'SECRETARÍA DE EQUIDAD PARA LA COMUNIDAD ETNICA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(384,'SECRETARÍA DE PROTECCIÓN Y FOMENTO AL EMPLEO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(385,'SECRETARIA DEL TRABAJO Y FOMENTO AL EMPLEO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(386,'SECRETARIA DE EDUCACIÓN','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(387,'SECRETARIA DE DESARROLLO RURAL Y EQUIDAD PARA LAS COMUNIDADES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(388,'FIDEICOMISO EDUCACIÓN GARANTIZADA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(389,'SISTEMA DE RADIO Y TELEVISION DIGITAL DEL G.D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(390,'FIDEICOMISO PUBLICO COMPLEJO AMBIENTAL XOCHIMILCO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(391,'FIDEICOMISO PUBLICO CIUDAD DIGITAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(392,'ESCUELA DE ADMINISTRACION PUBLICA DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(393,'CONSEJO DE EVALUACION DEL DESARROLLO SOCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(394,'CENTRO DE ATENCION A EMERGENCIAS Y PROTECCION CIUDADANA DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(395,'INSTITUTO DE VERIFICACION ADMINISTRATIVA DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(396,'INSTITUTO LOCAL DE LA INFRAESTRUCTURA FISICA EDUCATIVA DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(397,'INSTITUTO PARA LA ATENCION Y PREVENCION DE LAS ADICCIONES DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(398,'INSTITUTO PARA LA ATENCION DE LOS ADULTOS MAYORES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(399,'INSTITUTO PARA LA INTEGRACION AL DESARROLLO DE LAS PERSONAS CON DISCAPACIDAD DEL D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(400,'CONSEJO PARA PREVENIR Y ELIMINAR LA DESCRIMINACION DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(401,'CALIDAD DE VIDA, PROGRESO Y DESARROLLO PARA LA CIUDAD DE MEXICO,S.A. DE C.V.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(402,'FONDO AMBIENTAL PUBLICO DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(403,'FONDO DE APOYO A LA PROCURACION DE JUSTICIA EN EL D.F.','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(404,'FIDEICOMISO PARA LA CONSTRUCCION Y OPERACION DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(405,'FONDO DE DESARROLLO ECONOMICO Y SOCIAL DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(406,'FONDO DE DESASTRES NATURALES DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(407,'FIDEICOMISO PARA LA PROMOCION Y DESARROLLO DEL CINE MEXICANO EN EL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(408,'FIDEICOMISO PARA EL FONDO DE PROMOCION PARA EL FINANCIAMIENTO DEL TRANSPORTE PUBLICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(409,'FONDO DE SEGURIDAD PUBLICADEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(410,'INSTITUTO PARA LA SEGURIDAD DE LAS CONSTRUCCIONES EN EL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(411,'AGENCIA DE GESTION URBANA DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(412,'SECRETARIA DE CIENCIA, TECNOLOGIA E INNOVACION','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(413,'AUTORIDAD DE LA ZONA PATRIMONIO MUNDIAL NATURAL Y CULTURAL DE LA HUMANIDAD EN XOCHIMILCO, TLAHUAC Y','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(414,'AUTORIDAD DEL CENTRO HISTORICO DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(415,'SECRETARIA DE MOVILIDAD','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(416,'INSTITUTO DE CAPACITACION PARA EL TRABAJO DE LA CIUDAD DE MEXICO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(417,'AUTORIDAD DEL ESPACIO PUBLICO DEL DISTRITO FEDERAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(418,'FIDEICOMISO PÚBLICO INOVA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(422,'Secretaría de Administración y Finanzas','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(423,'Secretaría de la Contraloría General de la Ciudad de México ','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(424,'Secretaría de educación, Ciencia, Tecnología e Innovación ','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(425,'Secretaría de Gestión Integral de Riesgos y Protección Civil ','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(426,'Secretaría de Inclusión y Bienestar Social','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(427,'Secretaría de las Mujeres','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(428,'Secretaría de Pueblos y Barrios Originarios y Comunidades Indígenas Residentes','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(429,'Secretaría de Seguridad Ciudadana','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'');
/*!40000 ALTER TABLE `declaracion_catentespublicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catentidadesfederativas`
--

DROP TABLE IF EXISTS `declaracion_catentidadesfederativas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catentidadesfederativas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entidad_federativa` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catentidadesfederativas`
--

LOCK TABLES `declaracion_catentidadesfederativas` WRITE;
/*!40000 ALTER TABLE `declaracion_catentidadesfederativas` DISABLE KEYS */;
INSERT INTO `declaracion_catentidadesfederativas` VALUES (1,'Aguascalientes','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'01'),(2,'Baja California','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'02'),(3,'Baja California Sur','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'03'),(4,'Campeche','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'04'),(5,'Coahuila de Zaragoza','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'05'),(6,'Colima','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'06'),(7,'Chiapas','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'07'),(8,'Chihuahua','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'08'),(9,'Ciudad de México','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'09'),(10,'Durango','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'10'),(11,'Guanajuato','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'11'),(12,'Guerrero','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'12'),(13,'Hidalgo','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'13'),(14,'Jalisco','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'14'),(15,'México','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'15'),(16,'Michoacán de Ocampo','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'16'),(17,'Morelos','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'17'),(18,'Nayarit','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'18'),(19,'Nuevo León','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'19'),(20,'Oaxaca','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'20'),(21,'Puebla','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'21'),(22,'Querétaro','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'22'),(23,'Quintana Roo','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'23'),(24,'San Luis Potosí','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'24'),(25,'Sinaloa','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'25'),(26,'Sonora','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'26'),(27,'Tabasco','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'27'),(28,'Tamaulipas','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'28'),(29,'Tlaxcala','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'29'),(30,'Veracruz de Ignacio de la Llave','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'30'),(31,'Yucatán','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'31'),(32,'Zacatecas','2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'32');
/*!40000 ALTER TABLE `declaracion_catentidadesfederativas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catestadosciviles`
--

DROP TABLE IF EXISTS `declaracion_catestadosciviles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catestadosciviles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado_civil` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catestadosciviles`
--

LOCK TABLES `declaracion_catestadosciviles` WRITE;
/*!40000 ALTER TABLE `declaracion_catestadosciviles` DISABLE KEYS */;
INSERT INTO `declaracion_catestadosciviles` VALUES (1,'CASADO (A)','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'CAS'),(2,'SOLTERO (A)','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SOL'),(3,'VIUDO (A)','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'VIU'),(4,'DIVORCIADO (A)','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'DIV'),(5,'SOCIEDAD DE CONVIVENCIA','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SCO'),(6,'CONCUBINA/CONCUBINARIO/UNIÓN LIBRE','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'CON');
/*!40000 ALTER TABLE `declaracion_catestadosciviles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catestatusdeclaracion`
--

DROP TABLE IF EXISTS `declaracion_catestatusdeclaracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catestatusdeclaracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estatus_declaracion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catestatusdeclaracion`
--

LOCK TABLES `declaracion_catestatusdeclaracion` WRITE;
/*!40000 ALTER TABLE `declaracion_catestatusdeclaracion` DISABLE KEYS */;
INSERT INTO `declaracion_catestatusdeclaracion` VALUES (1,'Iniciada','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(2,'Pendiente','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(3,'Completa  ','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(4,'Transmitida','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'');
/*!40000 ALTER TABLE `declaracion_catestatusdeclaracion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catestatusestudios`
--

DROP TABLE IF EXISTS `declaracion_catestatusestudios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catestatusestudios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estatus_estudios` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catestatusestudios`
--

LOCK TABLES `declaracion_catestatusestudios` WRITE;
/*!40000 ALTER TABLE `declaracion_catestatusestudios` DISABLE KEYS */;
INSERT INTO `declaracion_catestatusestudios` VALUES (1,'CURSANDO','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'CURS'),(2,'FINALIZADO','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'FIN'),(3,'TRUNCO','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'TRUN');
/*!40000 ALTER TABLE `declaracion_catestatusestudios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catformasadquisiciones`
--

DROP TABLE IF EXISTS `declaracion_catformasadquisiciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catformasadquisiciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_adquisicion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catformasadquisiciones`
--

LOCK TABLES `declaracion_catformasadquisiciones` WRITE;
/*!40000 ALTER TABLE `declaracion_catformasadquisiciones` DISABLE KEYS */;
INSERT INTO `declaracion_catformasadquisiciones` VALUES (1,'Compraventa','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CPV'),(2,'Cesión','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CSN'),(3,'Donación','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'DNC'),(4,'Herencia','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'HRN'),(5,'Permuta','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'PRM'),(6,'Rifa o sorteo','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'RST'),(7,'Sentencia','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'STC');
/*!40000 ALTER TABLE `declaracion_catformasadquisiciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catfuncionesprincipales`
--

DROP TABLE IF EXISTS `declaracion_catfuncionesprincipales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catfuncionesprincipales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `funcion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catfuncionesprincipales`
--

LOCK TABLES `declaracion_catfuncionesprincipales` WRITE;
/*!40000 ALTER TABLE `declaracion_catfuncionesprincipales` DISABLE KEYS */;
INSERT INTO `declaracion_catfuncionesprincipales` VALUES (1,'Administración de bienes','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'ABI'),(2,'Administración de recursos humanos','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'ARH'),(3,'Áreas técnicas','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'ATEC'),(4,'Atención directa al público','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'ADP'),(5,'Fuerza de seguridad e inteligencia','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'FSI'),(6,'Regulación y políticas públicas','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'RPP'),(7,'Administración Financiera (Recaudación de Ingresos, Desembolso de Fondos, Presupuesto, Contabilidad)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'AFIN'),(8,'Auditoria, control interno, fiscalización, vigilancia, interventorias, inspección','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'AUDI'),(9,'Contratos, adquisiciones, servicios y obra pública','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CASOP'),(10,'Legislar a nivel federal, estatal y cabildo municipal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'LEG'),(11,'Otorgamiento de permisos, beneficios, subsidios, concesiones, licencias, derechos de uso inmobiliario, recursos naturales o propiedad ','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'OTORG'),(12,'Procuración de justicia (sentencias, ministerios públicos, fiscales, policías de investigación, auxiliares ministeriales, etc.)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'PJUS'),(13,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_catfuncionesprincipales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catgradosacademicos`
--

DROP TABLE IF EXISTS `declaracion_catgradosacademicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catgradosacademicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grado_academico` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catgradosacademicos`
--

LOCK TABLES `declaracion_catgradosacademicos` WRITE;
/*!40000 ALTER TABLE `declaracion_catgradosacademicos` DISABLE KEYS */;
INSERT INTO `declaracion_catgradosacademicos` VALUES (1,'PRIMARIA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'PRI'),(2,'SECUNDARIA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,2,'SEC'),(3,'BACHILLERATO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,3,'BCH'),(4,'CARRERA TÉCNICA O COMERCIAL','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,4,'CTC'),(5,'LICENCIATURA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,5,'LIC'),(6,'ESPECIALIDAD','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,6,'ESP'),(7,'MAESTRÍA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,7,'MAE'),(8,'DOCTORADO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,8,'DOC');
/*!40000 ALTER TABLE `declaracion_catgradosacademicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catmonedas`
--

DROP TABLE IF EXISTS `declaracion_catmonedas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catmonedas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moneda` varchar(255) NOT NULL,
  `moneda_abrev` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catmonedas`
--

LOCK TABLES `declaracion_catmonedas` WRITE;
/*!40000 ALTER TABLE `declaracion_catmonedas` DISABLE KEYS */;
INSERT INTO `declaracion_catmonedas` VALUES (1,'Dirham de los Emiratos Árabes Unidos','AED','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'AED'),(2,'Afgani afgano','AFN','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'AFN'),(3,'Lek albanés','ALL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'ALL'),(4,'Dram armenio','AMD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'AMD'),(5,'Florín antillano neerlandés','ANG','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'ANG'),(6,'Kwanza angoleño','AOA','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'AOA'),(7,'Peso argentino','ARS','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'ARS'),(8,'Dólar australiano','AUD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,4,'AUD'),(9,'Florín arubeño','AWG','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'AWG'),(10,'Manat azerbaiyano','AZM','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'AZM'),(11,'Marco convertible de Bosnia-Herzegovina','BAM','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BAM'),(12,'Dólar de Barbados','BBD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BBD'),(13,'Taka de Bangladesh','BDT','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BDT'),(14,'Lev búlgaro','BGN','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BGN'),(15,'Dinar bahreiní','BHD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BHD'),(16,'Franco burundés','BIF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BIF'),(17,'Dólar de Bermuda','BMD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BMD'),(18,'Dólar de Brunéi','BND','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BND'),(19,'Boliviano de Bolivia','BOB','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BOB'),(20,'Real brasileño','BRL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BRL'),(21,'Dólar bahameño','BSD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,5,'BSD'),(22,'Ngultrum de Bután','BTN','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BTN'),(23,'Pula de Botsuana','BWP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BWP'),(24,'Rublo bielorruso','BYR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BYR'),(25,'Dólar de Belice','BZD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'BZD'),(26,'Dólar canadiense','CAD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,3,'CAD'),(27,'Franco congoleño','CDF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'CDF'),(28,'Franco suizo','CHF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'CHF'),(29,'Peso chileno','CLP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'CLP'),(30,'Yuan Renminbi de China','CNY','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'CNY'),(31,'Peso colombiano','COP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'COP'),(32,'Colón costarricense','CRC','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'CRC'),(33,'Dinar serbio ','RSD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'RSD'),(34,'Peso cubano','CUP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'CUP'),(35,'Peso cubano convertible','CUC','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'CUC'),(36,'Escudo caboverdiano','CVE','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'CVE'),(37,'Libra chipriota','CYP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'CYP'),(38,'Koruna checa','CZK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'CZK'),(39,'Franco yibutiano','DJF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'DJF'),(40,'Corona danesa','DKK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'DKK'),(41,'Peso dominicano','DOP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'DOP'),(42,'Dinar algerino','DZD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'DZD'),(43,'Corona estonia','EEK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'EEK'),(44,'Libra egipcia','EGP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'EGP'),(45,'Nakfa eritreo','ERN','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'ERN'),(46,'Birr etíope','ETB','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'ETB'),(47,'Euro','EUR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'EUR'),(48,'Dólar fijiano','FJD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'FJD'),(49,'Libra malvinense','FKP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'FKP'),(50,'Libra esterlina (libra de Gran Bretaña)','GBP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,6,'GBP'),(51,'Lari georgiano','GEL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'GEL'),(52,'Cedi ghanés','GHS','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'GHS'),(53,'Libra de Gibraltar','GIP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'GIP'),(54,'Dalasi gambiano','GMD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'GMD'),(55,'Franco guineano','GNF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'GNF'),(56,'Quetzal guatemalteco','GTQ','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'GTQ'),(57,'Dólar guyanés','GYD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'GYD'),(58,'Dólar de Hong Kong','HKD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'HKD'),(59,'Lempira hondureño','HNL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'HNL'),(60,'Kuna croata','HRK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'HRK'),(61,'Gourde haitiano','HTG','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'HTG'),(62,'Forint húngaro','HUF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'HUF'),(63,'Rupiah indonesia','IDR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'IDR'),(64,'Nuevo shéquel israelí','ILS','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'ILS'),(65,'Rupia india','INR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'INR'),(66,'Dinar iraquí','IQD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'IQD'),(67,'Rial iraní','IRR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'IRR'),(68,'Króna islandesa','ISK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'ISK'),(69,'Dólar jamaicano','JMD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'JMD'),(70,'Dinar jordano','JOD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'JOD'),(71,'Yen japonés','JPY','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'JPY'),(72,'Chelín keniata','KES','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'KES'),(73,'Som kirguís (de Kirguistán)','KGS','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'KGS'),(74,'Riel camboyano','KHR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'KHR'),(75,'Franco comoriano (de Comoras)','KMF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'KMF'),(76,'Won norcoreano','KPW','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'KPW'),(77,'Won surcoreano','KRW','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'KRW'),(78,'Dinar kuwaití','KWD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'KWD'),(79,'Dólar caimano (de Islas Caimán)','KYD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'KYD'),(80,'Tenge kazajo','KZT','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'KZT'),(81,'Kip lao','LAK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'LAK'),(82,'Libra libanesa','LBP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'LBP'),(83,'Rupia de Sri Lanka','LKR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'LKR'),(84,'Dólar liberiano','LRD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'LRD'),(85,'Loti lesotense','LSL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'LSL'),(86,'Litas lituano','LTL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'LTL'),(87,'Lat letón','LVL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'LVL'),(88,'Dinar libio','LYD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'LYD'),(89,'Dirham marroquí','MAD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MAD'),(90,'Leu moldavo','MDL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MDL'),(91,'Ariary malgache','MGA','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MGA'),(92,'Denar macedonio','MKD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MKD'),(93,'Kyat birmano','MMK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MMK'),(94,'Tughrik mongol','MNT','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MNT'),(95,'Pataca de Macao','MOP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MOP'),(96,'Ouguiya mauritana','MRO','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MRO'),(97,'Lira maltesa','MTL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MTL'),(98,'Rupia mauricia','MUR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MUR'),(99,'Rufiyaa maldiva','MVR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MVR'),(100,'Kwacha malauiano','MWK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MWK'),(101,'Peso mexicano','MXN','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,1,'MXN'),(102,'Ringgit malayo','MYR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MYR'),(103,'Metical mozambiqueño','MZN','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'MZN'),(104,'Dólar namibio','NAD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'NAD'),(105,'Naira nigeriana','NGN','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'NGN'),(106,'Córdoba nicaragüense','NIO','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'NIO'),(107,'Corona noruega','NOK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'NOK'),(108,'Rupia nepalesa','NPR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'NPR'),(109,'Dólar neozelandés','NZD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'NZD'),(110,'Rial omaní','OMR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'OMR'),(111,'Balboa panameña','PAB','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'PAB'),(112,'Nuevo sol peruano','PEN','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'PEN'),(113,'Kina de Papúa Nueva Guinea','PGK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'PGK'),(114,'Peso filipino','PHP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'PHP'),(115,'Rupia pakistaní','PKR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'PKR'),(116,'zloty polaco','PLN','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'PLN'),(117,'Guaraní paraguayo','PYG','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'PYG'),(118,'Rial qatarí','QAR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'QAR'),(119,'Leu rumano','RON','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'RON'),(120,'Rublo ruso','RUB','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'RUB'),(121,'Franco ruandés','RWF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'RWF'),(122,'Riyal saudí','SAR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SAR'),(123,'Dólar de las Islas Salomón','SBD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SBD'),(124,'Rupia de Seychelles','SCR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SCR'),(125,'Dinar sudanés','SDG','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SDG'),(126,'Corona sueca','SEK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SEK'),(127,'Dólar de Singapur','SGD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SGD'),(128,'Libra de Santa Helena','SHP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SHP'),(129,'Corona eslovaca','SKK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SKK'),(130,'Leone de Sierra Leona','SLL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SLL'),(131,'Chelín somalí','SOS','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SOS'),(132,'Dólar surinamés','SRD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SRD'),(133,'Dobra de Santo Tomé y Príncipe','STD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'STD'),(134,'Libra siria','SYP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SYP'),(135,'Lilangeni suazi','SZL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'SZL'),(136,'Baht tailandés','THB','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'THB'),(137,'Somoni tayik (de Tayikistán)','TJS','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'TJS'),(138,'Manat turcomano turcomano','TMT','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'TMT'),(139,'Dinar tunecino','TND','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'TND'),(140,'Pa\'anga tongano','TOP','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'TOP'),(141,'Lira turca','TRY','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'TRY'),(142,'Dólar de Trinidad y Tobago','TTD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'TTD'),(143,'Dólar taiwanés','TWD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'TWD'),(144,'Chelín tanzano','TZS','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'TZS'),(145,'Grivna ucraniana','UAH','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'UAH'),(146,'Chelín ugandés','UGX','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'UGX'),(147,'Dólar estadounidense','USD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,2,'USD'),(148,'Peso uruguayo','UYU','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'UYU'),(149,'Som uzbeko','UZS','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'UZS'),(150,'Bolívar fuerte venezolano','VEF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'VEF'),(151,'Dong vietnamita','VND','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'VND'),(152,'Vatu vanuatense','VUV','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'VUV'),(153,'Tala samoana','WST','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'WST'),(154,'Franco CFA de África Central','XAF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'XAF'),(155,'European Composite Unit (EURCO) (unidad del mercado de bonos)','XBA','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'XBA'),(156,'European Monetary Unit (E.M.U.-6) (unidad del mercado de bonos)','XBB','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'XBB'),(157,'European Unit of Account 9 (E.U.A.-9) (unidad del mercado de bonos)','XBC','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'XBC'),(158,'European Unit of Account 17 (E.U.A.-17) (unidad del mercado de bonos)','XBD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'XBD'),(159,'Dólar del Caribe Oriental','XCD','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'XCD'),(160,'Derecho Especial de Giro (DEG  -  FMI)','XDR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'XDR'),(161,'Franco de oro (Special settlement currency)','XFO','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'XFO'),(162,'Franco UIC (Special settlement currency)','XFU','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'XFU'),(163,'Franco CFA de África Occidental','XOF','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'XOF'),(164,'Rial yemení (de Yemen)','YER','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'YER'),(165,'Rand sudafricano','ZAR','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'ZAR'),(166,'Kwacha zambiano','ZMK','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'ZMK'),(167,'Dólar zimbabuense','ZWL','2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000',0,0,'ZWL');
/*!40000 ALTER TABLE `declaracion_catmonedas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catmotivobaja`
--

DROP TABLE IF EXISTS `declaracion_catmotivobaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catmotivobaja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_baja` varchar(45) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catmotivobaja`
--

LOCK TABLES `declaracion_catmotivobaja` WRITE;
/*!40000 ALTER TABLE `declaracion_catmotivobaja` DISABLE KEYS */;
INSERT INTO `declaracion_catmotivobaja` VALUES (1,'VENTA','2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',0,1,'VNT'),(2,'DONACION','2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',0,1,'DNC'),(3,'SINIESTRO','2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',0,1,'SNT'),(4,'OTRO','2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_catmotivobaja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catmunicipios`
--

DROP TABLE IF EXISTS `declaracion_catmunicipios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catmunicipios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `clave` varchar(3) NOT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_catmunic_cat_entidades_federa_15697655_fk_declaraci` (`cat_entidades_federativas_id`),
  CONSTRAINT `declaracion_catmunic_cat_entidades_federa_15697655_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2467 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catmunicipios`
--

LOCK TABLES `declaracion_catmunicipios` WRITE;
/*!40000 ALTER TABLE `declaracion_catmunicipios` DISABLE KEYS */;
INSERT INTO `declaracion_catmunicipios` VALUES (1,'2019-03-11 00:00:00.000000','2019-03-11 00:00:00.000000','Guadalajara','GDL',1),(2,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aguascalientes','001',1),(3,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Asientos','002',1),(4,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calvillo','003',1),(5,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cosío','004',1),(6,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jesús María','005',1),(7,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pabellón de Arteaga','006',1),(8,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rincón de Romos','007',1),(9,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José de Gracia','008',1),(10,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepezalá','009',1),(11,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Llano','010',1),(12,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco de los Romo','011',1),(13,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ensenada','001',2),(14,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mexicali','002',2),(15,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecate','003',2),(16,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tijuana','004',2),(17,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Playas de Rosarito','005',2),(18,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Comondú','001',3),(19,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mulegé','002',3),(20,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Paz','003',3),(21,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Los Cabos','008',3),(22,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Loreto','009',3),(23,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calkiní','001',4),(24,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Campeche','002',4),(25,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Carmen','003',4),(26,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Champotón','004',4),(27,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hecelchakán','005',4),(28,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hopelchén','006',4),(29,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Palizada','007',4),(30,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenabo','008',4),(31,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Escárcega','009',4),(32,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calakmul','010',4),(33,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Candelaria','011',4),(34,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Abasolo','001',5),(35,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acuña','002',5),(36,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Allende','003',5),(37,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Arteaga','004',5),(38,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Candela','005',5),(39,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Castaños','006',5),(40,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuatro Ciénegas','007',5),(41,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Escobedo','008',5),(42,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Francisco I. Madero','009',5),(43,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Frontera','010',5),(44,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Cepeda','011',5),(45,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guerrero','012',5),(46,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hidalgo','013',5),(47,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jiménez','014',5),(48,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juárez','015',5),(49,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lamadrid','016',5),(50,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Matamoros','017',5),(51,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Monclova','018',5),(52,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Morelos','019',5),(53,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Múzquiz','020',5),(54,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nadadores','021',5),(55,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nava','022',5),(56,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocampo','023',5),(57,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Parras','024',5),(58,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Piedras Negras','025',5),(59,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Progreso','026',5),(60,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ramos Arizpe','027',5),(61,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sabinas','028',5),(62,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sacramento','029',5),(63,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Saltillo','030',5),(64,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Buenaventura','031',5),(65,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan de Sabinas','032',5),(66,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro','033',5),(67,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sierra Mojada','034',5),(68,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Torreón','035',5),(69,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Viesca','036',5),(70,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Unión','037',5),(71,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zaragoza','038',5),(72,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Armería','001',6),(73,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Colima','002',6),(74,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Comala','003',6),(75,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coquimatlán','004',6),(76,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuauhtémoc','005',6),(77,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtlahuacán','006',6),(78,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Manzanillo','007',6),(79,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Minatitlán','008',6),(80,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecomán','009',6),(81,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Álvarez','010',6),(82,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acacoyagua','001',7),(83,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acala','002',7),(84,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acapetahua','003',7),(85,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Altamirano','004',7),(86,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amatán','005',7),(87,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amatenango de la Frontera','006',7),(88,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amatenango del Valle','007',7),(89,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Angel Albino Corzo','008',7),(90,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Arriaga','009',7),(91,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bejucal de Ocampo','010',7),(92,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bella Vista','011',7),(93,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Berriozábal','012',7),(94,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bochil','013',7),(95,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Bosque','014',7),(96,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cacahoatán','015',7),(97,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Catazajá','016',7),(98,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cintalapa','017',7),(99,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coapilla','018',7),(100,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Comitán de Domínguez','019',7),(101,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Concordia','020',7),(102,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Copainalá','021',7),(103,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chalchihuitán','022',7),(104,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chamula','023',7),(105,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chanal','024',7),(106,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chapultenango','025',7),(107,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chenalhó','026',7),(108,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiapa de Corzo','027',7),(109,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiapilla','028',7),(110,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chicoasén','029',7),(111,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chicomuselo','030',7),(112,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chilón','031',7),(113,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Escuintla','032',7),(114,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Francisco León','033',7),(115,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Frontera Comalapa','034',7),(116,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Frontera Hidalgo','035',7),(117,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Grandeza','036',7),(118,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huehuetán','037',7),(119,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huixtán','038',7),(120,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huitiupán','039',7),(121,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huixtla','040',7),(122,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Independencia','041',7),(123,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixhuatán','042',7),(124,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtacomitán','043',7),(125,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtapa','044',7),(126,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtapangajoya','045',7),(127,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jiquipilas','046',7),(128,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jitotol','047',7),(129,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juárez','048',7),(130,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Larráinzar','049',7),(131,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Libertad','050',7),(132,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mapastepec','051',7),(133,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Las Margaritas','052',7),(134,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mazapa de Madero','053',7),(135,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mazatán','054',7),(136,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Metapa','055',7),(137,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mitontic','056',7),(138,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Motozintla','057',7),(139,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nicolás Ruíz','058',7),(140,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocosingo','059',7),(141,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocotepec','060',7),(142,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocozocoautla de Espinosa','061',7),(143,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ostuacán','062',7),(144,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Osumacinta','063',7),(145,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Oxchuc','064',7),(146,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Palenque','065',7),(147,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pantelhó','066',7),(148,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pantepec','067',7),(149,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pichucalco','068',7),(150,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pijijiapan','069',7),(151,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Porvenir','070',7),(152,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Comaltitlán','071',7),(153,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pueblo Nuevo Solistahuacán','072',7),(154,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rayón','073',7),(155,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Reforma','074',7),(156,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Las Rosas','075',7),(157,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sabanilla','076',7),(158,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Salto de Agua','077',7),(159,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Cristóbal de las Casas','078',7),(160,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Fernando','079',7),(161,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Siltepec','080',7),(162,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Simojovel','081',7),(163,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sitalá','082',7),(164,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Socoltenango','083',7),(165,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Solosuchiapa','084',7),(166,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soyaló','085',7),(167,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Suchiapa','086',7),(168,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Suchiate','087',7),(169,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sunuapa','088',7),(170,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tapachula','089',7),(171,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tapalapa','090',7),(172,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tapilula','091',7),(173,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecpatán','092',7),(174,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenejapa','093',7),(175,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teopisca','094',7),(176,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tila','096',7),(177,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tonalá','097',7),(178,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Totolapa','098',7),(179,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Trinitaria','099',7),(180,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tumbalá','100',7),(181,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuxtla Gutiérrez','101',7),(182,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuxtla Chico','102',7),(183,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuzantán','103',7),(184,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tzimol','104',7),(185,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Unión Juárez','105',7),(186,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Venustiano Carranza','106',7),(187,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Corzo','107',7),(188,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villaflores','108',7),(189,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yajalón','109',7),(190,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lucas','110',7),(191,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zinacantán','111',7),(192,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Cancuc','112',7),(193,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aldama','113',7),(194,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Benemérito de las Américas','114',7),(195,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Maravilla Tenejapa','115',7),(196,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Marqués de Comillas','116',7),(197,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Montecristo de Guerrero','117',7),(198,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Duraznal','118',7),(199,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago el Pinar','119',7),(200,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Capitán Luis Ángel Vidal','120',7),(201,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rincón Chamula San Pedro','121',7),(202,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Parral','122',7),(203,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Emiliano Zapata','123',7),(204,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mezcalapa','124',7),(205,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ahumada','001',8),(206,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aldama','002',8),(207,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Allende','003',8),(208,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aquiles Serdán','004',8),(209,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ascensión','005',8),(210,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bachíniva','006',8),(211,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Balleza','007',8),(212,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Batopilas de Manuel Gómez Morín','008',8),(213,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bocoyna','009',8),(214,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Buenaventura','010',8),(215,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Camargo','011',8),(216,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Carichí','012',8),(217,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Casas Grandes','013',8),(218,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coronado','014',8),(219,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coyame del Sotol','015',8),(220,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Cruz','016',8),(221,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuauhtémoc','017',8),(222,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cusihuiriachi','018',8),(223,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chihuahua','019',8),(224,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chínipas','020',8),(225,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Delicias','021',8),(226,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Dr. Belisario Domínguez','022',8),(227,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Galeana','023',8),(228,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Isabel','024',8),(229,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Gómez Farías','025',8),(230,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Gran Morelos','026',8),(231,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guachochi','027',8),(232,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalupe','028',8),(233,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalupe y Calvo','029',8),(234,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guazapares','030',8),(235,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guerrero','031',8),(236,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hidalgo del Parral','032',8),(237,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huejotitán','033',8),(238,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ignacio Zaragoza','034',8),(239,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Janos','035',8),(240,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jiménez','036',8),(241,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juárez','037',8),(242,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Julimes','038',8),(243,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','López','039',8),(244,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Madera','040',8),(245,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Maguarichi','041',8),(246,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Manuel Benavides','042',8),(247,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Matachí','043',8),(248,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Matamoros','044',8),(249,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Meoqui','045',8),(250,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Morelos','046',8),(251,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Moris','047',8),(252,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Namiquipa','048',8),(253,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nonoava','049',8),(254,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nuevo Casas Grandes','050',8),(255,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocampo','051',8),(256,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ojinaga','052',8),(257,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Praxedis G. Guerrero','053',8),(258,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Riva Palacio','054',8),(259,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rosales','055',8),(260,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rosario','056',8),(261,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco de Borja','057',8),(262,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco de Conchos','058',8),(263,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco del Oro','059',8),(264,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Bárbara','060',8),(265,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Satevó','061',8),(266,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Saucillo','062',8),(267,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Temósachic','063',8),(268,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Tule','064',8),(269,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Urique','065',8),(270,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Uruachi','066',8),(271,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Valle de Zaragoza','067',8),(272,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Azcapotzalco','002',9),(273,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coyoacán','003',9),(274,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuajimalpa de Morelos','004',9),(275,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Gustavo A. Madero','005',9),(276,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Iztacalco','006',9),(277,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Iztapalapa','007',9),(278,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Magdalena Contreras','008',9),(279,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Milpa Alta','009',9),(280,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Álvaro Obregón','010',9),(281,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tláhuac','011',9),(282,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlalpan','012',9),(283,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xochimilco','013',9),(284,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Benito Juárez','014',9),(285,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuauhtémoc','015',9),(286,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Miguel Hidalgo','016',9),(287,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Venustiano Carranza','017',9),(288,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Canatlán','001',10),(289,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Canelas','002',10),(290,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coneto de Comonfort','003',10),(291,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuencamé','004',10),(292,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Durango','005',10),(293,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Simón Bolívar','006',10),(294,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Gómez Palacio','007',10),(295,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalupe Victoria','008',10),(296,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guanaceví','009',10),(297,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hidalgo','010',10),(298,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Indé','011',10),(299,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lerdo','012',10),(300,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mapimí','013',10),(301,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mezquital','014',10),(302,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nazas','015',10),(303,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nombre de Dios','016',10),(304,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocampo','017',10),(305,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Oro','018',10),(306,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Otáez','019',10),(307,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pánuco de Coronado','020',10),(308,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Peñón Blanco','021',10),(309,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Poanas','022',10),(310,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pueblo Nuevo','023',10),(311,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rodeo','024',10),(312,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bernardo','025',10),(313,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Dimas','026',10),(314,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan de Guadalupe','027',10),(315,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan del Río','028',10),(316,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Luis del Cordero','029',10),(317,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro del Gallo','030',10),(318,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Clara','031',10),(319,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Papasquiaro','032',10),(320,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Súchil','033',10),(321,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tamazula','034',10),(322,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepehuanes','035',10),(323,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlahualilo','036',10),(324,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Topia','037',10),(325,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Vicente Guerrero','038',10),(326,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nuevo Ideal','039',10),(327,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Abasolo','001',11),(328,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acámbaro','002',11),(329,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel de Allende','003',11),(330,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apaseo el Alto','004',11),(331,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apaseo el Grande','005',11),(332,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atarjea','006',11),(333,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Celaya','007',11),(334,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Manuel Doblado','008',11),(335,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Comonfort','009',11),(336,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coroneo','010',11),(337,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cortazar','011',11),(338,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuerámaro','012',11),(339,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Doctor Mora','013',11),(340,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Dolores Hidalgo Cuna de la Independencia Nacional','014',11),(341,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guanajuato','015',11),(342,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huanímaro','016',11),(343,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Irapuato','017',11),(344,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jaral del Progreso','018',11),(345,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jerécuaro','019',11),(346,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','León','020',11),(347,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Moroleón','021',11),(348,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocampo','022',11),(349,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pénjamo','023',11),(350,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pueblo Nuevo','024',11),(351,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Purísima del Rincón','025',11),(352,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Romita','026',11),(353,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Salamanca','027',11),(354,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Salvatierra','028',11),(355,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Diego de la Unión','029',11),(356,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Felipe','030',11),(357,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco del Rincón','031',11),(358,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José Iturbide','032',11),(359,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Luis de la Paz','033',11),(360,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina','034',11),(361,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz de Juventino Rosas','035',11),(362,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Maravatío','036',11),(363,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Silao de la Victoria','037',11),(364,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tarandacuao','038',11),(365,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tarimoro','039',11),(366,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tierra Blanca','040',11),(367,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Uriangato','041',11),(368,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Valle de Santiago','042',11),(369,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Victoria','043',11),(370,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villagrán','044',11),(371,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xichú','045',11),(372,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yuriria','046',11),(373,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acapulco de Juárez','001',12),(374,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ahuacuotzingo','002',12),(375,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ajuchitlán del Progreso','003',12),(376,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Alcozauca de Guerrero','004',12),(377,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Alpoyeca','005',12),(378,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apaxtla','006',12),(379,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Arcelia','007',12),(380,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atenango del Río','008',12),(381,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atlamajalcingo del Monte','009',12),(382,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atlixtac','010',12),(383,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atoyac de Álvarez','011',12),(384,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ayutla de los Libres','012',12),(385,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Azoyú','013',12),(386,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Benito Juárez','014',12),(387,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Buenavista de Cuéllar','015',12),(388,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coahuayutla de José María Izazaga','016',12),(389,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cocula','017',12),(390,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Copala','018',12),(391,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Copalillo','019',12),(392,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Copanatoyac','020',12),(393,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coyuca de Benítez','021',12),(394,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coyuca de Catalán','022',12),(395,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuajinicuilapa','023',12),(396,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cualác','024',12),(397,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuautepec','025',12),(398,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuetzala del Progreso','026',12),(399,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cutzamala de Pinzón','027',12),(400,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chilapa de Álvarez','028',12),(401,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chilpancingo de los Bravo','029',12),(402,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Florencio Villarreal','030',12),(403,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Canuto A. Neri','031',12),(404,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Heliodoro Castillo','032',12),(405,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huamuxtitlán','033',12),(406,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huitzuco de los Figueroa','034',12),(407,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Iguala de la Independencia','035',12),(408,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Igualapa','036',12),(409,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixcateopan de Cuauhtémoc','037',12),(410,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zihuatanejo de Azueta','038',12),(411,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juan R. Escudero','039',12),(412,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Leonardo Bravo','040',12),(413,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Malinaltepec','041',12),(414,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mártir de Cuilapan','042',12),(415,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Metlatónoc','043',12),(416,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mochitlán','044',12),(417,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Olinalá','045',12),(418,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ometepec','046',12),(419,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pedro Ascencio Alquisiras','047',12),(420,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Petatlán','048',12),(421,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pilcaya','049',12),(422,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pungarabato','050',12),(423,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Quechultenango','051',12),(424,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Luis Acatlán','052',12),(425,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Marcos','053',12),(426,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Totolapan','054',12),(427,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Taxco de Alarcón','055',12),(428,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecoanapa','056',12),(429,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Técpan de Galeana','057',12),(430,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teloloapan','058',12),(431,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepecoacuilco de Trujano','059',12),(432,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tetipac','060',12),(433,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tixtla de Guerrero','061',12),(434,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlacoachistlahuaca','062',12),(435,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlacoapa','063',12),(436,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlalchapa','064',12),(437,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlalixtaquilla de Maldonado','065',12),(438,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlapa de Comonfort','066',12),(439,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlapehuala','067',12),(440,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Unión de Isidoro Montes de Oca','068',12),(441,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xalpatláhuac','069',12),(442,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xochihuehuetlán','070',12),(443,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xochistlahuaca','071',12),(444,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotitlán Tablas','072',12),(445,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zirándaro','073',12),(446,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zitlala','074',12),(447,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Eduardo Neri','075',12),(448,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acatepec','076',12),(449,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Marquelia','077',12),(450,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cochoapa el Grande','078',12),(451,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','José Joaquín de Herrera','079',12),(452,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juchitán','080',12),(453,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Iliatenco','081',12),(454,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acatlán','001',13),(455,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acaxochitlán','002',13),(456,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Actopan','003',13),(457,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Agua Blanca de Iturbide','004',13),(458,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ajacuba','005',13),(459,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Alfajayucan','006',13),(460,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Almoloya','007',13),(461,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apan','008',13),(462,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Arenal','009',13),(463,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atitalaquia','010',13),(464,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atlapexco','011',13),(465,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atotonilco el Grande','012',13),(466,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atotonilco de Tula','013',13),(467,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calnali','014',13),(468,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cardonal','015',13),(469,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuautepec de Hinojosa','016',13),(470,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chapantongo','017',13),(471,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chapulhuacán','018',13),(472,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chilcuautla','019',13),(473,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Eloxochitlán','020',13),(474,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Emiliano Zapata','021',13),(475,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Epazoyucan','022',13),(476,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Francisco I. Madero','023',13),(477,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huasca de Ocampo','024',13),(478,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huautla','025',13),(479,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huazalingo','026',13),(480,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huehuetla','027',13),(481,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huejutla de Reyes','028',13),(482,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huichapan','029',13),(483,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixmiquilpan','030',13),(484,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jacala de Ledezma','031',13),(485,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jaltocán','032',13),(486,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juárez Hidalgo','033',13),(487,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lolotla','034',13),(488,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Metepec','035',13),(489,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Agustín Metzquititlán','036',13),(490,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Metztitlán','037',13),(491,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mineral del Chico','038',13),(492,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mineral del Monte','039',13),(493,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Misión','040',13),(494,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mixquiahuala de Juárez','041',13),(495,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Molango de Escamilla','042',13),(496,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nicolás Flores','043',13),(497,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nopala de Villagrán','044',13),(498,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Omitlán de Juárez','045',13),(499,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Felipe Orizatlán','046',13),(500,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pacula','047',13),(501,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pachuca de Soto','048',13),(502,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pisaflores','049',13),(503,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Progreso de Obregón','050',13),(504,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mineral de la Reforma','051',13),(505,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Agustín Tlaxiaca','052',13),(506,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bartolo Tutotepec','053',13),(507,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Salvador','054',13),(508,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago de Anaya','055',13),(509,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Tulantepec de Lugo Guerrero','056',13),(510,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Singuilucan','057',13),(511,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tasquillo','058',13),(512,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecozautla','059',13),(513,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenango de Doria','060',13),(514,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepeapulco','061',13),(515,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepehuacán de Guerrero','062',13),(516,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepeji del Río de Ocampo','063',13),(517,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepetitlán','064',13),(518,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tetepango','065',13),(519,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Tezontepec','066',13),(520,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tezontepec de Aldama','067',13),(521,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tianguistengo','068',13),(522,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tizayuca','069',13),(523,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlahuelilpan','070',13),(524,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlahuiltepa','071',13),(525,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlanalapa','072',13),(526,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlanchinol','073',13),(527,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaxcoapan','074',13),(528,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tolcayuca','075',13),(529,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tula de Allende','076',13),(530,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tulancingo de Bravo','077',13),(531,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xochiatipan','078',13),(532,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xochicoatlán','079',13),(533,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yahualica','080',13),(534,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacualtipán de Ángeles','081',13),(535,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotlán de Juárez','082',13),(536,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zempoala','083',13),(537,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zimapán','084',13),(538,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acatic','001',14),(539,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acatlán de Juárez','002',14),(540,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ahualulco de Mercado','003',14),(541,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amacueca','004',14),(542,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amatitán','005',14),(543,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ameca','006',14),(544,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juanito de Escobedo','007',14),(545,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Arandas','008',14),(546,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Arenal','009',14),(547,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atemajac de Brizuela','010',14),(548,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atengo','011',14),(549,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atenguillo','012',14),(550,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atotonilco el Alto','013',14),(551,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atoyac','014',14),(552,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Autlán de Navarro','015',14),(553,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ayotlán','016',14),(554,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ayutla','017',14),(555,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Barca','018',14),(556,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bolaños','019',14),(557,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cabo Corrientes','020',14),(558,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Casimiro Castillo','021',14),(559,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cihuatlán','022',14),(560,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotlán el Grande','023',14),(561,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cocula','024',14),(562,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Colotlán','025',14),(563,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Concepción de Buenos Aires','026',14),(564,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuautitlán de García Barragán','027',14),(565,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuautla','028',14),(566,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuquío','029',14),(567,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chapala','030',14),(568,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chimaltitán','031',14),(569,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiquilistlán','032',14),(570,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Degollado','033',14),(571,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ejutla','034',14),(572,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Encarnación de Díaz','035',14),(573,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Etzatlán','036',14),(574,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Grullo','037',14),(575,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guachinango','038',14),(576,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalajara','039',14),(577,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hostotipaquillo','040',14),(578,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huejúcar','041',14),(579,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huejuquilla el Alto','042',14),(580,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Huerta','043',14),(581,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtlahuacán de los Membrillos','044',14),(582,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtlahuacán del Río','045',14),(583,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jalostotitlán','046',14),(584,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jamay','047',14),(585,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jesús María','048',14),(586,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jilotlán de los Dolores','049',14),(587,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jocotepec','050',14),(588,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juanacatlán','051',14),(589,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juchitlán','052',14),(590,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lagos de Moreno','053',14),(591,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Limón','054',14),(592,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena','055',14),(593,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María del Oro','056',14),(594,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Manzanilla de la Paz','057',14),(595,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mascota','058',14),(596,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mazamitla','059',14),(597,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mexticacán','060',14),(598,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mezquitic','061',14),(599,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mixtlán','062',14),(600,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocotlán','063',14),(601,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ojuelos de Jalisco','064',14),(602,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pihuamo','065',14),(603,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Poncitlán','066',14),(604,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Puerto Vallarta','067',14),(605,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Purificación','068',14),(606,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Quitupan','069',14),(607,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Salto','070',14),(608,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Cristóbal de la Barranca','071',14),(609,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Diego de Alejandría','072',14),(610,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan de los Lagos','073',14),(611,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Julián','074',14),(612,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Marcos','075',14),(613,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín de Bolaños','076',14),(614,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Hidalgo','077',14),(615,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel el Alto','078',14),(616,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Gómez Farías','079',14),(617,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Sebastián del Oeste','080',14),(618,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María de los Ángeles','081',14),(619,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sayula','082',14),(620,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tala','083',14),(621,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Talpa de Allende','084',14),(622,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tamazula de Gordiano','085',14),(623,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tapalpa','086',14),(624,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecalitlán','087',14),(625,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecolotlán','088',14),(626,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Techaluta de Montenegro','089',14),(627,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenamaxtlán','090',14),(628,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teocaltiche','091',14),(629,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teocuitatlán de Corona','092',14),(630,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepatitlán de Morelos','093',14),(631,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tequila','094',14),(632,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teuchitlán','095',14),(633,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tizapán el Alto','096',14),(634,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlajomulco de Zúñiga','097',14),(635,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Tlaquepaque','098',14),(636,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tolimán','099',14),(637,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tomatlán','100',14),(638,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tonalá','101',14),(639,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tonaya','102',14),(640,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tonila','103',14),(641,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Totatiche','104',14),(642,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tototlán','105',14),(643,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuxcacuesco','106',14),(644,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuxcueca','107',14),(645,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuxpan','108',14),(646,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Unión de San Antonio','109',14),(647,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Unión de Tula','110',14),(648,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Valle de Guadalupe','111',14),(649,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Valle de Juárez','112',14),(650,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Gabriel','113',14),(651,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Corona','114',14),(652,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Guerrero','115',14),(653,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Hidalgo','116',14),(654,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cañadas de Obregón','117',14),(655,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yahualica de González Gallo','118',14),(656,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacoalco de Torres','119',14),(657,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapopan','120',14),(658,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotiltic','121',14),(659,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotitlán de Vadillo','122',14),(660,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotlán del Rey','123',14),(661,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotlanejo','124',14),(662,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Ignacio Cerro Gordo','125',14),(663,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acambay de Ruíz Castañeda','001',15),(664,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acolman','002',15),(665,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aculco','003',15),(666,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Almoloya de Alquisiras','004',15),(667,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Almoloya de Juárez','005',15),(668,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Almoloya del Río','006',15),(669,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amanalco','007',15),(670,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amatepec','008',15),(671,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amecameca','009',15),(672,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apaxco','010',15),(673,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atenco','011',15),(674,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atizapán','012',15),(675,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atizapán de Zaragoza','013',15),(676,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atlacomulco','014',15),(677,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atlautla','015',15),(678,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Axapusco','016',15),(679,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ayapango','017',15),(680,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calimaya','018',15),(681,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Capulhuac','019',15),(682,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coacalco de Berriozábal','020',15),(683,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coatepec Harinas','021',15),(684,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cocotitlán','022',15),(685,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coyotepec','023',15),(686,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuautitlán','024',15),(687,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chalco','025',15),(688,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chapa de Mota','026',15),(689,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chapultepec','027',15),(690,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiautla','028',15),(691,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chicoloapan','029',15),(692,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiconcuac','030',15),(693,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chimalhuacán','031',15),(694,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Donato Guerra','032',15),(695,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ecatepec de Morelos','033',15),(696,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ecatzingo','034',15),(697,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huehuetoca','035',15),(698,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hueypoxtla','036',15),(699,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huixquilucan','037',15),(700,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Isidro Fabela','038',15),(701,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtapaluca','039',15),(702,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtapan de la Sal','040',15),(703,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtapan del Oro','041',15),(704,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtlahuaca','042',15),(705,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xalatlaco','043',15),(706,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jaltenco','044',15),(707,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jilotepec','045',15),(708,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jilotzingo','046',15),(709,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jiquipilco','047',15),(710,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jocotitlán','048',15),(711,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Joquicingo','049',15),(712,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juchitepec','050',15),(713,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lerma','051',15),(714,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Malinalco','052',15),(715,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Melchor Ocampo','053',15),(716,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Metepec','054',15),(717,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mexicaltzingo','055',15),(718,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Morelos','056',15),(719,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Naucalpan de Juárez','057',15),(720,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nezahualcóyotl','058',15),(721,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nextlalpan','059',15),(722,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nicolás Romero','060',15),(723,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nopaltepec','061',15),(724,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocoyoacac','062',15),(725,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocuilan','063',15),(726,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Oro','064',15),(727,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Otumba','065',15),(728,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Otzoloapan','066',15),(729,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Otzolotepec','067',15),(730,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ozumba','068',15),(731,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Papalotla','069',15),(732,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Paz','070',15),(733,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Polotitlán','071',15),(734,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rayón','072',15),(735,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonio la Isla','073',15),(736,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Felipe del Progreso','074',15),(737,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín de las Pirámides','075',15),(738,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Atenco','076',15),(739,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Simón de Guerrero','077',15),(740,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Tomás','078',15),(741,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soyaniquilpan de Juárez','079',15),(742,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sultepec','080',15),(743,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecámac','081',15),(744,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tejupilco','082',15),(745,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Temamatla','083',15),(746,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Temascalapa','084',15),(747,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Temascalcingo','085',15),(748,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Temascaltepec','086',15),(749,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Temoaya','087',15),(750,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenancingo','088',15),(751,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenango del Aire','089',15),(752,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenango del Valle','090',15),(753,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teoloyucan','091',15),(754,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teotihuacán','092',15),(755,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepetlaoxtoc','093',15),(756,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepetlixpa','094',15),(757,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepotzotlán','095',15),(758,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tequixquiac','096',15),(759,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Texcaltitlán','097',15),(760,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Texcalyacac','098',15),(761,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Texcoco','099',15),(762,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tezoyuca','100',15),(763,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tianguistenco','101',15),(764,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Timilpan','102',15),(765,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlalmanalco','103',15),(766,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlalnepantla de Baz','104',15),(767,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlatlaya','105',15),(768,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Toluca','106',15),(769,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tonatico','107',15),(770,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tultepec','108',15),(771,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tultitlán','109',15),(772,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Valle de Bravo','110',15),(773,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Allende','111',15),(774,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa del Carbón','112',15),(775,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Guerrero','113',15),(776,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Victoria','114',15),(777,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xonacatlán','115',15),(778,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacazonapan','116',15),(779,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacualpan','117',15),(780,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zinacantepec','118',15),(781,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zumpahuacán','119',15),(782,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zumpango','120',15),(783,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuautitlán Izcalli','121',15),(784,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Valle de Chalco Solidaridad','122',15),(785,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Luvianos','123',15),(786,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José del Rincón','124',15),(787,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tonanitla','125',15),(788,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acuitzio','001',16),(789,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aguililla','002',16),(790,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Álvaro Obregón','003',16),(791,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Angamacutiro','004',16),(792,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Angangueo','005',16),(793,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apatzingán','006',16),(794,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aporo','007',16),(795,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aquila','008',16),(796,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ario','009',16),(797,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Arteaga','010',16),(798,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Briseñas','011',16),(799,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Buenavista','012',16),(800,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Carácuaro','013',16),(801,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coahuayana','014',16),(802,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coalcomán de Vázquez Pallares','015',16),(803,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coeneo','016',16),(804,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Contepec','017',16),(805,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Copándaro','018',16),(806,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cotija','019',16),(807,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuitzeo','020',16),(808,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Charapan','021',16),(809,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Charo','022',16),(810,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chavinda','023',16),(811,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cherán','024',16),(812,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chilchota','025',16),(813,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chinicuila','026',16),(814,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chucándiro','027',16),(815,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Churintzio','028',16),(816,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Churumuco','029',16),(817,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ecuandureo','030',16),(818,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Epitacio Huerta','031',16),(819,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Erongarícuaro','032',16),(820,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Gabriel Zamora','033',16),(821,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hidalgo','034',16),(822,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Huacana','035',16),(823,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huandacareo','036',16),(824,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huaniqueo','037',16),(825,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huetamo','038',16),(826,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huiramba','039',16),(827,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Indaparapeo','040',16),(828,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Irimbo','041',16),(829,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtlán','042',16),(830,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jacona','043',16),(831,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jiménez','044',16),(832,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jiquilpan','045',16),(833,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juárez','046',16),(834,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jungapeo','047',16),(835,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lagunillas','048',16),(836,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Madero','049',16),(837,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Maravatío','050',16),(838,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Marcos Castellanos','051',16),(839,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lázaro Cárdenas','052',16),(840,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Morelia','053',16),(841,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Morelos','054',16),(842,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Múgica','055',16),(843,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nahuatzen','056',16),(844,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nocupétaro','057',16),(845,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nuevo Parangaricutiro','058',16),(846,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nuevo Urecho','059',16),(847,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Numarán','060',16),(848,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocampo','061',16),(849,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pajacuarán','062',16),(850,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Panindícuaro','063',16),(851,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Parácuaro','064',16),(852,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Paracho','065',16),(853,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pátzcuaro','066',16),(854,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Penjamillo','067',16),(855,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Peribán','068',16),(856,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Piedad','069',16),(857,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Purépero','070',16),(858,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Puruándiro','071',16),(859,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Queréndaro','072',16),(860,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Quiroga','073',16),(861,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cojumatlán de Régules','074',16),(862,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Los Reyes','075',16),(863,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sahuayo','076',16),(864,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lucas','077',16),(865,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana Maya','078',16),(866,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Salvador Escalante','079',16),(867,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Senguio','080',16),(868,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Susupuato','081',16),(869,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tacámbaro','082',16),(870,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tancítaro','083',16),(871,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tangamandapio','084',16),(872,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tangancícuaro','085',16),(873,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tanhuato','086',16),(874,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Taretan','087',16),(875,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tarímbaro','088',16),(876,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepalcatepec','089',16),(877,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tingambato','090',16),(878,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tingüindín','091',16),(879,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tiquicheo de Nicolás Romero','092',16),(880,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlalpujahua','093',16),(881,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlazazalca','094',16),(882,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tocumbo','095',16),(883,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tumbiscatío','096',16),(884,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Turicato','097',16),(885,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuxpan','098',16),(886,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuzantla','099',16),(887,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tzintzuntzan','100',16),(888,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tzitzio','101',16),(889,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Uruapan','102',16),(890,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Venustiano Carranza','103',16),(891,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villamar','104',16),(892,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Vista Hermosa','105',16),(893,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yurécuaro','106',16),(894,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacapu','107',16),(895,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zamora','108',16),(896,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zináparo','109',16),(897,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zinapécuaro','110',16),(898,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ziracuaretiro','111',16),(899,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zitácuaro','112',16),(900,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','José Sixto Verduzco','113',16),(901,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amacuzac','001',17),(902,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atlatlahucan','002',17),(903,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Axochiapan','003',17),(904,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ayala','004',17),(905,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coatlán del Río','005',17),(906,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuautla','006',17),(907,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuernavaca','007',17),(908,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Emiliano Zapata','008',17),(909,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huitzilac','009',17),(910,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jantetelco','010',17),(911,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jiutepec','011',17),(912,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jojutla','012',17),(913,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jonacatepec de Leandro Valle','013',17),(914,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mazatepec','014',17),(915,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Miacatlán','015',17),(916,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocuituco','016',17),(917,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Puente de Ixtla','017',17),(918,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Temixco','018',17),(919,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepalcingo','019',17),(920,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepoztlán','020',17),(921,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tetecala','021',17),(922,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tetela del Volcán','022',17),(923,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlalnepantla','023',17),(924,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaltizapán de Zapata','024',17),(925,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaquiltenango','025',17),(926,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlayacapan','026',17),(927,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Totolapan','027',17),(928,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xochitepec','028',17),(929,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yautepec','029',17),(930,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yecapixtla','030',17),(931,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacatepec','031',17),(932,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacualpan de Amilpas','032',17),(933,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Temoac','033',17),(934,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coatetelco','034',17),(935,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xoxocotla','035',17),(936,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acaponeta','001',18),(937,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ahuacatlán','002',18),(938,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amatlán de Cañas','003',18),(939,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Compostela','004',18),(940,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huajicori','005',18),(941,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtlán del Río','006',18),(942,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jala','007',18),(943,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xalisco','008',18),(944,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Del Nayar','009',18),(945,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rosamorada','010',18),(946,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ruíz','011',18),(947,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Blas','012',18),(948,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Lagunillas','013',18),(949,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María del Oro','014',18),(950,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Ixcuintla','015',18),(951,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecuala','016',18),(952,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepic','017',18),(953,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuxpan','018',18),(954,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Yesca','019',18),(955,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bahía de Banderas','020',18),(956,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Abasolo','001',19),(957,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Agualeguas','002',19),(958,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Los Aldamas','003',19),(959,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Allende','004',19),(960,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Anáhuac','005',19),(961,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apodaca','006',19),(962,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aramberri','007',19),(963,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bustamante','008',19),(964,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cadereyta Jiménez','009',19),(965,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Carmen','010',19),(966,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cerralvo','011',19),(967,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ciénega de Flores','012',19),(968,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','China','013',19),(969,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Doctor Arroyo','014',19),(970,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Doctor Coss','015',19),(971,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Doctor González','016',19),(972,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Galeana','017',19),(973,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','García','018',19),(974,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Garza García','019',19),(975,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Bravo','020',19),(976,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Escobedo','021',19),(977,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Terán','022',19),(978,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Treviño','023',19),(979,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Zaragoza','024',19),(980,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Zuazua','025',19),(981,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalupe','026',19),(982,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Los Herreras','027',19),(983,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Higueras','028',19),(984,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hualahuises','029',19),(985,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Iturbide','030',19),(986,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juárez','031',19),(987,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lampazos de Naranjo','032',19),(988,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Linares','033',19),(989,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Marín','034',19),(990,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Melchor Ocampo','035',19),(991,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mier y Noriega','036',19),(992,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mina','037',19),(993,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Montemorelos','038',19),(994,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Monterrey','039',19),(995,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Parás','040',19),(996,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pesquería','041',19),(997,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Los Ramones','042',19),(998,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rayones','043',19),(999,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sabinas Hidalgo','044',19),(1000,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Salinas Victoria','045',19),(1001,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Nicolás de los Garza','046',19),(1002,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hidalgo','047',19),(1003,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina','048',19),(1004,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago','049',19),(1005,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Vallecillo','050',19),(1006,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villaldama','051',19),(1007,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Abejones','001',20),(1008,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acatlán de Pérez Figueroa','002',20),(1009,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Asunción Cacalotepec','003',20),(1010,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Asunción Cuyotepeji','004',20),(1011,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Asunción Ixtaltepec','005',20),(1012,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Asunción Nochixtlán','006',20),(1013,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Asunción Ocotlán','007',20),(1014,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Asunción Tlacolulita','008',20),(1015,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ayotzintepec','009',20),(1016,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Barrio de la Soledad','010',20),(1017,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calihualá','011',20),(1018,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Candelaria Loxicha','012',20),(1019,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ciénega de Zimatlán','013',20),(1020,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ciudad Ixtepec','014',20),(1021,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coatecas Altas','015',20),(1022,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coicoyán de las Flores','016',20),(1023,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Compañía','017',20),(1024,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Concepción Buenavista','018',20),(1025,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Concepción Pápalo','019',20),(1026,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Constancia del Rosario','020',20),(1027,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cosolapa','021',20),(1028,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cosoltepec','022',20),(1029,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuilápam de Guerrero','023',20),(1030,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuyamecalco Villa de Zaragoza','024',20),(1031,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chahuites','025',20),(1032,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chalcatongo de Hidalgo','026',20),(1033,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiquihuitlán de Benito Juárez','027',20),(1034,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Heroica Ciudad de Ejutla de Crespo','028',20),(1035,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Eloxochitlán de Flores Magón','029',20),(1036,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Espinal','030',20),(1037,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tamazulápam del Espíritu Santo','031',20),(1038,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Fresnillo de Trujano','032',20),(1039,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalupe Etla','033',20),(1040,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalupe de Ramírez','034',20),(1041,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guelatao de Juárez','035',20),(1042,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guevea de Humboldt','036',20),(1043,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mesones Hidalgo','037',20),(1044,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Hidalgo','038',20),(1045,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Heroica Ciudad de Huajuapan de León','039',20),(1046,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huautepec','040',20),(1047,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huautla de Jiménez','041',20),(1048,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtlán de Juárez','042',20),(1049,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Heroica Ciudad de Juchitán de Zaragoza','043',20),(1050,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Loma Bonita','044',20),(1051,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena Apasco','045',20),(1052,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena Jaltepec','046',20),(1053,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Magdalena Jicotlán','047',20),(1054,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena Mixtepec','048',20),(1055,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena Ocotlán','049',20),(1056,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena Peñasco','050',20),(1057,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena Teitipac','051',20),(1058,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena Tequisistlán','052',20),(1059,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena Tlacotepec','053',20),(1060,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena Zahuatlán','054',20),(1061,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mariscala de Juárez','055',20),(1062,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mártires de Tacubaya','056',20),(1063,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Matías Romero Avendaño','057',20),(1064,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mazatlán Villa de Flores','058',20),(1065,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Miahuatlán de Porfirio Díaz','059',20),(1066,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mixistlán de la Reforma','060',20),(1067,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Monjas','061',20),(1068,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Natividad','062',20),(1069,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nazareno Etla','063',20),(1070,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nejapa de Madero','064',20),(1071,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixpantepec Nieves','065',20),(1072,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Niltepec','066',20),(1073,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Oaxaca de Juárez','067',20),(1074,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocotlán de Morelos','068',20),(1075,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Pe','069',20),(1076,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pinotepa de Don Luis','070',20),(1077,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pluma Hidalgo','071',20),(1078,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José del Progreso','072',20),(1079,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Putla Villa de Guerrero','073',20),(1080,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Quioquitani','074',20),(1081,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Reforma de Pineda','075',20),(1082,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Reforma','076',20),(1083,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Reyes Etla','077',20),(1084,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rojas de Cuauhtémoc','078',20),(1085,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Salina Cruz','079',20),(1086,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Agustín Amatengo','080',20),(1087,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Agustín Atenango','081',20),(1088,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Agustín Chayuco','082',20),(1089,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Agustín de las Juntas','083',20),(1090,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Agustín Etla','084',20),(1091,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Agustín Loxicha','085',20),(1092,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Agustín Tlacotepec','086',20),(1093,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Agustín Yatareni','087',20),(1094,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Cabecera Nueva','088',20),(1095,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Dinicuiti','089',20),(1096,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Huaxpaltepec','090',20),(1097,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Huayápam','091',20),(1098,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Ixtlahuaca','092',20),(1099,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Lagunas','093',20),(1100,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Nuxiño','094',20),(1101,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Paxtlán','095',20),(1102,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Sinaxtla','096',20),(1103,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Solaga','097',20),(1104,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Teotilálpam','098',20),(1105,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Tepetlapa','099',20),(1106,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Yaá','100',20),(1107,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Zabache','101',20),(1108,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Zautla','102',20),(1109,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonino Castillo Velasco','103',20),(1110,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonino el Alto','104',20),(1111,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonino Monte Verde','105',20),(1112,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonio Acutla','106',20),(1113,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonio de la Cal','107',20),(1114,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonio Huitepec','108',20),(1115,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonio Nanahuatípam','109',20),(1116,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonio Sinicahua','110',20),(1117,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonio Tepetlapa','111',20),(1118,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Baltazar Chichicápam','112',20),(1119,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Baltazar Loxicha','113',20),(1120,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Baltazar Yatzachi el Bajo','114',20),(1121,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bartolo Coyotepec','115',20),(1122,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bartolomé Ayautla','116',20),(1123,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bartolomé Loxicha','117',20),(1124,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bartolomé Quialana','118',20),(1125,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bartolomé Yucuañe','119',20),(1126,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bartolomé Zoogocho','120',20),(1127,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bartolo Soyaltepec','121',20),(1128,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bartolo Yautepec','122',20),(1129,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Bernardo Mixtepec','123',20),(1130,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Blas Atempa','124',20),(1131,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Carlos Yautepec','125',20),(1132,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Cristóbal Amatlán','126',20),(1133,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Cristóbal Amoltepec','127',20),(1134,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Cristóbal Lachirioag','128',20),(1135,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Cristóbal Suchixtlahuaca','129',20),(1136,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Dionisio del Mar','130',20),(1137,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Dionisio Ocotepec','131',20),(1138,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Dionisio Ocotlán','132',20),(1139,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Esteban Atatlahuca','133',20),(1140,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Felipe Jalapa de Díaz','134',20),(1141,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Felipe Tejalápam','135',20),(1142,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Felipe Usila','136',20),(1143,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Cahuacuá','137',20),(1144,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Cajonos','138',20),(1145,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Chapulapa','139',20),(1146,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Chindúa','140',20),(1147,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco del Mar','141',20),(1148,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Huehuetlán','142',20),(1149,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Ixhuatán','143',20),(1150,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Jaltepetongo','144',20),(1151,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Lachigoló','145',20),(1152,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Logueche','146',20),(1153,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Nuxaño','147',20),(1154,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Ozolotepec','148',20),(1155,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Sola','149',20),(1156,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Telixtlahuaca','150',20),(1157,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Teopan','151',20),(1158,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Tlapancingo','152',20),(1159,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Gabriel Mixtepec','153',20),(1160,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Ildefonso Amatlán','154',20),(1161,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Ildefonso Sola','155',20),(1162,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Ildefonso Villa Alta','156',20),(1163,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jacinto Amilpas','157',20),(1164,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jacinto Tlacotepec','158',20),(1165,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jerónimo Coatlán','159',20),(1166,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jerónimo Silacayoapilla','160',20),(1167,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jerónimo Sosola','161',20),(1168,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jerónimo Taviche','162',20),(1169,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jerónimo Tecóatl','163',20),(1170,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jorge Nuchita','164',20),(1171,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José Ayuquila','165',20),(1172,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José Chiltepec','166',20),(1173,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José del Peñasco','167',20),(1174,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José Estancia Grande','168',20),(1175,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José Independencia','169',20),(1176,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José Lachiguiri','170',20),(1177,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José Tenango','171',20),(1178,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Achiutla','172',20),(1179,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Atepec','173',20),(1180,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ánimas Trujano','174',20),(1181,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Atatlahuca','175',20),(1182,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Coixtlahuaca','176',20),(1183,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Cuicatlán','177',20),(1184,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Guelache','178',20),(1185,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Jayacatlán','179',20),(1186,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Lo de Soto','180',20),(1187,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Suchitepec','181',20),(1188,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Tlacoatzintepec','182',20),(1189,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Tlachichilco','183',20),(1190,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Tuxtepec','184',20),(1191,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Cacahuatepec','185',20),(1192,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Cieneguilla','186',20),(1193,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Coatzóspam','187',20),(1194,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Colorado','188',20),(1195,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Comaltepec','189',20),(1196,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Cotzocón','190',20),(1197,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Chicomezúchil','191',20),(1198,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Chilateca','192',20),(1199,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan del Estado','193',20),(1200,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan del Río','194',20),(1201,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Diuxi','195',20),(1202,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Evangelista Analco','196',20),(1203,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Guelavía','197',20),(1204,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Guichicovi','198',20),(1205,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Ihualtepec','199',20),(1206,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Juquila Mixes','200',20),(1207,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Juquila Vijanos','201',20),(1208,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Lachao','202',20),(1209,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Lachigalla','203',20),(1210,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Lajarcia','204',20),(1211,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Lalana','205',20),(1212,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan de los Cués','206',20),(1213,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Mazatlán','207',20),(1214,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Mixtepec','208',20),(1215,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Mixtepec','209',20),(1216,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Ñumí','210',20),(1217,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Ozolotepec','211',20),(1218,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Petlapa','212',20),(1219,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Quiahije','213',20),(1220,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Quiotepec','214',20),(1221,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Sayultepec','215',20),(1222,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Tabaá','216',20),(1223,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Tamazola','217',20),(1224,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Teita','218',20),(1225,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Teitipac','219',20),(1226,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Tepeuxila','220',20),(1227,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Teposcolula','221',20),(1228,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Yaeé','222',20),(1229,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Yatzona','223',20),(1230,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Yucuita','224',20),(1231,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lorenzo','225',20),(1232,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lorenzo Albarradas','226',20),(1233,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lorenzo Cacaotepec','227',20),(1234,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lorenzo Cuaunecuiltitla','228',20),(1235,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lorenzo Texmelúcan','229',20),(1236,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lorenzo Victoria','230',20),(1237,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lucas Camotlán','231',20),(1238,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lucas Ojitlán','232',20),(1239,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lucas Quiaviní','233',20),(1240,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lucas Zoquiápam','234',20),(1241,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Luis Amatlán','235',20),(1242,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Marcial Ozolotepec','236',20),(1243,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Marcos Arteaga','237',20),(1244,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín de los Cansecos','238',20),(1245,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Huamelúlpam','239',20),(1246,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Itunyoso','240',20),(1247,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Lachilá','241',20),(1248,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Peras','242',20),(1249,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Tilcajete','243',20),(1250,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Toxpalan','244',20),(1251,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Zacatepec','245',20),(1252,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Cajonos','246',20),(1253,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Capulálpam de Méndez','247',20),(1254,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo del Mar','248',20),(1255,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Yoloxochitlán','249',20),(1256,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Etlatongo','250',20),(1257,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Nejápam','251',20),(1258,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Peñasco','252',20),(1259,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Piñas','253',20),(1260,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Río Hondo','254',20),(1261,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Sindihui','255',20),(1262,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Tlapiltepec','256',20),(1263,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Melchor Betaza','257',20),(1264,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Achiutla','258',20),(1265,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Ahuehuetitlán','259',20),(1266,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Aloápam','260',20),(1267,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Amatitlán','261',20),(1268,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Amatlán','262',20),(1269,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Coatlán','263',20),(1270,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Chicahua','264',20),(1271,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Chimalapa','265',20),(1272,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel del Puerto','266',20),(1273,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel del Río','267',20),(1274,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Ejutla','268',20),(1275,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel el Grande','269',20),(1276,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Huautla','270',20),(1277,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Mixtepec','271',20),(1278,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Panixtlahuaca','272',20),(1279,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Peras','273',20),(1280,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Piedras','274',20),(1281,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Quetzaltepec','275',20),(1282,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Santa Flor','276',20),(1283,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Sola de Vega','277',20),(1284,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Soyaltepec','278',20),(1285,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Suchixtepec','279',20),(1286,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Talea de Castro','280',20),(1287,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Tecomatlán','281',20),(1288,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Tenango','282',20),(1289,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Tequixtepec','283',20),(1290,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Tilquiápam','284',20),(1291,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Tlacamama','285',20),(1292,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Tlacotepec','286',20),(1293,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Tulancingo','287',20),(1294,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Yotao','288',20),(1295,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Nicolás','289',20),(1296,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Nicolás Hidalgo','290',20),(1297,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo Coatlán','291',20),(1298,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo Cuatro Venados','292',20),(1299,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo Etla','293',20),(1300,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo Huitzo','294',20),(1301,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo Huixtepec','295',20),(1302,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo Macuiltianguis','296',20),(1303,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo Tijaltepec','297',20),(1304,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo Villa de Mitla','298',20),(1305,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo Yaganiza','299',20),(1306,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Amuzgos','300',20),(1307,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Apóstol','301',20),(1308,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Atoyac','302',20),(1309,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Cajonos','303',20),(1310,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Coxcaltepec Cántaros','304',20),(1311,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Comitancillo','305',20),(1312,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro el Alto','306',20),(1313,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Huamelula','307',20),(1314,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Huilotepec','308',20),(1315,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Ixcatlán','309',20),(1316,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Ixtlahuaca','310',20),(1317,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Jaltepetongo','311',20),(1318,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Jicayán','312',20),(1319,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Jocotipac','313',20),(1320,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Juchatengo','314',20),(1321,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Mártir','315',20),(1322,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Mártir Quiechapa','316',20),(1323,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Mártir Yucuxaco','317',20),(1324,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Mixtepec','318',20),(1325,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Mixtepec','319',20),(1326,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Molinos','320',20),(1327,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Nopala','321',20),(1328,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Ocopetatillo','322',20),(1329,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Ocotepec','323',20),(1330,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Pochutla','324',20),(1331,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Quiatoni','325',20),(1332,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Sochiápam','326',20),(1333,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Tapanatepec','327',20),(1334,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Taviche','328',20),(1335,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Teozacoalco','329',20),(1336,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Teutila','330',20),(1337,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Tidaá','331',20),(1338,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Topiltepec','332',20),(1339,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Totolápam','333',20),(1340,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Tututepec','334',20),(1341,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Yaneri','335',20),(1342,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Yólox','336',20),(1343,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro y San Pablo Ayutla','337',20),(1344,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Etla','338',20),(1345,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro y San Pablo Teposcolula','339',20),(1346,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro y San Pablo Tequixtepec','340',20),(1347,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Yucunama','341',20),(1348,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Raymundo Jalpan','342',20),(1349,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Sebastián Abasolo','343',20),(1350,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Sebastián Coatlán','344',20),(1351,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Sebastián Ixcapa','345',20),(1352,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Sebastián Nicananduta','346',20),(1353,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Sebastián Río Hondo','347',20),(1354,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Sebastián Tecomaxtlahuaca','348',20),(1355,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Sebastián Teitipac','349',20),(1356,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Sebastián Tutla','350',20),(1357,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Simón Almolongas','351',20),(1358,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Simón Zahuatlán','352',20),(1359,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana','353',20),(1360,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana Ateixtlahuaca','354',20),(1361,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana Cuauhtémoc','355',20),(1362,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana del Valle','356',20),(1363,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana Tavela','357',20),(1364,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana Tlapacoyan','358',20),(1365,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana Yareni','359',20),(1366,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana Zegache','360',20),(1367,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catalina Quierí','361',20),(1368,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Cuixtla','362',20),(1369,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Ixtepeji','363',20),(1370,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Juquila','364',20),(1371,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Lachatao','365',20),(1372,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Loxicha','366',20),(1373,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Mechoacán','367',20),(1374,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Minas','368',20),(1375,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Quiané','369',20),(1376,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Tayata','370',20),(1377,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Ticuá','371',20),(1378,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Yosonotú','372',20),(1379,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Zapoquila','373',20),(1380,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Acatepec','374',20),(1381,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Amilpas','375',20),(1382,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz de Bravo','376',20),(1383,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Itundujia','377',20),(1384,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Mixtepec','378',20),(1385,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Nundaco','379',20),(1386,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Papalutla','380',20),(1387,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Tacache de Mina','381',20),(1388,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Tacahua','382',20),(1389,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Tayata','383',20),(1390,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Xitla','384',20),(1391,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Xoxocotlán','385',20),(1392,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Zenzontepec','386',20),(1393,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Gertrudis','387',20),(1394,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Inés del Monte','388',20),(1395,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Inés Yatzeche','389',20),(1396,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Lucía del Camino','390',20),(1397,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Lucía Miahuatlán','391',20),(1398,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Lucía Monteverde','392',20),(1399,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Lucía Ocotlán','393',20),(1400,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Alotepec','394',20),(1401,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Apazco','395',20),(1402,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María la Asunción','396',20),(1403,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Heroica Ciudad de Tlaxiaco','397',20),(1404,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ayoquezco de Aldama','398',20),(1405,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Atzompa','399',20),(1406,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Camotlán','400',20),(1407,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Colotepec','401',20),(1408,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Cortijo','402',20),(1409,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Coyotepec','403',20),(1410,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Chachoápam','404',20),(1411,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Chilapa de Díaz','405',20),(1412,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Chilchotla','406',20),(1413,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Chimalapa','407',20),(1414,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María del Rosario','408',20),(1415,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María del Tule','409',20),(1416,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Ecatepec','410',20),(1417,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Guelacé','411',20),(1418,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Guienagati','412',20),(1419,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Huatulco','413',20),(1420,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Huazolotitlán','414',20),(1421,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Ipalapa','415',20),(1422,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Ixcatlán','416',20),(1423,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Jacatepec','417',20),(1424,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Jalapa del Marqués','418',20),(1425,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Jaltianguis','419',20),(1426,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Lachixío','420',20),(1427,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Mixtequilla','421',20),(1428,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Nativitas','422',20),(1429,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Nduayaco','423',20),(1430,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Ozolotepec','424',20),(1431,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Pápalo','425',20),(1432,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Peñoles','426',20),(1433,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Petapa','427',20),(1434,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Quiegolani','428',20),(1435,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Sola','429',20),(1436,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Tataltepec','430',20),(1437,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Tecomavaca','431',20),(1438,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Temaxcalapa','432',20),(1439,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Temaxcaltepec','433',20),(1440,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Teopoxco','434',20),(1441,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Tepantlali','435',20),(1442,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Texcatitlán','436',20),(1443,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Tlahuitoltepec','437',20),(1444,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Tlalixtac','438',20),(1445,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Tonameca','439',20),(1446,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Totolapilla','440',20),(1447,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Xadani','441',20),(1448,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Yalina','442',20),(1449,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Yavesía','443',20),(1450,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Yolotepec','444',20),(1451,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Yosoyúa','445',20),(1452,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Yucuhiti','446',20),(1453,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Zacatepec','447',20),(1454,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Zaniza','448',20),(1455,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María Zoquitlán','449',20),(1456,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Amoltepec','450',20),(1457,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Apoala','451',20),(1458,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Apóstol','452',20),(1459,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Astata','453',20),(1460,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Atitlán','454',20),(1461,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Ayuquililla','455',20),(1462,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Cacaloxtepec','456',20),(1463,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Camotlán','457',20),(1464,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Comaltepec','458',20),(1465,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Chazumba','459',20),(1466,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Choápam','460',20),(1467,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago del Río','461',20),(1468,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Huajolotitlán','462',20),(1469,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Huauclilla','463',20),(1470,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Ihuitlán Plumas','464',20),(1471,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Ixcuintepec','465',20),(1472,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Ixtayutla','466',20),(1473,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Jamiltepec','467',20),(1474,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Jocotepec','468',20),(1475,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Juxtlahuaca','469',20),(1476,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Lachiguiri','470',20),(1477,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Lalopa','471',20),(1478,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Laollaga','472',20),(1479,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Laxopa','473',20),(1480,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Llano Grande','474',20),(1481,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Matatlán','475',20),(1482,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Miltepec','476',20),(1483,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Minas','477',20),(1484,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Nacaltepec','478',20),(1485,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Nejapilla','479',20),(1486,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Nundiche','480',20),(1487,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Nuyoó','481',20),(1488,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Pinotepa Nacional','482',20),(1489,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Suchilquitongo','483',20),(1490,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Tamazola','484',20),(1491,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Tapextla','485',20),(1492,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Tejúpam de la Unión','486',20),(1493,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Tenango','487',20),(1494,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Tepetlapa','488',20),(1495,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Tetepec','489',20),(1496,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Texcalcingo','490',20),(1497,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Textitlán','491',20),(1498,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Tilantongo','492',20),(1499,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Tillo','493',20),(1500,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Tlazoyaltepec','494',20),(1501,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Xanica','495',20),(1502,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Xiacuí','496',20),(1503,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Yaitepec','497',20),(1504,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Yaveo','498',20),(1505,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Yolomécatl','499',20),(1506,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Yosondúa','500',20),(1507,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Yucuyachi','501',20),(1508,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Zacatepec','502',20),(1509,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Zoochila','503',20),(1510,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nuevo Zoquiápam','504',20),(1511,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Ingenio','505',20),(1512,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Albarradas','506',20),(1513,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Armenta','507',20),(1514,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Chihuitán','508',20),(1515,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo de Morelos','509',20),(1516,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Ixcatlán','510',20),(1517,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Nuxaá','511',20),(1518,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Ozolotepec','512',20),(1519,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Petapa','513',20),(1520,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Roayaga','514',20),(1521,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Tehuantepec','515',20),(1522,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Teojomulco','516',20),(1523,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Tepuxtepec','517',20),(1524,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Tlatayápam','518',20),(1525,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Tomaltepec','519',20),(1526,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Tonalá','520',20),(1527,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Tonaltepec','521',20),(1528,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Xagacía','522',20),(1529,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Yanhuitlán','523',20),(1530,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Yodohino','524',20),(1531,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo Zanatepec','525',20),(1532,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santos Reyes Nopala','526',20),(1533,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santos Reyes Pápalo','527',20),(1534,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santos Reyes Tepejillo','528',20),(1535,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santos Reyes Yucuná','529',20),(1536,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Tomás Jalieza','530',20),(1537,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Tomás Mazaltepec','531',20),(1538,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Tomás Ocotepec','532',20),(1539,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Tomás Tamazulapan','533',20),(1540,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Vicente Coatlán','534',20),(1541,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Vicente Lachixío','535',20),(1542,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Vicente Nuñú','536',20),(1543,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Silacayoápam','537',20),(1544,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sitio de Xitlapehua','538',20),(1545,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soledad Etla','539',20),(1546,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Tamazulápam del Progreso','540',20),(1547,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tanetze de Zaragoza','541',20),(1548,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Taniche','542',20),(1549,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tataltepec de Valdés','543',20),(1550,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teococuilco de Marcos Pérez','544',20),(1551,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teotitlán de Flores Magón','545',20),(1552,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teotitlán del Valle','546',20),(1553,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teotongo','547',20),(1554,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepelmeme Villa de Morelos','548',20),(1555,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Heroica Villa Tezoatlán de Segura y Luna, Cuna de la Independencia de Oaxaca','549',20),(1556,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jerónimo Tlacochahuaya','550',20),(1557,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlacolula de Matamoros','551',20),(1558,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlacotepec Plumas','552',20),(1559,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlalixtac de Cabrera','553',20),(1560,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Totontepec Villa de Morelos','554',20),(1561,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Trinidad Zaachila','555',20),(1562,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Trinidad Vista Hermosa','556',20),(1563,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Unión Hidalgo','557',20),(1564,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Valerio Trujano','558',20),(1565,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Bautista Valle Nacional','559',20),(1566,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Díaz Ordaz','560',20),(1567,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yaxe','561',20),(1568,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena Yodocono de Porfirio Díaz','562',20),(1569,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yogana','563',20),(1570,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yutanduchi de Guerrero','564',20),(1571,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Zaachila','565',20),(1572,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Mateo Yucutindoo','566',20),(1573,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotitlán Lagunas','567',20),(1574,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotitlán Palmas','568',20),(1575,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Inés de Zaragoza','569',20),(1576,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zimatlán de Álvarez','570',20),(1577,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acajete','001',21),(1578,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acateno','002',21),(1579,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acatlán','003',21),(1580,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acatzingo','004',21),(1581,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acteopan','005',21),(1582,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ahuacatlán','006',21),(1583,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ahuatlán','007',21),(1584,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ahuazotepec','008',21),(1585,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ahuehuetitla','009',21),(1586,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ajalpan','010',21),(1587,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Albino Zertuche','011',21),(1588,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aljojuca','012',21),(1589,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Altepexi','013',21),(1590,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amixtlán','014',21),(1591,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amozoc','015',21),(1592,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aquixtla','016',21),(1593,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atempan','017',21),(1594,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atexcal','018',21),(1595,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atlixco','019',21),(1596,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atoyatempan','020',21),(1597,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atzala','021',21),(1598,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atzitzihuacán','022',21),(1599,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atzitzintla','023',21),(1600,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Axutla','024',21),(1601,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ayotoxco de Guerrero','025',21),(1602,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calpan','026',21),(1603,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Caltepec','027',21),(1604,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Camocuautla','028',21),(1605,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Caxhuacan','029',21),(1606,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coatepec','030',21),(1607,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coatzingo','031',21),(1608,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cohetzala','032',21),(1609,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cohuecan','033',21),(1610,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coronango','034',21),(1611,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coxcatlán','035',21),(1612,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coyomeapan','036',21),(1613,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coyotepec','037',21),(1614,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuapiaxtla de Madero','038',21),(1615,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuautempan','039',21),(1616,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuautinchán','040',21),(1617,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuautlancingo','041',21),(1618,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuayuca de Andrade','042',21),(1619,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuetzalan del Progreso','043',21),(1620,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuyoaco','044',21),(1621,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chalchicomula de Sesma','045',21),(1622,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chapulco','046',21),(1623,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiautla','047',21),(1624,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiautzingo','048',21),(1625,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiconcuautla','049',21),(1626,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chichiquila','050',21),(1627,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chietla','051',21),(1628,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chigmecatitlán','052',21),(1629,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chignahuapan','053',21),(1630,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chignautla','054',21),(1631,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chila','055',21),(1632,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chila de la Sal','056',21),(1633,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Honey','057',21),(1634,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chilchotla','058',21),(1635,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chinantla','059',21),(1636,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Domingo Arenas','060',21),(1637,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Eloxochitlán','061',21),(1638,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Epatlán','062',21),(1639,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Esperanza','063',21),(1640,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Francisco Z. Mena','064',21),(1641,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Felipe Ángeles','065',21),(1642,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalupe','066',21),(1643,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalupe Victoria','067',21),(1644,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hermenegildo Galeana','068',21),(1645,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huaquechula','069',21),(1646,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huatlatlauca','070',21),(1647,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huauchinango','071',21),(1648,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huehuetla','072',21),(1649,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huehuetlán el Chico','073',21),(1650,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huejotzingo','074',21),(1651,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hueyapan','075',21),(1652,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hueytamalco','076',21),(1653,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hueytlalpan','077',21),(1654,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huitzilan de Serdán','078',21),(1655,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huitziltepec','079',21),(1656,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atlequizayan','080',21),(1657,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixcamilpa de Guerrero','081',21),(1658,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixcaquixtla','082',21),(1659,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtacamaxtitlán','083',21),(1660,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtepec','084',21),(1661,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Izúcar de Matamoros','085',21),(1662,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jalpan','086',21),(1663,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jolalpan','087',21),(1664,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jonotla','088',21),(1665,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jopala','089',21),(1666,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juan C. Bonilla','090',21),(1667,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juan Galindo','091',21),(1668,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juan N. Méndez','092',21),(1669,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lafragua','093',21),(1670,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Libres','094',21),(1671,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Magdalena Tlatlauquitepec','095',21),(1672,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mazapiltepec de Juárez','096',21),(1673,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mixtla','097',21),(1674,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Molcaxac','098',21),(1675,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cañada Morelos','099',21),(1676,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Naupan','100',21),(1677,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nauzontla','101',21),(1678,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nealtican','102',21),(1679,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nicolás Bravo','103',21),(1680,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nopalucan','104',21),(1681,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocotepec','105',21),(1682,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocoyucan','106',21),(1683,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Olintla','107',21),(1684,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Oriental','108',21),(1685,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pahuatlán','109',21),(1686,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Palmar de Bravo','110',21),(1687,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pantepec','111',21),(1688,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Petlalcingo','112',21),(1689,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Piaxtla','113',21),(1690,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Puebla','114',21),(1691,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Quecholac','115',21),(1692,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Quimixtlán','116',21),(1693,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rafael Lara Grajales','117',21),(1694,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Los Reyes de Juárez','118',21),(1695,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Cholula','119',21),(1696,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonio Cañada','120',21),(1697,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Diego la Mesa Tochimiltzingo','121',21),(1698,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Felipe Teotlalcingo','122',21),(1699,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Felipe Tepatlán','123',21),(1700,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Gabriel Chilac','124',21),(1701,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Gregorio Atzompa','125',21),(1702,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jerónimo Tecuanipan','126',21),(1703,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jerónimo Xayacatlán','127',21),(1704,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José Chiapa','128',21),(1705,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José Miahuatlán','129',21),(1706,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Atenco','130',21),(1707,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Atzompa','131',21),(1708,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Texmelucan','132',21),(1709,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Totoltepec','133',21),(1710,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Matías Tlalancaleca','134',21),(1711,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Ixitlán','135',21),(1712,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel Xoxtla','136',21),(1713,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Nicolás Buenos Aires','137',21),(1714,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Nicolás de los Ranchos','138',21),(1715,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo Anicano','139',21),(1716,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Cholula','140',21),(1717,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro Yeloixtlahuaca','141',21),(1718,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Salvador el Seco','142',21),(1719,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Salvador el Verde','143',21),(1720,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Salvador Huixcolotla','144',21),(1721,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Sebastián Tlacotepec','145',21),(1722,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Tlaltempan','146',21),(1723,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Inés Ahuatempan','147',21),(1724,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Isabel Cholula','148',21),(1725,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Miahuatlán','149',21),(1726,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huehuetlán el Grande','150',21),(1727,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Tomás Hueyotlipan','151',21),(1728,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soltepec','152',21),(1729,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecali de Herrera','153',21),(1730,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecamachalco','154',21),(1731,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecomatlán','155',21),(1732,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tehuacán','156',21),(1733,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tehuitzingo','157',21),(1734,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenampulco','158',21),(1735,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teopantlán','159',21),(1736,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teotlalco','160',21),(1737,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepanco de López','161',21),(1738,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepango de Rodríguez','162',21),(1739,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepatlaxco de Hidalgo','163',21),(1740,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepeaca','164',21),(1741,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepemaxalco','165',21),(1742,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepeojuma','166',21),(1743,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepetzintla','167',21),(1744,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepexco','168',21),(1745,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepexi de Rodríguez','169',21),(1746,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepeyahualco','170',21),(1747,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepeyahualco de Cuauhtémoc','171',21),(1748,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tetela de Ocampo','172',21),(1749,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teteles de Avila Castillo','173',21),(1750,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teziutlán','174',21),(1751,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tianguismanalco','175',21),(1752,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tilapa','176',21),(1753,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlacotepec de Benito Juárez','177',21),(1754,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlacuilotepec','178',21),(1755,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlachichuca','179',21),(1756,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlahuapan','180',21),(1757,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaltenango','181',21),(1758,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlanepantla','182',21),(1759,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaola','183',21),(1760,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlapacoya','184',21),(1761,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlapanalá','185',21),(1762,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlatlauquitepec','186',21),(1763,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaxco','187',21),(1764,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tochimilco','188',21),(1765,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tochtepec','189',21),(1766,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Totoltepec de Guerrero','190',21),(1767,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tulcingo','191',21),(1768,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuzamapan de Galeana','192',21),(1769,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tzicatlacoyan','193',21),(1770,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Venustiano Carranza','194',21),(1771,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Vicente Guerrero','195',21),(1772,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xayacatlán de Bravo','196',21),(1773,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xicotepec','197',21),(1774,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xicotlán','198',21),(1775,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xiutetelco','199',21),(1776,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xochiapulco','200',21),(1777,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xochiltepec','201',21),(1778,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xochitlán de Vicente Suárez','202',21),(1779,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xochitlán Todos Santos','203',21),(1780,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yaonáhuac','204',21),(1781,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yehualtepec','205',21),(1782,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacapala','206',21),(1783,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacapoaxtla','207',21),(1784,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacatlán','208',21),(1785,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotitlán','209',21),(1786,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zapotitlán de Méndez','210',21),(1787,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zaragoza','211',21),(1788,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zautla','212',21),(1789,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zihuateutla','213',21),(1790,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zinacatepec','214',21),(1791,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zongozotla','215',21),(1792,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zoquiapan','216',21),(1793,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zoquitlán','217',21),(1794,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amealco de Bonfil','001',22),(1795,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pinal de Amoles','002',22),(1796,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Arroyo Seco','003',22),(1797,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cadereyta de Montes','004',22),(1798,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Colón','005',22),(1799,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Corregidora','006',22),(1800,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ezequiel Montes','007',22),(1801,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huimilpan','008',22),(1802,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jalpan de Serra','009',22),(1803,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Landa de Matamoros','010',22),(1804,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Marqués','011',22),(1805,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pedro Escobedo','012',22),(1806,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Peñamiller','013',22),(1807,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Querétaro','014',22),(1808,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Joaquín','015',22),(1809,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan del Río','016',22),(1810,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tequisquiapan','017',22),(1811,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tolimán','018',22),(1812,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cozumel','001',23),(1813,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Felipe Carrillo Puerto','002',23),(1814,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Isla Mujeres','003',23),(1815,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Othón P. Blanco','004',23),(1816,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Benito Juárez','005',23),(1817,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','José María Morelos','006',23),(1818,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lázaro Cárdenas','007',23),(1819,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Solidaridad','008',23),(1820,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tulum','009',23),(1821,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bacalar','010',23),(1822,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Puerto Morelos','011',23),(1823,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ahualulco','001',24),(1824,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Alaquines','002',24),(1825,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aquismón','003',24),(1826,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Armadillo de los Infante','004',24),(1827,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cárdenas','005',24),(1828,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Catorce','006',24),(1829,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cedral','007',24),(1830,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cerritos','008',24),(1831,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cerro de San Pedro','009',24),(1832,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ciudad del Maíz','010',24),(1833,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ciudad Fernández','011',24),(1834,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tancanhuitz','012',24),(1835,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ciudad Valles','013',24),(1836,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coxcatlán','014',24),(1837,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Charcas','015',24),(1838,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ebano','016',24),(1839,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalcázar','017',24),(1840,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huehuetlán','018',24),(1841,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lagunillas','019',24),(1842,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Matehuala','020',24),(1843,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mexquitic de Carmona','021',24),(1844,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Moctezuma','022',24),(1845,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rayón','023',24),(1846,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rioverde','024',24),(1847,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Salinas','025',24),(1848,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Antonio','026',24),(1849,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Ciro de Acosta','027',24),(1850,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Luis Potosí','028',24),(1851,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Martín Chalchicuautla','029',24),(1852,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Nicolás Tolentino','030',24),(1853,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina','031',24),(1854,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María del Río','032',24),(1855,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santo Domingo','033',24),(1856,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Vicente Tancuayalab','034',24),(1857,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soledad de Graciano Sánchez','035',24),(1858,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tamasopo','036',24),(1859,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tamazunchale','037',24),(1860,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tampacán','038',24),(1861,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tampamolón Corona','039',24),(1862,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tamuín','040',24),(1863,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tanlajás','041',24),(1864,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tanquián de Escobedo','042',24),(1865,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tierra Nueva','043',24),(1866,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Vanegas','044',24),(1867,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Venado','045',24),(1868,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Arriaga','046',24),(1869,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Guadalupe','047',24),(1870,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de la Paz','048',24),(1871,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Ramos','049',24),(1872,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Reyes','050',24),(1873,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Hidalgo','051',24),(1874,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Juárez','052',24),(1875,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Axtla de Terrazas','053',24),(1876,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xilitla','054',24),(1877,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zaragoza','055',24),(1878,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Arista','056',24),(1879,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Matlapa','057',24),(1880,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Naranjo','058',24),(1881,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ahome','001',25),(1882,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Angostura','002',25),(1883,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Badiraguato','003',25),(1884,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Concordia','004',25),(1885,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cosalá','005',25),(1886,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Culiacán','006',25),(1887,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Choix','007',25),(1888,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Elota','008',25),(1889,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Escuinapa','009',25),(1890,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Fuerte','010',25),(1891,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guasave','011',25),(1892,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mazatlán','012',25),(1893,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mocorito','013',25),(1894,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rosario','014',25),(1895,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Salvador Alvarado','015',25),(1896,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Ignacio','016',25),(1897,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sinaloa','017',25),(1898,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Navolato','018',25),(1899,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aconchi','001',26),(1900,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Agua Prieta','002',26),(1901,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Alamos','003',26),(1902,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Altar','004',26),(1903,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Arivechi','005',26),(1904,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Arizpe','006',26),(1905,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atil','007',26),(1906,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bacadéhuachi','008',26),(1907,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bacanora','009',26),(1908,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bacerac','010',26),(1909,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bacoachi','011',26),(1910,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bácum','012',26),(1911,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Banámichi','013',26),(1912,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Baviácora','014',26),(1913,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bavispe','015',26),(1914,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Benjamín Hill','016',26),(1915,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Caborca','017',26),(1916,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cajeme','018',26),(1917,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cananea','019',26),(1918,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Carbó','020',26),(1919,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Colorada','021',26),(1920,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cucurpe','022',26),(1921,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cumpas','023',26),(1922,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Divisaderos','024',26),(1923,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Empalme','025',26),(1924,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Etchojoa','026',26),(1925,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Fronteras','027',26),(1926,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Granados','028',26),(1927,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guaymas','029',26),(1928,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hermosillo','030',26),(1929,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huachinera','031',26),(1930,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huásabas','032',26),(1931,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huatabampo','033',26),(1932,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huépac','034',26),(1933,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Imuris','035',26),(1934,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena','036',26),(1935,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mazatán','037',26),(1936,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Moctezuma','038',26),(1937,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Naco','039',26),(1938,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nácori Chico','040',26),(1939,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nacozari de García','041',26),(1940,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Navojoa','042',26),(1941,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nogales','043',26),(1942,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Onavas','044',26),(1943,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Opodepe','045',26),(1944,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Oquitoa','046',26),(1945,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pitiquito','047',26),(1946,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Puerto Peñasco','048',26),(1947,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Quiriego','049',26),(1948,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rayón','050',26),(1949,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rosario','051',26),(1950,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sahuaripa','052',26),(1951,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Felipe de Jesús','053',26),(1952,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Javier','054',26),(1953,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Luis Río Colorado','055',26),(1954,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Miguel de Horcasitas','056',26),(1955,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pedro de la Cueva','057',26),(1956,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana','058',26),(1957,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz','059',26),(1958,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sáric','060',26),(1959,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soyopa','061',26),(1960,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Suaqui Grande','062',26),(1961,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepache','063',26),(1962,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Trincheras','064',26),(1963,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tubutama','065',26),(1964,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ures','066',26),(1965,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Hidalgo','067',26),(1966,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Pesqueira','068',26),(1967,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yécora','069',26),(1968,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Plutarco Elías Calles','070',26),(1969,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Benito Juárez','071',26),(1970,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Ignacio Río Muerto','072',26),(1971,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Balancán','001',27),(1972,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cárdenas','002',27),(1973,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Centla','003',27),(1974,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Centro','004',27),(1975,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Comalcalco','005',27),(1976,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cunduacán','006',27),(1977,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Emiliano Zapata','007',27),(1978,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huimanguillo','008',27),(1979,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jalapa','009',27),(1980,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jalpa de Méndez','010',27),(1981,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jonuta','011',27),(1982,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Macuspana','012',27),(1983,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nacajuca','013',27),(1984,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Paraíso','014',27),(1985,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tacotalpa','015',27),(1986,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teapa','016',27),(1987,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenosique','017',27),(1988,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Abasolo','001',28),(1989,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aldama','002',28),(1990,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Altamira','003',28),(1991,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Antiguo Morelos','004',28),(1992,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Burgos','005',28),(1993,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bustamante','006',28),(1994,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Camargo','007',28),(1995,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Casas','008',28),(1996,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ciudad Madero','009',28),(1997,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cruillas','010',28),(1998,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Gómez Farías','011',28),(1999,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','González','012',28),(2000,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Güémez','013',28),(2001,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guerrero','014',28),(2002,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Gustavo Díaz Ordaz','015',28),(2003,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hidalgo','016',28),(2004,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jaumave','017',28),(2005,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jiménez','018',28),(2006,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Llera','019',28),(2007,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mainero','020',28),(2008,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Mante','021',28),(2009,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Matamoros','022',28),(2010,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Méndez','023',28),(2011,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mier','024',28),(2012,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Miguel Alemán','025',28),(2013,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Miquihuana','026',28),(2014,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nuevo Laredo','027',28),(2015,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nuevo Morelos','028',28),(2016,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ocampo','029',28),(2017,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Padilla','030',28),(2018,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Palmillas','031',28),(2019,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Reynosa','032',28),(2020,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Río Bravo','033',28),(2021,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Carlos','034',28),(2022,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Fernando','035',28),(2023,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Nicolás','036',28),(2024,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soto la Marina','037',28),(2025,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tampico','038',28),(2026,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tula','039',28),(2027,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Valle Hermoso','040',28),(2028,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Victoria','041',28),(2029,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villagrán','042',28),(2030,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xicoténcatl','043',28),(2031,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amaxac de Guerrero','001',29),(2032,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apetatitlán de Antonio Carvajal','002',29),(2033,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atlangatepec','003',29),(2034,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atltzayanca','004',29),(2035,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apizaco','005',29),(2036,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calpulalpan','006',29),(2037,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Carmen Tequexquitla','007',29),(2038,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuapiaxtla','008',29),(2039,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuaxomulco','009',29),(2040,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiautempan','010',29),(2041,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Muñoz de Domingo Arenas','011',29),(2042,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Españita','012',29),(2043,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huamantla','013',29),(2044,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hueyotlipan','014',29),(2045,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtacuixtla de Mariano Matamoros','015',29),(2046,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtenco','016',29),(2047,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mazatecochco de José María Morelos','017',29),(2048,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Contla de Juan Cuamatzi','018',29),(2049,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepetitla de Lardizábal','019',29),(2050,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sanctórum de Lázaro Cárdenas','020',29),(2051,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nanacamilpa de Mariano Arista','021',29),(2052,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acuamanala de Miguel Hidalgo','022',29),(2053,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Natívitas','023',29),(2054,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Panotla','024',29),(2055,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Pablo del Monte','025',29),(2056,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Tlaxcala','026',29),(2057,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenancingo','027',29),(2058,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teolocholco','028',29),(2059,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepeyanco','029',29),(2060,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Terrenate','030',29),(2061,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tetla de la Solidaridad','031',29),(2062,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tetlatlahuca','032',29),(2063,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaxcala','033',29),(2064,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaxco','034',29),(2065,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tocatlán','035',29),(2066,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Totolac','036',29),(2067,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ziltlaltépec de Trinidad Sánchez Santos','037',29),(2068,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tzompantepec','038',29),(2069,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xaloztoc','039',29),(2070,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xaltocan','040',29),(2071,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Papalotla de Xicohténcatl','041',29),(2072,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xicohtzinco','042',29),(2073,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yauhquemehcan','043',29),(2074,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacatelco','044',29),(2075,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Benito Juárez','045',29),(2076,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Emiliano Zapata','046',29),(2077,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lázaro Cárdenas','047',29),(2078,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Magdalena Tlaltelulco','048',29),(2079,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Damián Texóloc','049',29),(2080,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Francisco Tetlanohcan','050',29),(2081,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Jerónimo Zacualpan','051',29),(2082,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San José Teacalco','052',29),(2083,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Huactzinco','053',29),(2084,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lorenzo Axocomanitla','054',29),(2085,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Lucas Tecopilco','055',29),(2086,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Ana Nopalucan','056',29),(2087,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Apolonia Teacalco','057',29),(2088,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Catarina Ayometla','058',29),(2089,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Cruz Quilehtla','059',29),(2090,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Isabel Xiloxoxtla','060',29),(2091,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acajete','001',30),(2092,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acatlán','002',30),(2093,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acayucan','003',30),(2094,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Actopan','004',30),(2095,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acula','005',30),(2096,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acultzingo','006',30),(2097,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Camarón de Tejeda','007',30),(2098,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Alpatláhuac','008',30),(2099,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Alto Lucero de Gutiérrez Barrios','009',30),(2100,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Altotonga','010',30),(2101,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Alvarado','011',30),(2102,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amatitlán','012',30),(2103,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Naranjos Amatlán','013',30),(2104,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Amatlán de los Reyes','014',30),(2105,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Angel R. Cabada','015',30),(2106,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Antigua','016',30),(2107,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apazapan','017',30),(2108,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Aquila','018',30),(2109,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Astacinga','019',30),(2110,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atlahuilco','020',30),(2111,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atoyac','021',30),(2112,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atzacan','022',30),(2113,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atzalan','023',30),(2114,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaltetela','024',30),(2115,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ayahualulco','025',30),(2116,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Banderilla','026',30),(2117,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Benito Juárez','027',30),(2118,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Boca del Río','028',30),(2119,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calcahualco','029',30),(2120,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Camerino Z. Mendoza','030',30),(2121,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Carrillo Puerto','031',30),(2122,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Catemaco','032',30),(2123,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cazones de Herrera','033',30),(2124,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cerro Azul','034',30),(2125,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Citlaltépetl','035',30),(2126,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coacoatzintla','036',30),(2127,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coahuitlán','037',30),(2128,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coatepec','038',30),(2129,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coatzacoalcos','039',30),(2130,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coatzintla','040',30),(2131,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coetzala','041',30),(2132,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Colipa','042',30),(2133,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Comapa','043',30),(2134,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Córdoba','044',30),(2135,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cosamaloapan de Carpio','045',30),(2136,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cosautlán de Carvajal','046',30),(2137,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coscomatepec','047',30),(2138,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cosoleacaque','048',30),(2139,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cotaxtla','049',30),(2140,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coxquihui','050',30),(2141,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Coyutla','051',30),(2142,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuichapa','052',30),(2143,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuitláhuac','053',30),(2144,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chacaltianguis','054',30),(2145,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chalma','055',30),(2146,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiconamel','056',30),(2147,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chiconquiaco','057',30),(2148,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chicontepec','058',30),(2149,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chinameca','059',30),(2150,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chinampa de Gorostiza','060',30),(2151,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Las Choapas','061',30),(2152,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chocamán','062',30),(2153,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chontla','063',30),(2154,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chumatlán','064',30),(2155,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Emiliano Zapata','065',30),(2156,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Espinal','066',30),(2157,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Filomeno Mata','067',30),(2158,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Fortín','068',30),(2159,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Gutiérrez Zamora','069',30),(2160,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hidalgotitlán','070',30),(2161,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huatusco','071',30),(2162,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huayacocotla','072',30),(2163,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hueyapan de Ocampo','073',30),(2164,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huiloapan de Cuauhtémoc','074',30),(2165,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ignacio de la Llave','075',30),(2166,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ilamatlán','076',30),(2167,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Isla','077',30),(2168,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixcatepec','078',30),(2169,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixhuacán de los Reyes','079',30),(2170,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixhuatlán del Café','080',30),(2171,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixhuatlancillo','081',30),(2172,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixhuatlán del Sureste','082',30),(2173,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixhuatlán de Madero','083',30),(2174,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixmatlahuacan','084',30),(2175,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixtaczoquitlán','085',30),(2176,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jalacingo','086',30),(2177,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xalapa','087',30),(2178,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jalcomulco','088',30),(2179,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jáltipan','089',30),(2180,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jamapa','090',30),(2181,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jesús Carranza','091',30),(2182,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xico','092',30),(2183,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jilotepec','093',30),(2184,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juan Rodríguez Clara','094',30),(2185,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juchique de Ferrer','095',30),(2186,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Landero y Coss','096',30),(2187,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Lerdo de Tejada','097',30),(2188,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Magdalena','098',30),(2189,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Maltrata','099',30),(2190,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Manlio Fabio Altamirano','100',30),(2191,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mariano Escobedo','101',30),(2192,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Martínez de la Torre','102',30),(2193,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mecatlán','103',30),(2194,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mecayapan','104',30),(2195,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Medellín de Bravo','105',30),(2196,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Miahuatlán','106',30),(2197,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Las Minas','107',30),(2198,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Minatitlán','108',30),(2199,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Misantla','109',30),(2200,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mixtla de Altamirano','110',30),(2201,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Moloacán','111',30),(2202,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Naolinco','112',30),(2203,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Naranjal','113',30),(2204,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nautla','114',30),(2205,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nogales','115',30),(2206,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Oluta','116',30),(2207,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Omealca','117',30),(2208,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Orizaba','118',30),(2209,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Otatitlán','119',30),(2210,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Oteapan','120',30),(2211,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ozuluama de Mascareñas','121',30),(2212,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pajapan','122',30),(2213,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pánuco','123',30),(2214,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Papantla','124',30),(2215,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Paso del Macho','125',30),(2216,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Paso de Ovejas','126',30),(2217,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','La Perla','127',30),(2218,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Perote','128',30),(2219,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Platón Sánchez','129',30),(2220,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Playa Vicente','130',30),(2221,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Poza Rica de Hidalgo','131',30),(2222,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Las Vigas de Ramírez','132',30),(2223,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pueblo Viejo','133',30),(2224,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Puente Nacional','134',30),(2225,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rafael Delgado','135',30),(2226,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Rafael Lucio','136',30),(2227,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Los Reyes','137',30),(2228,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Río Blanco','138',30),(2229,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Saltabarranca','139',30),(2230,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Tenejapan','140',30),(2231,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Andrés Tuxtla','141',30),(2232,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Juan Evangelista','142',30),(2233,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Tuxtla','143',30),(2234,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sayula de Alemán','144',30),(2235,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soconusco','145',30),(2236,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sochiapa','146',30),(2237,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soledad Atzompa','147',30),(2238,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soledad de Doblado','148',30),(2239,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Soteapan','149',30),(2240,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tamalín','150',30),(2241,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tamiahua','151',30),(2242,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tampico Alto','152',30),(2243,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tancoco','153',30),(2244,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tantima','154',30),(2245,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tantoyuca','155',30),(2246,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tatatila','156',30),(2247,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Castillo de Teayo','157',30),(2248,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecolutla','158',30),(2249,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tehuipango','159',30),(2250,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Álamo Temapache','160',30),(2251,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tempoal','161',30),(2252,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenampa','162',30),(2253,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tenochtitlán','163',30),(2254,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teocelo','164',30),(2255,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepatlaxco','165',30),(2256,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepetlán','166',30),(2257,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepetzintla','167',30),(2258,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tequila','168',30),(2259,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','José Azueta','169',30),(2260,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Texcatepec','170',30),(2261,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Texhuacán','171',30),(2262,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Texistepec','172',30),(2263,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tezonapa','173',30),(2264,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tierra Blanca','174',30),(2265,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tihuatlán','175',30),(2266,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlacojalpan','176',30),(2267,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlacolulan','177',30),(2268,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlacotalpan','178',30),(2269,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlacotepec de Mejía','179',30),(2270,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlachichilco','180',30),(2271,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlalixcoyan','181',30),(2272,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlalnelhuayocan','182',30),(2273,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlapacoyan','183',30),(2274,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaquilpa','184',30),(2275,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlilapan','185',30),(2276,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tomatlán','186',30),(2277,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tonayán','187',30),(2278,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Totutla','188',30),(2279,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuxpan','189',30),(2280,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tuxtilla','190',30),(2281,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ursulo Galván','191',30),(2282,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Vega de Alatorre','192',30),(2283,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Veracruz','193',30),(2284,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Aldama','194',30),(2285,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xoxocotla','195',30),(2286,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yanga','196',30),(2287,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yecuatla','197',30),(2288,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacualpan','198',30),(2289,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zaragoza','199',30),(2290,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zentla','200',30),(2291,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zongolica','201',30),(2292,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zontecomatlán de López y Fuentes','202',30),(2293,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zozocolco de Hidalgo','203',30),(2294,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Agua Dulce','204',30),(2295,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Higo','205',30),(2296,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nanchital de Lázaro Cárdenas del Río','206',30),(2297,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tres Valles','207',30),(2298,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Carlos A. Carrillo','208',30),(2299,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tatahuicapan de Juárez','209',30),(2300,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Uxpanapa','210',30),(2301,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Rafael','211',30),(2302,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santiago Sochiapan','212',30),(2303,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Abalá','001',31),(2304,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Acanceh','002',31),(2305,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Akil','003',31),(2306,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Baca','004',31),(2307,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Bokobá','005',31),(2308,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Buctzotz','006',31),(2309,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cacalchén','007',31),(2310,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calotmul','008',31),(2311,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cansahcab','009',31),(2312,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cantamayec','010',31),(2313,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Celestún','011',31),(2314,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cenotillo','012',31),(2315,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Conkal','013',31),(2316,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuncunul','014',31),(2317,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuzamá','015',31),(2318,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chacsinkín','016',31),(2319,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chankom','017',31),(2320,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chapab','018',31),(2321,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chemax','019',31),(2322,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chicxulub Pueblo','020',31),(2323,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chichimilá','021',31),(2324,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chikindzonot','022',31),(2325,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chocholá','023',31),(2326,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chumayel','024',31),(2327,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Dzán','025',31),(2328,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Dzemul','026',31),(2329,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Dzidzantún','027',31),(2330,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Dzilam de Bravo','028',31),(2331,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Dzilam González','029',31),(2332,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Dzitás','030',31),(2333,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Dzoncauich','031',31),(2334,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Espita','032',31),(2335,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Halachó','033',31),(2336,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hocabá','034',31),(2337,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hoctún','035',31),(2338,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Homún','036',31),(2339,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huhí','037',31),(2340,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Hunucmá','038',31),(2341,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ixil','039',31),(2342,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Izamal','040',31),(2343,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Kanasín','041',31),(2344,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Kantunil','042',31),(2345,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Kaua','043',31),(2346,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Kinchil','044',31),(2347,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Kopomá','045',31),(2348,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mama','046',31),(2349,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Maní','047',31),(2350,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Maxcanú','048',31),(2351,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mayapán','049',31),(2352,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mérida','050',31),(2353,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mocochá','051',31),(2354,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Motul','052',31),(2355,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Muna','053',31),(2356,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Muxupip','054',31),(2357,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Opichén','055',31),(2358,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Oxkutzcab','056',31),(2359,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Panabá','057',31),(2360,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Peto','058',31),(2361,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Progreso','059',31),(2362,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Quintana Roo','060',31),(2363,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Río Lagartos','061',31),(2364,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sacalum','062',31),(2365,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Samahil','063',31),(2366,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sanahcat','064',31),(2367,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','San Felipe','065',31),(2368,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa Elena','066',31),(2369,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Seyé','067',31),(2370,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sinanché','068',31),(2371,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sotuta','069',31),(2372,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sucilá','070',31),(2373,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sudzal','071',31),(2374,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Suma','072',31),(2375,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tahdziú','073',31),(2376,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tahmek','074',31),(2377,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teabo','075',31),(2378,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tecoh','076',31),(2379,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tekal de Venegas','077',31),(2380,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tekantó','078',31),(2381,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tekax','079',31),(2382,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tekit','080',31),(2383,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tekom','081',31),(2384,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Telchac Pueblo','082',31),(2385,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Telchac Puerto','083',31),(2386,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Temax','084',31),(2387,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Temozón','085',31),(2388,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepakán','086',31),(2389,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tetiz','087',31),(2390,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teya','088',31),(2391,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ticul','089',31),(2392,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Timucuy','090',31),(2393,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tinum','091',31),(2394,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tixcacalcupul','092',31),(2395,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tixkokob','093',31),(2396,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tixmehuac','094',31),(2397,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tixpéhual','095',31),(2398,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tizimín','096',31),(2399,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tunkás','097',31),(2400,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tzucacab','098',31),(2401,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Uayma','099',31),(2402,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ucú','100',31),(2403,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Umán','101',31),(2404,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Valladolid','102',31),(2405,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Xocchel','103',31),(2406,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yaxcabá','104',31),(2407,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yaxkukul','105',31),(2408,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Yobaín','106',31),(2409,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apozol','001',32),(2410,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Apulco','002',32),(2411,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Atolinga','003',32),(2412,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Benito Juárez','004',32),(2413,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Calera','005',32),(2414,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cañitas de Felipe Pescador','006',32),(2415,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Concepción del Oro','007',32),(2416,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Cuauhtémoc','008',32),(2417,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Chalchihuites','009',32),(2418,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Fresnillo','010',32),(2419,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Trinidad García de la Cadena','011',32),(2420,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Genaro Codina','012',32),(2421,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Enrique Estrada','013',32),(2422,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Francisco R. Murguía','014',32),(2423,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Plateado de Joaquín Amaro','015',32),(2424,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','General Pánfilo Natera','016',32),(2425,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Guadalupe','017',32),(2426,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Huanusco','018',32),(2427,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jalpa','019',32),(2428,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jerez','020',32),(2429,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Jiménez del Teul','021',32),(2430,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juan Aldama','022',32),(2431,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Juchipila','023',32),(2432,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Loreto','024',32),(2433,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Luis Moya','025',32),(2434,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mazapil','026',32),(2435,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Melchor Ocampo','027',32),(2436,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Mezquital del Oro','028',32),(2437,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Miguel Auza','029',32),(2438,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Momax','030',32),(2439,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Monte Escobedo','031',32),(2440,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Morelos','032',32),(2441,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Moyahua de Estrada','033',32),(2442,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Nochistlán de Mejía','034',32),(2443,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Noria de Ángeles','035',32),(2444,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Ojocaliente','036',32),(2445,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pánuco','037',32),(2446,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Pinos','038',32),(2447,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Río Grande','039',32),(2448,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sain Alto','040',32),(2449,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','El Salvador','041',32),(2450,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Sombrerete','042',32),(2451,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Susticacán','043',32),(2452,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tabasco','044',32),(2453,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepechitlán','045',32),(2454,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tepetongo','046',32),(2455,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Teúl de González Ortega','047',32),(2456,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Tlaltenango de Sánchez Román','048',32),(2457,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Valparaíso','049',32),(2458,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Vetagrande','050',32),(2459,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa de Cos','051',32),(2460,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa García','052',32),(2461,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa González Ortega','053',32),(2462,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villa Hidalgo','054',32),(2463,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Villanueva','055',32),(2464,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Zacatecas','056',32),(2465,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Trancoso','057',32),(2466,'2019-03-11 00:00:00.000000','2019-03-12 00:00:00.000000','Santa María de la Paz','058',32);
/*!40000 ALTER TABLE `declaracion_catmunicipios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catnaturalezamembresia`
--

DROP TABLE IF EXISTS `declaracion_catnaturalezamembresia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catnaturalezamembresia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naturaleza_membresia` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catnaturalezamembresia`
--

LOCK TABLES `declaracion_catnaturalezamembresia` WRITE;
/*!40000 ALTER TABLE `declaracion_catnaturalezamembresia` DISABLE KEYS */;
INSERT INTO `declaracion_catnaturalezamembresia` VALUES (1,'Consejo y/o juntas','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CSJ'),(2,'Asociaciones civiles','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'ASC'),(3,'Organizaciones benéficas','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'ORGB'),(4,'Partidos políticos','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'PP'),(5,'Otros (especificar)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,2,'OTRO'),(6,'Directorio','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'DIR'),(7,'Junta','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'JNT');
/*!40000 ALTER TABLE `declaracion_catnaturalezamembresia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catordenesgobierno`
--

DROP TABLE IF EXISTS `declaracion_catordenesgobierno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catordenesgobierno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden_gobierno` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catordenesgobierno`
--

LOCK TABLES `declaracion_catordenesgobierno` WRITE;
/*!40000 ALTER TABLE `declaracion_catordenesgobierno` DISABLE KEYS */;
INSERT INTO `declaracion_catordenesgobierno` VALUES (1,'Federal','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'FEDERAL'),(2,'Estatal','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'ESTATAL'),(3,'Municipal alcaldia','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'MUNICIPAL_ALCALDIA');
/*!40000 ALTER TABLE `declaracion_catordenesgobierno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catpaises`
--

DROP TABLE IF EXISTS `declaracion_catpaises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catpaises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catpaises`
--

LOCK TABLES `declaracion_catpaises` WRITE;
/*!40000 ALTER TABLE `declaracion_catpaises` DISABLE KEYS */;
INSERT INTO `declaracion_catpaises` VALUES (1,'Afghanistan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AF'),(2,'Aland Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AX'),(3,'Albania','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AL'),(4,'Algeria','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'DZ'),(5,'American Samoa','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AS'),(6,'Andorra','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AD'),(7,'Angola','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AO'),(8,'Anguilla','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AI'),(9,'Antarctica','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AQ'),(10,'Antigua and Barbuda','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AG'),(11,'Argentina','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AR'),(12,'Armenia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AM'),(13,'Aruba','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AW'),(14,'Australia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AU'),(15,'Austria','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AT'),(16,'Azerbaijan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AZ'),(17,'Bahamas','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BS'),(18,'Bahrain','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BH'),(19,'Bangladesh','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BD'),(20,'Barbados','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BB'),(21,'Belarus','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BY'),(22,'Belgium','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BE'),(23,'Belize','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BZ'),(24,'Benin','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BJ'),(25,'Bermuda','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BM'),(26,'Bhutan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BT'),(27,'Bolivia, Plurinational State of','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BO'),(28,'Bosnia and Herzegovina','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BA'),(29,'Botswana','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BW'),(30,'Bouvet Island','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BV'),(31,'Brazil','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BR'),(32,'British Indian Ocean Territory','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'IO'),(33,'Brunei Darussalam','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BN'),(34,'Bulgaria','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BG'),(35,'Burkina Faso','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BF'),(36,'Burundi','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BI'),(37,'Cambodia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KH'),(38,'Cameroon','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CM'),(39,'Canada','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CA'),(40,'Cape Verde','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CV'),(41,'Cayman Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KY'),(42,'Central African Republic','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CF'),(43,'Chad','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TD'),(44,'Chile','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CL'),(45,'China','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CN'),(46,'Christmas Island','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CX'),(47,'Cocos (Keeling) Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CC'),(48,'Colombia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CO'),(49,'Comoros','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KM'),(50,'Congo','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CG'),(51,'Congo, the Democratic Republic of the','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CD'),(52,'Cook Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CK'),(53,'Costa Rica','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CR'),(54,'Cote d\'Ivoire','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CI'),(55,'Croatia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'HR'),(56,'Cuba','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CU'),(57,'Cyprus','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CY'),(58,'Czech Republic','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CZ'),(59,'Denmark','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'DK'),(60,'Djibouti','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'DJ'),(61,'Dominica','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'DM'),(62,'Dominican Republic','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'DO'),(63,'Ecuador','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'EC'),(64,'Egypt','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'EG'),(65,'El Salvador','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SV'),(66,'Equatorial Guinea','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GQ'),(67,'Eritrea','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'ER'),(68,'Estonia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'EE'),(69,'Ethiopia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'ET'),(70,'Falkland Islands (Malvinas)','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'FK'),(71,'Faroe Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'FO'),(72,'Fiji','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'FJ'),(73,'Finland','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'FI'),(74,'France','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'FR'),(75,'French Guiana','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GF'),(76,'French Polynesia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PF'),(77,'French Southern Territories','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TF'),(78,'Gabon','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GA'),(79,'Gambia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GM'),(80,'Georgia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GE'),(81,'Germany','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'DE'),(82,'Ghana','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GH'),(83,'Gibraltar','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GI'),(84,'Greece','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GR'),(85,'Greenland','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GL'),(86,'Grenada','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GD'),(87,'Guadeloupe','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GP'),(88,'Guam','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GU'),(89,'Guatemala','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GT'),(90,'Guernsey','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GG'),(91,'Guinea','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GN'),(92,'Guinea-Bissau','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GW'),(93,'Guyana','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GY'),(94,'Haiti','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'HT'),(95,'Heard Island and McDonald Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'HM'),(96,'Holy See (Vatican City State)','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'VA'),(97,'Honduras','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'HN'),(98,'Hong Kong','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'HK'),(99,'Hungary','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'HU'),(100,'Iceland','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'IS'),(101,'India','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'IN'),(102,'Indonesia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'ID'),(103,'Iran, Islamic Republic of','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'IR'),(104,'Iraq','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'IQ'),(105,'Ireland','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'IE'),(106,'Isle of Man','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'IM'),(107,'Israel','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'IL'),(108,'Italy','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'IT'),(109,'Jamaica','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'JM'),(110,'Japan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'JP'),(111,'Jersey','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'JE'),(112,'Jordan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'JO'),(113,'Kazakhstan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KZ'),(114,'Kenya','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KE'),(115,'Kiribati','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KI'),(116,'Korea, Democratic People\'s Republic of','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KP'),(117,'Korea, Republic of','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KR'),(118,'Kuwait','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KW'),(119,'Kyrgyzstan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KG'),(120,'Lao People\'s Democratic Republic','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LA'),(121,'Latvia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LV'),(122,'Lebanon','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LB'),(123,'Lesotho','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LS'),(124,'Liberia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LR'),(125,'Libyan Arab Jamahiriya','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LY'),(126,'Liechtenstein','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LI'),(127,'Lithuania','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LT'),(128,'Luxembourg','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LU'),(129,'Macao','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MO'),(130,'Macedonia, the former Yugoslav Republic of','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MK'),(131,'Madagascar','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MG'),(132,'Malawi','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MW'),(133,'Malaysia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MY'),(134,'Maldives','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MV'),(135,'Mali','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'ML'),(136,'Malta','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MT'),(137,'Marshall Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MH'),(138,'Martinique','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MQ'),(139,'Mauritania','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MR'),(140,'Mauritius','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MU'),(141,'Mayotte','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'YT'),(142,'México','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',1,1,'MX'),(143,'Micronesia, Federated States of','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'FM'),(144,'Moldova, Republic of','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MD'),(145,'Monaco','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MC'),(146,'Mongolia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MN'),(147,'Montenegro','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'ME'),(148,'Montserrat','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MS'),(149,'Morocco','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MA'),(150,'Mozambique','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MZ'),(151,'Myanmar','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MM'),(152,'Namibia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NA'),(153,'Nauru','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NR'),(154,'Nepal','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NP'),(155,'Netherlands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NL'),(156,'Netherlands Antilles','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AN'),(157,'New Caledonia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NC'),(158,'New Zealand','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NZ'),(159,'Nicaragua','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NI'),(160,'Niger','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NE'),(161,'Nigeria','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NG'),(162,'Niue','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NU'),(163,'Norfolk Island','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NF'),(164,'Northern Mariana Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MP'),(165,'Norway','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'NO'),(166,'Oman','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'OM'),(167,'Pakistan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PK'),(168,'Palau','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PW'),(169,'Palestinian Territory, Occupied','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PS'),(170,'Panama','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PA'),(171,'Papua New Guinea','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PG'),(172,'Paraguay','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PY'),(173,'Peru','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PE'),(174,'Philippines','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PH'),(175,'Pitcairn','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PN'),(176,'Poland','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PL'),(177,'Portugal','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PT'),(178,'Puerto Rico','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PR'),(179,'Qatar','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'QA'),(180,'Reunion  Réunion','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'RE'),(181,'Romania','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'RO'),(182,'Russian Federation','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'RU'),(183,'Rwanda','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'RW'),(184,'Saint Barthélemy','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'BL'),(185,'Saint Helena','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SH'),(186,'Saint Kitts and Nevis','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'KN'),(187,'Saint Lucia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LC'),(188,'Saint Martin (French part)','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'MF'),(189,'Saint Pierre and Miquelon','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'PM'),(190,'Saint Vincent and the Grenadines','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'VC'),(191,'Samoa','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'WS'),(192,'San Marino','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SM'),(193,'Sao Tome and Principe','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'ST'),(194,'Saudi Arabia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SA'),(195,'Senegal','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SN'),(196,'Serbia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'RS'),(197,'Seychelles','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SC'),(198,'Sierra Leone','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SL'),(199,'Singapore','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SG'),(200,'Slovakia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SK'),(201,'Slovenia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SI'),(202,'Solomon Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SB'),(203,'Somalia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SO'),(204,'South Africa','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'ZA'),(205,'South Georgia and the South Sandwich Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GS'),(206,'Spain','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'ES'),(207,'Sri Lanka','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'LK'),(208,'Sudan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SD'),(209,'Suriname','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SR'),(210,'Svalbard and Jan Mayen','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SJ'),(211,'Swaziland','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SZ'),(212,'Sweden','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SE'),(213,'Switzerland','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'CH'),(214,'Syrian Arab Republic','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'SY'),(215,'Taiwan, Province of China','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TW'),(216,'Tajikistan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TJ'),(217,'Tanzania, United Republic of','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TZ'),(218,'Thailand','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TH'),(219,'Timor-Leste','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TL'),(220,'Togo','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TG'),(221,'Tokelau','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TK'),(222,'Tonga','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TO'),(223,'Trinidad and Tobago','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TT'),(224,'Tunisia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TN'),(225,'Turkey','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TR'),(226,'Turkmenistan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TM'),(227,'Turks and Caicos Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TC'),(228,'Tuvalu','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'TV'),(229,'Uganda','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'UG'),(230,'Ukraine','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'UA'),(231,'United Arab Emirates','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'AE'),(232,'United Kingdom','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'GB'),(233,'United States','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'US'),(234,'United States Minor Outlying Islands','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'UM'),(235,'Uruguay','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'UY'),(236,'Uzbekistan','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'UZ'),(237,'Vanuatu','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'VU'),(238,'Venezuela, Bolivarian Republic of','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'VE'),(239,'Viet Nam','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'VN'),(240,'Virgin Islands, British','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'VG'),(241,'Virgin Islands, U.S.','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'VI'),(242,'Wallis and Futuna','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'WF'),(243,'Western Sahara','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'EH'),(244,'Yemen','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'YE'),(245,'Zambia','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'ZM'),(246,'Zimbabwe','2019-04-01 00:00:00.000000','2019-04-01 00:00:00.000000',0,1,'ZW');
/*!40000 ALTER TABLE `declaracion_catpaises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catpoderes`
--

DROP TABLE IF EXISTS `declaracion_catpoderes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catpoderes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poder` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catpoderes`
--

LOCK TABLES `declaracion_catpoderes` WRITE;
/*!40000 ALTER TABLE `declaracion_catpoderes` DISABLE KEYS */;
INSERT INTO `declaracion_catpoderes` VALUES (1,'Ejecutivo','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'EJECUTIVO'),(2,'Judicial','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'LEGISLATIVO'),(3,'Legislativo','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'JUDICIAL'),(4,'Organo autonomo','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'ORGANO_AUTONOMO');
/*!40000 ALTER TABLE `declaracion_catpoderes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catpuestos`
--

DROP TABLE IF EXISTS `declaracion_catpuestos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catpuestos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  `puesto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catpuestos`
--

LOCK TABLES `declaracion_catpuestos` WRITE;
/*!40000 ALTER TABLE `declaracion_catpuestos` DISABLE KEYS */;
INSERT INTO `declaracion_catpuestos` VALUES (1,'2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',0,1,'','SECRETARIA'),(2,'2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',0,1,'','DIRECCION DE TECNOLOGÍAS Y  PLATAFORMAS'),(3,'2019-04-02 16:03:21.000000','2019-04-02 16:03:21.000000',1,1,'','DIRECCION DE INTELIGENCIA DE DATOS');
/*!40000 ALTER TABLE `declaracion_catpuestos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catregimenesmatrimoniales`
--

DROP TABLE IF EXISTS `declaracion_catregimenesmatrimoniales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catregimenesmatrimoniales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regimen_matrimonial` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catregimenesmatrimoniales`
--

LOCK TABLES `declaracion_catregimenesmatrimoniales` WRITE;
/*!40000 ALTER TABLE `declaracion_catregimenesmatrimoniales` DISABLE KEYS */;
INSERT INTO `declaracion_catregimenesmatrimoniales` VALUES (1,'SOCIEDAD CONYUGAL','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SCO'),(2,'SEPARACIÓN DE BIENES','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SBI'),(3,'OTRO','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'OTR');
/*!40000 ALTER TABLE `declaracion_catregimenesmatrimoniales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catsectoresindustria`
--

DROP TABLE IF EXISTS `declaracion_catsectoresindustria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catsectoresindustria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sector_industria` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catsectoresindustria`
--

LOCK TABLES `declaracion_catsectoresindustria` WRITE;
/*!40000 ALTER TABLE `declaracion_catsectoresindustria` DISABLE KEYS */;
INSERT INTO `declaracion_catsectoresindustria` VALUES (21,'Agricultura','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'AGRI'),(22,'Minería','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'MIN'),(23,'Energía eléctrica','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'EELECT'),(24,'Construcción','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'CONS'),(25,'Industrias manufactureras','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'INDMANU'),(26,'Comercio al por mayor','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'CMAYOR'),(27,'Comercio al por menor','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'CMENOR'),(28,'Transportes','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'TRANS'),(29,'Medios masivos','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'MEDIOM'),(30,'Servicios financieros','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SERVFIN'),(31,'Servicios inmobiliarios','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SERVINM'),(32,'Servicios profesionales','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SERVPROF'),(33,'Servicios corporativos','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SERVCORP'),(34,'Servicios de salud','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SERVS'),(35,'Servicios de esparcimiento','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SERVESPAR'),(36,'Servicios de alojamiento','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SERVALOJ'),(37,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO'),(38,'Servicios de alojamiento temporal y de preparación de alimentos y bebidas','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'SATPAB'),(39,'Otros servicios excepto actividades gubernamentales','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'OSEAG'),(40,'Actividades legislativas, gubernamentales, de impartición de justicia y de organismos internacionales y extranjeras','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,'ALGIJOIE'),(41,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_catsectoresindustria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattipoparticipacion`
--

DROP TABLE IF EXISTS `declaracion_cattipoparticipacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattipoparticipacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_participacion` varchar(45) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattipoparticipacion`
--

LOCK TABLES `declaracion_cattipoparticipacion` WRITE;
/*!40000 ALTER TABLE `declaracion_cattipoparticipacion` DISABLE KEYS */;
INSERT INTO `declaracion_cattipoparticipacion` VALUES (1,'Co-vendedor','2019-04-04 04:29:57.000000','2019-04-04 04:29:57.000000',''),(2,'Copropietario','2019-04-04 04:29:57.000000','2019-04-04 04:29:57.000000',''),(3,'Fidecomitente','2019-04-04 04:29:57.000000','2019-04-04 04:29:57.000000','FIDECOMITENTE'),(4,'Fidecomisario','2019-04-04 04:29:57.000000','2019-04-04 04:29:57.000000','FIDECOMISARIO'),(5,'Fiduciario','2019-04-04 04:29:57.000000','2019-04-04 04:29:57.000000','FIDUCIARIO'),(6,'Prestario o Deudor','2019-04-04 04:29:57.000000','2019-04-04 04:29:57.000000',''),(7,'Declarante','2019-04-04 04:29:57.000000','2019-04-04 04:29:57.000000',''),(8,'Copropietario','2019-04-04 04:29:57.000000','2019-04-04 04:29:57.000000',''),(9,'Comité Técnico','2019-04-04 04:29:57.000000','2019-04-04 04:29:57.000000','COMITE_TECNICO'),(10,'Propietario anterior','2019-03-26 18:13:24.000000','2019-03-26 18:13:24.000000','');
/*!40000 ALTER TABLE `declaracion_cattipoparticipacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattipopersona`
--

DROP TABLE IF EXISTS `declaracion_cattipopersona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattipopersona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_persona` varchar(45) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattipopersona`
--

LOCK TABLES `declaracion_cattipopersona` WRITE;
/*!40000 ALTER TABLE `declaracion_cattipopersona` DISABLE KEYS */;
INSERT INTO `declaracion_cattipopersona` VALUES (1,'Declarante','2019-04-02 14:56:48.000000','2019-04-02 14:56:48.000000',0,1,''),(2,'Dependiente','2019-04-02 14:56:48.000000','2019-04-02 14:56:48.000000',0,1,''),(3,'Empresa','2019-04-02 14:56:48.000000','2019-04-02 14:56:48.000000',0,1,''),(4,'Socio','2019-04-02 14:56:48.000000','2019-04-02 14:56:48.000000',0,1,'SCIO'),(5,'Cliente','2019-04-02 14:56:48.000000','2019-04-02 14:56:48.000000',0,1,''),(6,'Sociedad','2019-04-02 14:56:48.000000','2019-04-02 14:56:48.000000',0,1,''),(7,'Institucin','2019-04-02 14:56:48.000000','2019-04-02 14:56:48.000000',0,1,''),(8,'Fideicomisario','2019-04-04 04:47:28.000000','2019-04-04 04:47:28.000000',0,1,'FIDEICOMITENTE'),(9,'Fideicomitente','2019-04-04 04:47:38.000000','2019-04-04 04:47:38.000000',0,1,'FIDUCIARIO'),(10,'Fiduciario','2019-04-04 04:47:51.000000','2019-04-04 04:47:51.000000',0,1,'FIDEICOMISARIO'),(11,'Comité Técnico','2019-04-04 04:47:51.000000','2019-04-04 04:47:51.000000',0,1,'COMITE_TECNICO'),(12,'Copropietario','2019-04-04 09:37:44.000000','2019-04-04 09:37:44.000000',0,1,''),(13,'Propietario anterior','2019-04-04 09:37:57.000000','2019-04-04 09:37:57.000000',0,1,''),(14,'Prestatario','2019-04-04 21:28:02.000000','2019-04-04 21:28:02.000000',0,1,''),(15,'Accionista','2019-04-04 21:28:02.000000','2019-04-04 21:28:02.000000',0,1,'ACCI'),(16,'Comisario','2019-04-04 21:28:02.000000','2019-04-04 21:28:02.000000',0,1,'COMI'),(17,'Representante','2019-04-04 21:28:02.000000','2019-04-04 21:28:02.000000',0,1,'REPR'),(18,'Apoderado','2019-04-04 21:28:02.000000','2019-04-04 21:28:02.000000',0,1,'APOD'),(19,'Colaborador','2019-04-04 21:28:02.000000','2019-04-04 21:28:02.000000',0,1,'COLB'),(20,'Beneficiario','2019-04-04 21:28:02.000000','2019-04-04 21:28:02.000000',0,1,'BENE'),(21,'Otros','2019-04-04 21:28:02.000000','2019-04-04 21:28:02.000000',0,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattipopersona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposacreedores`
--

DROP TABLE IF EXISTS `declaracion_cattiposacreedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposacreedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_acreedor` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposacreedores`
--

LOCK TABLES `declaracion_cattiposacreedores` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposacreedores` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposacreedores` VALUES (1,'Institución financiera','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'INSTF'),(2,'Persona moral no financiera','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'PMNF'),(3,'Persona física','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'PFIS'),(4,'Otro (especificar)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposacreedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposactividad`
--

DROP TABLE IF EXISTS `declaracion_cattiposactividad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposactividad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_actividad` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposactividad`
--

LOCK TABLES `declaracion_cattiposactividad` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposactividad` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposactividad` VALUES (1,'Arrendamiento','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'AVS'),(2,'Regalía','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'AL'),(3,'Sorteos','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'BR'),(4,'Concursos','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'COM'),(5,'Donaciones','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'DEC'),(6,'Seguros de vida','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'EDU'),(7,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposactividad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposadeudos`
--

DROP TABLE IF EXISTS `declaracion_cattiposadeudos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposadeudos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_adeudo` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposadeudos`
--

LOCK TABLES `declaracion_cattiposadeudos` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposadeudos` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposadeudos` VALUES (1,'Crédito hipotecario','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CHIP'),(2,'Crédito automotriz','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CAUT'),(3,'Crédito personal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CPER'),(4,'Tarjeta de crédito bancaria','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'TCRN'),(5,'Tarjeta de crédito departamental','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'TCRD'),(6,'Prestamo personal','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'PRPE'),(7,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposadeudos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposapoyos`
--

DROP TABLE IF EXISTS `declaracion_cattiposapoyos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposapoyos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_apoyo` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposapoyos`
--

LOCK TABLES `declaracion_cattiposapoyos` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposapoyos` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposapoyos` VALUES (1,'Subsidio','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'SUB'),(2,'Servicio','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'SER'),(3,'Obra','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'OBRA'),(4,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposapoyos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposbeneficios`
--

DROP TABLE IF EXISTS `declaracion_cattiposbeneficios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposbeneficios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_beneficio` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposbeneficios`
--

LOCK TABLES `declaracion_cattiposbeneficios` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposbeneficios` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposbeneficios` VALUES (1,'Sorteo','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'S'),(2,'Concurso','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'C'),(3,'Donación','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'D'),(4,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'O');
/*!40000 ALTER TABLE `declaracion_cattiposbeneficios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposbienes`
--

DROP TABLE IF EXISTS `declaracion_cattiposbienes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposbienes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_bien` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposbienes`
--

LOCK TABLES `declaracion_cattiposbienes` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposbienes` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposbienes` VALUES (1,'Mueble','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'MUE'),(2,'Inmueble','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'INM'),(3,'Vehiculo','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'VEH');
/*!40000 ALTER TABLE `declaracion_cattiposbienes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposdeclaracion`
--

DROP TABLE IF EXISTS `declaracion_cattiposdeclaracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposdeclaracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_declaracion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposdeclaracion`
--

LOCK TABLES `declaracion_cattiposdeclaracion` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposdeclaracion` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposdeclaracion` VALUES (1,'Declaración inicial','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'Inicial'),(2,'Declaración de modificación patrimonial','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'Modificada'),(3,'Declaración de conclusión','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'Conclusión'),(4,'Declaración de actualización','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'Actualización');
/*!40000 ALTER TABLE `declaracion_cattiposdeclaracion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposespecificosinversiones`
--

DROP TABLE IF EXISTS `declaracion_cattiposespecificosinversiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposespecificosinversiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_especifico_inversion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposespecificosinversiones`
--

LOCK TABLES `declaracion_cattiposespecificosinversiones` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposespecificosinversiones` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposespecificosinversiones` VALUES (1,'CUENTA DE NÓMINA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'ACCS'),(2,'CUENTA DE AHORRO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'BONS'),(3,'CUENTA DE CHEQUES','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'VALRS'),(4,'CUENTA MAESTRA','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'TITLS'),(5,'CUENTA EJE','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CONTSA'),(6,'DEPOSITO A PLAZOS','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposespecificosinversiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposfideicomisos`
--

DROP TABLE IF EXISTS `declaracion_cattiposfideicomisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposfideicomisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_fideicomiso` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposfideicomisos`
--

LOCK TABLES `declaracion_cattiposfideicomisos` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposfideicomisos` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposfideicomisos` VALUES (1,'Público','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(2,'Privado','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(3,'Mixto','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,''),(4,'Testamento','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(5,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'');
/*!40000 ALTER TABLE `declaracion_cattiposfideicomisos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposingresosvarios`
--

DROP TABLE IF EXISTS `declaracion_cattiposingresosvarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposingresosvarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_ingreso_varios` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposingresosvarios`
--

LOCK TABLES `declaracion_cattiposingresosvarios` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposingresosvarios` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposingresosvarios` VALUES (1,'Sueldos y salarios por otros empleos','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,''),(2,'Actividad profesional','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,''),(3,'Actividad industrial/comercial/empresarial','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,''),(4,'Actividad financiera','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,''),(5,'Ingreso conyugue y/o dependiente','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,''),(6,'Ingresos neto delcarante','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,''),(7,'Enajenación de bienes','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,''),(8,'Ingreso neto total','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',0,1,''),(9,'Otros ingresos','2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000',1,1,'');
/*!40000 ALTER TABLE `declaracion_cattiposingresosvarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposinmuebles`
--

DROP TABLE IF EXISTS `declaracion_cattiposinmuebles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposinmuebles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_inmueble` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposinmuebles`
--

LOCK TABLES `declaracion_cattiposinmuebles` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposinmuebles` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposinmuebles` VALUES (1,'Casa','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CASA'),(2,'Departamento','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'DPTO'),(3,'Edificio','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'EDIF'),(4,'Local comercial','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'LOCC'),(5,'Bodega','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'BODG'),(6,'Palco','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'PALC'),(7,'Terreno','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'TERR'),(8,'Rancho','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'RACH'),(9,'Otro (especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposinmuebles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposinstituciones`
--

DROP TABLE IF EXISTS `declaracion_cattiposinstituciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposinstituciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_institucion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposinstituciones`
--

LOCK TABLES `declaracion_cattiposinstituciones` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposinstituciones` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposinstituciones` VALUES (1,'Organizaciones civiles','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'OSC'),(2,'Organizaciones benéficas','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'OB'),(3,'Partidos políticos','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'PP'),(4,'Gremios/Sindicatos','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'GS'),(5,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposinstituciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposinstrumentos`
--

DROP TABLE IF EXISTS `declaracion_cattiposinstrumentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposinstrumentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `clave` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposinstrumentos`
--

LOCK TABLES `declaracion_cattiposinstrumentos` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposinstrumentos` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposinstrumentos` VALUES (1,'2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000','Capital','CAP'),(2,'2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000','Fondos de inversión','FIN'),(3,'2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000','Organizaciones privadas','OPR'),(4,'2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000','Seguro de separación individualizado','SSI'),(5,'2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000','Valores Busátiles','VBU'),(6,'2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000','Bonos','BON'),(7,'2019-03-12 00:00:00.000000','2019-03-12 00:00:00.000000','Otros','OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposinstrumentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposinversiones`
--

DROP TABLE IF EXISTS `declaracion_cattiposinversiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposinversiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_inversion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposinversiones`
--

LOCK TABLES `declaracion_cattiposinversiones` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposinversiones` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposinversiones` VALUES (1,'Bancaria','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'BANC'),(2,'Fondos de inversión','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'FINV'),(3,'Organizaciones privadas y/o mercantiles','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'ORPM'),(4,'Posesión de moneda y/o metales','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'POMM'),(5,'Seguros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'SEGR'),(6,'Valores bursátiles','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'VBUR'),(7,'Afores y otros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'AFOT');
/*!40000 ALTER TABLE `declaracion_cattiposinversiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposmetales`
--

DROP TABLE IF EXISTS `declaracion_cattiposmetales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposmetales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_metal` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposmetales`
--

LOCK TABLES `declaracion_cattiposmetales` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposmetales` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposmetales` VALUES (1,'Oro','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'ORO'),(2,'Plata','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'PLATA'),(3,'Cobre','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'COBRE'),(4,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposmetales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposmuebles`
--

DROP TABLE IF EXISTS `declaracion_cattiposmuebles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposmuebles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_mueble` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposmuebles`
--

LOCK TABLES `declaracion_cattiposmuebles` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposmuebles` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposmuebles` VALUES (1,'Automóvil/motocicleta','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'AUTMOT'),(2,'Aeronave','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'AERN'),(3,'Barco/Yate','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'BRCYTE'),(5,'Manaje de casa( muebles y accesorios de casa)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,0,'MECA'),(6,'Aparatos electrónicos y electrodomésticos','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,0,'APAE'),(7,'Joyas','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,0,'JOYAS'),(8,'Colecciones','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,0,'COLEC'),(9,'Obra de arte','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,0,'OBRA'),(10,'Otro (especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,0,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposmuebles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposoperaciones`
--

DROP TABLE IF EXISTS `declaracion_cattiposoperaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposoperaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_operacion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposoperaciones`
--

LOCK TABLES `declaracion_cattiposoperaciones` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposoperaciones` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposoperaciones` VALUES (1,'Agregar','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'AGR'),(2,'Modificar','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'MOD'),(3,'Sin cambio','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'SCM'),(4,'Baja','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'BAJ');
/*!40000 ALTER TABLE `declaracion_cattiposoperaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattipospasivos`
--

DROP TABLE IF EXISTS `declaracion_cattipospasivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattipospasivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_pasivo` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattipospasivos`
--

LOCK TABLES `declaracion_cattipospasivos` WRITE;
/*!40000 ALTER TABLE `declaracion_cattipospasivos` DISABLE KEYS */;
INSERT INTO `declaracion_cattipospasivos` VALUES (1,'Deudas','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(2,'Otras obligaciones','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'');
/*!40000 ALTER TABLE `declaracion_cattipospasivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposrelacionespersonales`
--

DROP TABLE IF EXISTS `declaracion_cattiposrelacionespersonales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposrelacionespersonales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_relacion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `grupo_familia` int(11) DEFAULT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposrelacionespersonales`
--

LOCK TABLES `declaracion_cattiposrelacionespersonales` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposrelacionespersonales` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposrelacionespersonales` VALUES (1,'Pareja','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,3,'PAREJA'),(2,'Cónyuge','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,1,'CONYUGUE'),(3,'Cónyuge','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'CONY'),(4,'Cónyuge','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'CY'),(5,'Concubina/Concubinario/unión libre','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,1,'CONCUBINA_CONCUBINARIO_UNION_LIBRE'),(6,'Concubina o concubinario','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'CONB'),(7,'Concubinario(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'CON'),(8,'Dependiente económico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,3,'DEPENDIENTE_ECONOMICO'),(9,'Dependiente económico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,0,''),(10,'Sociedad de Convivencia','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,1,'SOCIEDAD_DE_CONVIVENCIA'),(11,'Declarante','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,3,'DECLARANTE'),(12,'Declarante','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'DC'),(13,'Abuelo(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'ABU'),(14,'Bisabuelo(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'BISA'),(15,'Bisnieto(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'BISN'),(16,'Concuño(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'CONC'),(17,'Cuñado(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'CUN'),(18,'Hermano(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'HER'),(19,'Hijo(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'HIJ'),(20,'Madre','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'MAD'),(21,'Padre','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'PAD'),(22,'Primo(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'PRI'),(23,'Sobrino(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'SOB'),(24,'Suegro(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'SUE'),(25,'Tatarabuelo(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'TATA'),(26,'Tataranieto(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'TATN'),(27,'Tio(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'TIOA'),(28,'Nieto(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'NIE'),(29,'Ninguno','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'NIN'),(30,'Conviviente','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'CONV'),(31,'Cuñado','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'CU'),(32,'Padre','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'PAD'),(33,'Madre','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'MAD'),(34,'Tío(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'TIO'),(35,'Ahijado(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'AHI'),(36,'Nuera','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'NUE'),(37,'Yerno','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,4,'YER'),(39,'Tataranieto(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'TATN'),(40,'Conyuge','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'CY'),(41,'Ninguno','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'NIN'),(42,'Declarante','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'DC'),(43,'Concubina o concubinario','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'CON'),(44,'Conviviente','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'CONV'),(45,'Cuñado','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'CU'),(46,'Madre','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'MA'),(47,'Padre','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'PA'),(48,'Tío','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,2,'TIO'),(49,'Otro(a)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,2,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattiposrelacionespersonales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattiposrepresentaciones`
--

DROP TABLE IF EXISTS `declaracion_cattiposrepresentaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposrepresentaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_representacion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattiposrepresentaciones`
--

LOCK TABLES `declaracion_cattiposrepresentaciones` WRITE;
/*!40000 ALTER TABLE `declaracion_cattiposrepresentaciones` DISABLE KEYS */;
INSERT INTO `declaracion_cattiposrepresentaciones` VALUES (1,'REPRESENTANTE','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'REPT'),(2,'REPRESENTADO','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'REPD');
/*!40000 ALTER TABLE `declaracion_cattiposrepresentaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattipostitulares`
--

DROP TABLE IF EXISTS `declaracion_cattipostitulares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattipostitulares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_titular` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattipostitulares`
--

LOCK TABLES `declaracion_cattipostitulares` WRITE;
/*!40000 ALTER TABLE `declaracion_cattipostitulares` DISABLE KEYS */;
INSERT INTO `declaracion_cattipostitulares` VALUES (1,'Declarante','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'DEC'),(2,'Delcarante y cónyuge','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(3,'Delcarante en coporpiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(4,'Delcarante y cónyuge en coporpiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(5,'Delcarante y concubina o concubinario','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(6,'Delcarante y concubina o concubinario en coporpiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(7,'Cónyuge','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CYG'),(8,'Cónyuge en coporpiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(9,'Concubina o concubinario','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CBN'),(10,'Concubina o concubinario en coporpiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(11,'Conviviente','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CVV'),(12,'Declarante y conviviente','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(13,'Declarante y conviviente en copropiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(14,'Conviviente y dependiente economico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(15,'Conviviente y dependiente economico en copropiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(16,'Dependiente economico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'DEN'),(17,'Declarante y dependiente economico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(18,'Declarante y dependiente economico en copropiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(19,'Dependiente economico en copropiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(20,'Declarante, conyugye y dependiente economico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(21,'Declarante, concubina O concubinario y dependiente economico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(22,'Conyugue y dependiente economico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(23,'Concubina o concubinario y dependiente economico','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(24,'Conyugue y dependiente economicoen copropiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(25,'Concubina o concubinario y dependiente economico en copropiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,''),(26,'Copropiedad con terceros','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CTER'),(27,'Otro (Especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattipostitulares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattipovia`
--

DROP TABLE IF EXISTS `declaracion_cattipovia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattipovia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_via` varchar(45) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattipovia`
--

LOCK TABLES `declaracion_cattipovia` WRITE;
/*!40000 ALTER TABLE `declaracion_cattipovia` DISABLE KEYS */;
INSERT INTO `declaracion_cattipovia` VALUES (1,'Ampliación','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(2,'Andador','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(3,'Avenida','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(4,'Boulevard','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(5,'Calle','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(6,'Callejón','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(7,'Calzada','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(8,'Cerrada','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(9,'Circuito','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(10,'Circunvalación','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(11,'Continuación','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(12,'Corredor','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(13,'Diagonal','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(14,'Eje Vial','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(15,'Pasaje','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(16,'Peatonal','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(17,'Periférico','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(18,'Privada','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(19,'Prolongación','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(20,'Retorno','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,''),(21,'Viaducto','2019-04-04 23:27:57.000000','2019-04-04 23:27:57.000000',0,1,'');
/*!40000 ALTER TABLE `declaracion_cattipovia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cattitulartiposrelaciones`
--

DROP TABLE IF EXISTS `declaracion_cattitulartiposrelaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattitulartiposrelaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_relacion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cattitulartiposrelaciones`
--

LOCK TABLES `declaracion_cattitulartiposrelaciones` WRITE;
/*!40000 ALTER TABLE `declaracion_cattitulartiposrelaciones` DISABLE KEYS */;
INSERT INTO `declaracion_cattitulartiposrelaciones` VALUES (1,'Copropietarios de bienes no comerciales','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'CBNC'),(2,'Garantes de Préstamos Recibidos','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'GPR'),(3,'Otorgantes/Depositarios de Garantías','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',0,1,'ODG'),(4,'Otro (especifique)','2019-03-13 00:00:00.000000','2019-03-13 00:00:00.000000',1,1,'OTRO');
/*!40000 ALTER TABLE `declaracion_cattitulartiposrelaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catunidadesmedida`
--

DROP TABLE IF EXISTS `declaracion_catunidadesmedida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catunidadesmedida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `valor` varchar(255) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catunidadesmedida`
--

LOCK TABLES `declaracion_catunidadesmedida` WRITE;
/*!40000 ALTER TABLE `declaracion_catunidadesmedida` DISABLE KEYS */;
/*!40000 ALTER TABLE `declaracion_catunidadesmedida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_catunidadestemporales`
--

DROP TABLE IF EXISTS `declaracion_catunidadestemporales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catunidadestemporales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidad_temporal` varchar(45) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_catunidadestemporales`
--

LOCK TABLES `declaracion_catunidadestemporales` WRITE;
/*!40000 ALTER TABLE `declaracion_catunidadestemporales` DISABLE KEYS */;
INSERT INTO `declaracion_catunidadestemporales` VALUES (1,'Días','2019-04-04 18:55:52.000000','2019-04-04 18:55:52.000000',0,1,''),(2,'Meses','2019-04-04 18:55:52.000000','2019-04-04 18:55:52.000000',0,1,''),(3,'Años','2019-04-04 18:55:52.000000','2019-04-04 18:55:52.000000',0,1,'');
/*!40000 ALTER TABLE `declaracion_catunidadestemporales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_clientesprincipales`
--

DROP TABLE IF EXISTS `declaracion_clientesprincipales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_clientesprincipales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `porcentaje_facturacion_cliente` decimal(5,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `nombre_encargado` varchar(255) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `realizaActividadLucrativa` tinyint(1) DEFAULT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_clientes_declaraciones_id_147d7ff6_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_clientes_info_personal_var_id_6079b942_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_clientes_observaciones_id_97d3ca01_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_clientes_cat_tipos_relaciones_7223a8f6_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  KEY `declaracion_clientes_moneda_id_91342f75_fk_declaraci` (`moneda_id`),
  KEY `declaracion_clientes_cat_tipos_operacione_8ab9ba69_fk_declaraci` (`cat_tipos_operaciones_id`),
  CONSTRAINT `declaracion_clientes_cat_tipos_operacione_8ab9ba69_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_clientes_cat_tipos_relaciones_7223a8f6_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_clientes_declaraciones_id_147d7ff6_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_clientes_info_personal_var_id_6079b942_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_clientes_moneda_id_91342f75_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_clientes_observaciones_id_97d3ca01_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_clientesprincipales`
--

LOCK TABLES `declaracion_clientesprincipales` WRITE;
/*!40000 ALTER TABLE `declaracion_clientesprincipales` DISABLE KEYS */;
INSERT INTO `declaracion_clientesprincipales` VALUES (3,NULL,'2019-12-24 19:26:11.662843','2019-12-24 19:26:11.662894','Daniela Toscano',3,65,133,1,15,101,563,1),(4,NULL,'2020-01-22 18:32:58.102654','2020-01-22 18:32:58.102721',NULL,9,83,217,1,NULL,NULL,NULL,NULL),(5,NULL,'2020-02-03 23:30:35.700350','2020-02-26 22:23:16.187143',NULL,1,98,276,0,NULL,101,2560,3),(6,NULL,'2020-02-10 20:03:26.112444','2020-02-10 20:03:26.112480',NULL,2,151,338,1,NULL,101,NULL,NULL),(7,NULL,'2020-02-21 22:31:32.327550','2020-02-21 22:37:48.853390',NULL,10,179,391,1,NULL,101,5899,NULL),(8,NULL,'2020-02-25 18:03:55.141616','2020-02-25 18:03:55.141716',NULL,11,181,396,0,8,101,1500,1);
/*!40000 ALTER TABLE `declaracion_clientesprincipales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_conyugedependientes`
--

DROP TABLE IF EXISTS `declaracion_conyugedependientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_conyugedependientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `habita_domicilio` tinyint(1) DEFAULT NULL,
  `medio_contacto` varchar(255) NOT NULL,
  `ingresos_propios` tinyint(1) DEFAULT NULL,
  `ocupacion_profesion` varchar(255) NOT NULL,
  `otro_sector` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otra_relacion` varchar(255) NOT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `declarante_infopersonalvar_id` int(11) NOT NULL,
  `dependiente_infopersonalvar_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `otra_relacion_familiar` varchar(255) NOT NULL,
  `proveedor_contratista` tinyint(1) DEFAULT NULL,
  `actividadLaboral_id` int(11) DEFAULT NULL,
  `actividadLaboralSector_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `es_pareja` tinyint(1),
  `es_extranjero` tinyint(1),
  PRIMARY KEY (`id`),
  KEY `declaracion_conyuged_cat_tipos_relaciones_efff6ef0_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  KEY `declaracion_conyuged_declaraciones_id_9c8c7844_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_conyuged_declarante_infoperso_2bee774e_fk_declaraci` (`declarante_infopersonalvar_id`),
  KEY `declaracion_conyuged_dependiente_infopers_dad7aa87_fk_declaraci` (`dependiente_infopersonalvar_id`),
  KEY `declaracion_conyuged_observaciones_id_b340bffa_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_conyuged_actividadLaboral_id_450b8484_fk_declaraci` (`actividadLaboral_id`),
  KEY `declaracion_conyuged_actividadLaboralSect_dd29cd69_fk_declaraci` (`actividadLaboralSector_id`),
  KEY `declaracion_conyuged_cat_tipos_operacione_f431eb09_fk_declaraci` (`cat_tipos_operaciones_id`),
  CONSTRAINT `declaracion_conyuged_actividadLaboralSect_dd29cd69_fk_declaraci` FOREIGN KEY (`actividadLaboralSector_id`) REFERENCES `declaracion_encargos` (`id`),
  CONSTRAINT `declaracion_conyuged_actividadLaboral_id_450b8484_fk_declaraci` FOREIGN KEY (`actividadLaboral_id`) REFERENCES `declaracion_catambitoslaborales` (`id`),
  CONSTRAINT `declaracion_conyuged_cat_tipos_operacione_f431eb09_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_conyuged_cat_tipos_relaciones_efff6ef0_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_conyuged_declaraciones_id_9c8c7844_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_conyuged_declarante_infoperso_2bee774e_fk_declaraci` FOREIGN KEY (`declarante_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_conyuged_dependiente_infopers_dad7aa87_fk_declaraci` FOREIGN KEY (`dependiente_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_conyuged_observaciones_id_b340bffa_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_conyugedependientes`
--

LOCK TABLES `declaracion_conyugedependientes` WRITE;
/*!40000 ALTER TABLE `declaracion_conyugedependientes` DISABLE KEYS */;
INSERT INTO `declaracion_conyugedependientes` VALUES (1,1,'',1,'','','2019-12-10 15:24:06.275199','2020-02-03 02:33:18.564703','',5,1,1,12,27,'',0,1,4,1,0,1),(2,0,'carro',1,'','','2019-12-24 18:58:53.682233','2019-12-24 19:09:09.824048','',1,3,61,62,127,'',0,1,6,1,1,1),(4,0,'',0,'','','2020-01-28 04:11:52.427784','2020-02-20 00:04:51.461727','',1,10,84,88,236,'',0,1,10,1,1,1),(6,0,'',0,'','','2020-01-30 19:39:44.640778','2020-02-03 18:53:37.663352','',1,1,1,90,240,'',0,2,12,1,1,1),(7,0,'',0,'','','2020-02-04 00:54:16.704641','2020-02-04 06:01:50.860473','',1,9,82,102,281,'',0,2,14,3,1,1),(8,0,'',0,'','','2020-02-04 00:56:46.887223','2020-02-04 01:01:12.972498','',5,9,82,103,282,'',0,2,15,3,0,1),(9,0,'',0,'','','2020-02-10 18:53:21.676781','2020-02-10 18:53:21.676819','',1,2,54,145,328,'',0,1,19,3,1,1),(10,0,'',0,'','','2020-02-10 18:56:03.903926','2020-02-10 18:57:41.909554','',4,2,54,146,329,'',0,2,20,2,0,1),(11,0,'',0,'','','2020-02-17 22:19:56.811869','2020-02-17 22:19:56.811932','',18,4,77,156,365,'',0,2,21,1,0,1),(12,0,'',0,'','','2020-02-17 22:21:33.375332','2020-02-17 23:20:11.068296','',22,4,77,157,366,'',0,1,22,NULL,0,1),(13,1,'',1,'','','2020-02-18 15:50:14.273391','2020-02-26 22:12:00.383649','',22,11,155,158,367,'',0,1,23,1,0,1),(14,1,'',1,'','','2020-02-26 18:45:22.030185','2020-02-26 18:45:22.030223','',1,11,155,186,406,'',0,2,24,1,1,1),(15,0,'',0,'','','2020-02-28 16:04:01.164289','2020-03-06 18:02:11.302897','',13,12,187,188,420,'',0,1,25,1,0,1);
/*!40000 ALTER TABLE `declaracion_conyugedependientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_cuentasporcobrar`
--

DROP TABLE IF EXISTS `declaracion_cuentasporcobrar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cuentasporcobrar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_prestamo` date DEFAULT NULL,
  `monto_original` decimal(12,2) DEFAULT NULL,
  `tasa_interes` decimal(5,2) DEFAULT NULL,
  `saldo_pendiente` decimal(12,2) DEFAULT NULL,
  `fecha_vencimiento` date DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `activos_bienes_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `info_personal_var_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  `num_registro` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_cuentasp_activos_bienes_id_8edf9b5a_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_cuentasp_cat_monedas_id_e94dd753_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_cuentasp_declaraciones_id_d5aed7c6_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_cuentasp_info_personal_var_id_1d30c185_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_cuentasp_observaciones_id_b4d340d4_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_cuentasp_activos_bienes_id_8edf9b5a_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_cuentasp_cat_monedas_id_e94dd753_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_cuentasp_declaraciones_id_d5aed7c6_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_cuentasp_info_personal_var_id_1d30c185_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_cuentasp_observaciones_id_b4d340d4_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_cuentasporcobrar`
--

LOCK TABLES `declaracion_cuentasporcobrar` WRITE;
/*!40000 ALTER TABLE `declaracion_cuentasporcobrar` DISABLE KEYS */;
/*!40000 ALTER TABLE `declaracion_cuentasporcobrar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_datoscurriculares`
--

DROP TABLE IF EXISTS `declaracion_datoscurriculares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_datoscurriculares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `institucion_educativa` varchar(255) NOT NULL,
  `municipio_id` int(11) DEFAULT NULL,
  `carrera_o_area` varchar(255) NOT NULL,
  `conclusion` date DEFAULT NULL,
  `cedula_profesional` varchar(255) NOT NULL,
  `diploma` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_documentos_obtenidos_id` int(11) DEFAULT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  `cat_estatus_estudios_id` int(11) DEFAULT NULL,
  `cat_grados_academicos_id` int(11) DEFAULT NULL,
  `cat_pais_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_datoscur_cat_documentos_obten_50cd4b12_fk_declaraci` (`cat_documentos_obtenidos_id`),
  KEY `declaracion_datoscur_cat_entidades_federa_4c423d71_fk_declaraci` (`cat_entidades_federativas_id`),
  KEY `declaracion_datoscur_cat_estatus_estudios_66ccf80f_fk_declaraci` (`cat_estatus_estudios_id`),
  KEY `declaracion_datoscur_cat_grados_academico_f9f94605_fk_declaraci` (`cat_grados_academicos_id`),
  KEY `declaracion_datoscur_cat_pais_id_1974b772_fk_declaraci` (`cat_pais_id`),
  KEY `declaracion_datoscur_declaraciones_id_ce4fc49f_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_datoscur_observaciones_id_9b7a7cb2_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_datoscurriculares_municipio_id_8892a053` (`municipio_id`),
  KEY `declaracion_datoscur_cat_tipos_operacione_e16bc423_fk_declaraci` (`cat_tipos_operaciones_id`),
  CONSTRAINT `declaracion_datoscur_cat_documentos_obten_50cd4b12_fk_declaraci` FOREIGN KEY (`cat_documentos_obtenidos_id`) REFERENCES `declaracion_catdocumentosobtenidos` (`id`),
  CONSTRAINT `declaracion_datoscur_cat_entidades_federa_4c423d71_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`),
  CONSTRAINT `declaracion_datoscur_cat_estatus_estudios_66ccf80f_fk_declaraci` FOREIGN KEY (`cat_estatus_estudios_id`) REFERENCES `declaracion_catestatusestudios` (`id`),
  CONSTRAINT `declaracion_datoscur_cat_grados_academico_f9f94605_fk_declaraci` FOREIGN KEY (`cat_grados_academicos_id`) REFERENCES `declaracion_catgradosacademicos` (`id`),
  CONSTRAINT `declaracion_datoscur_cat_pais_id_1974b772_fk_declaraci` FOREIGN KEY (`cat_pais_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_datoscur_cat_tipos_operacione_e16bc423_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_datoscur_declaraciones_id_ce4fc49f_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_datoscur_municipio_id_8892a053_fk_declaraci` FOREIGN KEY (`municipio_id`) REFERENCES `declaracion_catmunicipios` (`id`),
  CONSTRAINT `declaracion_datoscur_observaciones_id_9b7a7cb2_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_datoscurriculares`
--

LOCK TABLES `declaracion_datoscurriculares` WRITE;
/*!40000 ALTER TABLE `declaracion_datoscurriculares` DISABLE KEYS */;
INSERT INTO `declaracion_datoscurriculares` VALUES (1,'UNAM',2,'Ingenería en Tecnológias de la Información y la comunicación','2019-03-11','15224555544','','2019-12-06 19:07:03.252449','2020-01-16 22:23:39.597272',4,9,2,4,142,1,8,1),(2,'UTZMG',NULL,'Nuevo dato','2019-11-25','','','2019-12-15 04:02:03.643639','2020-02-10 18:13:24.343433',4,NULL,1,4,142,2,86,1),(3,'UTZMG',13,'Ingenería en sistemas','2014-06-12','15224555544','','2019-12-24 18:47:51.951784','2019-12-24 18:47:51.951828',4,9,3,4,142,3,124,1),(4,'ST56',NULL,'Secundaria','2006-06-08','','','2020-01-16 22:24:54.380163','2020-02-01 20:43:18.790256',1,NULL,2,2,142,1,191,1),(5,'UNIVA',NULL,'LIC. CONTRALORIA','2010-02-20','','','2020-02-04 00:32:59.586536','2020-02-04 00:32:59.586583',4,NULL,2,3,142,9,278,3),(6,'UNAM',NULL,'Ingenería en Tecnológias de la Información y la comunicación','2016-01-11','','','2020-02-19 20:00:00.536818','2020-02-21 19:45:48.405827',3,NULL,2,2,142,10,376,2),(7,'UTZMG',NULL,'',NULL,'','','2020-02-19 20:20:11.028320','2020-02-21 19:43:49.260488',NULL,NULL,1,4,142,10,379,1),(8,'UTZMG',NULL,'Ingenería en sistemas','2016-06-21','','','2020-02-25 18:09:00.067116','2020-02-25 18:09:00.067175',4,NULL,2,5,142,11,398,1);
/*!40000 ALTER TABLE `declaracion_datoscurriculares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_declaraciones`
--

DROP TABLE IF EXISTS `declaracion_declaraciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_declaraciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` char(32) NOT NULL,
  `fecha_recepcion` datetime(6) DEFAULT NULL,
  `fecha_declaracion` date DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_tipos_declaracion_id` int(11) DEFAULT NULL,
  `info_personal_fija_id` int(11) NOT NULL,
  `cat_estatus_id` int(11) NOT NULL,
  `avance` int(11) NOT NULL,
  `sello` longtext NOT NULL,
  `datos_publicos` tinyint(1),
  PRIMARY KEY (`id`),
  KEY `declaracion_declarac_cat_tipos_declaracio_9358630c_fk_declaraci` (`cat_tipos_declaracion_id`),
  KEY `declaracion_declarac_info_personal_fija_i_df3b9808_fk_declaraci` (`info_personal_fija_id`),
  KEY `declaracion_declarac_cat_estatus_id_02b4ac18_fk_declaraci` (`cat_estatus_id`),
  CONSTRAINT `declaracion_declarac_cat_estatus_id_02b4ac18_fk_declaraci` FOREIGN KEY (`cat_estatus_id`) REFERENCES `declaracion_catestatusdeclaracion` (`id`),
  CONSTRAINT `declaracion_declarac_cat_tipos_declaracio_9358630c_fk_declaraci` FOREIGN KEY (`cat_tipos_declaracion_id`) REFERENCES `declaracion_cattiposdeclaracion` (`id`),
  CONSTRAINT `declaracion_declarac_info_personal_fija_i_df3b9808_fk_declaraci` FOREIGN KEY (`info_personal_fija_id`) REFERENCES `declaracion_infopersonalfija` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_declaraciones`
--

LOCK TABLES `declaracion_declaraciones` WRITE;
/*!40000 ALTER TABLE `declaracion_declaraciones` DISABLE KEYS */;
INSERT INTO `declaracion_declaraciones` VALUES (1,'d98ab35c443f4c31b58d774795ea4834',NULL,'2019-12-06','2019-12-06 18:33:37.952353','2020-02-26 22:33:30.325839',1,1,1,94,'',0),(2,'7f1318c7a09c4e4ab78ff924168698d3','2020-02-11 17:45:45.080105','2019-12-15','2019-12-15 03:15:10.601100','2020-02-11 17:45:45.080394',1,2,4,100,'',0),(3,'e908c28d264b4ed0bf51d71cb65b1ebf',NULL,'2019-12-24','2019-12-24 18:41:50.149747','2020-01-30 20:47:13.093298',1,5,1,35,'',0),(4,'92d923196fde4a5ebfa270b36c316cd8',NULL,'2020-01-13','2020-01-13 19:36:38.104488','2020-03-03 16:37:19.235899',1,6,1,14,'',0),(5,'9df0c8a9c13c46d4b3b0e562fca0e6a1',NULL,'2020-01-13','2020-01-13 22:33:56.721076','2020-01-24 17:51:11.011963',1,11,1,5,'',0),(6,'0c3ca671ac924d568da80e262dcd2b79',NULL,'2020-01-14','2020-01-14 15:41:21.859117','2020-02-18 17:03:39.288978',1,7,1,5,'',0),(9,'9e63f4a2a1754313a511973d5625303c',NULL,'2020-01-22','2020-01-22 18:29:18.684023','2020-02-26 22:34:29.768860',1,15,1,77,'',0),(10,'2fd17077625540c3a80e7f689d9c8b0e',NULL,'2020-01-22','2020-01-22 21:29:04.996520','2020-03-18 16:53:25.647732',1,13,1,100,'',0),(11,'bb93cebe481843c9b7611b41a0d1fed3',NULL,'2020-02-17','2020-02-17 15:21:11.341715','2020-02-27 18:13:47.065925',1,27,1,62,'',0),(12,'8e621201d3e144fd824fa780a45d6ecf',NULL,'2020-02-28','2020-02-28 16:01:52.432763','2020-03-18 16:53:51.638590',1,2,1,28,'',0),(13,'db8227dcb15e4601aa222e65da7415c4',NULL,'2020-03-03','2020-03-03 16:38:10.835069','2020-03-04 15:49:17.220869',3,9,1,6,'',0);
/*!40000 ALTER TABLE `declaracion_declaraciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_declaracionfiscal`
--

DROP TABLE IF EXISTS `declaracion_declaracionfiscal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_declaracionfiscal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archivo_xml` varchar(100) NOT NULL,
  `declaraciones_id` int(11) DEFAULT NULL,
  `archivo_pdf` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_declarac_declaraciones_id_dda251b7_fk_declaraci` (`declaraciones_id`),
  CONSTRAINT `declaracion_declarac_declaraciones_id_dda251b7_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_declaracionfiscal`
--

LOCK TABLES `declaracion_declaracionfiscal` WRITE;
/*!40000 ALTER TABLE `declaracion_declaracionfiscal` DISABLE KEYS */;
INSERT INTO `declaracion_declaracionfiscal` VALUES (3,'declaracion_fiscal/xml/FA0000000061-Recibo_de_Honorario-Factura_doctor_rehabilitacion.xml',4,'declaracion_fiscal/pdf/FA0000000061-Recibo_de_Honorario-Factura_doctor_rehabilitacion.pdf'),(4,'declaracion_fiscal/xml/FA0000000061-Recibo_de_Honorario-Factura_doctor_rehabilitacion_XAGoXUv.xml',5,'declaracion_fiscal/pdf/FA0000000061-Recibo_de_Honorario-Factura_doctor_rehabilitacion_rvlFEnf.pdf'),(9,'declaracion_fiscal/xml/FA0000000061-Recibo_de_Honorario-Factura_doctor_rehabilitacion.xml',6,'declaracion_fiscal/pdf/FA0000000061-Recibo_de_Honorario-Factura_doctor_rehabilitacion.pdf'),(10,'',2,'declaracion_fiscal/pdf/FA0000000061-Recibo_de_Honorario-Factura_doctor_rehabilitacion.pdf'),(11,'',10,'declaracion_fiscal/pdf/FA0000000061-Recibo_de_Honorario-Factura_doctor_rehabilitacion_CeAQhNE.pdf'),(12,'',12,'declaracion_fiscal/pdf/FA0000000061-Recibo_de_Honorario-Factura_doctor_rehabilitacion_RpYo3Cx.pdf');
/*!40000 ALTER TABLE `declaracion_declaracionfiscal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_deudasotros`
--

DROP TABLE IF EXISTS `declaracion_deudasotros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_deudasotros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otro_tipo_acreedor` varchar(255) NOT NULL,
  `otro_tipo_adeudo` varchar(255) NOT NULL,
  `numero_cuenta` varchar(255) NOT NULL,
  `fecha_generacion` date DEFAULT NULL,
  `monto_original` decimal(12,2) DEFAULT NULL,
  `tasa_interes` decimal(5,2) DEFAULT NULL,
  `saldo_pendiente` decimal(12,2) DEFAULT NULL,
  `monto_abonado` decimal(12,2) DEFAULT NULL,
  `plazo` decimal(6,2) DEFAULT NULL,
  `otro_titular` varchar(255) NOT NULL,
  `porcentaje_adeudo` decimal(5,2) DEFAULT NULL,
  `garantia` tinyint(1) DEFAULT NULL,
  `nombre_garantes` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `acreedor_infopersonalvar_id` int(11) NOT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipos_acreedores_id` int(11) DEFAULT NULL,
  `cat_tipos_adeudos_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_pasivos_id` int(11) DEFAULT NULL,
  `cat_tipos_titulares_id` int(11) DEFAULT NULL,
  `cat_unidades_temporales_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `acreedor_nombre` varchar(255) DEFAULT NULL,
  `acreedor_rfc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_deudasot_acreedor_infopersona_2d15b68b_fk_declaraci` (`acreedor_infopersonalvar_id`),
  KEY `declaracion_deudasot_cat_monedas_id_cc17183f_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_deudasot_cat_paises_id_33c78190_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_deudasot_cat_tipos_acreedores_5287dc71_fk_declaraci` (`cat_tipos_acreedores_id`),
  KEY `declaracion_deudasot_cat_tipos_adeudos_id_e73d9123_fk_declaraci` (`cat_tipos_adeudos_id`),
  KEY `declaracion_deudasot_cat_tipos_operacione_6508c122_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_deudasot_cat_tipos_pasivos_id_2289f5d6_fk_declaraci` (`cat_tipos_pasivos_id`),
  KEY `declaracion_deudasot_cat_tipos_titulares__b12ef02a_fk_declaraci` (`cat_tipos_titulares_id`),
  KEY `declaracion_deudasot_cat_unidades_tempora_3a4d00cc_fk_declaraci` (`cat_unidades_temporales_id`),
  KEY `declaracion_deudasot_declaraciones_id_50e8cf5f_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_deudasot_observaciones_id_ad9eaf9a_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_deudasot_acreedor_infopersona_2d15b68b_fk_declaraci` FOREIGN KEY (`acreedor_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_monedas_id_cc17183f_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_paises_id_33c78190_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_tipos_acreedores_5287dc71_fk_declaraci` FOREIGN KEY (`cat_tipos_acreedores_id`) REFERENCES `declaracion_cattiposacreedores` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_tipos_adeudos_id_e73d9123_fk_declaraci` FOREIGN KEY (`cat_tipos_adeudos_id`) REFERENCES `declaracion_cattiposadeudos` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_tipos_operacione_6508c122_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_tipos_pasivos_id_2289f5d6_fk_declaraci` FOREIGN KEY (`cat_tipos_pasivos_id`) REFERENCES `declaracion_cattipospasivos` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_tipos_titulares__b12ef02a_fk_declaraci` FOREIGN KEY (`cat_tipos_titulares_id`) REFERENCES `declaracion_cattipostitulares` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_unidades_tempora_3a4d00cc_fk_declaraci` FOREIGN KEY (`cat_unidades_temporales_id`) REFERENCES `declaracion_catunidadestemporales` (`id`),
  CONSTRAINT `declaracion_deudasot_declaraciones_id_50e8cf5f_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_deudasot_observaciones_id_ad9eaf9a_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_deudasotros`
--

LOCK TABLES `declaracion_deudasotros` WRITE;
/*!40000 ALTER TABLE `declaracion_deudasotros` DISABLE KEYS */;
INSERT INTO `declaracion_deudasotros` VALUES (1,'','','','824616465','1910-01-01',56666.00,0.23,68965.00,21.00,2.00,'',200.00,0,'','2020-02-10 19:35:48.834720','2020-02-10 19:36:54.503505',147,101,142,NULL,1,2,1,1,NULL,2,332,NULL,NULL),(2,'','','','859955259-855','2019-01-01',85963.00,NULL,1000.00,NULL,0.00,'',NULL,0,'','2020-02-21 22:24:09.033593','2020-03-18 16:53:14.889917',175,101,142,3,2,1,1,3,NULL,10,385,NULL,NULL);
/*!40000 ALTER TABLE `declaracion_deudasotros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_domicilios`
--

DROP TABLE IF EXISTS `declaracion_domicilios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_domicilios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `municipio_id` int(11) DEFAULT NULL,
  `cp` varchar(255) NOT NULL,
  `colonia` varchar(255) NOT NULL,
  `nombre_via` varchar(255) NOT NULL,
  `num_exterior` varchar(255) NOT NULL,
  `num_interior` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  `cat_pais_id` int(11) DEFAULT NULL,
  `cat_tipo_via_id` int(11) DEFAULT NULL,
  `ciudadLocalidad` varchar(100) DEFAULT NULL,
  `estadoProvincia` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_domicili_cat_entidades_federa_456c9653_fk_declaraci` (`cat_entidades_federativas_id`),
  KEY `declaracion_domicili_cat_pais_id_930ca211_fk_declaraci` (`cat_pais_id`),
  KEY `declaracion_domicili_cat_tipo_via_id_bc93edfc_fk_declaraci` (`cat_tipo_via_id`),
  KEY `declaracion_domicilios_municipio_id_7055d0b1` (`municipio_id`),
  CONSTRAINT `declaracion_domicili_cat_entidades_federa_456c9653_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`),
  CONSTRAINT `declaracion_domicili_cat_pais_id_930ca211_fk_declaraci` FOREIGN KEY (`cat_pais_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_domicili_cat_tipo_via_id_bc93edfc_fk_declaraci` FOREIGN KEY (`cat_tipo_via_id`) REFERENCES `declaracion_cattipovia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_domicilios`
--

LOCK TABLES `declaracion_domicilios` WRITE;
/*!40000 ALTER TABLE `declaracion_domicilios` DISABLE KEYS */;
INSERT INTO `declaracion_domicilios` VALUES (1,NULL,'','','','','','2019-12-06 18:33:37.930955','2020-02-26 22:26:03.697832',14,142,NULL,NULL,NULL),(2,NULL,'4558','','Filipinas','350','5','2019-12-06 19:19:15.532619','2020-02-26 22:25:11.157162',14,142,NULL,'Tokio','Shinguko'),(3,NULL,'','','','','','2019-12-09 15:36:20.989911','2020-02-01 21:27:45.373760',NULL,142,NULL,NULL,NULL),(4,1,'4456','Saruno','Filipinas','350','5','2019-12-09 16:28:40.373674','2019-12-09 16:28:40.373725',14,142,5,NULL,NULL),(5,1,'4456','Saruno','Filipinas','125','5','2019-12-09 16:32:45.114829','2019-12-09 22:03:44.469741',14,142,5,NULL,NULL),(6,1,'4456','Saruno','Filipinas','125','5','2019-12-09 17:51:06.829159','2019-12-09 17:51:06.829206',14,142,5,NULL,NULL),(7,1,'4456','Saruno','Filipinas','125','5','2019-12-09 17:51:32.532038','2019-12-09 17:51:32.532089',14,142,5,NULL,NULL),(8,2,'4456','Tabachines','','125','2','2019-12-09 17:54:57.345848','2019-12-09 17:55:14.693644',14,142,NULL,NULL,NULL),(9,1,'4456','JArdines del bosque','filipinas','125','2','2019-12-09 17:57:18.055325','2019-12-09 17:57:18.055369',9,142,16,NULL,NULL),(10,1,'4456','JArdines del bosque','filipinas','125','5','2019-12-09 18:11:14.720116','2019-12-09 18:11:14.720150',14,142,14,NULL,NULL),(11,1,'','JArdines del bosque','filipinas','125','1','2019-12-09 18:33:03.699605','2019-12-09 18:33:03.699650',14,142,5,NULL,NULL),(12,1,'4456','Jardines del bosque','filipinas','125','5','2019-12-09 18:35:13.252986','2019-12-09 21:04:20.750545',14,142,5,NULL,NULL),(13,2,'12','Las cebollas','Itsukami','350','5','2019-12-09 22:22:36.554504','2019-12-09 23:47:08.613282',14,142,10,NULL,NULL),(14,1,'4456','JArdines del bosque','Filipinas','125','2','2019-12-10 15:24:06.052410','2020-02-03 02:33:18.405470',14,142,NULL,NULL,NULL),(15,NULL,'','','','','','2019-12-10 23:35:36.445183','2020-02-26 22:22:55.534281',14,142,NULL,NULL,NULL),(16,NULL,'','','','','','2019-12-11 22:18:58.817359','2020-02-26 22:23:08.679873',NULL,NULL,NULL,NULL,NULL),(17,1,'4456','Saruno','Chapultepec','767','1','2019-12-11 22:46:53.142473','2019-12-11 23:28:38.681028',14,142,5,NULL,NULL),(18,2,'4456','Jardines del bosque','Filipinas','125','2','2019-12-11 23:30:40.870972','2020-01-22 17:39:37.912759',9,142,5,NULL,NULL),(19,1,'4558','Oblatos','Filipinas','125','2','2019-12-12 22:17:59.078062','2019-12-13 04:55:15.512993',14,142,5,NULL,NULL),(20,1,'4456','JArdines del bosque','Itsukami','125','5','2019-12-13 04:58:15.158044','2019-12-13 05:01:37.415542',3,142,14,NULL,NULL),(21,1,'','','','','','2019-12-13 16:13:55.691012','2019-12-14 04:52:30.395545',NULL,NULL,NULL,NULL,NULL),(22,1,'','','','','','2019-12-13 16:58:52.898333','2019-12-14 04:18:58.768999',NULL,NULL,NULL,NULL,NULL),(23,1,'','','','','','2019-12-13 17:01:02.444845','2019-12-14 04:30:35.662322',NULL,NULL,NULL,NULL,NULL),(24,1,'44950','Jardines del bosque','filipinas','125','2','2019-12-13 22:25:08.658341','2019-12-14 04:23:06.438130',14,142,3,NULL,NULL),(25,1,'','','','','','2019-12-14 04:20:36.111403','2019-12-14 04:20:36.111470',NULL,NULL,NULL,NULL,NULL),(26,1,'','','','','','2019-12-14 04:52:01.256136','2019-12-14 05:03:32.165880',NULL,NULL,NULL,NULL,NULL),(27,1,'','','','','','2019-12-14 05:03:02.532593','2019-12-14 05:03:02.532700',NULL,NULL,NULL,NULL,NULL),(28,1,'','','','','','2019-12-14 05:03:47.893140','2019-12-14 05:03:47.893207',NULL,NULL,NULL,NULL,NULL),(29,1,'','','','','','2019-12-14 18:35:44.638027','2019-12-14 18:35:44.638081',NULL,NULL,NULL,NULL,NULL),(30,1,'','','','','','2019-12-14 19:18:06.751072','2019-12-14 19:18:06.751115',NULL,NULL,NULL,NULL,NULL),(31,1,'','','','','','2019-12-14 19:19:03.819331','2019-12-14 19:19:03.819431',NULL,NULL,NULL,NULL,NULL),(32,1,'','','','','','2019-12-14 19:19:17.004915','2019-12-14 19:19:17.004986',NULL,NULL,NULL,NULL,NULL),(33,1,'','','','','','2019-12-14 19:20:29.482080','2019-12-14 19:20:29.482123',NULL,NULL,NULL,NULL,NULL),(34,1,'','','','','','2019-12-14 19:23:19.406502','2019-12-14 19:23:19.406546',NULL,NULL,NULL,NULL,NULL),(35,1,'','','','','','2019-12-14 19:34:17.988570','2019-12-14 19:34:17.988614',NULL,NULL,NULL,NULL,NULL),(36,1,'','','','','','2019-12-14 19:37:14.533320','2019-12-14 19:37:14.533364',NULL,NULL,NULL,NULL,NULL),(37,1,'','','','','','2019-12-14 19:38:37.849638','2019-12-14 19:38:37.849722',NULL,NULL,NULL,NULL,NULL),(38,1,'4456','Saruno','Filipinas','350','1','2019-12-14 19:46:38.721179','2019-12-14 19:46:38.721350',14,142,19,NULL,NULL),(39,1,'4456','JArdines del bosque','Av. de los arcos','125','','2019-12-14 20:22:18.660855','2019-12-14 20:22:18.660897',7,142,11,NULL,NULL),(40,1,'4456','Tapalaco','Libra','356','5','2019-12-14 20:24:38.827768','2019-12-14 20:24:38.827811',9,142,5,NULL,NULL),(41,1,'58','Juan manuel vallarta','Soles','589','1','2019-12-14 21:06:59.745047','2019-12-14 21:06:59.745074',9,142,5,NULL,NULL),(42,1,'','','','','','2019-12-14 21:07:50.603518','2019-12-14 21:07:50.603554',9,142,NULL,NULL,NULL),(43,1,'','','','','','2019-12-14 21:09:07.597501','2019-12-14 21:09:07.597530',9,142,NULL,NULL,NULL),(44,1,'','','','','','2019-12-14 21:10:06.041723','2019-12-14 21:10:06.041752',9,142,NULL,NULL,NULL),(45,1,'','','','','','2019-12-14 21:11:42.745714','2019-12-14 21:11:42.745743',9,142,NULL,NULL,NULL),(46,1,'','','','','','2019-12-14 21:15:49.042937','2019-12-14 21:15:49.043001',9,142,NULL,NULL,NULL),(47,1,'','','','','','2019-12-14 21:18:28.767452','2019-12-14 21:18:28.767478',9,142,NULL,NULL,NULL),(48,1,'','','','','','2019-12-14 21:25:14.629196','2019-12-14 21:25:14.629246',9,142,NULL,NULL,NULL),(49,1,'','','','','','2019-12-14 22:14:14.805483','2019-12-14 22:14:14.805514',9,142,NULL,NULL,NULL),(50,1,'','','','','','2019-12-14 22:15:39.097468','2019-12-14 22:15:39.097497',9,142,NULL,NULL,NULL),(51,1,'','','','','','2019-12-14 22:54:41.133476','2019-12-14 22:54:41.133502',9,142,NULL,NULL,NULL),(52,1,'','','','','','2019-12-14 22:54:59.610248','2020-01-09 18:22:24.753335',NULL,NULL,NULL,NULL,NULL),(53,1,'','','','','','2019-12-15 00:28:00.185659','2019-12-24 17:33:16.393363',NULL,NULL,NULL,NULL,NULL),(54,1,'','','','','','2019-12-15 01:04:19.247749','2019-12-23 20:25:32.035897',NULL,NULL,NULL,NULL,NULL),(55,576,'4456','Jardines del bosque','Filipinas','284','1','2019-12-15 03:15:10.525956','2020-02-10 18:12:30.589382',14,142,NULL,NULL,NULL),(56,1,'','','','','','2019-12-20 16:04:24.781547','2019-12-20 16:04:24.781617',NULL,NULL,NULL,NULL,NULL),(57,1,'','','','','','2019-12-20 16:33:41.399229','2019-12-20 16:47:31.276677',NULL,NULL,NULL,NULL,NULL),(58,1,'','','','','','2019-12-20 18:22:13.580123','2019-12-20 18:22:13.580164',NULL,NULL,NULL,NULL,NULL),(59,1,'','','','','','2019-12-20 18:24:21.919919','2019-12-20 18:24:21.919960',NULL,NULL,NULL,NULL,NULL),(60,1,'','','','','','2019-12-23 20:25:59.583337','2019-12-24 17:54:51.325355',NULL,NULL,NULL,NULL,NULL),(61,656,'44580','Las cebollas','Torres','450','2','2019-12-24 18:41:50.127951','2019-12-24 18:41:50.128004',9,142,5,NULL,NULL),(62,13,'4558','Saruno','Filipinas','125','2','2019-12-24 18:48:33.490863','2019-12-24 18:48:33.490928',9,142,5,NULL,NULL),(63,15,'520','Jardines de la c','Itsukami','356','2','2019-12-24 18:56:23.385415','2019-12-24 18:56:23.385468',9,142,5,NULL,NULL),(64,24,'12','Saruno','ejemplo','125','2','2019-12-24 18:58:53.644759','2019-12-24 19:09:09.797560',9,142,16,NULL,NULL),(65,34,'520','Las cebollas','arteaa','125','5','2019-12-24 19:11:54.634483','2019-12-24 19:12:20.561011',9,142,5,NULL,NULL),(66,1,'','','','','','2019-12-24 19:23:19.449619','2019-12-24 19:23:19.449672',NULL,142,NULL,NULL,NULL),(67,12,'4456','Jardines del bosque','Filipinas','125','5','2019-12-24 19:26:11.655663','2019-12-24 19:26:11.655718',9,142,5,NULL,NULL),(68,1,'','','','','','2019-12-24 19:33:59.304286','2019-12-24 19:33:59.304341',NULL,NULL,NULL,NULL,NULL),(69,1,'','','','','','2019-12-24 19:34:15.537389','2019-12-24 19:34:15.537431',NULL,NULL,NULL,NULL,NULL),(70,1,'','','','','','2019-12-24 19:35:07.651127','2019-12-24 19:35:07.651184',NULL,NULL,NULL,NULL,NULL),(71,1,'','','','','','2019-12-24 19:37:28.709824','2019-12-24 19:37:28.709868',NULL,NULL,NULL,NULL,NULL),(72,1,'','','','','','2019-12-24 19:38:03.389825','2019-12-24 19:38:03.389905',NULL,NULL,NULL,NULL,NULL),(73,1,'','','','','','2019-12-24 19:38:33.308445','2019-12-24 19:38:33.308501',NULL,NULL,NULL,NULL,NULL),(74,1,'','','','','','2019-12-24 19:38:54.142749','2019-12-24 19:38:54.142801',NULL,NULL,NULL,NULL,NULL),(75,1,'','','','','','2019-12-24 19:45:35.886853','2019-12-24 19:45:35.886927',NULL,NULL,NULL,NULL,NULL),(76,1,'','','','','','2019-12-24 19:56:30.919454','2019-12-24 19:56:30.919522',NULL,NULL,NULL,NULL,NULL),(77,656,'4456','JArdines del bosque','Filipinas','125','5','2020-01-13 19:36:37.968320','2020-01-13 19:36:37.968372',14,142,6,NULL,NULL),(78,17,'4558','Jardines del bosque','filipinas','125','2','2020-01-13 20:02:19.261200','2020-01-13 20:02:19.261256',9,142,14,NULL,NULL),(79,16,'520','Jardines de la c','filipinas','450','5','2020-01-13 22:33:56.703735','2020-01-13 22:33:56.703772',9,142,12,NULL,NULL),(80,13,'4456','Jardines del bosque','Filipinas','450','5','2020-01-14 15:41:21.831409','2020-01-14 16:10:08.086735',9,142,5,NULL,NULL),(81,240,'4558','Jardines del bosque','filipinas','125','2','2020-01-22 17:48:46.145441','2020-01-22 17:48:46.145471',9,142,7,NULL,NULL),(82,4,'44950','JArdines del bosque','filipinas','125','5','2020-01-22 18:07:06.437412','2020-01-22 18:07:06.437442',9,142,3,NULL,NULL),(83,1,'4254','JARDINES DE LA CRUZ','Isla cozumel','10','1','2020-01-22 18:29:18.672649','2020-02-04 00:28:59.689245',14,142,NULL,NULL,NULL),(84,1,'','','','','','2020-01-22 18:32:58.098498','2020-01-22 18:32:58.098537',9,142,NULL,NULL,NULL),(85,NULL,'','','','','','2020-01-22 21:29:04.987477','2020-02-27 23:06:47.614047',NULL,NULL,NULL,NULL,NULL),(86,539,'4456','C4','Toronja','125','5','2020-01-27 18:14:14.900157','2020-01-27 18:14:14.900183',14,142,5,NULL,NULL),(87,549,'4456','JArdines del bosque','Filipinas','125','5','2020-01-27 18:59:07.777648','2020-01-27 19:28:57.377016',14,142,5,NULL,NULL),(88,539,'4456','C4','Filipinas','125','2','2020-01-27 20:01:01.671290','2020-01-27 20:01:01.671319',14,142,5,NULL,NULL),(89,539,'4456','C4','Filipinas','125','2','2020-01-27 20:06:24.254515','2020-01-28 16:25:24.754013',14,142,5,NULL,NULL),(90,539,'450','Jardines del bosque','Filipinas','125','2','2020-01-28 04:11:52.369136','2020-02-20 00:04:51.301720',14,142,NULL,NULL,NULL),(91,540,'458','Sodiaco','Acuario','350','2','2020-01-28 16:28:15.142976','2020-01-28 16:28:15.143004',14,142,5,NULL,NULL),(92,553,'4558','Jardines del bosque','Calle 32','5','5','2020-01-30 19:39:44.211475','2020-02-03 18:53:37.636365',14,142,NULL,NULL,NULL),(93,557,'123','Jardines de la c','Filipinas','125','5','2020-01-31 22:23:11.846915','2020-02-26 22:25:32.067315',14,142,NULL,NULL,NULL),(94,555,'852','Jarillas','Salinas','203','200','2020-01-31 22:35:30.796384','2020-01-31 22:37:26.608983',14,142,5,NULL,NULL),(95,NULL,'','','','','','2020-02-03 23:30:35.554082','2020-02-26 22:23:16.180316',14,142,NULL,NULL,NULL),(96,NULL,'','','','','','2020-02-04 00:25:22.367503','2020-02-04 00:27:25.098253',NULL,NULL,NULL,NULL,NULL),(97,NULL,'','','','','','2020-02-04 00:25:22.369386','2020-02-04 00:27:25.101708',NULL,NULL,NULL,NULL,NULL),(98,NULL,'','','','','','2020-02-04 00:25:22.371189','2020-02-04 00:27:25.104847',NULL,NULL,NULL,NULL,NULL),(99,1,'452','Paseos del sol','Legaspi','552','A','2020-02-04 00:40:00.717893','2020-02-04 00:40:00.717966',14,142,NULL,NULL,NULL),(100,NULL,'','','','','','2020-02-04 00:44:29.565843','2020-02-04 00:46:46.389255',NULL,142,NULL,NULL,NULL),(101,1,'452','LOMAS','TORRES','250','1','2020-02-04 00:54:16.693684','2020-02-04 06:01:50.844345',14,142,NULL,NULL,NULL),(102,1,'756','Bosque','SARRO','52','45','2020-02-04 00:56:46.861723','2020-02-04 01:01:12.959848',14,142,NULL,NULL,NULL),(103,1,'','ARCOS','SAGITARIO','200','2','2020-02-04 01:38:25.458859','2020-02-04 01:38:25.458914',14,142,NULL,NULL,NULL),(104,NULL,'','','','','','2020-02-04 01:55:39.772138','2020-02-04 01:55:39.772169',NULL,NULL,NULL,NULL,NULL),(105,NULL,'','','','','','2020-02-04 01:57:01.218999','2020-02-04 02:48:21.158207',14,142,NULL,NULL,NULL),(106,NULL,'','','','','','2020-02-04 14:24:14.184832','2020-02-04 14:24:14.184858',14,142,NULL,NULL,NULL),(107,NULL,'','','','','','2020-02-04 14:26:19.795067','2020-02-04 14:26:19.795119',NULL,142,NULL,NULL,NULL),(108,NULL,'','','','','','2020-02-04 14:29:29.735199','2020-02-04 15:05:23.967419',NULL,NULL,NULL,NULL,NULL),(109,NULL,'','','','','','2020-02-04 14:29:29.736944','2020-02-04 15:05:23.977451',NULL,NULL,NULL,NULL,NULL),(110,NULL,'','','','','','2020-02-04 14:29:29.737982','2020-02-04 15:05:23.993553',NULL,NULL,NULL,NULL,NULL),(111,NULL,'','','','','','2020-02-04 16:12:46.703943','2020-02-04 16:12:46.704001',14,142,NULL,NULL,NULL),(112,NULL,'','','','','','2020-02-04 16:15:57.740946','2020-02-04 16:15:57.741006',NULL,NULL,NULL,NULL,NULL),(113,NULL,'','','','','','2020-02-04 16:15:57.744202','2020-02-04 16:15:57.744260',NULL,NULL,NULL,NULL,NULL),(114,NULL,'','','','','','2020-02-04 16:15:57.747369','2020-02-04 16:15:57.747426',NULL,NULL,NULL,NULL,NULL),(115,NULL,'','','','','','2020-02-04 16:25:03.193899','2020-02-04 16:25:03.193961',NULL,142,NULL,NULL,NULL),(116,NULL,'','','','','','2020-02-04 16:27:59.864552','2020-02-04 16:27:59.864581',NULL,142,NULL,NULL,NULL),(117,NULL,'','','','','','2020-02-04 16:38:56.924321','2020-02-04 16:38:56.924356',NULL,142,NULL,NULL,NULL),(118,NULL,'','','','','','2020-02-04 16:48:41.981867','2020-02-04 16:56:24.820290',NULL,142,NULL,NULL,NULL),(119,540,'458','Sodiaco','Acuario','350','2','2020-02-04 17:47:50.641883','2020-02-04 17:47:50.641948',14,142,NULL,NULL,NULL),(120,NULL,'','','','','','2020-02-04 17:53:39.716314','2020-02-04 17:53:39.716372',14,142,NULL,NULL,NULL),(121,NULL,'','','','','','2020-02-04 18:30:23.839835','2020-02-04 18:30:23.839898',14,142,NULL,NULL,NULL),(122,NULL,'','','','','','2020-02-04 18:31:41.496110','2020-02-04 18:31:41.496139',14,142,NULL,NULL,NULL),(123,NULL,'','','','','','2020-02-04 21:34:43.495271','2020-02-04 22:31:16.725874',NULL,NULL,NULL,NULL,NULL),(124,NULL,'','','','','','2020-02-04 22:32:42.461790','2020-02-04 22:32:42.461824',NULL,NULL,NULL,NULL,NULL),(125,NULL,'','','','','','2020-02-04 22:33:04.168050','2020-02-04 22:33:04.168082',NULL,NULL,NULL,NULL,NULL),(126,NULL,'','','','','','2020-02-04 22:33:48.522759','2020-02-04 22:33:48.522790',NULL,NULL,NULL,NULL,NULL),(127,NULL,'','','','','','2020-02-04 22:36:02.781306','2020-02-04 22:36:02.781334',NULL,NULL,NULL,NULL,NULL),(128,NULL,'450','LA CRUZ','ARTEAGA','450','0','2020-02-04 22:40:02.577430','2020-02-06 22:12:50.860374',14,142,NULL,NULL,NULL),(129,538,'123','','','fsdg','788','2020-02-06 20:00:23.662057','2020-02-06 20:00:23.662090',14,142,NULL,'dfsg',NULL),(130,539,'12','DFSF','DSDSS','12','22','2020-02-06 20:06:34.783264','2020-02-06 20:06:34.783302',14,142,NULL,NULL,NULL),(131,539,'12','DFSF','DSDSS','12','22','2020-02-06 20:09:27.618745','2020-02-06 20:09:27.618777',14,142,NULL,NULL,NULL),(132,539,'12','DFSF','DSDSS','12','22','2020-02-06 20:11:44.186262','2020-02-06 20:11:44.186300',14,142,NULL,NULL,NULL),(133,539,'12','DFSF','DSDSS','12','22','2020-02-06 20:22:39.446923','2020-02-06 20:22:39.446955',14,142,NULL,NULL,NULL),(134,539,'12','DFSF','DSDSS','12','22','2020-02-06 20:22:55.276051','2020-02-06 20:22:55.276090',14,142,NULL,NULL,NULL),(135,NULL,'','','','','','2020-02-06 23:01:37.605775','2020-02-06 23:01:37.605859',14,142,NULL,NULL,NULL),(136,NULL,'','','','','','2020-02-07 04:15:45.480852','2020-02-07 04:15:45.480937',NULL,NULL,NULL,NULL,NULL),(137,576,'4456','Jardines de la c','Filipinas','125','20','2020-02-07 05:03:41.066249','2020-02-07 05:03:41.066335',14,142,NULL,NULL,NULL),(138,NULL,'','','','','','2020-02-10 18:16:13.694675','2020-02-10 18:19:00.730682',14,142,NULL,NULL,NULL),(139,NULL,'','','','','','2020-02-10 18:31:35.363425','2020-02-10 18:31:35.363470',NULL,142,NULL,NULL,NULL),(140,NULL,'','','','','','2020-02-10 18:53:21.561574','2020-02-10 18:53:21.561612',14,142,NULL,NULL,NULL),(141,NULL,'','','','','','2020-02-10 18:56:03.877905','2020-02-10 18:57:41.894245',14,142,NULL,NULL,NULL),(142,NULL,'','','','','','2020-02-10 19:35:48.829396','2020-02-10 19:36:54.494930',NULL,NULL,NULL,NULL,NULL),(143,NULL,'','','','','','2020-02-10 19:38:32.597117','2020-02-10 19:38:32.597158',NULL,NULL,NULL,NULL,NULL),(144,NULL,'','','','','','2020-02-10 19:40:33.392641','2020-02-10 19:40:47.018150',NULL,NULL,NULL,NULL,NULL),(145,NULL,'','','','','','2020-02-10 19:57:18.613418','2020-02-10 19:57:18.613464',14,142,NULL,NULL,NULL),(146,NULL,'','','','','','2020-02-10 20:03:26.106027','2020-02-10 20:03:26.106064',14,142,NULL,NULL,NULL),(147,NULL,'','','','','','2020-02-10 20:07:35.813090','2020-02-10 22:46:22.363706',NULL,NULL,NULL,NULL,NULL),(148,NULL,'','','','','','2020-02-10 20:07:35.814360','2020-02-10 22:46:22.366469',NULL,NULL,NULL,NULL,NULL),(149,NULL,'','','','','','2020-02-10 20:07:35.815612','2020-02-10 22:46:22.370529',NULL,NULL,NULL,NULL,NULL),(150,576,'4456','Saruno','Itsukami','125','','2020-02-17 15:21:11.037434','2020-02-25 18:08:27.894114',14,142,NULL,NULL,NULL),(151,656,'589','TABACHINES','ARTEAGA','1203','','2020-02-17 22:19:56.710219','2020-02-17 22:19:56.710278',14,142,NULL,NULL,NULL),(152,NULL,'','','','','','2020-02-17 22:21:33.326939','2020-02-17 23:20:11.035060',14,142,NULL,NULL,NULL),(153,568,'450','COLINAS','Colinas el alto','452','0','2020-02-18 15:50:14.159967','2020-02-26 22:12:00.363021',14,142,NULL,NULL,NULL),(154,576,'45850','','','250','0','2020-02-19 15:38:39.181644','2020-02-19 15:38:39.181681',14,142,NULL,'Jardines de la cruz',NULL),(155,576,'4450','JARDINES DEL BOSQUE','Marimar','120','','2020-02-19 17:21:32.166539','2020-02-19 17:21:32.166580',14,142,NULL,NULL,NULL),(156,NULL,'','','','','','2020-02-21 17:30:27.555315','2020-02-21 17:30:27.555342',NULL,NULL,NULL,NULL,NULL),(157,NULL,'','','','','','2020-02-21 17:30:27.672762','2020-02-21 17:30:27.672815',NULL,NULL,NULL,NULL,NULL),(158,NULL,'','','','','','2020-02-21 17:30:27.675628','2020-02-21 17:30:27.675677',NULL,NULL,NULL,NULL,NULL),(159,NULL,'','','','','','2020-02-21 17:44:38.593188','2020-02-21 19:33:53.390190',NULL,NULL,NULL,NULL,NULL),(160,NULL,'','','','','','2020-02-21 17:44:38.594590','2020-02-21 19:33:53.392911',NULL,NULL,NULL,NULL,NULL),(161,NULL,'','','','','','2020-02-21 17:44:38.596124','2020-02-21 19:33:53.395228',NULL,NULL,NULL,NULL,NULL),(162,NULL,'','','','','','2020-02-21 22:21:51.799808','2020-02-21 22:21:51.799871',NULL,NULL,NULL,NULL,NULL),(163,NULL,'','','','','','2020-02-21 22:24:09.024591','2020-03-18 16:53:14.883669',NULL,NULL,NULL,NULL,NULL),(164,576,'45001','ALCALDE','TOBAGO','85','85','2020-02-21 22:24:30.223554','2020-03-18 16:52:04.646261',14,142,NULL,NULL,NULL),(165,NULL,'','','','','','2020-02-21 22:26:10.962684','2020-02-21 22:26:10.963020',14,142,NULL,NULL,NULL),(166,NULL,'','','','','','2020-02-21 22:27:25.868638','2020-02-21 22:30:45.680327',14,142,NULL,NULL,NULL),(167,NULL,'','','','','','2020-02-21 22:31:32.314447','2020-02-21 22:37:48.845498',14,142,NULL,NULL,NULL),(168,NULL,'','','','','','2020-02-25 17:11:05.025085','2020-02-25 19:08:37.268190',NULL,NULL,NULL,NULL,NULL),(169,NULL,'','','','','','2020-02-25 18:03:55.111148','2020-02-25 18:03:55.111368',14,142,NULL,NULL,NULL),(170,NULL,'','','','','','2020-02-25 18:10:41.161424','2020-02-25 18:10:41.161547',NULL,142,NULL,NULL,NULL),(171,NULL,'','','','','','2020-02-25 18:11:51.062436','2020-02-25 23:03:21.939389',14,142,NULL,NULL,NULL),(172,NULL,'','','','','','2020-02-25 19:09:35.819978','2020-02-25 19:09:35.820013',NULL,NULL,NULL,NULL,NULL),(173,NULL,'','','','','','2020-02-26 18:45:21.863083','2020-02-26 18:45:21.863117',14,142,NULL,NULL,NULL),(174,576,'4456','El sauz','Isla mexcaltitang','10','4750','2020-02-28 16:01:52.315319','2020-03-04 17:08:19.401876',14,142,NULL,NULL,NULL),(175,576,'452','Mascota','Filipinas','125','5','2020-02-28 16:04:00.947020','2020-03-06 18:02:11.289564',14,142,NULL,NULL,NULL),(176,NULL,'','','','','','2020-03-03 16:38:10.758911','2020-03-03 16:38:10.758958',NULL,NULL,NULL,NULL,NULL),(177,NULL,'','','','','','2020-03-03 17:00:05.607136','2020-03-03 17:00:05.607171',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `declaracion_domicilios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_efectivometales`
--

DROP TABLE IF EXISTS `declaracion_efectivometales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_efectivometales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otro_tipo_operacion` varchar(255) NOT NULL,
  `monto_efectivo` decimal(12,2) DEFAULT NULL,
  `otro_metal` varchar(255) NOT NULL,
  `unidades` varchar(255) NOT NULL,
  `monto_metales` decimal(12,2) DEFAULT NULL,
  `otra_forma` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_formas_adquisiciones_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_tipos_metales_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_efectivo_cat_formas_adquisici_0f051214_fk_declaraci` (`cat_formas_adquisiciones_id`),
  KEY `declaracion_efectivo_cat_monedas_id_20f7762a_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_efectivo_cat_tipos_metales_id_66a58429_fk_declaraci` (`cat_tipos_metales_id`),
  KEY `declaracion_efectivo_cat_tipos_operacione_66a95818_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_efectivo_declaraciones_id_91c3f1bd_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_efectivo_observaciones_id_8aa79397_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_efectivo_cat_formas_adquisici_0f051214_fk_declaraci` FOREIGN KEY (`cat_formas_adquisiciones_id`) REFERENCES `declaracion_catformasadquisiciones` (`id`),
  CONSTRAINT `declaracion_efectivo_cat_monedas_id_20f7762a_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_efectivo_cat_tipos_metales_id_66a58429_fk_declaraci` FOREIGN KEY (`cat_tipos_metales_id`) REFERENCES `declaracion_cattiposmetales` (`id`),
  CONSTRAINT `declaracion_efectivo_cat_tipos_operacione_66a95818_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_efectivo_declaraciones_id_91c3f1bd_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_efectivo_observaciones_id_8aa79397_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_efectivometales`
--

LOCK TABLES `declaracion_efectivometales` WRITE;
/*!40000 ALTER TABLE `declaracion_efectivometales` DISABLE KEYS */;
/*!40000 ALTER TABLE `declaracion_efectivometales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_empresassociedades`
--

DROP TABLE IF EXISTS `declaracion_empresassociedades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_empresassociedades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rol_empresa` varchar(255) NOT NULL,
  `actividad_economica` tinyint(1) DEFAULT NULL,
  `porcentaje_participacion` decimal(5,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `declarante_infopersonalvar_id` int(11) NOT NULL,
  `empresa_infopersonalvar_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_empresas_declaraciones_id_5c363649_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_empresas_declarante_infoperso_c632e522_fk_declaraci` (`declarante_infopersonalvar_id`),
  KEY `declaracion_empresas_empresa_infopersonal_33224da1_fk_declaraci` (`empresa_infopersonalvar_id`),
  KEY `declaracion_empresas_observaciones_id_fb8ea203_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_empresas_declaraciones_id_5c363649_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_empresas_declarante_infoperso_c632e522_fk_declaraci` FOREIGN KEY (`declarante_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_empresas_empresa_infopersonal_33224da1_fk_declaraci` FOREIGN KEY (`empresa_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_empresas_observaciones_id_fb8ea203_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_empresassociedades`
--

LOCK TABLES `declaracion_empresassociedades` WRITE;
/*!40000 ALTER TABLE `declaracion_empresassociedades` DISABLE KEYS */;
/*!40000 ALTER TABLE `declaracion_empresassociedades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_encargos`
--

DROP TABLE IF EXISTS `declaracion_encargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_encargos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empleo_cargo_comision` varchar(255) DEFAULT NULL,
  `honorarios` tinyint(1) DEFAULT NULL,
  `nivel_encargo` varchar(255) DEFAULT NULL,
  `area_adscripcion` varchar(255) DEFAULT NULL,
  `posesion_conclusion` date DEFAULT NULL,
  `telefono_laboral` varchar(255) DEFAULT NULL,
  `email_laboral` varchar(255) DEFAULT NULL,
  `otro_sector` varchar(255) DEFAULT NULL,
  `otra_funcion` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `posesion_inicio` date DEFAULT NULL,
  `telefono_extension` varchar(255) DEFAULT NULL,
  `otro_naturalezas_juridicas` varchar(255) DEFAULT NULL,
  `otro_ente` varchar(255) DEFAULT NULL,
  `cat_entes_publicos_id` int(11) DEFAULT NULL,
  `cat_funciones_principales_id` int(11) DEFAULT NULL,
  `cat_ordenes_gobierno_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_poderes_id` int(11) DEFAULT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) DEFAULT NULL,
  `domicilios_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) DEFAULT NULL,
  `salarioMensualNeto` int(11) DEFAULT NULL,
  `rfc` varchar(255) DEFAULT NULL,
  `nombreEmpresaSociedadAsociacion` varchar(255) DEFAULT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `nombre_ente_publico` varchar(300) DEFAULT NULL,
  `cat_puestos_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_encargos_cat_entes_publicos_i_c694e7a5_fk_declaraci` (`cat_entes_publicos_id`),
  KEY `declaracion_encargos_cat_funciones_princi_7856c833_fk_declaraci` (`cat_funciones_principales_id`),
  KEY `declaracion_encargos_cat_ordenes_gobierno_a8b7d240_fk_declaraci` (`cat_ordenes_gobierno_id`),
  KEY `declaracion_encargos_cat_paises_id_1274280c_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_encargos_cat_poderes_id_f5b206b8_fk_declaraci` (`cat_poderes_id`),
  KEY `declaracion_encargos_cat_sectores_industr_3165053e_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_encargos_declaraciones_id_2316fade_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_encargos_domicilios_id_c5d14b3c_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_encargos_observaciones_id_8abf3420_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_encargos_moneda_id_9c3a3546_fk_declaraci` (`moneda_id`),
  KEY `declaracion_encargos_cat_tipos_operacione_9230d23b_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_encargos_cat_puestos_id_cf665b8e_fk_declaraci` (`cat_puestos_id`),
  CONSTRAINT `declaracion_encargos_cat_entes_publicos_i_c694e7a5_fk_declaraci` FOREIGN KEY (`cat_entes_publicos_id`) REFERENCES `declaracion_catentespublicos` (`id`),
  CONSTRAINT `declaracion_encargos_cat_funciones_princi_7856c833_fk_declaraci` FOREIGN KEY (`cat_funciones_principales_id`) REFERENCES `declaracion_catfuncionesprincipales` (`id`),
  CONSTRAINT `declaracion_encargos_cat_ordenes_gobierno_a8b7d240_fk_declaraci` FOREIGN KEY (`cat_ordenes_gobierno_id`) REFERENCES `declaracion_catordenesgobierno` (`id`),
  CONSTRAINT `declaracion_encargos_cat_paises_id_1274280c_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_encargos_cat_poderes_id_f5b206b8_fk_declaraci` FOREIGN KEY (`cat_poderes_id`) REFERENCES `declaracion_catpoderes` (`id`),
  CONSTRAINT `declaracion_encargos_cat_puestos_id_cf665b8e_fk_declaraci` FOREIGN KEY (`cat_puestos_id`) REFERENCES `declaracion_catpuestos` (`id`),
  CONSTRAINT `declaracion_encargos_cat_sectores_industr_3165053e_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_encargos_cat_tipos_operacione_9230d23b_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_encargos_declaraciones_id_2316fade_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_encargos_domicilios_id_c5d14b3c_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_encargos_moneda_id_9c3a3546_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_encargos_observaciones_id_8abf3420_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_encargos`
--

LOCK TABLES `declaracion_encargos` WRITE;
/*!40000 ALTER TABLE `declaracion_encargos` DISABLE KEYS */;
INSERT INTO `declaracion_encargos` VALUES (1,NULL,1,'Jefatura de departamento','Desarrollador Web',NULL,'213213123213','',NULL,NULL,'2019-12-06 19:19:15.538329','2020-02-26 22:25:11.160934','2019-11-07','033',NULL,NULL,NULL,1,2,NULL,1,21,1,2,9,NULL,NULL,NULL,NULL,2,'Secretaría ejecutiva',NULL),(3,'Jefe de empresa',0,'k','Gastronomia','2019-12-06','','','','','2019-12-09 22:22:36.572279','2019-12-09 23:47:08.621656','2019-11-25','','',NULL,3,1,1,1,1,NULL,1,13,26,20000,'',NULL,NULL,NULL,NULL,NULL),(4,'Jefe de empresa',0,NULL,'Gastronomia',NULL,'','',NULL,NULL,'2019-12-10 15:24:06.247184','2020-02-03 02:33:18.532705','2019-11-25','',NULL,NULL,3,4,2,NULL,4,NULL,1,14,27,10000,'NEAT010101JKL','NT',101,NULL,NULL,NULL),(5,'Jefe de empresa',1,'Jefatura de departamento','Gastronomia','2019-12-06','','','','','2019-12-24 18:48:33.495420','2019-12-24 18:48:33.495471','2019-11-26','','',NULL,4,11,1,1,1,NULL,3,62,125,NULL,'',NULL,NULL,1,NULL,NULL),(6,'Gerente',0,'','Ninguna area',NULL,'','','','','2019-12-24 18:58:53.653367','2019-12-24 19:09:09.810890','2019-09-30','','',NULL,18,5,NULL,NULL,1,NULL,3,64,127,85693,'NAAH010101589','No hay nombre',101,NULL,NULL,NULL),(7,'Jefe de departamento Fron-end',1,'Jefatura de departamento','Gastronomia','2020-01-01','213213123213','galileaseajal@gmail.com','','','2020-01-27 18:59:07.779750','2020-01-27 19:28:57.379994','2014-08-07','033','',NULL,2,2,1,NULL,1,NULL,10,87,233,NULL,'',NULL,NULL,1,NULL,NULL),(8,'Jefe de departamento Fron-end',NULL,NULL,'Gastronomia','1910-01-01','','',NULL,NULL,'2020-01-27 20:01:01.677225','2020-01-27 20:01:01.677250','2020-01-01','',NULL,NULL,15,1,1,NULL,1,NULL,10,88,234,5000,'NEAT010101JKL','No hay nombre',101,NULL,NULL,NULL),(9,'Jefe de departamento Fron-end',NULL,NULL,'Gastronomia',NULL,'','',NULL,NULL,'2020-01-27 20:06:24.266273','2020-01-28 16:25:24.761414','2020-01-01','',NULL,NULL,15,1,1,NULL,1,NULL,10,89,235,5000,'NEAT010101JKL','No hay nombre',101,NULL,NULL,NULL),(10,'Jefe de empresa',0,NULL,'Diseño',NULL,'','',NULL,NULL,'2020-01-28 04:11:52.377884','2020-02-20 00:04:51.312710','2020-01-01','',NULL,NULL,NULL,1,1,NULL,2,NULL,10,90,236,10000,'SEAJ010101568','Seajal',101,NULL,NULL,NULL),(11,NULL,0,NULL,NULL,NULL,'','',NULL,NULL,'2020-01-28 16:28:15.151233','2020-01-28 16:28:15.151302','1910-01-01','',NULL,NULL,NULL,7,NULL,NULL,NULL,NULL,10,91,237,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,NULL,0,NULL,NULL,NULL,'','',NULL,NULL,'2020-01-30 19:39:44.340659','2020-02-03 18:53:37.644918','2010-05-15','',NULL,NULL,NULL,NULL,1,NULL,3,NULL,1,92,240,1230,'PEEM010101458','EMPRESA PRIVADA',150,NULL,NULL,NULL),(13,'Jefe de empresa',1,'Jefatura de departamento','Gastronomia',NULL,'33666666','',NULL,NULL,'2020-02-04 00:40:00.725412','2020-02-04 00:40:00.725442','2016-09-17','347',NULL,NULL,6,5,3,NULL,2,NULL,9,99,279,NULL,NULL,NULL,NULL,3,NULL,NULL),(14,'JEFATURA',0,NULL,'GUBERNAMENTAL',NULL,'','',NULL,NULL,'2020-02-04 00:54:16.698402','2020-02-04 06:01:50.851066','2013-09-17','',NULL,NULL,17,5,2,NULL,2,NULL,9,101,281,19000,'MEEP010101458','SECTOR PRIVADO EMPRESA',101,NULL,NULL,NULL),(15,NULL,0,NULL,NULL,NULL,'','',NULL,NULL,'2020-02-04 00:56:46.865852','2020-02-04 01:01:12.966995','2016-05-20','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,9,102,282,8560,'NEAT010101JKL','Mulsiseajal',101,NULL,NULL,NULL),(16,NULL,0,NULL,NULL,NULL,'','',NULL,NULL,'2020-02-04 17:47:50.653158','2020-02-04 17:47:50.653216','1910-01-01','',NULL,NULL,NULL,7,NULL,NULL,NULL,NULL,10,119,304,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,NULL,0,NULL,NULL,NULL,'','',NULL,NULL,'2020-02-04 17:53:39.725787','2020-02-04 17:53:39.725843','1910-01-01','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,10,120,305,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,NULL,1,'Jefatura de departamento','Web DEVELOPED',NULL,'','',NULL,NULL,'2020-02-10 18:16:13.697283','2020-02-10 18:19:00.734093','2020-01-01','',NULL,NULL,NULL,5,2,NULL,2,NULL,2,138,326,NULL,NULL,NULL,NULL,3,'SEAJAL',2),(19,'ADSF',0,NULL,'ASDF',NULL,'','',NULL,NULL,'2020-02-10 18:53:21.665451','2020-02-10 18:53:21.665488','1910-01-01','',NULL,NULL,NULL,5,2,NULL,2,NULL,2,140,328,NULL,NULL,NULL,101,NULL,'SDAF',NULL),(20,NULL,0,NULL,NULL,NULL,'','',NULL,NULL,'2020-02-10 18:56:03.885313','2020-02-10 18:57:41.902069','2016-07-26','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,141,329,2000,'SAAT010101256','STX',101,NULL,NULL,NULL),(21,NULL,0,NULL,NULL,NULL,'','',NULL,NULL,'2020-02-17 22:19:56.751585','2020-02-17 22:19:56.751646','2020-02-01','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,151,365,5000,'ZUUK010101256','ZUKO',101,NULL,NULL,NULL),(22,'GDSFG',0,NULL,'SDFGSDG',NULL,'','',NULL,NULL,'2020-02-17 22:21:33.343665','2020-02-17 23:20:11.050815','2020-01-01','',NULL,NULL,NULL,5,2,NULL,2,NULL,4,152,366,5000,'KAEW010101256','STX',101,NULL,'SD',NULL),(23,NULL,1,NULL,'ANTICURRPCIÓN',NULL,'','',NULL,NULL,'2020-02-18 15:50:14.242961','2020-02-26 22:12:00.375816','2018-01-01','',NULL,NULL,NULL,2,1,NULL,1,NULL,11,153,367,856,NULL,NULL,NULL,NULL,'Secretaría ejecutiva',NULL),(24,NULL,0,NULL,NULL,NULL,'','',NULL,NULL,'2020-02-26 18:45:21.978419','2020-02-26 18:45:21.978458','2016-01-01','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,11,173,406,8560,'KAEW010101256','STX',101,NULL,NULL,NULL),(25,'Jefe de departamento Fron-end',0,NULL,'Area de adscripción',NULL,'','',NULL,NULL,'2020-02-28 16:04:00.964362','2020-03-06 18:02:11.296776','1910-01-01','',NULL,NULL,NULL,1,1,NULL,1,NULL,12,175,420,8900,NULL,NULL,101,NULL,NULL,NULL);
/*!40000 ALTER TABLE `declaracion_encargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_experiencialaboral`
--

DROP TABLE IF EXISTS `declaracion_experiencialaboral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_experiencialaboral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otro_poder` varchar(255) NOT NULL,
  `nombre_institucion` varchar(255) NOT NULL,
  `unidad_area_administrativa` varchar(255) NOT NULL,
  `otro_sector` varchar(255) NOT NULL,
  `jerarquia_rango` varchar(255) NOT NULL,
  `cargo_puesto` varchar(255) NOT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `fecha_salida` date DEFAULT NULL,
  `otra_funcion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otro_ambitos_laborales` varchar(255) NOT NULL,
  `cat_ambitos_laborales_id` int(11) DEFAULT NULL,
  `cat_funciones_principales_id` int(11) DEFAULT NULL,
  `cat_ordenes_gobierno_id` int(11) DEFAULT NULL,
  `cat_poderes_id` int(11) DEFAULT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `domicilios_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `rfc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_experien_cat_ambitos_laborale_79a45acd_fk_declaraci` (`cat_ambitos_laborales_id`),
  KEY `declaracion_experien_cat_funciones_princi_88f728f5_fk_declaraci` (`cat_funciones_principales_id`),
  KEY `declaracion_experien_cat_ordenes_gobierno_7b2c7dfe_fk_declaraci` (`cat_ordenes_gobierno_id`),
  KEY `declaracion_experien_cat_poderes_id_400030b2_fk_declaraci` (`cat_poderes_id`),
  KEY `declaracion_experien_cat_sectores_industr_d5ad472b_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_experien_declaraciones_id_f1ba0aad_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_experien_domicilios_id_a7959cd2_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_experien_observaciones_id_36520823_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_experiencialaboral_cat_tipos_operaciones_id_f363162d` (`cat_tipos_operaciones_id`),
  CONSTRAINT `declaracion_experien_cat_ambitos_laborale_79a45acd_fk_declaraci` FOREIGN KEY (`cat_ambitos_laborales_id`) REFERENCES `declaracion_catambitoslaborales` (`id`),
  CONSTRAINT `declaracion_experien_cat_funciones_princi_88f728f5_fk_declaraci` FOREIGN KEY (`cat_funciones_principales_id`) REFERENCES `declaracion_catfuncionesprincipales` (`id`),
  CONSTRAINT `declaracion_experien_cat_ordenes_gobierno_7b2c7dfe_fk_declaraci` FOREIGN KEY (`cat_ordenes_gobierno_id`) REFERENCES `declaracion_catordenesgobierno` (`id`),
  CONSTRAINT `declaracion_experien_cat_poderes_id_400030b2_fk_declaraci` FOREIGN KEY (`cat_poderes_id`) REFERENCES `declaracion_catpoderes` (`id`),
  CONSTRAINT `declaracion_experien_cat_sectores_industr_d5ad472b_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_experien_cat_tipos_operacione_f363162d_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_experien_declaraciones_id_f1ba0aad_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_experien_domicilios_id_a7959cd2_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_experien_observaciones_id_36520823_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_experiencialaboral`
--

LOCK TABLES `declaracion_experiencialaboral` WRITE;
/*!40000 ALTER TABLE `declaracion_experiencialaboral` DISABLE KEYS */;
INSERT INTO `declaracion_experiencialaboral` VALUES (1,'','SEAJAL','Tecnologías y plataformas','Anticurrupción','','Jefatura de departamento','2019-08-01','2019-12-09','','2019-12-09 15:36:21.189839','2020-02-01 21:27:45.376947','',1,3,2,1,41,1,3,10,3,'PAET010101125'),(2,'','SEAJAL','Tecnologías y plataformas','','Jefatura de departamento','Jefatura de departamento','2019-11-25','2019-12-24','','2019-12-24 18:56:23.389324','2019-12-24 18:56:23.389401','',1,7,NULL,1,31,3,63,126,1,'PAET010101125'),(3,'','SEAJAL-SEL','Tecnologías y plataformas','','','Jefatura de departamento','2019-04-09','2019-11-13','','2020-02-04 00:44:29.567564','2020-02-04 00:46:46.392390','',3,8,3,3,28,9,100,280,3,'SEAJ010101598'),(4,'','xcvxcv','xcvxcv','','','cvxxcv','2016-07-26','2019-07-26','','2020-02-10 18:31:35.365988','2020-02-10 18:31:35.366020','',2,7,2,2,25,2,139,327,2,''),(5,'','stx','Tecnologías y plataformas','','','Desarrollador','2016-07-26','2019-07-01','','2020-02-25 18:10:41.166597','2020-02-25 18:10:41.166671','',2,5,2,1,32,11,170,399,1,'PAET010101125');
/*!40000 ALTER TABLE `declaracion_experiencialaboral` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_fideicomisos`
--

DROP TABLE IF EXISTS `declaracion_fideicomisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_fideicomisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otro_fideicomiso` varchar(255) NOT NULL,
  `objetivo_fideicomiso` varchar(255) NOT NULL,
  `num_registro` varchar(255) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `plazo_vigencia` varchar(255) NOT NULL,
  `valor_fideicomiso` decimal(12,2) DEFAULT NULL,
  `ingreso_monetario` decimal(12,2) DEFAULT NULL,
  `institucion_fiduciaria` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipos_fideicomisos_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `nombre_fideicomiso` varchar(255) NOT NULL,
  `porcentaje` decimal(5,2) DEFAULT NULL,
  `rfc` varchar(255) NOT NULL,
  `rfc_fideicomiso` varchar(255) NOT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `tipo_persona_id` int(11) DEFAULT NULL,
  `tipo_relacion_id` int(11) DEFAULT NULL,
  `cat_tipo_participacion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_fideicom_activos_bienes_id_df3314c3_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_fideicom_cat_monedas_id_cebff38e_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_fideicom_cat_paises_id_d151330c_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_fideicom_cat_tipos_fideicomis_f258a97c_fk_declaraci` (`cat_tipos_fideicomisos_id`),
  KEY `declaracion_fideicom_cat_tipos_operacione_ff4aec47_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_fideicom_declaraciones_id_9ddd40ea_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_fideicom_observaciones_id_f261c138_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_fideicom_tipo_persona_id_43c4405d_fk_declaraci` (`tipo_persona_id`),
  KEY `declaracion_fideicom_tipo_relacion_id_9e45cbce_fk_declaraci` (`tipo_relacion_id`),
  KEY `declaracion_fideicom_cat_tipo_participaci_9616776a_fk_declaraci` (`cat_tipo_participacion_id`),
  KEY `declaracion_fideicom_cat_sectores_industr_bb5fb50f_fk_declaraci` (`cat_sectores_industria_id`),
  CONSTRAINT `declaracion_fideicom_activos_bienes_id_df3314c3_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_monedas_id_cebff38e_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_paises_id_d151330c_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_sectores_industr_bb5fb50f_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_tipo_participaci_9616776a_fk_declaraci` FOREIGN KEY (`cat_tipo_participacion_id`) REFERENCES `declaracion_cattipoparticipacion` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_tipos_fideicomis_f258a97c_fk_declaraci` FOREIGN KEY (`cat_tipos_fideicomisos_id`) REFERENCES `declaracion_cattiposfideicomisos` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_tipos_operacione_ff4aec47_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_fideicom_declaraciones_id_9ddd40ea_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_fideicom_observaciones_id_f261c138_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_fideicom_tipo_persona_id_43c4405d_fk_declaraci` FOREIGN KEY (`tipo_persona_id`) REFERENCES `declaracion_cattipopersona` (`id`),
  CONSTRAINT `declaracion_fideicom_tipo_relacion_id_9e45cbce_fk_declaraci` FOREIGN KEY (`tipo_relacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_fideicomisos`
--

LOCK TABLES `declaracion_fideicomisos` WRITE;
/*!40000 ALTER TABLE `declaracion_fideicomisos` DISABLE KEYS */;
INSERT INTO `declaracion_fideicomisos` VALUES (1,'','','','',NULL,'',NULL,NULL,'','2020-02-04 00:25:22.385164','2020-02-04 00:27:25.125145',4,NULL,NULL,2,2,1,277,'',NULL,'','',NULL,NULL,NULL,2),(2,'','','','',NULL,'',NULL,NULL,'','2020-02-04 14:29:29.748141','2020-02-04 15:05:24.050403',7,NULL,NULL,2,2,9,292,'',NULL,'','',NULL,NULL,NULL,2),(3,'','','','',NULL,'',NULL,NULL,'','2020-02-10 20:07:35.830599','2020-02-10 22:46:22.410340',14,NULL,142,2,3,2,340,'',NULL,'FIED010101569','',23,NULL,16,4),(5,'','','','',NULL,'',NULL,NULL,'','2020-02-21 17:44:38.610687','2020-02-21 19:33:53.421848',9,NULL,142,2,2,10,381,'',NULL,'FIED010101585','',30,NULL,8,4);
/*!40000 ALTER TABLE `declaracion_fideicomisos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_infopersonalfija`
--

DROP TABLE IF EXISTS `declaracion_infopersonalfija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_infopersonalfija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) NOT NULL,
  `apellido1` varchar(255) NOT NULL,
  `apellido2` varchar(255) NOT NULL,
  `curp` varchar(255) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `otro_ente` varchar(255) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `cat_ente_publico_id` int(11) DEFAULT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  `cat_pais_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `telefono` varchar(14) NOT NULL,
  `puesto` varchar(255) NOT NULL,
  `nombre_ente_publico` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_infopers_cat_ente_publico_id_a3b30fba_fk_declaraci` (`cat_ente_publico_id`),
  KEY `declaracion_infopers_cat_entidades_federa_e29e8024_fk_declaraci` (`cat_entidades_federativas_id`),
  KEY `declaracion_infopers_cat_pais_id_484e2f32_fk_declaraci` (`cat_pais_id`),
  KEY `declaracion_infopersonalfija_usuario_id_2e744aa8_fk_auth_user_id` (`usuario_id`),
  CONSTRAINT `declaracion_infopers_cat_ente_publico_id_a3b30fba_fk_declaraci` FOREIGN KEY (`cat_ente_publico_id`) REFERENCES `declaracion_catentespublicos` (`id`),
  CONSTRAINT `declaracion_infopers_cat_entidades_federa_e29e8024_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`),
  CONSTRAINT `declaracion_infopers_cat_pais_id_484e2f32_fk_declaraci` FOREIGN KEY (`cat_pais_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_infopersonalfija_usuario_id_2e744aa8_fk_auth_user_id` FOREIGN KEY (`usuario_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_infopersonalfija`
--

LOCK TABLES `declaracion_infopersonalfija` WRITE;
/*!40000 ALTER TABLE `declaracion_infopersonalfija` DISABLE KEYS */;
INSERT INTO `declaracion_infopersonalfija` VALUES (1,'GALILEA','GRANADOS','ESCOBAR','GAER940419MJCRSS19','GEER940421300','2019-12-06 18:29:33.693408','2020-01-30 19:33:27.265818','1994-04-21',NULL,'2019-08-01',3,14,142,1,'3333333333','PROGRAMADORA','Secretaria Ejecutiva Anticurrupción de Jalisco'),(2,'DANIEL','AVALOS','TORRES','DAER940419MJCRSS19','DEAF010101256','2019-12-14 23:22:06.021865','2020-02-28 16:01:52.429425','2001-01-01',NULL,'2019-11-25',3,9,142,2,'3324726011','PROGRAMADOR','Secretaria Ejecutiva Anticurrupción de Jalisco'),(3,'SALVADOR','MORALES','MORA','SAER940419MJCRSS19','SAAL121212689','2019-12-15 23:03:36.048577','2019-12-15 23:03:36.048609','2012-12-12',NULL,'2019-11-27',3,9,142,3,'3316537783','PROGRAMADOR','Secretaria Ejecutiva Anticurrupción de Jalisco'),(4,'RAUL','TORRES','SILVA','RUAT010101MJCRSS58','RUAT010101596','2019-12-17 15:38:57.624598','2019-12-17 15:38:57.624675','2001-01-01',NULL,'2019-11-25',3,9,142,4,'3145891456','DOCTOR','Secretaria Ejecutiva Anticurrupción de Jalisco'),(5,'MONICA','ROSALES','UREÑA','MOUR010101MJCRSS19','MOUR010101458','2019-12-17 16:05:27.991926','2019-12-24 18:41:50.139473','2001-01-01',NULL,'2019-11-25',3,9,142,5,'3316535980','PROGRAMADORA','Secretaria Ejecutiva Anticurrupción de Jalisco'),(6,'JOSE LUIS','ISIDRO','LOPEZ','JOID010101MJCRSS19','JOID010101589','2019-12-17 16:08:14.392479','2020-01-13 19:36:38.101597','2001-01-01',NULL,'2019-11-25',3,9,142,6,'3333333333','DOCTOR','Secretaria Ejecutiva Anticurrupción de Jalisco'),(7,'PEDRO','PARAFAN','IRAIS','PEOD010101MJCRSS85','PEOD010101589','2019-12-17 16:10:38.484234','2020-01-14 16:10:08.110706','2001-01-01',NULL,'2019-10-29',3,9,142,7,'3324726011','DOCTOR',NULL),(8,'ISMAEL','PRADO','HUERTA','SIAM010101MJCRSS78','SIAM010101018','2019-12-17 16:21:27.162473','2019-12-17 16:21:27.162506','2001-01-01',NULL,'2019-11-25',2,9,142,8,'3316535980','PROGRAMADOR',NULL),(9,'LORENA','SUAREZ','MONTANEGRO','LOAS010101MJCRSS85','LOAS010101589','2019-12-17 16:24:24.326364','2020-03-03 16:38:10.831076','2001-01-01',NULL,'2019-11-25',2,9,142,9,'3145891456','PROGRAMADORA',NULL),(10,'ARMANDO','DE LA BARRERA','CHAVEZ','RAOB010101MJCRSS85','ARDB010101589','2019-12-17 16:33:22.757484','2019-12-17 16:33:22.757546','2001-01-01',NULL,'2019-11-25',4,9,142,10,'3316535980','PROGRAMADOR',NULL),(11,'TERESA','CHAVEZ','GUTIERREZ','TEER010101MJCRSS56','TEER010101589','2019-12-30 17:24:16.258072','2020-01-13 22:33:56.717156','2001-01-01',NULL,'2019-11-26',3,9,142,14,'3316535980','CONTADOR',NULL),(12,'SEBASTIA','MIRAMAR','ALETA','MIAL010101MJCRSS56','MIAL010101892','2019-12-30 17:52:52.016010','2019-12-30 17:52:52.016046','2001-01-01',NULL,'2019-11-25',3,9,142,15,'3324726011','CONTADOR',NULL),(13,'LISA','SIMPSOM','SIMPSON','LIAS010101MJCRSS89','LIAS010101256','2019-12-30 17:55:53.797009','2020-02-27 23:06:47.772403','2001-01-01',NULL,'2019-11-26',5,9,142,16,'3316535980','LIDER',NULL),(14,'ZARA','MORALES','MORALES','ZAER010101MJCRSS56','ZAER010101123','2019-12-30 20:51:16.286458','2020-01-22 17:48:46.198727','2001-01-01',NULL,'2019-11-25',2,9,142,18,'3324726011','CONTRALOR',NULL),(15,'ZARA','MORALES','MORALES','ZAER010101MJCRSS58','ZAER010101023','2019-12-30 21:17:11.522219','2020-02-04 00:28:15.024702','2001-01-01',NULL,'2019-12-05',2,9,142,13,'3145891456','CONTRALOR',NULL),(16,'ZARA','MORALES','MORALES','ZAER010101MJCRSS58','ZAER010101023','2019-12-30 21:19:50.080010','2019-12-30 21:19:50.080049','2001-01-01',NULL,'2019-12-05',2,9,142,13,'3145891456','CONTRALOR',NULL),(17,'ZARA','MORALES','MORALES','ZAER010101MJCRSS58','ZAER010101023','2019-12-30 21:23:08.190684','2019-12-30 21:23:08.190728','2001-01-01',NULL,'2019-11-25',2,9,142,13,'3324726011','CONTRALOR',NULL),(18,'ZARA','MORALES','MORALES','ZAER010101MJCRSS58','ZAER010101023','2019-12-30 21:33:25.223368','2019-12-30 21:33:25.223415','2001-01-01',NULL,'2019-11-25',2,9,142,13,'3324726011','CONTRALOR',NULL),(19,'ZARA','MORALES','MORA','ZAER010101MJCRSS58','ZAER010101023','2019-12-30 21:38:15.458435','2019-12-30 21:38:15.458471','2001-01-01',NULL,'2019-11-25',2,9,142,13,'3324726011','CONTRALOR',NULL),(20,'ZARA','MORALES','MORA','ZAER010101MJCRSS58','ZAER010101023','2019-12-30 22:15:44.910205','2019-12-30 22:15:44.910237','2001-01-01',NULL,'2019-09-30',2,9,142,13,'3333333333','CONTRALOR',NULL),(21,'OTRA ZARA','MOULLE','FLOR','ZAER010101MJCRSS59','ZAER010101123','2019-12-30 22:35:03.257744','2019-12-30 22:35:03.257779','2001-01-01',NULL,'2019-10-28',4,9,142,18,'3324726011','OFICIAL',NULL),(22,'TERESA','CHAVEZ','AGUIRRE','TEER010101MJCRSS56','TEER010101589','2019-12-30 22:38:02.721114','2019-12-30 22:38:02.721170','2001-01-01',NULL,'2019-10-28',5,9,142,14,'3333333333','DIRECTOR DE PROCURADORIA',NULL),(23,'LISA','SIMPSON','SIMPSON','LIAS010101MJCRSS52','LIAS010101256','2020-01-13 18:22:46.914553','2020-01-13 18:22:46.914584','2001-01-01',NULL,'2020-01-01',3,9,142,16,'3145891456','PROGRAMADORA',NULL),(24,'ARMANDO','ARMANDO','ARMANDO','RAEB010101MJCRSS15','ARDB010101589','2020-01-29 17:09:35.034277','2020-01-29 17:09:35.034313','2001-01-01',NULL,'2000-04-20',3,14,142,10,'3333333333','ADMINISTRADOR',NULL),(25,'MARTIN','MARTIN','MARTIN','MIAL010101HJCRSS23','MIAL010101892','2020-01-29 17:18:26.905834','2020-01-29 17:18:26.905874',NULL,NULL,'2000-04-20',171,14,142,15,'3333333333','CONTABILIDAD',NULL),(26,'PRUEBA UNO','PRUEBA UNO','PRUEBA UNO','PEOD010101MJCRSS85','PUER010101123','2020-02-06 18:17:08.917097','2020-02-06 18:17:08.917130','2001-01-01',NULL,'2020-01-01',NULL,14,142,20,'3145891456','LIDER DE PROYECTO','SECRETARIA EJECUTIVA'),(27,'PRUEBA DOS','PRUEBA DOS','PRUEBA DOS','PUER010101MJCRSS56','PUER010101321','2020-02-14 23:43:44.105466','2020-02-17 15:21:11.337077','2001-01-01',NULL,'2020-01-01',NULL,14,142,24,'3145891456','JEFATURA','SEAJAL'),(28,'PRUEBA TRES','PRUEBA TRES','PRUEBA TRES','PUER010101MJCRSS55','PUER010101852','2020-02-17 15:41:21.318850','2020-02-17 15:41:21.318960','2001-01-01',NULL,'2020-01-01',NULL,14,142,26,'3333333333','DIRECTOR DE DEPARTAMENTO','SEAJAL'),(29,'PRUEBA G','PRUEBA G','PRUEBA G','PUER010101MJCRRSS2','PUER010101569','2020-02-17 17:31:20.515075','2020-02-17 17:31:20.515112','2001-01-01',NULL,'2020-02-01',NULL,14,142,28,'3324726011','JEFATURA DE DEPARTAMENTO','SESAJ SECRETARIA EJECUTIVA');
/*!40000 ALTER TABLE `declaracion_infopersonalfija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_infopersonalvar`
--

DROP TABLE IF EXISTS `declaracion_infopersonalvar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_infopersonalvar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `es_fisica` tinyint(1) DEFAULT NULL,
  `razon_social` varchar(255) DEFAULT NULL,
  `nombres` varchar(255) NOT NULL,
  `apellido1` varchar(255) NOT NULL,
  `apellido2` varchar(255) NOT NULL,
  `curp` varchar(255) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `num_id_identificacion` varchar(255) NOT NULL,
  `email_personal` varchar(255) NOT NULL,
  `tel_particular` varchar(255) NOT NULL,
  `tel_movil` varchar(255) NOT NULL,
  `otro_sector` varchar(255) DEFAULT NULL,
  `otro_estado_civil` varchar(255) DEFAULT NULL,
  `actividad_economica` varchar(255) DEFAULT NULL,
  `ocupacion_girocomercial` varchar(255) DEFAULT NULL,
  `nombre_negocio` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  `cat_estados_civiles_id` int(11) DEFAULT NULL,
  `cat_pais_id` int(11) DEFAULT NULL,
  `cat_regimenes_matrimoniales_id` int(11) DEFAULT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `cat_tipo_persona_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `domicilios_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) DEFAULT NULL,
  `rfc_negocio` varchar(50) DEFAULT NULL,
  `homoclave` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_infopers_cat_entidades_federa_aca67810_fk_declaraci` (`cat_entidades_federativas_id`),
  KEY `declaracion_infopers_cat_estados_civiles__ea86eb1a_fk_declaraci` (`cat_estados_civiles_id`),
  KEY `declaracion_infopers_cat_pais_id_3c350fea_fk_declaraci` (`cat_pais_id`),
  KEY `declaracion_infopers_cat_regimenes_matrim_b8803425_fk_declaraci` (`cat_regimenes_matrimoniales_id`),
  KEY `declaracion_infopers_cat_sectores_industr_c9c540d2_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_infopers_cat_tipo_persona_id_b7db6218_fk_declaraci` (`cat_tipo_persona_id`),
  KEY `declaracion_infopers_declaraciones_id_75d0c55e_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_infopers_domicilios_id_f9298f9b_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_infopers_observaciones_id_641a285c_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_infopers_cat_entidades_federa_aca67810_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`),
  CONSTRAINT `declaracion_infopers_cat_estados_civiles__ea86eb1a_fk_declaraci` FOREIGN KEY (`cat_estados_civiles_id`) REFERENCES `declaracion_catestadosciviles` (`id`),
  CONSTRAINT `declaracion_infopers_cat_pais_id_3c350fea_fk_declaraci` FOREIGN KEY (`cat_pais_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_infopers_cat_regimenes_matrim_b8803425_fk_declaraci` FOREIGN KEY (`cat_regimenes_matrimoniales_id`) REFERENCES `declaracion_catregimenesmatrimoniales` (`id`),
  CONSTRAINT `declaracion_infopers_cat_sectores_industr_c9c540d2_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_infopers_cat_tipo_persona_id_b7db6218_fk_declaraci` FOREIGN KEY (`cat_tipo_persona_id`) REFERENCES `declaracion_cattipopersona` (`id`),
  CONSTRAINT `declaracion_infopers_declaraciones_id_75d0c55e_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_infopers_domicilios_id_f9298f9b_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_infopers_observaciones_id_641a285c_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_infopersonalvar`
--

LOCK TABLES `declaracion_infopersonalvar` WRITE;
/*!40000 ALTER TABLE `declaracion_infopersonalvar` DISABLE KEYS */;
INSERT INTO `declaracion_infopersonalvar` VALUES (1,NULL,NULL,'GALILEA','GRANADOS','ESCOBAR','GAER940419MJCRSS19','GEER940421300','1994-04-21','','jo@hotmail.com','3355887814','33333333',NULL,NULL,NULL,NULL,NULL,'2019-12-06 18:33:37.954778','2020-01-30 19:33:27.274818',14,2,142,1,NULL,1,1,1,2,NULL,'300'),(2,NULL,NULL,'Josefina','Morales','De la rosa','JAER010101MJCRSS52','JAER010101333','1967-03-19','37525547885','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Comerciante',NULL,'2019-12-09 16:28:40.383774','2019-12-09 16:28:40.383814',NULL,NULL,NULL,NULL,34,2,1,4,NULL,NULL,NULL),(3,NULL,NULL,'Josefina Josefa','Rosales','De la rosa','JAER010101MJCRSS52','JAER010101333','1965-03-19','2566999','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Comerciante',NULL,'2019-12-09 16:32:45.124075','2019-12-09 22:03:44.473534',NULL,NULL,NULL,NULL,30,2,1,5,NULL,NULL,NULL),(4,NULL,NULL,'Josefina Josefa','Rosales','De la rosa','JAER010101MJCRSS52','JAER010101333','1965-03-19','2566999','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Comerciante',NULL,'2019-12-09 17:51:06.837517','2019-12-09 17:51:06.837554',NULL,NULL,NULL,NULL,24,2,1,6,NULL,NULL,NULL),(5,NULL,NULL,'Josefina Josefa','Rosales','De la rosa','JAER010101MJCRSS52','JAER010101333','1965-03-19','2566999','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Comerciante',NULL,'2019-12-09 17:51:32.535555','2019-12-09 17:51:32.535591',NULL,NULL,NULL,NULL,24,2,1,7,NULL,NULL,NULL),(6,NULL,NULL,'Santiago','Torres','Reveles','CAOS010101MJCRSS11','DEAD010101458','1947-03-24','2566999','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Comerciante',NULL,'2019-12-09 17:54:57.354216','2019-12-09 17:55:14.697110',NULL,NULL,NULL,NULL,30,2,1,8,NULL,NULL,NULL),(7,NULL,NULL,'Daniel','Torres','Reveles','GAER010101MJCRSS36','DEAD010101458','2006-01-04','37525547885','jggee@hotmail.com','3355887814','',NULL,NULL,NULL,'Estudiante',NULL,'2019-12-09 17:57:18.058719','2019-12-09 17:57:18.058759',NULL,NULL,NULL,NULL,30,2,1,9,NULL,NULL,NULL),(8,NULL,NULL,'Daniel','Rosales','Reveles','CAOS010101MJCRSS11','DEAD010101458','2014-01-01','2566999','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Comerciante',NULL,'2019-12-09 18:11:14.722544','2019-12-09 18:11:14.722577',NULL,NULL,NULL,NULL,27,2,1,10,NULL,NULL,NULL),(9,NULL,NULL,'Josefina','Teldo','Reveles','','',NULL,'2566999','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Calidad',NULL,'2019-12-09 18:33:03.708920','2019-12-09 18:33:03.708962',NULL,NULL,NULL,NULL,21,2,1,11,NULL,NULL,NULL),(10,NULL,NULL,'Ulises','Teldo','Reveles','GAER010101MJCRSS36','DEAD010101458','1994-01-01','2566999','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Comerciante',NULL,'2019-12-09 18:35:13.254953','2019-12-09 21:04:20.754175',NULL,NULL,NULL,NULL,24,2,1,12,NULL,NULL,NULL),(11,NULL,NULL,'Josefina','Morales','Reveles','JAER010101MJCRSS52','DEAD010101458','2011-11-11','37525547885','jggee@hotmail.com','3355887814','',NULL,NULL,NULL,'Comerciante',NULL,'2019-12-09 22:22:36.557920','2019-12-09 23:47:08.616427',NULL,NULL,NULL,NULL,21,2,1,13,NULL,NULL,NULL),(12,NULL,NULL,'Josefina','Morales','Reveles','JAER010101MJCRSS52','JAER010101333',NULL,'','','','',NULL,NULL,NULL,'Comerciante',NULL,'2019-12-10 15:24:06.066300','2020-02-03 02:33:18.506740',NULL,NULL,NULL,NULL,24,2,1,14,NULL,NULL,NULL),(13,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-10 18:09:21.823147','2020-02-26 22:22:47.531101',14,NULL,142,NULL,24,NULL,1,NULL,NULL,NULL,NULL),(14,NULL,NULL,'Norma Nayeli','Morales','Sepeda','CAOS010101MJCRSS11','DEAD010101458','2001-01-01','','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-11 19:11:05.985292','2019-12-11 20:00:18.995730',NULL,NULL,NULL,NULL,30,NULL,1,NULL,NULL,NULL,NULL),(15,1,'salasecas SA de CV','Norma Nayeli','Teldo','Reveles','','JAER010101333',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-11 22:18:59.471109','2020-02-26 22:23:08.683208',14,NULL,142,NULL,25,NULL,1,16,NULL,NULL,NULL),(17,1,'Mulisys S.A. de C.V.','Judith','Torres','Sepeda','GAER940419MJCRSS19','JAER010101333',NULL,'GAE245800','','','',NULL,NULL,NULL,NULL,'Multisys','2019-12-11 23:30:40.882182','2020-01-22 17:39:37.924860',NULL,NULL,NULL,NULL,31,NULL,1,18,NULL,'MUYS010101256',NULL),(18,NULL,NULL,'Anastasia','Fresas','Magaña','DEIF010101MJCRSS58','DEIF010101859',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-12 22:17:59.082205','2019-12-13 04:55:15.516125',NULL,NULL,NULL,NULL,32,NULL,1,19,NULL,NULL,NULL),(19,NULL,'rosgal SA de CV','Daniel','Teldo','De la rosa','GAER010101MJCRSS36','GEER940421300',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-13 04:58:15.228073','2019-12-13 05:01:37.423323',NULL,NULL,NULL,NULL,24,NULL,1,20,NULL,NULL,NULL),(20,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-13 16:13:55.755846','2019-12-14 04:52:30.405221',NULL,NULL,NULL,NULL,NULL,NULL,1,21,NULL,NULL,NULL),(21,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-13 16:58:52.901566','2019-12-14 04:18:58.774433',NULL,NULL,NULL,NULL,NULL,NULL,1,22,NULL,NULL,NULL),(22,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-13 17:01:02.446694','2019-12-14 04:30:35.673237',NULL,NULL,NULL,NULL,NULL,NULL,1,23,NULL,NULL,NULL),(23,NULL,'Mulisys S.A. de C.V.','Norma Nayeli','Teldo','Sepeda','CAOS010101MJCRSS11','DEAD010101458',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-13 22:25:08.664419','2019-12-14 04:23:06.443539',NULL,NULL,NULL,NULL,NULL,NULL,1,24,NULL,NULL,NULL),(24,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 04:20:36.115445','2019-12-14 04:20:36.115507',NULL,NULL,NULL,NULL,NULL,NULL,1,25,NULL,NULL,NULL),(25,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 04:52:01.377718','2019-12-14 05:03:32.170934',NULL,NULL,NULL,NULL,NULL,NULL,1,26,NULL,NULL,NULL),(26,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 05:03:02.536598','2019-12-14 05:03:02.536661',NULL,NULL,NULL,NULL,NULL,NULL,1,27,NULL,NULL,NULL),(27,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 05:03:47.897582','2019-12-14 05:03:47.897674',NULL,NULL,NULL,NULL,NULL,NULL,1,28,NULL,NULL,NULL),(28,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 18:35:44.641980','2019-12-14 18:35:44.642027',NULL,NULL,NULL,NULL,NULL,NULL,1,29,NULL,NULL,NULL),(29,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 19:18:06.754235','2019-12-14 19:18:06.754271',NULL,NULL,NULL,NULL,NULL,NULL,1,30,NULL,NULL,NULL),(30,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 19:19:03.822919','2019-12-14 19:19:03.822971',NULL,NULL,NULL,NULL,NULL,NULL,1,31,NULL,NULL,NULL),(31,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 19:19:17.018876','2019-12-14 19:19:17.018919',NULL,NULL,NULL,NULL,NULL,NULL,1,32,NULL,NULL,NULL),(32,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 19:20:29.535022','2019-12-14 19:20:29.536566',NULL,NULL,NULL,NULL,NULL,NULL,1,33,NULL,NULL,NULL),(33,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 19:23:19.424667','2019-12-14 19:23:19.424711',NULL,NULL,NULL,NULL,NULL,NULL,1,34,NULL,NULL,NULL),(34,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 19:34:17.991408','2019-12-14 19:34:17.991471',NULL,NULL,NULL,NULL,NULL,NULL,1,35,NULL,NULL,NULL),(35,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 19:37:14.536435','2019-12-14 19:37:14.536474',NULL,NULL,NULL,NULL,NULL,NULL,1,36,NULL,NULL,NULL),(36,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 19:38:37.852587','2019-12-14 19:38:37.852626',NULL,NULL,NULL,NULL,NULL,NULL,1,37,NULL,NULL,NULL),(37,NULL,NULL,'Josefina','Teldo','Sepeda','JAER010101MJCRSS52','DEAD010101458',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 19:46:38.816709','2019-12-14 19:46:38.816821',NULL,NULL,NULL,NULL,24,NULL,1,38,NULL,NULL,NULL),(38,NULL,'Klayware S.A de C.V.','Klayware S.A de C.V','','','','KAEW010101859',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 20:22:18.663514','2019-12-14 20:22:18.663548',NULL,NULL,NULL,NULL,28,NULL,1,39,NULL,NULL,NULL),(39,NULL,'Secretaria ejecutiva anti corrupción','','','','','SEAJ010101256',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 20:24:38.831312','2019-12-14 20:24:38.831347',NULL,NULL,NULL,NULL,22,NULL,1,40,NULL,NULL,NULL),(40,NULL,'Empresa patito','','','','','PAAT010101236',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 21:06:59.747290','2019-12-14 21:06:59.747319',NULL,NULL,NULL,NULL,29,NULL,1,41,NULL,NULL,NULL),(41,NULL,'teresa tv','','','','','TEER010101598',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 21:07:50.606956','2019-12-14 21:07:50.607026',NULL,NULL,NULL,NULL,39,NULL,1,42,NULL,NULL,NULL),(42,NULL,'Asest','','','','','SEES010101756',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 21:09:07.599623','2019-12-14 21:09:07.599676',NULL,NULL,NULL,NULL,35,NULL,1,43,NULL,NULL,NULL),(43,NULL,'VARIOS SA de CV','','','','','VAIS010101589',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 21:10:06.043810','2019-12-14 21:10:06.043834',NULL,NULL,NULL,NULL,36,NULL,1,44,NULL,NULL,NULL),(44,NULL,'ROSA CA de CV','','','','','ROAS010101569',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 21:11:42.747922','2019-12-14 21:11:42.747948',NULL,NULL,NULL,NULL,37,NULL,1,45,NULL,NULL,NULL),(45,NULL,'Premios SA de CV','','','','','PEEM010101568',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 21:15:49.069428','2019-12-14 21:15:49.069494',NULL,NULL,NULL,NULL,29,NULL,1,46,NULL,NULL,NULL),(46,NULL,'Otro SA de CV','','','','','TOOR010101589',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 21:18:28.768828','2019-12-14 21:18:28.768855',NULL,NULL,NULL,NULL,25,NULL,1,47,NULL,NULL,NULL),(47,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 21:25:14.633619','2019-12-14 21:25:14.633646',NULL,NULL,NULL,NULL,NULL,NULL,1,48,NULL,NULL,NULL),(48,NULL,'salasecas SA de CV','','','','','DEAD010101458',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 22:14:14.810898','2019-12-14 22:14:14.810926',NULL,NULL,NULL,NULL,31,NULL,1,49,NULL,NULL,NULL),(49,NULL,'salasecas SA de CV','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 22:15:39.098792','2019-12-14 22:15:39.098815',NULL,NULL,NULL,NULL,34,NULL,1,50,NULL,NULL,NULL),(50,NULL,'nayeli torres','','','','','JAER010101333',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 22:54:41.135522','2019-12-14 22:54:41.135548',NULL,NULL,NULL,NULL,30,NULL,1,51,NULL,NULL,NULL),(51,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-14 22:54:59.612514','2020-01-09 18:22:24.797401',NULL,NULL,NULL,NULL,NULL,NULL,1,52,NULL,NULL,NULL),(52,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-15 00:28:00.296749','2019-12-24 17:33:16.396785',NULL,NULL,NULL,NULL,NULL,NULL,1,53,NULL,NULL,NULL),(53,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-15 01:04:19.249674','2019-12-23 20:25:32.039843',NULL,NULL,NULL,NULL,NULL,NULL,1,54,NULL,NULL,NULL),(54,NULL,NULL,'DANIEL','AVALOS','TORRES','DAER940419MJCRSS19','DEAF010101256',NULL,'','daniboy@gmail.com','365997542','2280011455',NULL,NULL,NULL,NULL,NULL,'2019-12-15 03:15:10.609315','2020-02-10 18:10:39.320750',NULL,2,142,NULL,NULL,1,2,55,80,NULL,'256'),(55,NULL,'Seajal','','','','','SEAJ010101856',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-20 16:04:24.871159','2019-12-20 16:04:24.871227',NULL,NULL,NULL,NULL,NULL,NULL,1,56,NULL,NULL,NULL),(56,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-20 16:33:41.401185','2019-12-20 16:47:31.307207',NULL,NULL,NULL,NULL,NULL,NULL,1,57,NULL,NULL,NULL),(57,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-20 18:22:13.582763','2019-12-20 18:22:13.582795',NULL,NULL,NULL,NULL,NULL,NULL,1,58,NULL,NULL,NULL),(58,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-20 18:24:21.921678','2019-12-20 18:24:21.921712',NULL,NULL,NULL,NULL,NULL,NULL,1,59,NULL,NULL,NULL),(59,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-23 20:25:59.659188','2019-12-24 17:54:51.328631',NULL,NULL,NULL,NULL,NULL,NULL,1,60,NULL,NULL,NULL),(60,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 16:35:36.894833','2019-12-24 16:35:36.894874',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(61,NULL,NULL,'MONICA','ROSALES','UREÑA','MOUR010101MJCRSS19','MOUR010101458','2001-01-01','37525547885','monica58l@gmail.com','3355887814','33333333',NULL,NULL,NULL,NULL,NULL,'2019-12-24 18:41:50.153591','2019-12-24 18:41:50.153640',9,2,142,2,NULL,1,3,61,118,NULL,NULL),(62,NULL,NULL,'Norma Nayeli','Morales','Reveles','JAER010101MJCRSS52','DEAD010101458','1901-01-30','37525547885','monuy@hotmail.com','3301010101','',NULL,NULL,NULL,'Gerente general industrial',NULL,'2019-12-24 18:58:53.648221','2019-12-24 19:09:09.802379',NULL,NULL,NULL,NULL,28,2,3,64,NULL,NULL,NULL),(63,1,'salasecas SA de CV','','','','','DEAD010101458','1913-01-30','','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:23:19.469333','2019-12-24 19:23:19.469427',NULL,NULL,NULL,NULL,24,NULL,3,66,NULL,NULL,NULL),(64,0,'Persona S.A de C.V','','','','','JAER010101333','2019-09-30','','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:24:36.115823','2019-12-24 19:24:36.115871',9,NULL,142,NULL,28,NULL,3,NULL,NULL,NULL,NULL),(65,1,'Persona S.A de C.V','','','','','DEAD010101458',NULL,'852440000','','','',NULL,NULL,NULL,NULL,'Rosgal','2019-12-24 19:26:11.659157','2019-12-24 19:26:11.659208',NULL,NULL,NULL,NULL,34,NULL,3,67,NULL,'MUYS010101256',NULL),(66,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:33:59.318512','2019-12-24 19:33:59.318562',NULL,NULL,NULL,NULL,NULL,NULL,3,68,NULL,NULL,NULL),(67,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:34:15.540377','2019-12-24 19:34:15.540511',NULL,NULL,NULL,NULL,NULL,NULL,3,69,NULL,NULL,NULL),(68,1,'nayeli torres','','','','','GEER940421300',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:35:07.656880','2019-12-24 19:35:07.656956',NULL,NULL,NULL,NULL,NULL,NULL,3,70,NULL,NULL,NULL),(69,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:37:28.714726','2019-12-24 19:37:28.714787',NULL,NULL,NULL,NULL,NULL,NULL,3,71,NULL,NULL,NULL),(70,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:38:03.393058','2019-12-24 19:38:03.393111',NULL,NULL,NULL,NULL,NULL,NULL,3,72,NULL,NULL,NULL),(71,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:38:33.311897','2019-12-24 19:38:33.311974',NULL,NULL,NULL,NULL,NULL,NULL,3,73,NULL,NULL,NULL),(72,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:38:54.145875','2019-12-24 19:38:54.145923',NULL,NULL,NULL,NULL,NULL,NULL,3,74,NULL,NULL,NULL),(73,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:45:35.889674','2019-12-24 19:45:35.889714',NULL,NULL,NULL,NULL,NULL,NULL,3,75,NULL,NULL,NULL),(74,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-24 19:56:30.924454','2019-12-24 19:56:30.924519',NULL,NULL,NULL,NULL,NULL,NULL,3,76,NULL,NULL,NULL),(75,1,'fsd','','','','','READ010101263',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-31 19:16:53.039927','2020-02-26 22:26:03.701338',NULL,NULL,NULL,NULL,NULL,11,1,NULL,NULL,NULL,NULL),(76,1,'ASFSADF','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2019-12-31 19:16:53.135816','2020-02-26 22:26:03.704934',NULL,NULL,NULL,NULL,NULL,12,1,NULL,NULL,NULL,NULL),(77,NULL,NULL,'JOSE LUIS','ISIDRO','LOPEZ','JOID010101MJCRSS19','JOID010101589','2001-01-01','2566999','jggee@hotmail.com','3355887814','2280011455',NULL,NULL,NULL,NULL,NULL,'2020-01-13 19:36:38.107511','2020-01-13 19:36:38.107545',9,2,142,1,NULL,1,4,77,160,NULL,NULL),(78,NULL,NULL,'TERESA','CHAVEZ','GUTIERREZ','TEER010101MJCRSS56','TEER010101589','2001-01-01','2566999','jggee@hotmail.com','3355887814','33333333',NULL,NULL,NULL,NULL,NULL,'2020-01-13 22:33:56.728825','2020-01-13 22:33:56.728864',9,1,142,2,NULL,1,5,79,169,NULL,NULL),(79,NULL,NULL,'PEDRO','PARAFAN','IRAIS','PEOD010101MJCRSS85','PEOD010101589','2001-01-01','37525547885','pedritosola@gmail.com','3355887814','33333333',NULL,NULL,NULL,NULL,NULL,'2020-01-14 15:41:21.863708','2020-01-14 16:10:08.124804',14,1,142,1,NULL,1,6,80,178,NULL,NULL),(82,NULL,NULL,'ZARA','MORALES','MORALES','ZAER010101MJCRSS58','ZAER010101',NULL,'','jo@hotmail.com','3355887814','2280011455',NULL,NULL,NULL,NULL,NULL,'2020-01-22 18:29:18.687020','2020-02-04 00:28:15.039211',NULL,2,142,NULL,NULL,1,9,83,210,NULL,NULL),(83,1,'','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-01-22 18:32:58.101237','2020-01-22 18:32:58.101275',NULL,NULL,NULL,NULL,NULL,NULL,9,84,NULL,NULL,NULL),(84,NULL,NULL,'LISA','SIMPSOM','SIMPSON','LIAS010101MJCRSS89','LIAS010101256',NULL,'','jo@hotmail.com','3355887814','3314024567',NULL,NULL,NULL,NULL,NULL,'2020-01-22 21:29:05.005149','2020-02-27 23:06:47.789047',NULL,1,142,NULL,NULL,1,10,85,224,NULL,'256'),(85,NULL,NULL,'Fernanda','Trujillos','Trijillos','FEAR010101MJCRSS62','FEAR010101253','1994-04-20','','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Calidad',NULL,'2020-01-27 18:14:14.980476','2020-01-27 18:14:14.980504',NULL,NULL,NULL,NULL,21,2,10,86,NULL,NULL,NULL),(86,NULL,NULL,'Fernanda','Trujillos','Trujillos','FAER010101MJCRSS78','FAER010101203','1994-04-20','2566999','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Comerciante',NULL,'2020-01-27 20:01:01.673207','2020-01-27 20:01:01.673233',NULL,NULL,NULL,NULL,NULL,2,10,88,NULL,NULL,NULL),(87,NULL,NULL,'Fernanda','Trujillos','Trujillos','FAER010101MJCRSS78','FAER010101203','1994-04-20','2566999','jo@hotmail.com','3355887814','',NULL,NULL,NULL,'Comerciante',NULL,'2020-01-27 20:06:24.258606','2020-01-28 16:25:24.757004',NULL,NULL,NULL,NULL,31,2,10,89,NULL,NULL,NULL),(88,NULL,NULL,'Luis','Teldo','Toblero','LUIS010101MJCRSS45','LUIS010101563','2016-04-01','','','','',NULL,NULL,NULL,'Diseñador',NULL,'2020-01-28 04:11:52.372614','2020-02-20 00:04:51.306477',NULL,NULL,NULL,NULL,21,2,10,90,NULL,NULL,NULL),(89,NULL,NULL,'Juana','Barragan','Barragan','JUAN010101MJCRSS16','JUAN010101523','1980-10-10','2566999','juana@gmail.com','3023569784','',NULL,NULL,NULL,'Estudiante',NULL,'2020-01-28 16:28:15.145451','2020-01-28 16:28:15.145480',NULL,NULL,NULL,NULL,21,2,10,91,NULL,NULL,NULL),(90,NULL,NULL,'PAREJA','PAREJA','PAREJA','PEAR010101MJCRSS45','PEAR010101023',NULL,'','','','',NULL,NULL,NULL,'EMPRENDEDORA',NULL,'2020-01-30 19:39:44.220203','2020-02-03 18:53:37.640186',NULL,NULL,NULL,NULL,28,2,1,92,NULL,NULL,NULL),(91,1,NULL,'COOPROPIETARIO','COOPROPETARIO','COOPROPIETARIO','','COOP010101569',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-01-31 22:05:00.152980','2020-01-31 22:33:23.966972',NULL,NULL,NULL,NULL,NULL,12,1,NULL,NULL,NULL,NULL),(92,1,'Esta es una razon social','','','','','TEAS010101456',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-01-31 22:05:00.340667','2020-01-31 22:33:23.970433',NULL,NULL,NULL,NULL,NULL,11,1,NULL,NULL,NULL,NULL),(93,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-01-31 22:11:50.096182','2020-01-31 22:11:50.096219',NULL,NULL,NULL,NULL,NULL,12,1,NULL,NULL,NULL,NULL),(94,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-01-31 22:11:50.098771','2020-01-31 22:11:50.098803',NULL,NULL,NULL,NULL,NULL,11,1,NULL,NULL,NULL,NULL),(95,1,'Esta es una razon social','','','','','DEAR010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-01-31 22:23:11.927735','2020-02-26 22:25:32.070567',NULL,NULL,NULL,NULL,NULL,11,1,NULL,NULL,NULL,NULL),(96,1,'sdf','','','','','adsf',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-01-31 22:23:11.929550','2020-02-26 22:25:32.074674',NULL,NULL,NULL,NULL,NULL,12,1,NULL,NULL,NULL,NULL),(97,NULL,'Inversiones SA','','','','','NIIN010101569',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-01-31 22:35:30.798708','2020-01-31 22:37:26.612252',NULL,NULL,NULL,NULL,28,7,1,94,NULL,NULL,NULL),(98,1,'GKFL','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,'NEGOCIO DE LA SECCION CLIENTES PRINCIPALES','2020-02-03 23:30:35.587101','2020-02-26 22:23:16.183867',NULL,NULL,NULL,NULL,28,NULL,1,95,NULL,'CIEL010101458',NULL),(99,1,'FIDEICOMISARIO S.A. DE C.V.','','','','','FIOD010101569',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 00:25:22.372517','2020-02-04 00:27:25.108168',NULL,NULL,NULL,NULL,NULL,8,1,97,NULL,NULL,NULL),(100,0,'FIDEICOMITENTE S.A DE C.V','','','','','FIED010101258',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 00:25:22.374508','2020-02-04 00:27:25.111358',NULL,NULL,NULL,NULL,NULL,9,1,96,NULL,NULL,NULL),(101,0,'FIDUCIARIO S.A. DE C.V','','','','','FIAC010101458',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 00:25:22.377216','2020-02-04 00:27:25.114649',NULL,NULL,NULL,NULL,NULL,10,1,98,NULL,NULL,NULL),(102,NULL,NULL,'SEBASTIAN','SEBASTIAN','SEBASTIAN','SEAB010101HJCRSS02','SEAB010101365',NULL,'','','','',NULL,NULL,NULL,'JEFATURA',NULL,'2020-02-04 00:54:16.695362','2020-02-04 06:01:50.847312',NULL,NULL,NULL,NULL,28,2,9,101,NULL,NULL,NULL),(103,NULL,NULL,'JOSE JUAN','JIMENEZj','JIMENEZ','JOES010101MJCRSS56','JOES010101586',NULL,'','','','',NULL,NULL,NULL,'Comerciante',NULL,'2020-02-04 00:56:46.864097','2020-02-04 01:01:12.962892',NULL,NULL,NULL,NULL,28,2,9,102,NULL,NULL,NULL),(104,1,'Esta es una razon social','','','','','DEAR010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 01:38:25.467704','2020-02-04 01:38:25.467764',NULL,NULL,NULL,NULL,NULL,11,9,NULL,NULL,NULL,NULL),(105,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 01:38:25.474522','2020-02-04 01:38:25.474563',NULL,NULL,NULL,NULL,31,12,9,NULL,NULL,NULL,NULL),(106,1,NULL,'','','','','DEAR010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 01:55:39.774015','2020-02-04 02:48:21.161306',NULL,NULL,NULL,NULL,NULL,11,9,NULL,NULL,NULL,NULL),(107,1,'1250','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 01:55:39.776648','2020-02-04 02:48:21.164213',NULL,NULL,NULL,NULL,NULL,12,9,NULL,NULL,NULL,NULL),(108,1,'Esta es una razon social','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 01:57:01.221224','2020-02-04 01:57:01.221257',NULL,NULL,NULL,NULL,NULL,11,9,NULL,NULL,NULL,NULL),(109,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 01:57:01.223611','2020-02-04 01:57:01.223648',NULL,NULL,NULL,NULL,NULL,12,9,NULL,NULL,NULL,NULL),(110,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 05:46:08.151343','2020-02-04 05:46:08.151381',14,NULL,142,NULL,28,NULL,9,NULL,NULL,NULL,NULL),(111,1,'Persona S.A de C.V','','','','','GEER940421300',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 14:26:19.797209','2020-02-04 14:26:19.797237',14,NULL,142,NULL,28,NULL,9,107,NULL,NULL,NULL),(112,1,'FIDEICOMISARIO S.A. DE C.V.','','','','','DEER010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 14:29:29.739140','2020-02-04 15:05:24.009855',NULL,NULL,NULL,NULL,NULL,8,9,109,NULL,NULL,NULL),(113,1,'FIDEICOMITENTE S.A DE C.V','','','','','DEER010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 14:29:29.741144','2020-02-04 15:05:24.019315',NULL,NULL,NULL,NULL,NULL,9,9,108,NULL,NULL,NULL),(114,1,'FIDUCIARIO S.A. DE C.V','','','','','TEAC010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 14:29:29.742389','2020-02-04 15:05:24.028608',NULL,NULL,NULL,NULL,NULL,10,9,110,NULL,NULL,NULL),(115,1,'2.TERCERO','','','','','TEER010101563',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 16:12:46.977516','2020-02-04 22:31:16.728463',NULL,NULL,NULL,NULL,NULL,11,10,NULL,NULL,NULL,NULL),(116,1,'2.TRANMISOR','','','','','TAAR010101256',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 16:12:47.186810','2020-02-04 22:31:16.730427',NULL,NULL,NULL,NULL,NULL,12,10,NULL,NULL,NULL,NULL),(120,NULL,NULL,'Juana','Barragan','Barragan','JUAN010101MJCRSS16','JUAN010101523',NULL,'','','','',NULL,NULL,NULL,'Estudiante',NULL,'2020-02-04 17:47:50.647334','2020-02-04 17:47:50.647449',NULL,NULL,NULL,NULL,21,2,10,119,NULL,NULL,NULL),(121,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 17:53:39.720160','2020-02-04 17:53:39.720219',NULL,NULL,NULL,NULL,NULL,2,10,120,NULL,NULL,NULL),(122,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 18:30:23.846190','2020-02-04 18:30:23.846256',NULL,NULL,NULL,NULL,NULL,11,10,NULL,NULL,NULL,NULL),(123,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 18:30:23.856839','2020-02-04 18:30:23.856905',NULL,NULL,NULL,NULL,NULL,12,10,NULL,NULL,NULL,NULL),(124,1,'Esta es una razon social','','','','','DAAD010101012',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 18:31:41.498734','2020-02-04 18:31:41.498831',NULL,NULL,NULL,NULL,NULL,11,10,NULL,NULL,NULL,NULL),(125,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 18:31:41.501127','2020-02-04 18:31:41.501150',NULL,NULL,NULL,NULL,NULL,12,10,NULL,NULL,NULL,NULL),(126,1,'TERCERO SA DE CV','','','','','TEER010101236',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 21:34:43.537844','2020-02-04 21:34:43.537922',NULL,NULL,NULL,NULL,NULL,11,10,NULL,NULL,NULL,NULL),(127,1,'TRANSIMOR SA DE CV','','','','','TAAR010101256',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 21:34:43.584241','2020-02-04 21:34:43.584306',NULL,NULL,NULL,NULL,NULL,12,10,NULL,NULL,NULL,NULL),(128,1,'Esta es una razon social del tercero','','','','','DAAD010101012',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 22:32:42.464510','2020-02-04 22:32:42.464538',NULL,NULL,NULL,NULL,NULL,11,10,NULL,NULL,NULL,NULL),(129,1,'89.transmisor','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 22:32:42.467289','2020-02-04 22:32:42.467319',NULL,NULL,NULL,NULL,NULL,12,10,NULL,NULL,NULL,NULL),(130,1,'Esta es una razon social del tercero','','','','','DAAD010101012',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 22:33:04.170237','2020-02-04 22:33:04.170265',NULL,NULL,NULL,NULL,NULL,11,10,NULL,NULL,NULL,NULL),(131,1,'89.transmisor','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 22:33:04.172752','2020-02-04 22:33:04.172786',NULL,NULL,NULL,NULL,NULL,12,10,NULL,NULL,NULL,NULL),(132,1,'Esta es una razon social del tercero','','','','','DAAD010101012',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 22:33:48.525407','2020-02-04 22:33:48.525435',NULL,NULL,NULL,NULL,NULL,11,10,NULL,NULL,NULL,NULL),(133,1,'89.transmisor','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 22:33:48.527297','2020-02-04 22:33:48.527322',NULL,NULL,NULL,NULL,NULL,12,10,NULL,NULL,NULL,NULL),(134,1,'99.TERCERO','','','','','REET010101235',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 22:36:02.782631','2020-02-04 22:36:02.782654',NULL,NULL,NULL,NULL,NULL,11,10,NULL,NULL,NULL,NULL),(135,1,'99.TRANSMISOR','','','','','TAAR010101256',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 22:36:02.784835','2020-02-04 22:36:02.784860',NULL,NULL,NULL,NULL,NULL,12,10,NULL,NULL,NULL,NULL),(136,1,'255.TERCERO','','','','','TEER010101258',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 22:40:02.578881','2020-02-06 22:12:50.868922',NULL,NULL,NULL,NULL,NULL,11,2,NULL,NULL,NULL,NULL),(137,1,'255.TRANSMISOR','','','','','TAAR010101256',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-04 22:40:02.580876','2020-02-06 22:12:50.877846',NULL,NULL,NULL,NULL,NULL,12,2,NULL,NULL,NULL,NULL),(138,1,'12312313','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-06 20:22:55.279898','2020-02-06 20:22:55.279936',NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL),(139,1,'55.TRANSMISOR','','','','','TAAR010101123',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-06 23:01:37.618771','2020-02-06 23:01:37.618839',NULL,NULL,NULL,NULL,NULL,11,2,NULL,NULL,NULL,NULL),(140,1,'55.TERCERO','','','','','TEER010101369',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-06 23:01:37.626481','2020-02-06 23:01:37.626548',NULL,NULL,NULL,NULL,NULL,12,2,NULL,NULL,NULL,NULL),(141,0,'MORAL RAZON TERCERO','','','','','TAAR010101256',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-07 03:13:09.392972','2020-02-07 03:13:09.393194',NULL,NULL,NULL,NULL,NULL,12,2,NULL,NULL,NULL,NULL),(142,0,'MORAL RAZON','','','','','SEAF010101256',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-07 03:13:09.440504','2020-02-07 03:13:09.440585',NULL,NULL,NULL,NULL,NULL,11,2,NULL,NULL,NULL,NULL),(143,0,'255.inversiones razon social','','','','','NIIV010101569',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-07 04:15:45.536858','2020-02-07 04:15:45.536945',NULL,NULL,NULL,NULL,NULL,7,2,136,NULL,NULL,NULL),(144,0,'99.moral representante','','','','','REEP010101569',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-07 05:03:41.070999','2020-02-07 05:03:41.071078',9,NULL,142,NULL,25,NULL,2,137,NULL,NULL,NULL),(145,NULL,NULL,'SULEIMA','SOLEI','SOLEOº','SUEL010101MJCRSS63','SUEL010101236',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-10 18:53:21.563241','2020-02-10 18:53:21.563273',NULL,NULL,NULL,NULL,NULL,2,2,140,NULL,NULL,NULL),(146,NULL,NULL,'TORIBIO','TORIBIO','TORIBIO','','',NULL,'','','','',NULL,NULL,NULL,'Desarrollador',NULL,'2020-02-10 18:56:03.879735','2020-02-10 18:57:41.897662',NULL,NULL,NULL,NULL,21,2,2,141,NULL,NULL,NULL),(147,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-10 19:35:48.830943','2020-02-10 19:36:54.498415',NULL,NULL,NULL,NULL,NULL,NULL,2,142,NULL,NULL,NULL),(148,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-10 19:38:32.599319','2020-02-10 19:38:32.599351',NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL),(149,1,'ADSFASDF','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-10 19:40:33.402405','2020-02-10 19:40:47.021499',NULL,NULL,NULL,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL),(150,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-10 19:44:45.795016','2020-02-10 19:51:02.897607',9,NULL,142,NULL,NULL,NULL,2,NULL,NULL,NULL,NULL),(151,1,'','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,'DESARROLLADORES DE IDEAS','2020-02-10 20:03:26.108592','2020-02-10 20:03:26.108628',NULL,NULL,NULL,NULL,NULL,NULL,2,146,NULL,NULL,NULL),(152,0,'55.FIDEICOMISARIO S.A. DE C.V.','','','','','DEER010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-10 20:07:35.817925','2020-02-10 22:46:22.375141',NULL,NULL,NULL,NULL,NULL,8,2,148,NULL,NULL,NULL),(153,0,'55.FIDEICOMITENTE S.A DE C.V','','','','','DEER010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-10 20:07:35.820405','2020-02-10 22:46:22.379269',NULL,NULL,NULL,NULL,NULL,9,2,147,NULL,NULL,NULL),(154,0,'55.FIDUCIARIO S.A. DE C.V','','','','','TEAC010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-10 20:07:35.822026','2020-02-10 22:46:22.390613',NULL,NULL,NULL,NULL,NULL,10,2,149,NULL,NULL,NULL),(155,NULL,NULL,'PRUEBA DOS','PRUEBA DOS','PRUEBA DOS','PUER010101MJCRSS56','PUER010101321',NULL,'','prueba2@gmail.com','3333','2280011455',NULL,NULL,NULL,NULL,NULL,'2020-02-17 15:21:11.447116','2020-02-17 15:21:11.447200',NULL,2,142,NULL,NULL,1,11,150,356,NULL,'321'),(156,NULL,NULL,'JULIO','JULIO','JULIO','JUUL010101MJCRSS56','JUUL010101896',NULL,'','','','',NULL,NULL,NULL,'Jefe de departamento',NULL,'2020-02-17 22:19:56.744663','2020-02-17 22:19:56.744730',NULL,NULL,NULL,NULL,28,2,4,151,NULL,NULL,NULL),(157,NULL,NULL,'ARMANDO','ARMANDO','ARMANDO','RAAM010101MJCRSS56','RAAM010101256',NULL,'','','','',NULL,NULL,NULL,'PROGRAMADOR',NULL,'2020-02-17 22:21:33.333675','2020-02-17 23:20:11.043142',NULL,NULL,NULL,NULL,41,2,4,152,NULL,NULL,NULL),(158,NULL,NULL,'RAMON','VALDEZ','VALDEZ','RAOM010101MJCRSS45','RAOM010101856',NULL,'','','','',NULL,NULL,NULL,'Comerciante',NULL,'2020-02-18 15:50:14.187787','2020-02-26 22:12:00.371806',NULL,NULL,NULL,NULL,28,2,11,153,NULL,NULL,NULL),(159,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-18 22:54:26.278928','2020-02-18 22:54:26.278999',NULL,NULL,NULL,NULL,NULL,12,11,NULL,NULL,NULL,NULL),(160,1,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-18 22:54:26.427378','2020-02-18 22:54:26.427446',NULL,NULL,NULL,NULL,NULL,11,11,NULL,NULL,NULL,NULL),(161,1,'Laura Gonzales','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-19 15:38:39.270394','2020-02-19 15:38:39.272513',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(162,0,'RGGA S.A. de C.V.','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-19 17:21:32.169840','2020-02-19 17:21:32.169870',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(163,0,'nayeli torres','','','','','JAER010101333',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-19 17:38:43.286477','2020-02-19 17:38:43.286507',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),(164,1,NULL,'Norma Nayeli','AMARANTO','BILLAREAL','','NOAM010101589',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-19 17:43:03.080733','2020-02-19 17:45:02.945139',NULL,NULL,NULL,NULL,NULL,NULL,10,NULL,NULL,NULL,NULL),(165,1,NULL,'JULIO','ARTEAGA','ARTEAGA','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-19 17:47:27.843243','2020-02-19 17:48:41.180020',NULL,NULL,NULL,NULL,NULL,NULL,10,NULL,NULL,NULL,NULL),(169,0,'55.FIDEICOMISARIO S.A. DE C.V.','','','','','DEER010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-21 17:44:38.597357','2020-02-21 19:33:53.397572',NULL,NULL,NULL,NULL,NULL,8,10,160,NULL,NULL,NULL),(170,0,'55.FIDEICOMITENTE S.A DE C.V','','','','','DEER010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-21 17:44:38.601690','2020-02-21 19:33:53.404252',NULL,NULL,NULL,NULL,NULL,9,10,159,NULL,NULL,NULL),(171,0,'55.FIDUCIARIO S.A. DE C.V','','','','','TEAC010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-21 17:44:38.603810','2020-02-21 19:33:53.408289',NULL,NULL,NULL,NULL,NULL,10,10,161,NULL,NULL,NULL),(172,0,'TERCERO SA DE CV','','','','','SEAF010101MJC',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-21 22:11:27.187705','2020-02-21 22:11:27.187731',NULL,NULL,NULL,NULL,NULL,12,10,NULL,NULL,NULL,NULL),(173,0,'TRANSMISOR SA de CV','','','','','DAAD010101012',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-21 22:11:27.189072','2020-02-21 22:11:27.189096',NULL,NULL,NULL,NULL,NULL,11,10,NULL,NULL,NULL,NULL),(174,1,NULL,'Norma Nayeli','ARMANDO','Sepeda','','DEAD010101458',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-21 22:21:51.804415','2020-02-21 22:21:51.804483',NULL,NULL,NULL,NULL,NULL,7,10,162,NULL,NULL,NULL),(175,0,'99.TERCERO DEUDAS','','','','','TEER010101489',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-21 22:24:09.028849','2020-03-18 16:53:14.886630',NULL,NULL,NULL,NULL,NULL,NULL,10,163,NULL,NULL,NULL),(176,0,'1.TITULAR','','','','','TIUL010101598',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-21 22:24:30.227829','2020-03-18 16:52:04.660646',NULL,NULL,NULL,NULL,NULL,NULL,10,NULL,NULL,NULL,NULL),(177,NULL,NULL,'','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-21 22:25:45.007908','2020-02-21 22:25:45.008014',9,NULL,142,NULL,NULL,NULL,10,NULL,NULL,NULL,NULL),(178,0,'REPRESENTANTE SA de CV','','','','','JAER010101333',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-21 22:27:25.873518','2020-02-21 22:30:45.692788',9,NULL,142,NULL,NULL,NULL,10,166,NULL,NULL,NULL),(179,0,'rosgal SA de CV.','Ulises','','','','RAAM010101256',NULL,'','','','',NULL,NULL,NULL,NULL,'Rosgal','2020-02-21 22:31:32.318452','2020-02-21 22:37:48.849273',NULL,NULL,NULL,NULL,NULL,NULL,10,167,NULL,'MUYS010101256',NULL),(180,0,'salasecas SA de CV','','','','','PEER010101256',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-25 17:11:05.030729','2020-02-25 19:08:37.278785',14,NULL,142,NULL,25,NULL,11,168,NULL,NULL,NULL),(181,0,'rosgal SA de CV.','','','','','RAAM010101256',NULL,'','','','',NULL,NULL,NULL,NULL,'Rosgal','2020-02-25 18:03:55.132893','2020-02-25 18:03:55.132964',NULL,NULL,NULL,NULL,28,NULL,11,169,NULL,'MUYS010101256',NULL),(182,0,'Persona S.A de C.V','','','','','TOOR010101589',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-25 18:05:10.569434','2020-02-25 18:05:10.569500',NULL,NULL,NULL,NULL,NULL,NULL,11,NULL,NULL,NULL,NULL),(183,0,'TERCERO SA DE CV','','','','','TAAR010101589',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-25 18:11:51.064049','2020-02-25 23:03:21.943187',NULL,NULL,NULL,NULL,NULL,11,11,NULL,NULL,NULL,NULL),(184,0,'TRANSIMOR SA DE CV','','','','','',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-25 18:11:51.066388','2020-02-25 23:03:21.945372',NULL,NULL,NULL,NULL,NULL,12,11,NULL,NULL,NULL,NULL),(185,0,'REPRESENTACIONES S.A','','','','','JAER010101333',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-25 19:09:35.822335','2020-02-25 19:09:35.822362',14,NULL,142,NULL,24,NULL,11,172,NULL,NULL,NULL),(186,NULL,NULL,'TOMAS','TORIBIO','LOMELI','TOAM010101MJCRSS58','TOAM010101589','2016-10-18','','','','',NULL,NULL,NULL,'Diseñador',NULL,'2020-02-26 18:45:21.901056','2020-02-26 18:45:21.901092',NULL,NULL,NULL,NULL,32,2,11,173,NULL,NULL,NULL),(187,NULL,NULL,'DANIEL','AVALOS','TORRES','DAER940419MJCRSS19','DEAF010101256',NULL,'','daniel.avalos@gmail.com','33366','33333333',NULL,NULL,NULL,NULL,NULL,'2020-02-28 16:01:52.488183','2020-02-28 16:01:52.488214',NULL,2,142,NULL,NULL,1,12,174,416,NULL,'256'),(188,NULL,NULL,'Tadeo','Torres','Galindo','TAED010101MJCRSS58','TAED010101589','1987-02-18','','','','',NULL,NULL,NULL,NULL,NULL,'2020-02-28 16:04:00.958682','2020-03-06 18:02:11.292668',NULL,NULL,NULL,NULL,NULL,2,12,175,NULL,NULL,NULL),(189,NULL,NULL,'LORENA','SUAREZ','MONTANEGRO','LOAS010101MJCRSS85','LOAS010101589',NULL,'','lorena@gmail.com','3355887814','33333333',NULL,NULL,NULL,NULL,NULL,'2020-03-03 16:38:10.837772','2020-03-03 16:38:10.837808',NULL,2,142,NULL,NULL,1,13,176,422,NULL,'589'),(190,0,'99TERCERO INVERSIONES','','','','','REET010101893',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-03-03 17:00:05.609978','2020-03-03 17:00:05.610057',NULL,NULL,NULL,NULL,NULL,7,13,177,NULL,NULL,NULL),(191,0,'Mulisys S.A. de C.V.','','','','','GEER940421',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-03-06 22:35:09.475554','2020-03-06 22:35:09.475616',NULL,NULL,NULL,NULL,NULL,NULL,12,NULL,NULL,NULL,NULL),(192,0,'salasecas SA de CV','','','','','PUER010101896',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-03-06 22:38:30.061645','2020-03-06 22:38:30.061710',NULL,NULL,NULL,NULL,NULL,NULL,12,NULL,NULL,NULL,NULL),(193,0,'salasecas SA de CV','','','','','PUER010101896',NULL,'','','','',NULL,NULL,NULL,NULL,NULL,'2020-03-06 22:47:25.050480','2020-03-06 22:47:25.050542',NULL,NULL,NULL,NULL,NULL,NULL,12,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `declaracion_infopersonalvar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_ingresosdeclaracion`
--

DROP TABLE IF EXISTS `declaracion_ingresosdeclaracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_ingresosdeclaracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `ingreso_mensual_cargo` int(11) DEFAULT NULL,
  `ingreso_mensual_otros_ingresos` int(11) DEFAULT NULL,
  `ingreso_mensual_actividad` int(11) DEFAULT NULL,
  `razon_social_negocio` varchar(100) DEFAULT NULL,
  `tipo_negocio` varchar(300) DEFAULT NULL,
  `ingreso_mensual_financiera` int(11) DEFAULT NULL,
  `ingreso_mensual_servicios` int(11) DEFAULT NULL,
  `tipo_servicio` varchar(100) DEFAULT NULL,
  `ingreso_otros_ingresos` int(11) DEFAULT NULL,
  `ingreso_mensual_neto` int(11) DEFAULT NULL,
  `ingreso_mensual_pareja_dependientes` int(11) DEFAULT NULL,
  `ingreso_mensual_total` int(11) DEFAULT NULL,
  `cat_moneda_actividad_id` int(11) NOT NULL,
  `cat_moneda_cargo_id` int(11) NOT NULL,
  `cat_moneda_financiera_id` int(11) NOT NULL,
  `cat_moneda_otro_ingresos_mensual_id` int(11) NOT NULL,
  `cat_moneda_otros_ingresos_id` int(11) NOT NULL,
  `cat_moneda_servicios_id` int(11) NOT NULL,
  `cat_tipo_instrumento_id` int(11) DEFAULT NULL,
  `cat_tipos_actividad_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `otro_tipo_instrumento` varchar(255) NOT NULL,
  `cat_moneda_neto_id` int(11) NOT NULL,
  `cat_moneda_pareja_dependientes_id` int(11) NOT NULL,
  `cat_moneda_total_id` int(11) NOT NULL,
  `cat_moneda_enajenacion_bienes_id` int(11) NOT NULL,
  `cat_tipos_bienes_id` int(11) DEFAULT NULL,
  `fecha_conclusion` date DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `ingreso_anio_anterior` tinyint(1) DEFAULT NULL,
  `ingreso_enajenacion_bienes` int(11) DEFAULT NULL,
  `tipo_ingreso` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_ingresos_cat_tipo_instrumento_9e66ee82_fk_declaraci` (`cat_tipo_instrumento_id`),
  KEY `declaracion_ingresos_cat_tipos_actividad__73e74b0c_fk_declaraci` (`cat_tipos_actividad_id`),
  KEY `declaracion_ingresos_declaraciones_id_d4ef074c_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_ingresos_cat_moneda_actividad_d7bdf545_fk_declaraci` (`cat_moneda_actividad_id`),
  KEY `declaracion_ingresos_cat_moneda_cargo_id_23912393_fk_declaraci` (`cat_moneda_cargo_id`),
  KEY `declaracion_ingresos_cat_moneda_financier_314c5e99_fk_declaraci` (`cat_moneda_financiera_id`),
  KEY `declaracion_ingresos_cat_moneda_neto_id_139501ad_fk_declaraci` (`cat_moneda_neto_id`),
  KEY `declaracion_ingresos_cat_moneda_otro_ingr_3880dff4_fk_declaraci` (`cat_moneda_otro_ingresos_mensual_id`),
  KEY `declaracion_ingresos_cat_moneda_otros_ing_3e834e0c_fk_declaraci` (`cat_moneda_otros_ingresos_id`),
  KEY `declaracion_ingresos_cat_moneda_pareja_de_fbd119b9_fk_declaraci` (`cat_moneda_pareja_dependientes_id`),
  KEY `declaracion_ingresos_cat_moneda_servicios_0df72867_fk_declaraci` (`cat_moneda_servicios_id`),
  KEY `declaracion_ingresos_cat_moneda_total_id_dda49ed1_fk_declaraci` (`cat_moneda_total_id`),
  KEY `declaracion_ingresos_observaciones_id_dcc54453_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_ingresos_cat_moneda_enajenaci_4b8f2266_fk_declaraci` (`cat_moneda_enajenacion_bienes_id`),
  KEY `declaracion_ingresos_cat_tipos_bienes_id_1abb50ce_fk_declaraci` (`cat_tipos_bienes_id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_actividad_d7bdf545_fk_declaraci` FOREIGN KEY (`cat_moneda_actividad_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_cargo_id_23912393_fk_declaraci` FOREIGN KEY (`cat_moneda_cargo_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_enajenaci_4b8f2266_fk_declaraci` FOREIGN KEY (`cat_moneda_enajenacion_bienes_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_financier_314c5e99_fk_declaraci` FOREIGN KEY (`cat_moneda_financiera_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_neto_id_139501ad_fk_declaraci` FOREIGN KEY (`cat_moneda_neto_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_otro_ingr_3880dff4_fk_declaraci` FOREIGN KEY (`cat_moneda_otro_ingresos_mensual_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_otros_ing_3e834e0c_fk_declaraci` FOREIGN KEY (`cat_moneda_otros_ingresos_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_pareja_de_fbd119b9_fk_declaraci` FOREIGN KEY (`cat_moneda_pareja_dependientes_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_servicios_0df72867_fk_declaraci` FOREIGN KEY (`cat_moneda_servicios_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_total_id_dda49ed1_fk_declaraci` FOREIGN KEY (`cat_moneda_total_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipo_instrumento_9e66ee82_fk_declaraci` FOREIGN KEY (`cat_tipo_instrumento_id`) REFERENCES `declaracion_cattiposinstrumentos` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipos_actividad__73e74b0c_fk_declaraci` FOREIGN KEY (`cat_tipos_actividad_id`) REFERENCES `declaracion_cattiposactividad` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipos_bienes_id_1abb50ce_fk_declaraci` FOREIGN KEY (`cat_tipos_bienes_id`) REFERENCES `declaracion_cattiposbienes` (`id`),
  CONSTRAINT `declaracion_ingresos_declaraciones_id_d4ef074c_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_ingresos_observaciones_id_dcc54453_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_ingresosdeclaracion`
--

LOCK TABLES `declaracion_ingresosdeclaracion` WRITE;
/*!40000 ALTER TABLE `declaracion_ingresosdeclaracion` DISABLE KEYS */;
INSERT INTO `declaracion_ingresosdeclaracion` VALUES (1,'2020-01-30 20:32:03.121170','2020-02-03 02:34:53.471711',80000,5000,1200,NULL,'TECNOLOGICO',1000,4582,'ESTA ES UNA PRUEBA DE TIPO DE SERVICIO PRESTADO',1250,4560,12500,12000,101,101,101,101,101,101,5,NULL,1,253,'',101,101,101,101,NULL,NULL,NULL,0,NULL,1),(2,'2020-01-30 23:43:06.666329','2020-02-04 15:58:28.245428',50000,4567,1234,'RAZON SOCIAL SERVIDOR','GOBIERNO',5200,8000,'NO TIPO DE SERVICIO PRESTADO',45,8693,NULL,NULL,101,101,101,101,101,101,2,2,1,255,'',101,101,101,101,2,'2010-01-02','2002-02-02',1,8523,0),(3,'2020-02-04 01:08:20.024524','2020-02-04 01:08:20.024559',1000,1000,2000,'RAZON SOCIAL SERVIDOR','TECNOLOGICO',3000,1000,NULL,4000,20000,5000,25000,101,101,101,101,101,101,NULL,5,9,283,'',101,101,101,101,NULL,NULL,NULL,0,NULL,1),(4,'2020-02-04 01:30:47.873137','2020-02-04 01:30:47.873174',10000,1000,1000,'RAZON SOCIAL DEPENDIENTES','TIPO NEGOCIO DEPENDIENTE',4000,3000,NULL,5000,25000,NULL,NULL,101,101,101,101,101,101,5,2,9,284,'',101,101,101,101,2,'2019-11-18','2017-07-06',1,3000,0),(5,'2020-02-04 16:12:36.403092','2020-02-21 21:43:20.786965',999,999,999,'RAZON SOCIAL DEPENDIENTES','GOBIERNO',123132,6300,'1',900458,5687,589,5898,101,101,101,101,101,101,2,NULL,10,297,'',101,101,101,101,NULL,'2019-12-31','2019-08-01',1,58900,0),(6,'2020-02-10 19:02:19.829501','2020-02-10 19:02:19.829545',10000,10000,2000,NULL,NULL,3000,1000,NULL,4000,20000,5000,25000,101,101,101,101,101,101,NULL,NULL,2,330,'',101,101,101,101,NULL,NULL,NULL,0,NULL,1),(7,'2020-02-10 19:03:39.821290','2020-02-10 19:04:02.480011',10000,10000,4000,NULL,NULL,1000,2000,NULL,5000,20000,NULL,NULL,101,101,101,101,101,101,NULL,2,2,331,'',101,101,101,101,2,'2019-07-26','2016-07-26',1,3000,0),(8,'2020-02-21 20:43:47.269248','2020-02-21 21:57:30.198326',1000,100,100,'RAZON SOCIAL SERVIDOR','TECNOLOGICO',1000,100,'1',100,100,100,100,101,101,101,101,101,101,2,2,10,382,'',101,101,101,101,NULL,NULL,NULL,0,NULL,1),(9,'2020-02-26 21:38:11.068636','2020-02-26 21:38:11.068691',500,2000,1000,NULL,NULL,700,200,NULL,100,2500,8560,11060,101,101,101,101,101,101,NULL,NULL,11,407,'',101,101,101,101,NULL,NULL,NULL,0,NULL,1),(10,'2020-02-26 21:42:24.750486','2020-02-26 21:42:24.750520',500,2100,100,NULL,NULL,400,500,NULL,600,2600,NULL,NULL,101,101,101,101,101,101,NULL,NULL,11,408,'',101,101,101,101,NULL,NULL,NULL,1,500,0);
/*!40000 ALTER TABLE `declaracion_ingresosdeclaracion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_ingresosvarios`
--

DROP TABLE IF EXISTS `declaracion_ingresosvarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_ingresosvarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ingreso_bruto_anual` int(11) DEFAULT NULL,
  `duracion_dias` decimal(6,2) DEFAULT NULL,
  `duracion_meses` decimal(6,2) DEFAULT NULL,
  `duracion_anual` decimal(6,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otra_actividad` varchar(255) NOT NULL,
  `es_transaccion` tinyint(1) DEFAULT NULL,
  `otro_mueble` varchar(255) DEFAULT NULL,
  `descripcion_actividad` varchar(255) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_tipos_actividad_id` int(11) DEFAULT NULL,
  `cat_tipos_ingresos_varios_id` int(11) DEFAULT NULL,
  `cat_tipos_muebles_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `ingreso_neto_mensual` int(11) DEFAULT NULL,
  `cat_tipo_instrumento_id` int(11) DEFAULT NULL,
  `ingreso_anio_anterior` tinyint(1) DEFAULT NULL,
  `servidor_publico_anio_anterior` tinyint(1) DEFAULT NULL,
  `cat_tipos_bienes_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_ingresos_cat_monedas_id_9bdc233f_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_ingresos_cat_tipos_actividad__63f6b0d2_fk_declaraci` (`cat_tipos_actividad_id`),
  KEY `declaracion_ingresos_cat_tipos_ingresos_v_f2a5bdcc_fk_declaraci` (`cat_tipos_ingresos_varios_id`),
  KEY `declaracion_ingresos_cat_tipos_muebles_id_f7703ab3_fk_declaraci` (`cat_tipos_muebles_id`),
  KEY `declaracion_ingresos_declaraciones_id_5ce400f7_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_ingresos_info_personal_var_id_c79fca54_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_ingresos_observaciones_id_7c542469_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_ingresosvarios_tipo_instrumento_id_64861786` (`cat_tipo_instrumento_id`),
  KEY `declaracion_ingresos_cat_tipos_bienes_id_57654905_fk_declaraci` (`cat_tipos_bienes_id`),
  CONSTRAINT `declaracion_ingresos_cat_monedas_id_9bdc233f_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipo_instrumento_4821a151_fk_declaraci` FOREIGN KEY (`cat_tipo_instrumento_id`) REFERENCES `declaracion_cattiposinstrumentos` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipos_actividad__63f6b0d2_fk_declaraci` FOREIGN KEY (`cat_tipos_actividad_id`) REFERENCES `declaracion_cattiposactividad` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipos_bienes_id_57654905_fk_declaraci` FOREIGN KEY (`cat_tipos_bienes_id`) REFERENCES `declaracion_cattiposbienes` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipos_ingresos_v_f2a5bdcc_fk_declaraci` FOREIGN KEY (`cat_tipos_ingresos_varios_id`) REFERENCES `declaracion_cattiposingresosvarios` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipos_muebles_id_f7703ab3_fk_declaraci` FOREIGN KEY (`cat_tipos_muebles_id`) REFERENCES `declaracion_cattiposmuebles` (`id`),
  CONSTRAINT `declaracion_ingresos_declaraciones_id_5ce400f7_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_ingresos_info_personal_var_id_c79fca54_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_ingresos_observaciones_id_7c542469_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_ingresosvarios`
--

LOCK TABLES `declaracion_ingresosvarios` WRITE;
/*!40000 ALTER TABLE `declaracion_ingresosvarios` DISABLE KEYS */;
INSERT INTO `declaracion_ingresosvarios` VALUES (33,4589,NULL,NULL,NULL,'2019-12-14 22:54:41.136916','2019-12-14 22:54:41.136940','',NULL,NULL,NULL,101,1,9,NULL,1,50,75,1,1,1,1,NULL),(34,NULL,NULL,NULL,NULL,'2019-12-14 22:54:59.614443','2020-01-09 18:22:24.801967','',NULL,NULL,NULL,101,NULL,8,NULL,1,51,76,1528,NULL,NULL,NULL,1),(35,5892,NULL,NULL,NULL,'2019-12-15 00:28:00.302862','2019-12-24 17:33:16.400437','',NULL,NULL,'Neu',101,1,1,NULL,1,52,77,1,NULL,0,0,NULL),(36,NULL,NULL,NULL,NULL,'2019-12-15 01:04:19.252278','2019-12-23 20:25:32.043600','',NULL,NULL,'ninguna',101,1,2,NULL,1,53,78,5289,NULL,0,0,NULL),(37,NULL,NULL,NULL,NULL,'2019-12-20 16:04:24.968503','2019-12-20 16:04:24.968570','',NULL,NULL,'Desarrollo de web',101,5,3,NULL,1,55,100,1500,1,1,1,NULL),(38,NULL,NULL,NULL,NULL,'2019-12-20 16:33:41.403881','2019-12-20 16:47:31.318294','',NULL,NULL,'Es una siempre descripción de la actividad',101,4,4,NULL,1,56,101,10500,1,1,1,NULL),(39,NULL,NULL,NULL,NULL,'2019-12-20 18:22:13.585544','2019-12-20 18:22:13.585583','',NULL,NULL,'t',101,4,5,NULL,1,57,102,1500,NULL,1,1,NULL),(40,NULL,NULL,NULL,NULL,'2019-12-20 18:24:21.924050','2019-12-20 18:24:21.924088','',NULL,NULL,NULL,101,NULL,6,NULL,1,58,103,30000,NULL,1,1,NULL),(41,8592,NULL,NULL,NULL,'2019-12-23 20:25:59.780690','2019-12-24 17:54:51.332267','',NULL,NULL,'Este es un tipo de descricpción',101,7,2,NULL,1,59,115,5000,NULL,0,0,NULL),(42,NULL,NULL,NULL,NULL,'2019-12-24 19:33:59.321437','2019-12-24 19:33:59.321487','',NULL,NULL,NULL,101,4,1,NULL,3,66,137,1200,NULL,0,0,NULL),(43,NULL,NULL,NULL,NULL,'2019-12-24 19:34:15.558058','2019-12-24 19:34:15.558108','',NULL,NULL,NULL,101,1,2,NULL,3,67,138,1500,NULL,0,0,NULL),(44,NULL,NULL,NULL,NULL,'2019-12-24 19:35:07.660936','2019-12-24 19:35:07.661003','',NULL,NULL,NULL,101,7,3,NULL,3,68,139,8200,NULL,0,0,NULL),(45,NULL,NULL,NULL,NULL,'2019-12-24 19:37:28.718337','2019-12-24 19:37:28.718410','',NULL,NULL,'Produce un ingreso ya que trabajo',101,4,4,NULL,3,69,140,8963,1,0,0,NULL),(46,NULL,NULL,NULL,NULL,'2019-12-24 19:38:03.398965','2019-12-24 19:38:03.399018','',NULL,NULL,NULL,101,1,5,NULL,3,70,141,8563,NULL,0,0,NULL),(47,NULL,NULL,NULL,NULL,'2019-12-24 19:38:33.315136','2019-12-24 19:38:33.315182','',NULL,NULL,NULL,101,NULL,6,NULL,3,71,142,5230,NULL,0,0,NULL),(48,NULL,NULL,NULL,NULL,'2019-12-24 19:38:54.149129','2019-12-24 19:38:54.149177','',NULL,NULL,NULL,101,NULL,7,NULL,3,72,143,3589,NULL,0,0,NULL),(49,NULL,NULL,NULL,NULL,'2019-12-24 19:45:35.892688','2019-12-24 19:45:35.892753','',NULL,NULL,NULL,101,NULL,8,NULL,3,73,144,8560,NULL,0,0,NULL),(50,NULL,NULL,NULL,NULL,'2019-12-24 19:56:30.929778','2019-12-24 19:56:30.929844','',NULL,NULL,NULL,101,NULL,9,NULL,3,74,145,5288,NULL,0,0,NULL);
/*!40000 ALTER TABLE `declaracion_ingresosvarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_inversiones`
--

DROP TABLE IF EXISTS `declaracion_inversiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_inversiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otra_inversion` varchar(255) NOT NULL,
  `otro_tipo_especifico` varchar(255) NOT NULL,
  `num_cuenta` varchar(255) NOT NULL,
  `otra_forma` varchar(255) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `monto_original` decimal(12,2) DEFAULT NULL,
  `tasa_interes` decimal(5,2) DEFAULT NULL,
  `saldo_actual` decimal(10,2) DEFAULT NULL,
  `plazo` decimal(6,2) DEFAULT NULL,
  `otro_tipo_titular` varchar(255) NOT NULL,
  `porcentaje_inversion` decimal(5,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_formas_adquisiciones_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipos_especificos_inversiones_id` int(11) DEFAULT NULL,
  `cat_tipos_inversiones_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `cat_tipos_titulares_id` int(11) DEFAULT NULL,
  `cat_tipo_persona_id` int(11) DEFAULT NULL,
  `fondo_inversion` varchar(255) DEFAULT NULL,
  `organizaciones` varchar(255) DEFAULT NULL,
  `afores` varchar(255) DEFAULT NULL,
  `posesiones` varchar(255) DEFAULT NULL,
  `seguros` varchar(255) DEFAULT NULL,
  `valores_bursatiles` varchar(255) DEFAULT NULL,
  `cat_unidades_temporales_id` int(11) DEFAULT NULL,
  `institucion` varchar(255) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_inversio_cat_formas_adquisici_7a34b96c_fk_declaraci` (`cat_formas_adquisiciones_id`),
  KEY `declaracion_inversio_cat_monedas_id_f4c04ea9_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_inversio_cat_paises_id_adf125bf_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_inversio_cat_tipos_especifico_f4b4003b_fk_declaraci` (`cat_tipos_especificos_inversiones_id`),
  KEY `declaracion_inversio_cat_tipos_inversione_9d3d2b86_fk_declaraci` (`cat_tipos_inversiones_id`),
  KEY `declaracion_inversio_cat_tipos_operacione_eab54072_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_inversio_declaraciones_id_2688f2af_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_inversio_info_personal_var_id_65a231a6_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_inversio_observaciones_id_37615d8b_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_inversio_cat_tipos_titulares__52574655_fk_declaraci` (`cat_tipos_titulares_id`),
  KEY `declaracion_inversio_cat_tipo_persona_id_868c64e2_fk_declaraci` (`cat_tipo_persona_id`),
  KEY `declaracion_inversio_cat_unidades_tempora_e115056e_fk_declaraci` (`cat_unidades_temporales_id`),
  CONSTRAINT `declaracion_inversio_cat_formas_adquisici_7a34b96c_fk_declaraci` FOREIGN KEY (`cat_formas_adquisiciones_id`) REFERENCES `declaracion_catformasadquisiciones` (`id`),
  CONSTRAINT `declaracion_inversio_cat_monedas_id_f4c04ea9_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_inversio_cat_paises_id_adf125bf_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_inversio_cat_tipo_persona_id_868c64e2_fk_declaraci` FOREIGN KEY (`cat_tipo_persona_id`) REFERENCES `declaracion_cattipopersona` (`id`),
  CONSTRAINT `declaracion_inversio_cat_tipos_especifico_f4b4003b_fk_declaraci` FOREIGN KEY (`cat_tipos_especificos_inversiones_id`) REFERENCES `declaracion_cattiposespecificosinversiones` (`id`),
  CONSTRAINT `declaracion_inversio_cat_tipos_inversione_9d3d2b86_fk_declaraci` FOREIGN KEY (`cat_tipos_inversiones_id`) REFERENCES `declaracion_cattiposinversiones` (`id`),
  CONSTRAINT `declaracion_inversio_cat_tipos_operacione_eab54072_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_inversio_cat_tipos_titulares__52574655_fk_declaraci` FOREIGN KEY (`cat_tipos_titulares_id`) REFERENCES `declaracion_cattipostitulares` (`id`),
  CONSTRAINT `declaracion_inversio_cat_unidades_tempora_e115056e_fk_declaraci` FOREIGN KEY (`cat_unidades_temporales_id`) REFERENCES `declaracion_catunidadestemporales` (`id`),
  CONSTRAINT `declaracion_inversio_declaraciones_id_2688f2af_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_inversio_info_personal_var_id_65a231a6_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_inversio_observaciones_id_37615d8b_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_inversiones`
--

LOCK TABLES `declaracion_inversiones` WRITE;
/*!40000 ALTER TABLE `declaracion_inversiones` DISABLE KEYS */;
INSERT INTO `declaracion_inversiones` VALUES (1,'','','','018001250148','','2020-01-01',3333.00,9.00,5682.00,5.00,'',98.00,'2020-01-31 22:35:30.800760','2020-01-31 22:37:26.615829',3,101,142,3,3,3,1,97,273,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',''),(2,'','','','528221651616546','',NULL,NULL,NULL,2563.00,0.00,'',NULL,'2020-02-07 04:15:45.543423','2020-02-07 04:15:45.543585',NULL,101,142,3,2,2,2,143,324,5,NULL,'SOICIEDADES DE INVERSION','ACCIONES','AFORES','CENTENARIOS','SEGURO DE SEPARACION INDIVIDUALIZADO','ACCIONES Y DERIVADOS',NULL,'',''),(3,'','','','528221651616546','',NULL,NULL,NULL,85900.00,0.00,'',NULL,'2020-02-21 22:21:51.813961','2020-02-21 22:21:51.814030',NULL,101,142,3,3,1,10,174,384,2,NULL,'SOICIEDADES DE INVERSION','ACCIONES','AFORES','CENTENARIOS','SEGURO DE SEPARACION INDIVIDUALIZADO','ACCIONES Y DERIVADOS',NULL,'',''),(4,'','','','528221651616546','',NULL,NULL,NULL,785589.00,0.00,'',NULL,'2020-03-03 17:00:05.613126','2020-03-03 17:00:05.613208',NULL,101,142,1,1,1,13,190,426,10,NULL,'SOICIEDADES DE INVERSION','ACCIONES','AFORES','CENTENARIOS','SEGURO DE SEPARACION INDIVIDUALIZADO','ACCIONES Y DERIVADOS',NULL,'','');
/*!40000 ALTER TABLE `declaracion_inversiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_membresias`
--

DROP TABLE IF EXISTS `declaracion_membresias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_membresias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otras_instituciones` varchar(255) NOT NULL,
  `nombre_institucion` varchar(255) NOT NULL,
  `otro_sector` varchar(255) NOT NULL,
  `puesto_rol` varchar(255) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `pagado` tinyint(1) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otra_naturaleza` varchar(255) DEFAULT NULL,
  `cat_naturaleza_membresia_id` int(11) DEFAULT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `cat_tipos_instituciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `domicilios_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `monto` decimal(13,2) DEFAULT NULL,
  `tipoRelacion_id` int(11) DEFAULT NULL,
  `rfc` varchar(255) NOT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_membresi_cat_naturaleza_membr_e3502c57_fk_declaraci` (`cat_naturaleza_membresia_id`),
  KEY `declaracion_membresi_cat_sectores_industr_f72dfe5d_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_membresi_cat_tipos_institucio_aede5c47_fk_declaraci` (`cat_tipos_instituciones_id`),
  KEY `declaracion_membresi_declaraciones_id_45741598_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_membresi_domicilios_id_f8d850a4_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_membresi_observaciones_id_d0f2470e_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_membresi_tipoRelacion_id_1d92a718_fk_declaraci` (`tipoRelacion_id`),
  KEY `declaracion_membresi_moneda_id_afa0614e_fk_declaraci` (`moneda_id`),
  KEY `declaracion_membresi_cat_tipos_operacione_f5d60af4_fk_declaraci` (`cat_tipos_operaciones_id`),
  CONSTRAINT `declaracion_membresi_cat_naturaleza_membr_e3502c57_fk_declaraci` FOREIGN KEY (`cat_naturaleza_membresia_id`) REFERENCES `declaracion_catnaturalezamembresia` (`id`),
  CONSTRAINT `declaracion_membresi_cat_sectores_industr_f72dfe5d_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_membresi_cat_tipos_institucio_aede5c47_fk_declaraci` FOREIGN KEY (`cat_tipos_instituciones_id`) REFERENCES `declaracion_cattiposinstituciones` (`id`),
  CONSTRAINT `declaracion_membresi_cat_tipos_operacione_f5d60af4_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_membresi_declaraciones_id_45741598_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_membresi_domicilios_id_f8d850a4_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_membresi_moneda_id_afa0614e_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_membresi_observaciones_id_d0f2470e_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_membresi_tipoRelacion_id_1d92a718_fk_declaraci` FOREIGN KEY (`tipoRelacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_membresias`
--

LOCK TABLES `declaracion_membresias` WRITE;
/*!40000 ALTER TABLE `declaracion_membresias` DISABLE KEYS */;
INSERT INTO `declaracion_membresias` VALUES (1,'','MoveW Urbania','','Jefa','2019-11-26',0,'2019-12-10 23:35:36.449777','2020-02-26 22:22:55.537976',NULL,NULL,NULL,1,1,15,29,200.00,NULL,'MOIV010101256',101,4),(2,'','MoveW Urbania','','Jefa','2019-11-26',0,'2019-12-24 19:11:54.647379','2019-12-24 19:12:20.566850',NULL,NULL,24,1,3,65,129,NULL,NULL,'MOIV010101256',101,1),(3,'','MoveW Urbania','','Jefa','2019-12-31',0,'2020-01-13 20:02:19.263387','2020-01-13 20:02:19.263410',NULL,NULL,28,1,4,78,167,5263.00,NULL,'MOIV010101256',107,1),(4,'','SMART TEC','','SUPERVISOR','2017-09-08',0,'2020-02-04 14:24:14.186639','2020-02-04 14:24:14.186664',NULL,NULL,NULL,3,9,106,290,NULL,NULL,'SAAR010101235',NULL,3),(5,'','','','','2020-02-01',1,'2020-02-10 19:57:18.614871','2020-02-10 19:57:18.614902',NULL,NULL,NULL,NULL,2,145,336,NULL,NULL,'',NULL,NULL),(6,'','MoveW Urbania','','Jefa','2019-01-01',1,'2020-02-21 22:26:10.968487','2020-02-21 22:26:10.968542',NULL,NULL,NULL,NULL,10,165,388,NULL,NULL,'MOIV010101256',NULL,NULL);
/*!40000 ALTER TABLE `declaracion_membresias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_mueblesnoregistrables`
--

DROP TABLE IF EXISTS `declaracion_mueblesnoregistrables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_mueblesnoregistrables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otro_bien_mueble` varchar(255) NOT NULL,
  `descripcion_bien` varchar(255) NOT NULL,
  `otro_titular` varchar(255) NOT NULL,
  `otra_forma` varchar(255) NOT NULL,
  `fecha_adquisicion` date DEFAULT NULL,
  `precio_adquisicion` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_formas_adquisiciones_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_tipos_muebles_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_titulares_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `anio` int(11) DEFAULT NULL,
  `cat_motivo_baja_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `domicilios_id` int(11) DEFAULT NULL,
  `forma_pago` varchar(255) NOT NULL,
  `marca` varchar(255) NOT NULL,
  `modelo` varchar(255) NOT NULL,
  `num_serie` varchar(255) NOT NULL,
  `tipo_relacion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_mueblesn_activos_bienes_id_024dcf19_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_mueblesn_cat_formas_adquisici_faa85e62_fk_declaraci` (`cat_formas_adquisiciones_id`),
  KEY `declaracion_mueblesn_cat_monedas_id_e6c992b9_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_mueblesn_cat_tipos_muebles_id_47f1bccc_fk_declaraci` (`cat_tipos_muebles_id`),
  KEY `declaracion_mueblesn_cat_tipos_operacione_b7fe9022_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_mueblesn_cat_tipos_titulares__f8d5dc97_fk_declaraci` (`cat_tipos_titulares_id`),
  KEY `declaracion_mueblesn_declaraciones_id_c619a54e_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_mueblesn_observaciones_id_cc910b1b_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_mueblesn_cat_motivo_baja_id_9dc2f165_fk_declaraci` (`cat_motivo_baja_id`),
  KEY `declaracion_mueblesn_cat_paises_id_3ca85c81_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_mueblesn_domicilios_id_67d417c4_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_mueblesn_tipo_relacion_id_ddb07b73_fk_declaraci` (`tipo_relacion_id`),
  CONSTRAINT `declaracion_mueblesn_activos_bienes_id_024dcf19_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_formas_adquisici_faa85e62_fk_declaraci` FOREIGN KEY (`cat_formas_adquisiciones_id`) REFERENCES `declaracion_catformasadquisiciones` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_monedas_id_e6c992b9_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_motivo_baja_id_9dc2f165_fk_declaraci` FOREIGN KEY (`cat_motivo_baja_id`) REFERENCES `declaracion_catmotivobaja` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_paises_id_3ca85c81_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_tipos_muebles_id_47f1bccc_fk_declaraci` FOREIGN KEY (`cat_tipos_muebles_id`) REFERENCES `declaracion_cattiposmuebles` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_tipos_operacione_b7fe9022_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_tipos_titulares__f8d5dc97_fk_declaraci` FOREIGN KEY (`cat_tipos_titulares_id`) REFERENCES `declaracion_cattipostitulares` (`id`),
  CONSTRAINT `declaracion_mueblesn_declaraciones_id_c619a54e_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_mueblesn_domicilios_id_67d417c4_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_mueblesn_observaciones_id_cc910b1b_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_mueblesn_tipo_relacion_id_ddb07b73_fk_declaraci` FOREIGN KEY (`tipo_relacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_mueblesnoregistrables`
--

LOCK TABLES `declaracion_mueblesnoregistrables` WRITE;
/*!40000 ALTER TABLE `declaracion_mueblesnoregistrables` DISABLE KEYS */;
INSERT INTO `declaracion_mueblesnoregistrables` VALUES (1,'','','','','','2019-11-27',4589.00,'2019-12-31 19:16:53.140011','2020-02-26 22:26:03.708922',1,3,101,1,1,2,1,151,859,NULL,NULL,1,'CREDITO','FORD','SPARK','',NULL),(3,'','','','','','2019-07-18',98000.00,'2020-02-04 01:57:01.225224','2020-02-04 02:48:21.167936',6,3,101,2,2,2,9,287,2016,1,NULL,105,'CRÉDITO','FORD','SPARK','1230',NULL),(4,'','','','','','2013-11-03',12324324.00,'2020-02-04 18:31:41.502434','2020-02-04 18:31:41.502456',10,3,101,2,2,2,10,307,2000,3,NULL,122,'CONTADO','FORD','SPARK','1230',NULL),(5,'','','','','','2020-01-16',58620.00,'2020-02-06 23:01:37.635342','2020-02-06 23:01:37.635406',12,3,101,3,3,3,2,322,2020,3,NULL,135,'CRÉDITO','FORD','SPARK 2020','158963',NULL);
/*!40000 ALTER TABLE `declaracion_mueblesnoregistrables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_nacionalidades`
--

DROP TABLE IF EXISTS `declaracion_nacionalidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_nacionalidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_nacional_cat_paises_id_33c12751_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_nacional_info_personal_var_id_69f68616_fk_declaraci` (`info_personal_var_id`),
  CONSTRAINT `declaracion_nacional_cat_paises_id_33c12751_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_nacional_info_personal_var_id_69f68616_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_nacionalidades`
--

LOCK TABLES `declaracion_nacionalidades` WRITE;
/*!40000 ALTER TABLE `declaracion_nacionalidades` DISABLE KEYS */;
INSERT INTO `declaracion_nacionalidades` VALUES (1,'2019-12-06 18:41:00.224558','2019-12-06 18:41:00.224597',142,1),(2,'2019-12-06 18:47:38.878232','2019-12-06 18:47:38.878272',142,1),(3,'2019-12-06 18:59:58.638183','2019-12-06 18:59:58.638221',142,1),(4,'2019-12-09 16:32:45.138247','2019-12-09 16:32:45.138292',142,3),(5,'2019-12-09 17:51:06.840386','2019-12-09 17:51:06.840424',142,4),(6,'2019-12-09 17:51:32.543009','2019-12-09 17:51:32.543051',142,5),(7,'2019-12-09 17:53:10.932789','2019-12-09 17:53:10.932825',142,3),(8,'2019-12-09 17:54:57.357088','2019-12-09 17:54:57.357127',142,6),(9,'2019-12-09 17:55:14.700975','2019-12-09 17:55:14.701013',142,6),(10,'2019-12-09 17:55:40.670820','2019-12-09 17:55:40.670857',142,3),(11,'2019-12-09 17:57:18.066662','2019-12-09 17:57:18.066705',142,7),(12,'2019-12-09 18:02:34.291525','2019-12-09 18:02:34.291568',142,3),(13,'2019-12-09 18:05:34.602863','2019-12-09 18:05:34.602903',142,3),(14,'2019-12-09 18:05:52.744444','2019-12-09 18:05:52.744502',142,3),(15,'2019-12-09 18:06:18.015456','2019-12-09 18:06:18.015497',142,3),(16,'2019-12-09 18:10:01.202848','2019-12-09 18:10:01.202890',142,3),(17,'2019-12-09 18:11:14.724973','2019-12-09 18:11:14.725006',142,8),(18,'2019-12-09 18:12:38.876329','2019-12-09 18:12:38.876404',142,3),(19,'2019-12-09 18:21:49.909271','2019-12-09 18:21:49.909305',142,3),(20,'2019-12-09 18:23:25.053574','2019-12-09 18:23:25.053617',142,3),(21,'2019-12-09 18:27:34.165091','2019-12-09 18:27:34.165128',142,3),(22,'2019-12-09 18:28:19.143566','2019-12-09 18:28:19.143603',142,3),(23,'2019-12-09 18:29:20.435793','2019-12-09 18:29:20.435836',142,3),(24,'2019-12-09 18:33:03.722497','2019-12-09 18:33:03.722542',142,9),(25,'2019-12-09 18:34:19.954560','2019-12-09 18:34:19.954604',142,3),(26,'2019-12-09 18:35:13.257316','2019-12-09 18:35:13.257354',142,10),(27,'2019-12-09 18:42:59.341563','2019-12-09 18:42:59.341604',142,10),(28,'2019-12-09 18:45:04.524800','2019-12-09 18:45:04.524840',142,10),(29,'2019-12-09 18:45:34.250289','2019-12-09 18:45:34.250328',142,3),(30,'2019-12-09 18:58:31.412329','2019-12-09 18:58:31.412368',142,3),(31,'2019-12-09 18:59:29.845686','2019-12-09 18:59:29.845733',142,3),(32,'2019-12-09 19:12:13.977552','2019-12-09 19:12:13.977593',142,10),(33,'2019-12-09 21:01:34.181198','2019-12-09 21:01:34.181247',142,10),(34,'2019-12-09 21:02:24.633055','2019-12-09 21:02:24.633097',142,10),(35,'2019-12-09 21:04:20.758270','2019-12-09 21:04:20.758312',142,10),(36,'2019-12-09 21:20:20.315839','2019-12-09 21:20:20.315892',142,3),(37,'2019-12-09 21:23:54.622998','2019-12-09 21:23:54.623043',142,3),(38,'2019-12-09 21:25:15.090535','2019-12-09 21:25:15.090571',142,3),(39,'2019-12-09 21:56:25.600140','2019-12-09 21:56:25.600183',142,3),(40,'2019-12-09 22:02:40.329658','2019-12-09 22:02:40.329697',142,3),(41,'2019-12-09 22:03:44.477405','2019-12-09 22:03:44.477444',142,3),(42,'2019-12-09 22:22:36.561158','2019-12-09 22:22:36.561202',142,11),(43,'2019-12-09 22:29:11.187917','2019-12-09 22:29:11.187958',142,11),(44,'2019-12-09 22:29:36.255148','2019-12-09 22:29:36.255189',142,11),(45,'2019-12-09 22:32:56.628979','2019-12-09 22:32:56.629018',142,11),(46,'2019-12-09 22:33:32.836625','2019-12-09 22:33:32.836664',142,11),(47,'2019-12-09 22:36:49.801502','2019-12-09 22:36:49.801544',142,11),(48,'2019-12-09 23:43:21.521862','2019-12-09 23:43:21.521929',142,11),(49,'2019-12-09 23:47:08.619419','2019-12-09 23:47:08.619561',142,11),(50,'2019-12-10 15:24:06.181385','2019-12-10 15:24:06.181512',142,12),(51,'2019-12-10 15:38:41.134175','2019-12-10 15:38:41.134319',142,12),(52,'2019-12-10 16:06:51.243784','2019-12-10 16:06:51.243912',142,12),(53,'2019-12-15 03:15:10.612960','2019-12-15 03:15:10.612989',142,54),(54,'2019-12-15 03:59:50.911152','2019-12-15 03:59:50.911184',142,54),(55,'2019-12-19 20:03:33.069059','2019-12-19 20:03:33.069162',142,12),(56,'2019-12-24 18:41:50.157939','2019-12-24 18:41:50.158049',142,61),(57,'2019-12-24 18:58:53.650393','2019-12-24 18:58:53.650436',142,62),(58,'2019-12-24 19:09:09.807827','2019-12-24 19:09:09.807895',142,62),(59,'2020-01-13 19:36:38.132442','2020-01-13 19:36:38.132475',142,77),(60,'2020-01-13 22:33:56.738899','2020-01-13 22:33:56.738940',142,78),(61,'2020-01-14 15:41:21.868597','2020-01-14 15:41:21.868666',142,79),(62,'2020-01-14 16:10:08.134910','2020-01-14 16:10:08.134976',142,79),(63,'2020-01-15 16:14:43.310157','2020-01-15 16:14:43.310196',142,1),(67,'2020-01-22 21:29:05.006675','2020-01-22 21:29:05.006700',142,84),(68,'2020-01-22 21:38:46.355304','2020-01-22 21:38:46.355331',142,84),(69,'2020-01-27 18:14:15.036242','2020-01-27 18:14:15.036274',142,85),(70,'2020-01-27 20:01:01.675520','2020-01-27 20:01:01.675581',142,86),(71,'2020-01-27 20:06:24.262995','2020-01-27 20:06:24.263027',142,87),(72,'2020-01-27 20:08:16.376810','2020-01-27 20:08:16.376837',142,87),(73,'2020-01-27 20:31:52.913147','2020-01-27 20:31:52.913177',142,87),(74,'2020-01-27 20:45:13.546502','2020-01-27 20:45:13.546531',142,87),(75,'2020-01-27 20:51:29.489265','2020-01-27 20:51:29.489293',142,87),(76,'2020-01-27 21:29:30.030919','2020-01-27 21:29:30.030949',142,87),(77,'2020-01-27 21:51:47.158929','2020-01-27 21:51:47.158960',142,87),(78,'2020-01-28 03:52:18.715470','2020-01-28 03:52:18.715532',142,87),(79,'2020-01-28 04:03:36.544842','2020-01-28 04:03:36.544877',142,87),(80,'2020-01-28 04:11:52.376032','2020-01-28 04:11:52.376073',142,88),(81,'2020-01-28 15:30:35.937667','2020-01-28 15:30:35.937697',142,87),(82,'2020-01-28 16:23:34.749630','2020-01-28 16:23:34.749666',142,88),(83,'2020-01-28 16:25:24.760059','2020-01-28 16:25:24.760086',142,87),(84,'2020-01-28 16:28:15.149090','2020-01-28 16:28:15.149128',142,89),(85,'2020-01-28 19:28:23.922913','2020-01-28 19:28:23.922945',142,12),(86,'2020-01-29 18:29:37.711758','2020-01-29 18:29:37.711796',142,1),(87,'2020-01-29 18:54:34.584224','2020-01-29 18:54:34.584288',142,1),(88,'2020-01-29 18:56:35.031709','2020-01-29 18:56:35.031741',142,1),(89,'2020-01-29 18:58:59.973563','2020-01-29 18:58:59.973599',142,1),(90,'2020-01-29 19:16:29.447892','2020-01-29 19:16:29.447928',142,1),(91,'2020-01-29 19:17:45.284455','2020-01-29 19:17:45.284491',142,1),(92,'2020-01-29 20:00:44.467310','2020-01-29 20:00:44.467340',142,1),(93,'2020-01-29 20:26:44.819481','2020-01-29 20:26:44.819519',142,1),(94,'2020-01-29 20:34:25.854129','2020-01-29 20:34:25.854160',142,1),(95,'2020-01-29 20:35:36.129457','2020-01-29 20:35:36.129492',142,1),(96,'2020-01-29 22:02:16.757221','2020-01-29 22:02:16.757283',142,1),(97,'2020-01-29 22:10:19.735808','2020-01-29 22:10:19.735892',142,1),(98,'2020-01-30 19:33:27.314340','2020-01-30 19:33:27.314379',142,1),(99,'2020-01-30 19:39:44.338369','2020-01-30 19:39:44.338408',142,90),(100,'2020-01-30 19:43:23.249569','2020-01-30 19:43:23.249603',142,12),(101,'2020-01-30 20:15:38.512845','2020-01-30 20:15:38.512883',142,12),(102,'2020-01-30 23:55:46.446111','2020-01-30 23:55:46.446191',142,12),(103,'2020-02-04 00:28:15.052828','2020-02-04 00:28:15.052867',142,82),(104,'2020-02-06 22:47:54.477438','2020-02-06 22:47:54.477501',142,54),(105,'2020-02-10 18:09:04.001381','2020-02-10 18:09:04.001415',142,54),(106,'2020-02-10 18:10:39.324719','2020-02-10 18:10:39.324754',142,54),(107,'2020-02-17 15:21:11.577765','2020-02-17 15:21:11.577801',142,155),(108,'2020-02-19 20:34:51.943706','2020-02-19 20:34:51.943770',142,84),(109,'2020-02-19 20:45:48.336420','2020-02-19 20:45:48.336452',142,84),(110,'2020-02-27 23:06:47.796941','2020-02-27 23:06:47.797005',110,84),(111,'2020-02-27 23:06:47.824019','2020-02-27 23:06:47.824086',142,84),(112,'2020-02-28 16:01:52.648027','2020-02-28 16:01:52.648061',142,187),(113,'2020-03-03 16:38:10.934559','2020-03-03 16:38:10.934591',142,189);
/*!40000 ALTER TABLE `declaracion_nacionalidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_observaciones`
--

DROP TABLE IF EXISTS `declaracion_observaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_observaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `observacion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=433 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_observaciones`
--

LOCK TABLES `declaracion_observaciones` WRITE;
/*!40000 ALTER TABLE `declaracion_observaciones` DISABLE KEYS */;
INSERT INTO `declaracion_observaciones` VALUES (1,'','2019-12-06 18:30:02.050211','2019-12-06 18:30:02.050251'),(2,'TEST','2019-12-06 18:33:37.939901','2020-01-30 19:33:27.259447'),(3,'','2019-12-06 18:33:37.982781','2019-12-06 18:33:37.982819'),(4,'','2019-12-06 18:33:37.999030','2019-12-06 18:33:37.999071'),(5,'','2019-12-06 18:33:38.008301','2019-12-06 18:33:38.008339'),(6,'','2019-12-06 18:33:38.020437','2019-12-06 18:33:38.020474'),(7,'','2019-12-06 18:33:38.027849','2019-12-06 18:33:38.027884'),(8,'Esta es una observación de datos curriculares','2019-12-06 19:07:03.249887','2020-01-16 22:23:39.588603'),(9,'Este es un comentario del encargo actual','2019-12-06 19:19:15.524976','2020-02-26 22:25:11.153558'),(10,'Este es un comentario de experencia laboral','2019-12-09 15:36:20.908195','2020-02-01 21:27:45.370448'),(11,'Este es un comentario de dependientes economicos','2019-12-09 16:28:40.369968','2019-12-09 16:28:40.370015'),(12,'1 dependiente economico','2019-12-09 16:32:45.104631','2019-12-09 22:03:44.462289'),(13,'Apoyos observaciones','2019-12-09 16:32:45.335663','2020-02-26 22:23:01.413358'),(14,'1 dependiente economico','2019-12-09 17:51:06.826131','2019-12-09 17:51:06.826172'),(15,'1 dependiente economico','2019-12-09 17:51:32.523224','2019-12-09 17:51:32.523270'),(16,'2 Segundo dependiente economico','2019-12-09 17:54:57.342054','2019-12-09 17:55:14.685312'),(17,'','2019-12-09 17:54:57.363672','2019-12-09 17:55:14.717263'),(18,'Test','2019-12-09 17:57:18.047364','2019-12-09 17:57:18.047404'),(19,'','2019-12-09 17:57:18.080340','2019-12-09 17:57:18.080383'),(20,'3 observación de dependientes','2019-12-09 18:11:14.717805','2019-12-09 18:11:14.717847'),(21,'','2019-12-09 18:30:06.291044','2019-12-09 18:30:06.291086'),(22,'','2019-12-09 18:31:34.526750','2019-12-09 18:31:34.526791'),(23,'ñ','2019-12-09 18:33:03.693389','2019-12-09 18:33:03.693434'),(24,'ESt','2019-12-09 18:35:13.247895','2019-12-09 21:04:20.745863'),(25,'Esta es la observación del modulo de información personal pero-----------------Vamos a ver','2019-12-09 19:12:46.847039','2019-12-24 17:04:20.380801'),(26,'4 observación de dependientes economicos','2019-12-09 22:22:36.547170','2019-12-09 23:47:08.606845'),(27,'Vamos cambiando la observación','2019-12-10 15:24:06.017939','2020-02-03 02:33:18.389137'),(28,'Este es un comentario de socios comerciales','2019-12-10 18:09:21.820539','2020-02-26 22:22:47.527924'),(29,'Membresias que se cambio por toma de desiciones','2019-12-10 23:35:36.440615','2020-02-26 22:22:55.521885'),(30,'Representación activa!','2019-12-11 19:11:05.978983','2019-12-11 20:00:18.992060'),(31,'Representación activa','2019-12-11 22:13:52.128754','2019-12-11 22:13:52.128818'),(32,'Represen A -con domicilio','2019-12-11 22:18:58.813754','2020-02-26 22:23:08.676945'),(33,'Cliente principal observaciones','2019-12-11 22:46:53.134196','2019-12-11 23:28:38.674774'),(34,'kiuo','2019-12-11 23:30:40.548152','2020-01-22 17:39:37.908643'),(35,'Beneficios-gratuitos observaciones-MODIFICADO','2019-12-12 19:07:56.034775','2020-02-19 17:38:43.283089'),(36,'Esta es una observación del modulo de intereses','2019-12-12 21:54:13.146062','2019-12-12 21:54:13.146141'),(37,'Ingresos - Esta es una observación','2019-12-12 22:17:59.075886','2019-12-13 04:55:15.508656'),(38,'test','2019-12-13 04:58:15.154498','2019-12-13 05:01:37.409885'),(39,'Este es otra observación','2019-12-13 15:37:12.991151','2019-12-23 19:50:50.966414'),(40,'tdrt','2019-12-13 16:13:55.387202','2019-12-14 04:52:30.370266'),(41,'testtest','2019-12-13 16:58:52.895323','2019-12-14 04:18:58.759856'),(42,'Nueva observación','2019-12-13 17:01:02.442079','2019-12-14 04:30:35.065350'),(43,'','2019-12-13 20:53:52.768672','2019-12-13 20:53:52.768712'),(44,'','2019-12-13 21:01:39.587127','2019-12-13 21:01:39.587166'),(45,'Nuevo comentario','2019-12-13 21:49:36.313466','2019-12-13 21:49:36.313539'),(46,'desde','2019-12-13 22:25:08.654815','2019-12-14 04:23:06.427368'),(47,'','2019-12-14 03:58:15.249886','2019-12-14 03:58:15.249953'),(48,'Test de financiera','2019-12-14 04:20:36.103768','2019-12-14 04:20:36.103838'),(49,'ulimps','2019-12-14 04:52:01.247912','2019-12-14 05:03:32.155379'),(50,'Esta es la ultima observacion de ingresoso','2019-12-14 05:03:02.524172','2019-12-14 05:03:02.524239'),(51,'l','2019-12-14 05:03:47.886601','2019-12-14 05:03:47.886724'),(52,'tesrt','2019-12-14 18:35:44.581165','2019-12-14 18:35:44.581206'),(53,'tesrt','2019-12-14 19:18:06.741731','2019-12-14 19:18:06.741774'),(54,'fdg','2019-12-14 19:19:03.808106','2019-12-14 19:19:03.808158'),(55,'fdhbdh','2019-12-14 19:19:16.986111','2019-12-14 19:19:16.986154'),(56,'ljn','2019-12-14 19:20:29.472273','2019-12-14 19:20:29.472318'),(57,'khb','2019-12-14 19:23:19.396768','2019-12-14 19:23:19.396854'),(58,'ljn','2019-12-14 19:34:17.965353','2019-12-14 19:34:17.965392'),(59,'Nueva observación','2019-12-14 19:37:14.517097','2019-12-14 19:37:14.517143'),(60,'ljnl','2019-12-14 19:38:37.840826','2019-12-14 19:38:37.840870'),(61,'srhsfh','2019-12-14 19:46:38.696320','2019-12-14 19:46:38.696449'),(62,'sdfdsfsdf','2019-12-14 20:22:18.654837','2019-12-14 20:22:18.654875'),(63,'Nuestro obligación','2019-12-14 20:24:38.816268','2019-12-14 20:24:38.816307'),(64,'nueva','2019-12-14 21:06:59.742679','2019-12-14 21:06:59.742710'),(65,'SDSD78','2019-12-14 21:07:50.591540','2019-12-14 21:07:50.591570'),(66,'nUEVA','2019-12-14 21:09:07.595463','2019-12-14 21:09:07.595490'),(67,'nUEVA SE','2019-12-14 21:10:06.039371','2019-12-14 21:10:06.039405'),(68,'NUNCA','2019-12-14 21:11:42.743690','2019-12-14 21:11:42.743718'),(69,'ZFVGZDG','2019-12-14 21:15:49.033930','2019-12-14 21:15:49.033998'),(70,'FDAADSF','2019-12-14 21:18:28.765511','2019-12-14 21:18:28.765542'),(71,'','2019-12-14 21:25:14.602561','2019-12-14 21:25:14.602591'),(72,'sysysyds','2019-12-14 21:25:37.672077','2019-12-14 21:25:37.672114'),(73,'sdsds','2019-12-14 22:14:14.803314','2019-12-14 22:14:14.803343'),(74,'x','2019-12-14 22:15:39.095300','2019-12-14 22:15:39.095331'),(75,'dx','2019-12-14 22:54:41.131135','2019-12-14 22:54:41.131183'),(76,'Este es un mensaje de prueba para el guardado de tipo de inmuble','2019-12-14 22:54:59.604217','2020-01-09 18:22:24.749993'),(77,'Ninguna','2019-12-15 00:28:00.183499','2019-12-24 17:33:16.385049'),(78,'Nueva','2019-12-15 01:04:19.245071','2019-12-23 20:25:32.028051'),(79,'','2019-12-15 01:31:16.470548','2019-12-15 01:31:16.470585'),(80,'Primer comentario de mi segundo usario','2019-12-15 03:15:10.571021','2020-02-10 18:10:39.308311'),(81,'','2019-12-15 03:15:10.641988','2019-12-15 03:15:10.642020'),(82,'','2019-12-15 03:15:10.657060','2019-12-15 03:15:10.657093'),(83,'','2019-12-15 03:15:10.666320','2019-12-15 03:15:10.666357'),(84,'','2019-12-15 03:15:10.678497','2019-12-15 03:15:10.678527'),(85,'','2019-12-15 03:15:10.689045','2019-12-15 03:15:10.689080'),(86,'Observacion','2019-12-15 04:02:03.640152','2020-02-10 18:13:24.338720'),(87,'','2019-12-15 23:04:59.232778','2019-12-15 23:04:59.232817'),(88,'','2019-12-16 01:48:58.555279','2019-12-16 01:48:58.555344'),(89,'','2019-12-17 16:53:44.084480','2019-12-17 16:53:44.084589'),(90,'','2019-12-17 17:16:03.516896','2019-12-17 17:16:03.516932'),(91,'','2019-12-17 17:57:13.051761','2019-12-17 17:57:13.051798'),(92,'','2019-12-17 18:08:18.730108','2019-12-17 18:08:18.730145'),(93,'','2019-12-17 18:11:13.699868','2019-12-17 18:11:13.699903'),(94,'','2019-12-17 18:50:47.548225','2019-12-17 18:50:47.548258'),(95,'','2019-12-17 19:46:21.018452','2019-12-17 19:46:21.018489'),(96,'','2019-12-17 21:07:29.050955','2019-12-17 21:07:29.050992'),(97,'','2019-12-18 15:55:40.711771','2019-12-18 15:55:40.711806'),(98,'','2019-12-18 16:53:37.456417','2019-12-18 16:53:37.456471'),(99,'','2019-12-19 15:47:12.946198','2019-12-19 15:47:12.946237'),(100,'Esta es una observación para la sección de actividad industrial, comercial y empresarial','2019-12-20 16:04:24.775694','2019-12-20 16:04:24.775771'),(101,'Esta es una observación de actividad financiera','2019-12-20 16:33:41.396579','2019-12-20 16:47:30.942039'),(102,'t','2019-12-20 18:22:13.533380','2019-12-20 18:22:13.533424'),(103,'Total de ingresos comentarios','2019-12-20 18:24:21.917040','2019-12-20 18:24:21.917081'),(104,'','2019-12-20 18:33:36.965754','2019-12-20 18:33:36.965794'),(105,'','2019-12-20 18:34:21.883041','2019-12-20 18:34:21.883076'),(106,'','2019-12-20 18:41:21.831852','2019-12-20 18:41:21.831894'),(107,'','2019-12-20 18:43:39.611191','2019-12-20 18:43:39.611235'),(108,'','2019-12-20 18:47:10.296091','2019-12-20 18:47:10.296128'),(109,'','2019-12-20 18:48:28.529393','2019-12-20 18:48:28.529467'),(110,'','2019-12-23 16:52:46.314991','2019-12-23 16:52:46.315072'),(111,'TERER','2019-12-23 18:36:00.082003','2019-12-23 18:36:00.082043'),(112,'AERGAFDSG','2019-12-23 19:51:46.974768','2019-12-23 19:51:46.974811'),(113,'Nuevo trabajo','2019-12-23 19:52:18.195365','2019-12-23 19:52:18.195405'),(114,'SGFDG','2019-12-23 19:53:08.736805','2019-12-23 19:53:08.736847'),(115,'Esta es otra obervacion','2019-12-23 20:25:59.580615','2019-12-24 17:54:51.321684'),(116,'','2019-12-24 16:35:36.884290','2019-12-24 16:35:36.884335'),(117,'','2019-12-24 18:39:15.455395','2019-12-24 18:39:15.455432'),(118,'Primer comentario de esta declaracón','2019-12-24 18:41:50.132475','2019-12-24 18:41:50.132536'),(119,'','2019-12-24 18:41:50.193546','2019-12-24 18:41:50.193587'),(120,'','2019-12-24 18:41:50.214660','2019-12-24 18:41:50.214707'),(121,'','2019-12-24 18:41:50.234538','2019-12-24 18:41:50.234589'),(122,'','2019-12-24 18:41:50.249779','2019-12-24 18:41:50.249823'),(123,'','2019-12-24 18:41:50.270815','2019-12-24 18:41:50.270856'),(124,'Nueva observacion','2019-12-24 18:47:51.944692','2019-12-24 18:47:51.944748'),(125,'tesr','2019-12-24 18:48:33.481807','2019-12-24 18:48:33.481860'),(126,'Otra observacion de un nuevo usuario','2019-12-24 18:56:23.378401','2019-12-24 18:56:23.378520'),(127,'Observacion de dependientes economicos','2019-12-24 18:58:53.636418','2019-12-24 19:09:09.786416'),(128,'Nueva observación final de esta sección','2019-12-24 19:10:03.871070','2019-12-24 19:10:03.871123'),(129,'Declaraciones observaciones','2019-12-24 19:11:54.626736','2019-12-24 19:12:20.550571'),(130,'Esta es una prueba de esta sección de apoyos','2019-12-24 19:13:02.804378','2019-12-24 19:13:02.804427'),(131,'Observación de representación activa','2019-12-24 19:23:19.440184','2019-12-24 19:23:19.440234'),(132,'Esta es una observación de intereses','2019-12-24 19:24:36.107222','2019-12-24 19:24:36.107276'),(133,'Cliente principales observaciones','2019-12-24 19:26:11.648023','2019-12-24 19:26:11.648082'),(134,'Observacion de beneficios gratuitos','2019-12-24 19:26:46.323006','2019-12-24 19:26:46.323056'),(135,'Ultima observacion de la seccion de intereses','2019-12-24 19:27:05.970342','2019-12-24 19:27:05.970396'),(136,'Esta es otra observación de INGRESOS PUBLICOS','2019-12-24 19:28:52.518494','2019-12-24 19:28:52.518544'),(137,'Observacion de ingresos de tipo otros empleos','2019-12-24 19:33:59.298673','2019-12-24 19:33:59.298727'),(138,'tet','2019-12-24 19:34:15.534185','2019-12-24 19:34:15.534238'),(139,'Observacion de ingresos de tipo industrial','2019-12-24 19:35:07.642296','2019-12-24 19:35:07.642352'),(140,'Actividad financiera observaciones','2019-12-24 19:37:28.686772','2019-12-24 19:37:28.686818'),(141,'conyugue dependiente','2019-12-24 19:38:03.386568','2019-12-24 19:38:03.386619'),(142,'Ingresos netos del declarante','2019-12-24 19:38:33.301051','2019-12-24 19:38:33.301149'),(143,'Otro comentario de ingresos totales','2019-12-24 19:38:54.118257','2019-12-24 19:38:54.118316'),(144,'Enajenación de bienes - Observaciones','2019-12-24 19:45:35.875127','2019-12-24 19:45:35.875209'),(145,'Observación de otros ingresos!','2019-12-24 19:56:30.904470','2019-12-24 19:56:30.904529'),(146,'Ultima obsrvacion','2019-12-24 19:56:46.583244','2019-12-24 19:56:46.583298'),(147,'','2019-12-30 16:18:34.632549','2019-12-30 16:18:34.632628'),(148,'','2019-12-30 21:51:43.381239','2019-12-30 21:51:43.381329'),(149,'','2019-12-30 22:26:46.146850','2019-12-30 22:26:46.146883'),(150,'','2019-12-30 22:38:22.875579','2019-12-30 22:38:22.875611'),(151,'AÑSDFKMLKASMDF','2019-12-31 19:16:53.019134','2020-02-26 22:26:03.636433'),(152,'','2020-01-07 16:34:21.121016','2020-01-07 16:34:21.121091'),(153,'Esta es una prueba de guardado de las fechas en sueldos públicos del año anterior','2020-01-09 16:29:41.400283','2020-01-09 16:29:41.400325'),(154,'Esta es una observación para pobrar el guardado delas fechas!','2020-01-09 16:40:17.603669','2020-01-09 16:49:12.128920'),(155,'SFD','2020-01-09 16:49:35.769028','2020-01-09 16:50:23.691262'),(156,'HKBVB','2020-01-09 16:50:54.814244','2020-01-09 16:50:54.814310'),(157,'testet','2020-01-13 18:35:54.470444','2020-01-13 18:47:20.783764'),(158,'','2020-01-13 19:33:48.741508','2020-01-13 19:33:48.741543'),(159,'','2020-01-13 19:34:11.484825','2020-01-13 19:34:11.484856'),(160,'fsdfsdf','2020-01-13 19:36:38.098067','2020-01-13 19:36:38.098099'),(161,'','2020-01-13 19:36:38.151901','2020-01-13 19:36:38.151932'),(162,'','2020-01-13 19:36:38.161678','2020-01-13 19:36:38.161710'),(163,'','2020-01-13 19:36:38.171313','2020-01-13 19:36:38.171351'),(164,'','2020-01-13 19:36:38.265370','2020-01-13 19:36:38.265402'),(165,'','2020-01-13 19:36:38.274792','2020-01-13 19:36:38.274825'),(166,'','2020-01-13 19:36:38.282571','2020-01-13 19:36:38.282602'),(167,'te','2020-01-13 20:02:19.258291','2020-01-13 20:02:19.258319'),(168,'','2020-01-13 22:32:26.276326','2020-01-13 22:32:26.276365'),(169,'Esta es una observación','2020-01-13 22:33:56.707290','2020-01-13 22:33:56.707323'),(170,'','2020-01-13 22:33:56.761147','2020-01-13 22:33:56.761223'),(171,'','2020-01-13 22:33:56.780168','2020-01-13 22:33:56.780210'),(172,'','2020-01-13 22:33:56.793241','2020-01-13 22:33:56.793288'),(173,'','2020-01-13 22:33:56.815419','2020-01-13 22:33:56.815572'),(174,'','2020-01-13 22:33:56.833367','2020-01-13 22:33:56.833432'),(175,'','2020-01-13 22:33:56.858737','2020-01-13 22:33:56.858797'),(176,'','2020-01-13 22:53:55.395670','2020-01-13 22:53:55.395709'),(177,'','2020-01-14 15:38:19.853961','2020-01-14 15:38:19.853995'),(178,'Esta es una prueba','2020-01-14 15:41:21.839326','2020-01-14 16:10:08.097383'),(179,'','2020-01-14 15:41:21.916547','2020-01-14 15:41:21.916615'),(180,'','2020-01-14 15:41:21.951645','2020-01-14 15:41:21.951730'),(181,'','2020-01-14 15:41:21.975153','2020-01-14 15:41:21.975256'),(182,'','2020-01-14 15:41:21.998689','2020-01-14 15:41:21.998824'),(183,'','2020-01-14 15:41:22.020030','2020-01-14 15:41:22.020097'),(184,'','2020-01-14 15:41:22.044460','2020-01-14 15:41:22.044527'),(185,'','2020-01-14 16:48:54.709566','2020-01-14 16:48:54.709633'),(186,'','2020-01-14 16:51:50.748622','2020-01-14 16:51:50.748692'),(187,'','2020-01-14 17:02:55.995506','2020-01-14 17:02:55.995541'),(188,'','2020-01-14 22:57:17.445402','2020-01-14 22:57:17.445436'),(189,'','2020-01-15 16:05:55.156985','2020-01-15 16:05:55.157080'),(190,'','2020-01-15 16:14:04.921137','2020-01-15 16:14:04.921185'),(191,'Este es un comentario','2020-01-16 22:24:54.375624','2020-02-01 20:43:18.783505'),(192,'','2020-01-22 17:47:53.980905','2020-01-22 17:47:53.980935'),(193,'SAFSADF','2020-01-22 17:48:46.195412','2020-01-22 17:48:46.195484'),(194,'','2020-01-22 17:48:46.260372','2020-01-22 17:48:46.260406'),(195,'','2020-01-22 17:48:46.283472','2020-01-22 17:48:46.283506'),(196,'','2020-01-22 17:48:46.289796','2020-01-22 17:48:46.289826'),(197,'','2020-01-22 17:48:46.313234','2020-01-22 17:48:46.313265'),(198,'','2020-01-22 17:48:46.320674','2020-01-22 17:48:46.320704'),(199,'','2020-01-22 17:48:46.331026','2020-01-22 17:48:46.331059'),(200,'','2020-01-22 18:00:25.495224','2020-01-22 18:00:25.495259'),(201,'','2020-01-22 18:06:26.508141','2020-01-22 18:06:26.508169'),(202,'SDAFSADF','2020-01-22 18:07:06.439474','2020-01-22 18:07:06.439502'),(203,'','2020-01-22 18:07:06.474176','2020-01-22 18:07:06.474208'),(204,'','2020-01-22 18:07:06.528483','2020-01-22 18:07:06.528513'),(205,'','2020-01-22 18:07:06.541775','2020-01-22 18:07:06.541807'),(206,'','2020-01-22 18:07:06.547979','2020-01-22 18:07:06.548007'),(207,'','2020-01-22 18:07:06.556949','2020-01-22 18:07:06.556979'),(208,'','2020-01-22 18:07:06.565572','2020-01-22 18:07:06.565636'),(209,'','2020-01-22 18:28:31.673302','2020-01-22 18:28:31.673343'),(210,'esta','2020-01-22 18:29:18.675570','2020-02-04 00:28:15.012643'),(211,'','2020-01-22 18:29:18.714022','2020-01-22 18:29:18.714063'),(212,'','2020-01-22 18:29:18.725168','2020-01-22 18:29:18.725208'),(213,'','2020-01-22 18:29:18.742770','2020-01-22 18:29:18.742848'),(214,'','2020-01-22 18:29:18.751873','2020-01-22 18:29:18.751912'),(215,'','2020-01-22 18:29:18.870218','2020-01-22 18:29:18.870259'),(216,'','2020-01-22 18:29:18.880503','2020-01-22 18:29:18.880566'),(217,'','2020-01-22 18:32:58.075888','2020-01-22 18:32:58.075928'),(218,'','2020-01-22 18:52:19.421082','2020-01-22 18:52:19.421125'),(219,'','2020-01-22 18:55:20.270737','2020-01-22 18:55:20.270777'),(220,'','2020-01-22 19:01:22.134700','2020-01-22 19:01:22.134738'),(221,'','2020-01-22 19:01:35.816502','2020-01-22 19:04:15.949048'),(222,'','2020-01-22 21:25:59.548568','2020-01-22 21:25:59.548598'),(223,'','2020-01-22 21:28:17.098419','2020-01-22 21:28:17.098449'),(224,'DSD','2020-01-22 21:29:04.989783','2020-02-27 23:06:47.754062'),(225,'','2020-01-22 21:29:05.169227','2020-01-22 21:29:05.169257'),(226,'','2020-01-22 21:29:05.177941','2020-01-22 21:29:05.177973'),(227,'','2020-01-22 21:29:05.189850','2020-01-22 21:29:05.189880'),(228,'','2020-01-22 21:29:05.206961','2020-01-22 21:29:05.206993'),(229,'','2020-01-22 21:29:05.214126','2020-01-22 21:29:05.214156'),(230,'','2020-01-22 21:29:05.239603','2020-01-22 21:29:05.239635'),(231,'','2020-01-24 17:41:31.460036','2020-01-24 17:41:31.460108'),(232,'Esta es una observación de datos pareja','2020-01-27 18:14:14.897772','2020-01-27 18:14:14.897805'),(233,'Esta es una observacion','2020-01-27 18:59:07.774689','2020-01-27 19:28:57.373007'),(234,'Esta es una prueba','2020-01-27 20:01:01.669142','2020-01-27 20:01:01.669174'),(235,'Esta es una prueba','2020-01-27 20:06:24.247860','2020-01-28 16:25:24.731167'),(236,'Esta es una nueva relación','2020-01-28 04:11:52.355761','2020-02-20 00:04:51.296916'),(237,'Esta es una observación es de dependientes economicos','2020-01-28 16:28:15.140739','2020-01-28 16:28:15.140772'),(238,'','2020-01-29 17:09:43.539975','2020-01-29 17:09:43.540013'),(239,'','2020-01-29 17:18:33.909310','2020-01-29 17:18:33.909348'),(240,'Esta es una prueba de pareja NUEVO -MODIFICADO','2020-01-30 19:39:44.209081','2020-02-03 18:53:37.633200'),(241,'','2020-01-30 19:52:48.735578','2020-01-30 19:52:48.735616'),(242,'Esta es una prueba','2020-01-30 19:55:16.514446','2020-01-30 19:55:16.514484'),(243,'Esta es una prueba','2020-01-30 19:59:09.230285','2020-01-30 19:59:09.230325'),(244,'Esta es una prueba','2020-01-30 20:02:42.872520','2020-01-30 20:02:42.872558'),(245,'','2020-01-30 20:08:39.555648','2020-01-30 20:08:39.555689'),(246,'Este es un comnetario','2020-01-30 20:12:02.203764','2020-01-30 20:12:02.203805'),(247,'','2020-01-30 20:22:32.586463','2020-01-30 20:22:32.586501'),(248,'','2020-01-30 20:23:26.941432','2020-01-30 20:23:26.941470'),(249,'Esta es una prueba de ingresos declaración','2020-01-30 20:25:11.978076','2020-01-30 20:25:11.978114'),(250,'Ingresos declaracion','2020-01-30 20:29:09.833084','2020-01-30 20:29:09.833181'),(251,'Ingresos declaracion','2020-01-30 20:30:05.686419','2020-01-30 20:30:05.686458'),(252,'dsaf','2020-01-30 20:30:32.852997','2020-01-30 20:30:32.853038'),(253,'Observaciónde servidor publioco','2020-01-30 20:32:03.096154','2020-02-03 02:34:53.381072'),(254,'','2020-01-30 20:48:22.638797','2020-01-30 20:48:22.638832'),(255,'Test servidor publico','2020-01-30 23:43:06.657548','2020-02-04 15:58:28.241078'),(256,'','2020-01-31 00:38:24.826994','2020-01-31 00:38:24.827063'),(257,'','2020-01-31 05:26:56.645415','2020-01-31 05:26:56.645453'),(258,'','2020-01-31 05:41:23.850479','2020-01-31 05:41:23.850509'),(259,'','2020-01-31 15:28:52.725620','2020-01-31 15:28:52.725692'),(260,'','2020-01-31 15:28:52.749390','2020-01-31 15:28:52.749460'),(261,'','2020-01-31 15:28:52.772588','2020-01-31 15:28:52.772655'),(262,'','2020-01-31 15:28:52.794969','2020-01-31 15:28:52.795038'),(263,'','2020-01-31 19:08:21.250798','2020-01-31 19:08:21.250841'),(264,'','2020-01-31 19:13:06.372530','2020-01-31 19:13:06.372569'),(265,'','2020-01-31 19:29:48.408775','2020-01-31 19:29:48.408818'),(266,'','2020-01-31 19:29:48.430676','2020-01-31 19:29:48.430714'),(267,'','2020-01-31 19:29:48.452637','2020-01-31 19:29:48.452675'),(268,'','2020-01-31 19:29:48.469222','2020-01-31 19:29:48.469266'),(269,'','2020-01-31 19:33:06.177195','2020-01-31 19:33:06.177236'),(270,'Esta es una','2020-01-31 22:05:00.150442','2020-01-31 22:05:00.150481'),(271,'Este es otro comentario','2020-01-31 22:11:50.093243','2020-01-31 22:33:23.941257'),(272,'Esta es una prueba','2020-01-31 22:23:11.837180','2020-02-26 22:25:32.050293'),(273,'Esta es una prueba de activos en inversiones','2020-01-31 22:35:30.793904','2020-01-31 22:37:26.601950'),(274,'','2020-02-03 00:03:02.390618','2020-02-03 00:03:02.390658'),(275,'','2020-02-03 00:03:16.306119','2020-02-03 00:03:16.306159'),(276,'Esta es una observacion de clientes princpales','2020-02-03 23:30:35.547906','2020-02-26 22:23:16.177160'),(277,'ESTA ES UNA OBSERVACION DE FIDEICOMISOS','2020-02-04 00:25:22.365087','2020-02-04 00:27:25.094583'),(278,'ESTA ES UNA OBSERVACION DE OTRA DECLARACION DE DATOS CURRICULARES','2020-02-04 00:32:59.577711','2020-02-04 00:32:59.577774'),(279,'OBSERVACION DEL ENCARGO ACTUAL 25303C','2020-02-04 00:40:00.715426','2020-02-04 00:40:00.715478'),(280,'OBSERVACIONES DE EXPERIENCIA LABORAL 5303C','2020-02-04 00:44:29.563654','2020-02-04 00:46:46.385907'),(281,'ESTA ES UNA OBSERVACION DE LA PAREJA','2020-02-04 00:54:16.691554','2020-02-04 06:01:50.837833'),(282,'DEPENDIENTES OBSERVACIONES','2020-02-04 00:56:46.859203','2020-02-04 01:01:12.956466'),(283,'','2020-02-04 01:08:20.022070','2020-02-04 01:08:20.022107'),(284,'ESTA ES UNA PRUEBA INGRESOS SERVIDOR PUBLICO','2020-02-04 01:30:47.870522','2020-02-04 01:30:47.870560'),(285,'OBSERVACION BIENES INMUEBLES','2020-02-04 01:38:25.455246','2020-02-04 01:38:25.455311'),(286,'OBSERVACIONES','2020-02-04 01:55:39.770001','2020-02-04 01:55:39.770041'),(287,'DFGSDFG','2020-02-04 01:57:01.216583','2020-02-04 02:48:19.511389'),(288,'OBSERVACIONES SOCIO COMERCIAL','2020-02-04 05:46:08.148523','2020-02-04 05:46:08.148560'),(289,'asf','2020-02-04 05:50:24.887308','2020-02-04 05:50:24.887344'),(290,'ESTA ES UNA OBSERVACION DE PARTICIPACION','2020-02-04 14:24:14.182631','2020-02-04 14:24:14.182662'),(291,'REPRESENTACION OBSERVACION','2020-02-04 14:26:19.770662','2020-02-04 14:26:19.770692'),(292,'FIDEICOMISOS OBSERVACIONES LISA','2020-02-04 14:29:29.728763','2020-02-04 15:05:23.962484'),(293,'','2020-02-04 15:38:38.059785','2020-02-04 15:38:38.059852'),(294,'','2020-02-04 15:57:40.139111','2020-02-04 15:57:40.139141'),(295,'','2020-02-04 16:11:59.839020','2020-02-04 16:11:59.839089'),(296,'','2020-02-04 16:12:18.434897','2020-02-04 16:12:18.434989'),(297,'','2020-02-04 16:12:36.337316','2020-02-21 21:43:20.776446'),(298,'','2020-02-04 16:12:46.700886','2020-02-04 16:12:46.700955'),(299,'','2020-02-04 16:15:57.735542','2020-02-04 16:15:57.735612'),(300,'','2020-02-04 16:25:03.190460','2020-02-04 16:25:03.190525'),(301,'','2020-02-04 16:27:59.862728','2020-02-04 16:27:59.862759'),(302,'','2020-02-04 16:38:56.906504','2020-02-04 16:38:56.906533'),(303,'','2020-02-04 16:48:41.954841','2020-02-04 16:56:24.817343'),(304,'Esta es una observación es de dependientes economicos','2020-02-04 17:47:50.592173','2020-02-04 17:47:50.592244'),(305,'','2020-02-04 17:53:39.712713','2020-02-04 17:53:39.712778'),(306,'','2020-02-04 18:30:23.823935','2020-02-04 18:30:23.823995'),(307,'SDAFASDF','2020-02-04 18:31:41.490195','2020-02-04 18:31:41.490223'),(308,'ESTA ES UNA OBSERVACION DE BIENE INMIEBLRE','2020-02-04 21:34:43.486940','2020-02-04 22:31:16.719069'),(309,'ESTA ES OTRA OBSERVACION DE BIENES INMUEBLES','2020-02-04 22:32:42.443839','2020-02-04 22:32:42.443868'),(310,'ESTA ES OTRA OBSERVACION DE BIENES INMUEBLES','2020-02-04 22:33:04.144388','2020-02-04 22:33:04.144479'),(311,'ESTA ES OTRA OBSERVACION DE BIENES INMUEBLES','2020-02-04 22:33:48.516767','2020-02-04 22:33:48.516795'),(312,'99.INMUEBLES','2020-02-04 22:36:02.779060','2020-02-04 22:36:02.779089'),(313,'','2020-02-04 22:38:05.554349','2020-02-04 22:38:05.554382'),(314,'ESTA ES UNA PRUEBA','2020-02-04 22:40:02.576340','2020-02-06 22:12:50.853673'),(315,'','2020-02-06 18:18:02.156804','2020-02-06 18:18:02.156855'),(316,'whrthwrh','2020-02-06 20:00:23.659772','2020-02-06 20:00:23.659815'),(317,'123123123123','2020-02-06 20:06:34.769983','2020-02-06 20:06:34.770019'),(318,'123123123123','2020-02-06 20:09:27.616474','2020-02-06 20:09:27.616514'),(319,'123123123123','2020-02-06 20:11:44.105480','2020-02-06 20:11:44.105519'),(320,'123123123123','2020-02-06 20:22:39.444576','2020-02-06 20:22:39.444614'),(321,'123123123123','2020-02-06 20:22:55.267067','2020-02-06 20:22:55.267106'),(322,'ESTA ES UNA PRUEBA','2020-02-06 23:01:37.561367','2020-02-06 23:01:37.561463'),(323,'ESTA ES NA OBSERVACION DE BIENE MUEBLES PRUEBA PARA MI DECLARACIÓN','2020-02-07 03:13:09.383661','2020-02-07 03:13:09.383842'),(324,'ESTA ES UNA PRUEBA DE INVERSIONES OBSERVACIONES','2020-02-07 04:15:45.471554','2020-02-07 04:15:45.471643'),(325,'DFSDF','2020-02-07 05:03:41.041448','2020-02-07 05:03:41.041545'),(326,'','2020-02-10 18:16:13.692066','2020-02-10 18:19:00.727034'),(327,'xcvxcv','2020-02-10 18:31:35.360665','2020-02-10 18:31:35.360701'),(328,'ADSFASDF','2020-02-10 18:53:21.558782','2020-02-10 18:53:21.558830'),(329,'ESTA ES UNA PRUEBA','2020-02-10 18:56:03.875129','2020-02-10 18:57:41.890163'),(330,'ESTA ES UNA OBSERVACION','2020-02-10 19:02:19.824624','2020-02-10 19:02:19.824667'),(331,'ESTA ES UNA PRUEBA','2020-02-10 19:03:39.818276','2020-02-10 19:04:02.466099'),(332,'','2020-02-10 19:35:48.827137','2020-02-10 19:36:54.491869'),(333,'','2020-02-10 19:38:32.590864','2020-02-10 19:38:32.590905'),(334,'','2020-02-10 19:40:33.388637','2020-02-10 19:40:47.009516'),(335,'','2020-02-10 19:44:45.790360','2020-02-10 19:51:02.894722'),(336,'','2020-02-10 19:57:18.610920','2020-02-10 19:57:18.610969'),(337,'adfasdf','2020-02-10 20:01:45.808525','2020-02-10 21:08:17.815737'),(338,'','2020-02-10 20:03:26.103837','2020-02-10 20:03:26.103875'),(339,'ASDFASDF','2020-02-10 20:04:28.683002','2020-02-10 20:05:35.393401'),(340,'','2020-02-10 20:07:35.810705','2020-02-10 22:46:22.340036'),(341,'','2020-02-11 17:46:51.502337','2020-02-11 17:46:51.502415'),(342,'','2020-02-11 19:09:54.872691','2020-02-11 19:09:54.872764'),(343,'','2020-02-14 16:04:13.936698','2020-02-14 16:04:13.936762'),(344,'','2020-02-14 16:09:40.807471','2020-02-14 16:09:40.807615'),(345,'','2020-02-14 16:10:54.565047','2020-02-14 16:10:54.565088'),(346,'','2020-02-14 16:12:05.100618','2020-02-14 16:12:05.100680'),(347,'','2020-02-14 16:13:18.544464','2020-02-14 16:13:18.544504'),(348,'','2020-02-14 19:35:55.030661','2020-02-14 19:35:55.030695'),(349,'','2020-02-14 23:43:55.259946','2020-02-14 23:43:55.260033'),(350,'','2020-02-15 01:24:36.540462','2020-02-15 01:24:36.540547'),(351,'','2020-02-15 01:31:00.089458','2020-02-15 01:31:00.089543'),(352,'','2020-02-15 01:45:02.039789','2020-02-15 01:45:02.039873'),(353,'','2020-02-15 01:50:33.074520','2020-02-15 01:50:33.074608'),(354,'','2020-02-15 02:00:41.793165','2020-02-15 02:00:41.793253'),(355,'','2020-02-15 02:06:40.476590','2020-02-15 02:06:40.476679'),(356,'Esta es una prueba 2 para guardar por  primera vez','2020-02-17 15:21:11.239020','2020-02-17 15:21:11.239058'),(357,'','2020-02-17 15:21:11.736694','2020-02-17 15:21:11.736734'),(358,'','2020-02-17 15:21:11.802769','2020-02-17 15:21:11.802812'),(359,'','2020-02-17 15:21:11.855094','2020-02-17 15:21:11.855137'),(360,'','2020-02-17 15:41:46.316503','2020-02-17 15:41:46.316550'),(361,'','2020-02-17 15:42:17.759764','2020-02-17 15:42:17.759804'),(362,'','2020-02-17 17:09:51.422094','2020-02-17 17:09:51.422137'),(363,'','2020-02-17 17:31:28.035166','2020-02-17 17:31:28.035203'),(364,'','2020-02-17 22:17:03.114036','2020-02-17 22:17:03.114111'),(365,'ESTA ES UNA OBSERVACIÓN','2020-02-17 22:19:56.706517','2020-02-17 22:19:56.706583'),(366,'DSFGDSFG','2020-02-17 22:21:33.254978','2020-02-17 23:20:11.025517'),(367,'Esta es una observación de un dependiente económico.','2020-02-18 15:50:14.155852','2020-02-26 22:12:00.357055'),(368,'','2020-02-18 17:03:26.611498','2020-02-18 17:03:26.611566'),(369,'ESTA ES UNA PRUEBA DE GUARDADO NO APLICA DE FORMA DE PAGO','2020-02-18 22:54:26.229087','2020-02-18 22:54:26.229162'),(370,'','2020-02-19 15:32:58.044991','2020-02-19 15:32:58.045080'),(371,'Esta es una prueba','2020-02-19 15:38:39.175272','2020-02-19 15:38:39.175312'),(372,'ESTA ES UNA OBSERVACIÓN DE PRESTAMO COMO DATO','2020-02-19 17:21:32.161954','2020-02-19 17:21:32.161988'),(373,'','2020-02-19 17:41:06.842992','2020-02-19 17:41:06.843025'),(374,'AQUI HAY UNA OBSERVACIÓN DE BENEFICIOS PRIVADOS','2020-02-19 17:43:03.077833','2020-02-19 17:45:02.941797'),(375,'SEGUNDA OBSERVACION DE BENEFICIOS GRATUITOS','2020-02-19 17:47:27.840215','2020-02-19 17:48:41.175505'),(376,'','2020-02-19 20:00:00.532275','2020-02-21 19:45:48.400152'),(377,'','2020-02-19 20:10:11.990024','2020-02-19 20:10:11.990067'),(378,'','2020-02-19 20:16:33.888301','2020-02-19 20:16:33.888340'),(379,'','2020-02-19 20:20:11.024671','2020-02-21 19:43:49.257604'),(380,'ULTIMA OBSERVACION','2020-02-21 17:30:27.553267','2020-02-21 17:30:27.553298'),(381,'ESTA ES LA ULTIMA OBSRVACION DE DECLARACION DE INTERESES','2020-02-21 17:44:38.590919','2020-02-21 19:33:53.386478'),(382,'ESTA ES UNA PRUEBA','2020-02-21 20:43:47.214112','2020-02-21 21:57:30.192574'),(383,'','2020-02-21 22:11:27.185402','2020-02-21 22:11:27.185447'),(384,'ESTA ES UNA PRUEBA','2020-02-21 22:21:51.795602','2020-02-21 22:21:51.795676'),(385,'','2020-02-21 22:24:09.017482','2020-03-18 16:53:14.880354'),(386,'','2020-02-21 22:24:30.219888','2020-03-18 16:52:04.642197'),(387,'','2020-02-21 22:25:44.990439','2020-02-21 22:25:44.990511'),(388,'','2020-02-21 22:26:10.958752','2020-02-21 22:26:10.958783'),(389,'','2020-02-21 22:26:32.148659','2020-02-21 22:26:32.148768'),(390,'','2020-02-21 22:27:25.860357','2020-02-21 22:30:45.670818'),(391,'','2020-02-21 22:31:32.307953','2020-02-21 22:37:48.839711'),(392,'','2020-02-25 15:46:26.600826','2020-02-25 15:46:26.600926'),(393,'','2020-02-25 16:39:30.871160','2020-02-25 16:39:30.871208'),(394,'','2020-02-25 16:39:37.948056','2020-02-25 16:39:37.948125'),(395,'ESTA ES UNA OBSERVACION','2020-02-25 17:11:05.021484','2020-02-25 19:08:37.242957'),(396,'ESTA ES UNA OBSERVACIÓN DE PRUEBA','2020-02-25 18:03:55.104935','2020-02-25 18:03:55.105033'),(397,'OTRA OBSERVACIÓN DE BENEFICIOS GRATUITOS','2020-02-25 18:05:10.564556','2020-02-25 18:05:10.564628'),(398,'','2020-02-25 18:09:00.062154','2020-02-25 18:09:00.062272'),(399,'','2020-02-25 18:10:41.150009','2020-02-25 18:10:41.150078'),(400,'','2020-02-25 18:11:51.060033','2020-02-25 23:03:21.933807'),(401,'SEGUNDA OBSERVACIÓN DE LA REPRESENTACIÓN','2020-02-25 19:09:35.785977','2020-02-25 19:09:35.786006'),(402,'','2020-02-25 20:52:22.255423','2020-02-25 20:52:22.255456'),(403,'','2020-02-25 23:00:24.276585','2020-02-25 23:00:24.276632'),(404,'','2020-02-26 16:48:23.081115','2020-02-26 16:48:23.081152'),(405,'','2020-02-26 18:00:18.725687','2020-02-26 18:00:18.725722'),(406,'OBSERVACION DE DATOS DE LA PAREJA','2020-02-26 18:45:21.860439','2020-02-26 18:45:21.860479'),(407,'','2020-02-26 21:38:10.976384','2020-02-26 21:38:10.976426'),(408,'','2020-02-26 21:42:24.747899','2020-02-26 21:42:24.747947'),(409,'','2020-02-27 15:31:54.894944','2020-02-27 15:31:54.895028'),(410,'','2020-02-27 18:14:34.480901','2020-02-27 18:14:34.480977'),(411,'','2020-02-27 21:29:15.890708','2020-02-27 21:29:15.890746'),(412,'','2020-02-27 23:00:30.979826','2020-02-27 23:00:30.979858'),(413,'','2020-02-27 23:04:36.258973','2020-02-27 23:04:36.259004'),(414,'','2020-02-27 23:05:29.522213','2020-02-27 23:05:29.522285'),(415,'','2020-02-28 15:58:44.992865','2020-02-28 15:58:44.992968'),(416,'','2020-02-28 16:01:52.426471','2020-02-28 16:01:52.426505'),(417,'','2020-02-28 16:01:52.726381','2020-02-28 16:01:52.726411'),(418,'','2020-02-28 16:01:52.733277','2020-02-28 16:01:52.733307'),(419,'','2020-02-28 16:01:52.746332','2020-02-28 16:01:52.746361'),(420,'OBSERVACIÓN DE USUARIO DE PRUEBA','2020-02-28 16:04:00.943552','2020-03-06 18:02:11.270271'),(421,'','2020-03-03 16:37:31.702396','2020-03-03 16:37:31.702430'),(422,'ESTA ES UNA DECLARACION DE CONCLUSIÓN','2020-03-03 16:38:10.824012','2020-03-03 16:38:10.824135'),(423,'','2020-03-03 16:38:10.949114','2020-03-03 16:38:10.949146'),(424,'','2020-03-03 16:38:10.959007','2020-03-03 16:38:10.959040'),(425,'','2020-03-03 16:38:10.967383','2020-03-03 16:38:10.967410'),(426,'ATAEGEAFDG','2020-03-03 17:00:05.574177','2020-03-03 17:00:05.574210'),(427,'','2020-03-03 20:21:26.261327','2020-03-03 20:21:26.261412'),(428,'','2020-03-03 20:32:27.865727','2020-03-03 20:32:27.865760'),(429,'xvsdvgs','2020-03-06 22:35:09.454230','2020-03-06 22:35:09.454294'),(430,'ASDFSDF','2020-03-06 22:38:30.045541','2020-03-06 22:38:30.045606'),(431,'ASDFSDF','2020-03-06 22:47:25.008166','2020-03-06 22:47:25.008232'),(432,'','2020-03-17 18:31:17.680365','2020-03-17 18:31:17.680399');
/*!40000 ALTER TABLE `declaracion_observaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_otraspartes`
--

DROP TABLE IF EXISTS `declaracion_otraspartes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_otraspartes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio_relacion` date DEFAULT NULL,
  `otra_relacion` varchar(255) NOT NULL,
  `ocupacion_profesion` varchar(255) NOT NULL,
  `intereses_comunes` tinyint(1) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_titular_tipos_relaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `declarante_infopersonalvar_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `otraspartes_infopersonalvar_id` int(11) NOT NULL,
  `otra_relacion_familiar` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_otraspar_cat_titular_tipos_re_b8503d3b_fk_declaraci` (`cat_titular_tipos_relaciones_id`),
  KEY `declaracion_otraspar_declaraciones_id_a16c80b4_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_otraspar_declarante_infoperso_714ae770_fk_declaraci` (`declarante_infopersonalvar_id`),
  KEY `declaracion_otraspar_observaciones_id_2ed813cf_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_otraspar_otraspartes_infopers_d1a88dc5_fk_declaraci` (`otraspartes_infopersonalvar_id`),
  CONSTRAINT `declaracion_otraspar_cat_titular_tipos_re_b8503d3b_fk_declaraci` FOREIGN KEY (`cat_titular_tipos_relaciones_id`) REFERENCES `declaracion_cattitulartiposrelaciones` (`id`),
  CONSTRAINT `declaracion_otraspar_declaraciones_id_a16c80b4_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_otraspar_declarante_infoperso_714ae770_fk_declaraci` FOREIGN KEY (`declarante_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_otraspar_observaciones_id_2ed813cf_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_otraspar_otraspartes_infopers_d1a88dc5_fk_declaraci` FOREIGN KEY (`otraspartes_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_otraspartes`
--

LOCK TABLES `declaracion_otraspartes` WRITE;
/*!40000 ALTER TABLE `declaracion_otraspartes` DISABLE KEYS */;
/*!40000 ALTER TABLE `declaracion_otraspartes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_prestamocomodato`
--

DROP TABLE IF EXISTS `declaracion_prestamocomodato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_prestamocomodato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otro_tipo_inmueble` varchar(100) DEFAULT NULL,
  `otro_tipo_mueble` varchar(255) NOT NULL,
  `mueble_marca` varchar(255) NOT NULL,
  `mueble_submarca` varchar(255) NOT NULL,
  `mueble_modelo` int(11) DEFAULT NULL,
  `mueble_num_serie` varchar(255) NOT NULL,
  `mueble_num_registro` varchar(255) NOT NULL,
  `otro_tipo_adeudo` varchar(255) NOT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipos_inmueble_id` int(11) DEFAULT NULL,
  `cat_tipos_muebles_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `inmueble_domicilios_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  `titular_relacion_id` int(11) DEFAULT NULL,
  `tipo_obj_comodato` varchar(100) DEFAULT NULL,
  `titular_infopersonalVar_id` int(11) DEFAULT NULL,
  `otro_tipo_relacion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_prestamo_cat_paises_id_4ab8eda8_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_prestamo_cat_tipos_inmueble_i_831c45ff_fk_declaraci` (`cat_tipos_inmueble_id`),
  KEY `declaracion_prestamo_cat_tipos_operacione_3fab732c_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_prestamo_observaciones_id_252fa429_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_prestamo_declaraciones_id_b63d3649_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_prestamo_inmueble_domicilios__1f840946_fk_declaraci` (`inmueble_domicilios_id`),
  KEY `declaracion_prestamo_titular_infopersonal_cb34e238_fk_declaraci` (`titular_infopersonalVar_id`),
  KEY `declaracion_prestamo_titular_relacion_id_a35cb496_fk_declaraci` (`titular_relacion_id`),
  KEY `declaracion_prestamocomodato_cat_tipos_muebles_id_1b69f005` (`cat_tipos_muebles_id`),
  CONSTRAINT `declaracion_prestamo_cat_paises_id_4ab8eda8_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_prestamo_cat_tipos_inmueble_i_831c45ff_fk_declaraci` FOREIGN KEY (`cat_tipos_inmueble_id`) REFERENCES `declaracion_cattiposinmuebles` (`id`),
  CONSTRAINT `declaracion_prestamo_cat_tipos_muebles_id_1b69f005_fk_declaraci` FOREIGN KEY (`cat_tipos_muebles_id`) REFERENCES `declaracion_cattiposmuebles` (`id`),
  CONSTRAINT `declaracion_prestamo_cat_tipos_operacione_3fab732c_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_prestamo_declaraciones_id_b63d3649_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_prestamo_inmueble_domicilios__1f840946_fk_declaraci` FOREIGN KEY (`inmueble_domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_prestamo_observaciones_id_252fa429_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_prestamo_titular_infopersonal_cb34e238_fk_declaraci` FOREIGN KEY (`titular_infopersonalVar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_prestamo_titular_relacion_id_a35cb496_fk_declaraci` FOREIGN KEY (`titular_relacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_prestamocomodato`
--

LOCK TABLES `declaracion_prestamocomodato` WRITE;
/*!40000 ALTER TABLE `declaracion_prestamocomodato` DISABLE KEYS */;
INSERT INTO `declaracion_prestamocomodato` VALUES (1,'',NULL,'','TOYOTA','',NULL,'','','Comodato',142,1,1,2,2,144,334,14,NULL,149,''),(3,'',NULL,'','FORD','FORD',8589958,'8888888888-A','789798797','Comodato',142,1,1,2,1,155,372,11,NULL,162,''),(4,'',NULL,'','FORD','SPARK',2018,'494987','34234','Comodato',142,1,1,1,10,164,386,11,NULL,176,'');
/*!40000 ALTER TABLE `declaracion_prestamocomodato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_representaciones`
--

DROP TABLE IF EXISTS `declaracion_representaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_representaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_representacion` varchar(255) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `pagado` tinyint(1) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `es_representacion_activa` tinyint(1) DEFAULT NULL,
  `monto` decimal(13,2) DEFAULT NULL,
  `es_mueble` tinyint(1) DEFAULT NULL,
  `otro_mueble` varchar(255) DEFAULT NULL,
  `cat_tipos_muebles_id` int(11) DEFAULT NULL,
  `cat_tipos_representaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  `cat_moneda_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_represen_cat_tipos_muebles_id_3d480067_fk_declaraci` (`cat_tipos_muebles_id`),
  KEY `declaracion_represen_cat_tipos_representa_15700f00_fk_declaraci` (`cat_tipos_representaciones_id`),
  KEY `declaracion_represen_declaraciones_id_9fc9f340_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_represen_info_personal_var_id_c945bcb6_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_represen_observaciones_id_acef1dc3_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_represen_cat_tipos_relaciones_93e0bbf5_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  KEY `declaracion_represen_cat_moneda_id_ede36c3f_fk_declaraci` (`cat_moneda_id`),
  KEY `declaracion_represen_cat_tipos_operacione_4fcd608c_fk_declaraci` (`cat_tipos_operaciones_id`),
  CONSTRAINT `declaracion_represen_cat_moneda_id_ede36c3f_fk_declaraci` FOREIGN KEY (`cat_moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_represen_cat_tipos_muebles_id_3d480067_fk_declaraci` FOREIGN KEY (`cat_tipos_muebles_id`) REFERENCES `declaracion_cattiposmuebles` (`id`),
  CONSTRAINT `declaracion_represen_cat_tipos_operacione_4fcd608c_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_represen_cat_tipos_relaciones_93e0bbf5_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_represen_cat_tipos_representa_15700f00_fk_declaraci` FOREIGN KEY (`cat_tipos_representaciones_id`) REFERENCES `declaracion_cattiposrepresentaciones` (`id`),
  CONSTRAINT `declaracion_represen_declaraciones_id_9fc9f340_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_represen_info_personal_var_id_c945bcb6_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_represen_observaciones_id_acef1dc3_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_representaciones`
--

LOCK TABLES `declaracion_representaciones` WRITE;
/*!40000 ALTER TABLE `declaracion_representaciones` DISABLE KEYS */;
INSERT INTO `declaracion_representaciones` VALUES (2,'','2019-11-26',0,'2019-12-11 22:18:59.476241','2020-02-26 22:23:08.687963',1,NULL,NULL,NULL,NULL,2,1,15,32,NULL,NULL,2),(3,'','2019-11-25',0,'2019-12-24 19:23:19.472757','2019-12-24 19:23:19.472803',1,1458.00,NULL,NULL,NULL,1,3,63,131,16,101,1),(4,'','2013-09-11',1,'2020-02-04 14:26:19.799911','2020-02-04 14:26:19.799934',1,1200.00,NULL,NULL,NULL,2,9,111,291,15,101,2),(5,'','2012-01-01',NULL,'2020-02-07 05:03:41.076158','2020-02-07 05:03:41.076231',1,NULL,NULL,NULL,NULL,2,2,144,325,15,101,3),(6,'','1910-01-01',NULL,'2020-02-21 22:27:25.880842','2020-02-21 22:30:45.707698',1,NULL,NULL,NULL,NULL,1,10,178,390,1,NULL,NULL),(7,'','2019-01-01',1,'2020-02-25 17:11:05.074780','2020-02-25 19:08:37.282136',1,1500.00,NULL,NULL,NULL,2,11,180,395,8,101,1),(8,'','2018-01-01',0,'2020-02-25 19:09:35.823850','2020-02-25 19:09:35.823882',1,NULL,NULL,NULL,NULL,1,11,185,401,1,NULL,1);
/*!40000 ALTER TABLE `declaracion_representaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_secciondeclaracion`
--

DROP TABLE IF EXISTS `declaracion_secciondeclaracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_secciondeclaracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estatus` int(11) NOT NULL,
  `aplica` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) DEFAULT NULL,
  `seccion_id` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_secciond_declaraciones_id_d2a22dad_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_secciond_observaciones_id_0ac9ba06_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_secciond_seccion_id_809df765_fk_declaraci` (`seccion_id`),
  CONSTRAINT `declaracion_secciond_declaraciones_id_d2a22dad_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_secciond_observaciones_id_0ac9ba06_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_secciond_seccion_id_809df765_fk_declaraci` FOREIGN KEY (`seccion_id`) REFERENCES `declaracion_secciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_secciondeclaracion`
--

LOCK TABLES `declaracion_secciondeclaracion` WRITE;
/*!40000 ALTER TABLE `declaracion_secciondeclaracion` DISABLE KEYS */;
INSERT INTO `declaracion_secciondeclaracion` VALUES (1,1,1,'2019-12-06 18:33:37.972040','2020-02-26 22:33:29.886093',1,NULL,2,4,4),(2,0,1,'2019-12-06 18:33:37.985654','2020-02-26 22:33:30.174750',1,3,1,52,0),(3,0,1,'2019-12-06 18:33:38.001464','2020-02-26 22:33:30.315740',1,4,17,18,0),(7,1,1,'2019-12-06 19:07:03.266401','2020-02-26 22:33:29.927842',1,NULL,4,3,3),(8,1,1,'2019-12-06 19:19:15.551601','2020-02-26 22:33:29.950158',1,NULL,5,3,2),(9,1,1,'2019-12-09 15:36:21.332154','2020-02-26 22:33:29.966943',1,NULL,6,3,3),(10,1,1,'2019-12-09 16:32:45.487047','2020-02-26 22:33:29.988026',1,NULL,7,4,4),(13,1,1,'2019-12-10 23:35:36.476372','2020-02-26 22:33:30.206845',1,NULL,19,3,3),(14,1,1,'2019-12-11 16:26:41.559645','2020-02-26 22:33:30.225939',1,NULL,20,2,2),(15,1,1,'2019-12-11 19:11:06.054673','2020-02-26 22:33:30.243448',1,NULL,21,3,3),(16,1,1,'2019-12-11 22:46:53.275629','2020-02-26 22:33:30.264893',1,NULL,22,3,3),(17,1,1,'2019-12-12 19:07:56.085911','2020-02-26 22:33:30.287690',1,NULL,23,2,2),(30,1,1,'2019-12-15 03:15:10.620220','2020-02-11 16:50:31.801249',2,NULL,2,4,4),(31,0,1,'2019-12-15 03:15:10.647653','2020-02-11 16:50:32.654120',2,81,1,82,0),(32,0,1,'2019-12-15 03:15:10.659976','2020-02-11 16:50:33.088309',2,82,17,7,0),(36,1,1,'2019-12-15 04:02:03.718507','2020-02-11 16:50:31.883517',2,NULL,4,5,5),(37,1,1,'2019-12-24 18:41:50.178884','2020-01-30 20:47:12.160405',3,NULL,2,4,4),(38,0,1,'2019-12-24 18:41:50.196117','2020-01-30 20:47:12.395334',3,119,1,64,0),(39,0,1,'2019-12-24 18:41:50.222143','2020-01-30 20:47:12.633402',3,120,17,41,0),(43,1,1,'2019-12-24 18:47:51.976348','2020-01-30 20:47:12.218295',3,NULL,4,5,5),(44,1,1,'2019-12-24 18:48:33.512145','2020-01-30 20:47:12.269320',3,NULL,5,10,10),(45,1,1,'2019-12-24 18:56:23.413802','2020-01-30 20:47:12.297291',3,NULL,6,11,11),(46,1,1,'2019-12-24 18:58:53.710350','2020-01-30 20:47:12.332868',3,NULL,7,15,15),(48,1,1,'2019-12-24 19:11:54.661787','2020-01-30 20:47:12.430919',3,NULL,19,7,0),(49,1,1,'2019-12-24 19:13:02.835992','2020-01-30 20:47:12.486470',3,NULL,20,7,7),(50,1,1,'2019-12-24 19:23:19.497855','2020-01-30 20:47:12.522525',3,NULL,21,6,6),(52,1,1,'2019-12-24 19:26:11.694679','2020-01-30 20:47:12.582996',3,NULL,22,6,6),(53,1,1,'2019-12-24 19:26:46.355512','2020-01-30 20:47:12.608686',3,NULL,23,5,5),(68,1,1,'2020-01-13 19:36:38.141748','2020-03-03 16:37:18.772185',4,NULL,2,4,4),(69,0,1,'2020-01-13 19:36:38.153853','2020-03-03 16:37:18.960872',4,161,1,64,0),(70,0,1,'2020-01-13 19:36:38.164532','2020-03-03 16:37:19.140529',4,162,17,41,0),(75,1,1,'2020-01-13 20:02:19.306627','2020-03-03 16:37:19.123024',4,NULL,19,3,3),(76,1,1,'2020-01-13 22:33:56.749689','2020-01-24 17:51:09.885578',5,NULL,2,4,4),(77,0,1,'2020-01-13 22:33:56.764471','2020-01-24 17:51:09.908589',5,170,1,64,0),(78,0,1,'2020-01-13 22:33:56.783605','2020-01-13 22:33:56.783644',5,171,17,41,0),(84,1,1,'2020-01-14 15:41:21.891549','2020-02-18 17:03:39.058841',6,NULL,2,4,4),(85,0,1,'2020-01-14 15:41:21.922431','2020-02-18 17:03:39.070502',6,179,1,64,0),(86,0,1,'2020-01-14 15:41:21.956843','2020-01-14 15:41:21.956924',6,180,17,41,0),(112,1,1,'2020-01-22 18:29:18.700303','2020-02-26 22:34:29.258402',9,NULL,2,4,4),(113,0,1,'2020-01-22 18:29:18.716589','2020-02-26 22:34:29.594948',9,211,1,66,0),(114,0,1,'2020-01-22 18:29:18.730752','2020-02-26 22:34:29.758925',9,212,17,45,0),(119,1,0,'2020-01-22 18:32:58.113956','2020-02-26 22:34:29.713772',9,NULL,22,3,3),(120,1,1,'2020-01-22 19:01:22.274995','2020-02-26 22:34:29.680234',9,NULL,20,2,2),(121,1,0,'2020-01-22 19:01:35.847377','2020-02-26 22:34:29.732834',9,NULL,23,2,2),(123,1,1,'2020-01-22 21:29:05.152071','2020-03-18 16:53:25.150764',10,NULL,2,4,4),(124,0,1,'2020-01-22 21:29:05.171078','2020-03-18 16:53:25.465087',10,225,1,55,0),(125,0,1,'2020-01-22 21:29:05.180173','2020-03-18 16:53:25.630943',10,226,17,18,0),(131,1,0,'2020-01-27 18:59:07.791089','2020-03-18 16:53:25.221104',10,NULL,5,3,3),(132,1,1,'2020-01-27 20:08:16.398964','2020-03-18 16:53:25.257883',10,NULL,7,4,4),(138,1,1,'2020-01-31 05:26:56.653069','2020-02-26 22:33:30.167274',1,257,16,4,4),(144,0,1,'2020-01-31 19:08:21.253525','2020-01-31 19:29:48.823890',1,263,25,1,0),(145,1,1,'2020-01-31 19:13:06.374814','2020-02-26 22:33:30.083396',1,264,12,5,4),(146,1,1,'2020-01-31 19:29:48.412845','2020-02-26 22:33:30.106410',1,265,13,4,3),(147,1,1,'2020-01-31 19:29:48.440156','2020-02-26 22:33:30.128299',1,266,14,1,1),(148,0,1,'2020-01-31 19:29:48.455431','2020-02-26 22:33:30.144833',1,267,15,4,4),(151,1,1,'2020-01-31 22:23:12.015056','2020-02-26 22:33:30.067547',1,NULL,11,3,3),(152,0,1,'2020-02-03 00:03:16.308895','2020-02-03 00:03:16.308952',9,275,25,1,0),(153,1,1,'2020-02-03 02:33:18.672342','2020-02-26 22:33:30.009326',1,NULL,8,4,4),(154,1,1,'2020-02-03 02:34:53.632354','2020-02-26 22:33:30.027253',1,NULL,9,6,6),(155,1,1,'2020-02-03 02:36:12.385662','2020-02-26 22:33:29.909868',1,NULL,3,2,2),(156,1,1,'2020-02-03 02:37:40.752181','2020-02-26 22:33:30.048865',1,NULL,10,5,5),(157,1,1,'2020-02-04 00:25:22.449955','2020-02-26 22:33:30.309204',1,NULL,24,2,2),(158,1,1,'2020-02-04 00:28:59.705397','2020-02-26 22:34:29.287797',9,NULL,3,2,2),(159,1,1,'2020-02-04 00:32:59.711380','2020-02-26 22:34:29.319138',9,NULL,4,3,3),(160,1,1,'2020-02-04 00:40:00.812524','2020-02-26 22:34:29.353679',9,NULL,5,3,1),(161,1,1,'2020-02-04 00:44:29.578244','2020-02-26 22:34:29.389251',9,NULL,6,3,3),(162,1,1,'2020-02-04 00:54:16.806027','2020-02-26 22:34:29.419919',9,NULL,7,4,4),(163,1,1,'2020-02-04 00:56:46.899271','2020-02-26 22:34:29.451824',9,NULL,8,4,4),(164,1,1,'2020-02-04 01:08:21.434462','2020-02-26 22:34:29.488306',9,NULL,9,6,6),(165,1,1,'2020-02-04 01:30:47.892035','2020-02-26 22:34:29.518544',9,NULL,10,5,5),(166,1,1,'2020-02-04 01:38:25.547125','2020-02-26 22:34:29.547817',9,NULL,11,3,2),(167,1,1,'2020-02-04 01:55:39.962251','2020-02-26 22:34:29.580810',9,NULL,12,5,5),(168,1,1,'2020-02-04 05:46:08.163051','2020-02-26 22:34:29.634014',9,NULL,18,3,3),(169,1,1,'2020-02-04 14:24:14.231855','2020-02-26 22:34:29.656792',9,NULL,19,3,3),(170,1,1,'2020-02-04 14:26:19.809466','2020-02-26 22:34:29.698820',9,NULL,21,3,3),(171,1,1,'2020-02-04 14:29:29.812426','2020-02-26 22:34:29.752580',9,NULL,24,2,2),(172,0,1,'2020-02-04 16:12:18.437677','2020-03-18 16:53:25.645263',10,296,25,1,0),(173,1,1,'2020-02-04 16:12:36.644845','2020-03-18 16:53:25.328433',10,NULL,10,5,5),(174,1,1,'2020-02-04 16:12:47.365523','2020-03-18 16:53:25.346774',10,NULL,11,3,3),(175,1,1,'2020-02-04 16:15:58.073773','2020-03-18 16:53:25.622457',10,NULL,24,2,2),(176,1,0,'2020-02-04 16:25:03.217254','2020-03-18 16:53:25.234712',10,NULL,6,3,3),(177,1,0,'2020-02-04 17:53:39.857522','2020-03-18 16:53:25.278641',10,NULL,8,4,4),(178,1,1,'2020-02-04 18:31:41.586117','2020-03-18 16:53:25.365048',10,NULL,12,5,5),(179,0,1,'2020-02-04 22:38:05.556806','2020-02-11 16:50:33.117809',2,313,25,1,0),(180,1,1,'2020-02-04 22:40:02.749626','2020-02-11 16:50:32.270308',2,NULL,11,7,7),(181,1,1,'2020-02-06 23:01:37.760472','2020-02-11 16:50:32.296334',2,NULL,12,1,1),(182,1,1,'2020-02-07 03:13:09.513942','2020-02-11 16:50:32.345874',2,NULL,13,7,7),(183,1,1,'2020-02-07 04:15:45.579191','2020-02-11 16:50:32.451309',2,NULL,14,6,6),(184,1,1,'2020-02-07 05:03:41.210743','2020-02-11 16:50:32.910776',2,NULL,21,1,1),(185,1,1,'2020-02-10 18:12:30.981723','2020-02-11 16:50:31.831968',2,NULL,3,2,2),(186,1,1,'2020-02-10 18:16:14.027817','2020-02-11 16:50:31.962661',2,NULL,5,7,7),(187,1,1,'2020-02-10 18:31:35.723928','2020-02-11 16:50:32.027868',2,NULL,6,4,4),(188,1,1,'2020-02-10 18:53:21.833057','2020-02-11 16:50:32.094103',2,NULL,7,2,2),(189,1,1,'2020-02-10 18:56:03.925722','2020-02-11 16:50:32.159949',2,NULL,8,4,4),(190,1,1,'2020-02-10 19:02:19.952838','2020-02-11 16:50:32.197668',2,NULL,9,6,6),(191,1,1,'2020-02-10 19:03:39.845147','2020-02-11 16:50:32.237170',2,NULL,10,3,3),(192,1,1,'2020-02-10 19:35:48.845256','2020-02-11 16:50:32.524240',2,NULL,15,1,1),(193,1,1,'2020-02-10 19:40:33.430729','2020-02-11 16:50:32.611396',2,NULL,16,1,1),(194,1,1,'2020-02-10 19:44:45.849366','2020-02-11 16:50:32.707763',2,NULL,18,1,1),(195,1,1,'2020-02-10 19:57:18.675893','2020-02-11 16:50:32.763337',2,NULL,19,1,1),(196,1,1,'2020-02-10 20:01:45.869951','2020-02-11 16:50:32.862508',2,NULL,20,1,1),(197,1,1,'2020-02-10 20:03:26.199846','2020-02-11 16:50:32.936322',2,NULL,22,1,1),(198,1,1,'2020-02-10 20:04:28.716257','2020-02-11 16:50:32.982069',2,NULL,23,1,1),(199,1,1,'2020-02-10 20:07:35.984540','2020-02-11 16:50:33.045871',2,NULL,24,1,1),(200,1,1,'2020-02-10 20:07:52.622074','2020-02-11 16:50:33.105673',2,NULL,26,1,1),(201,1,1,'2020-02-17 15:21:11.593042','2020-02-27 18:13:45.808986',11,NULL,2,4,4),(202,0,1,'2020-02-17 15:21:11.794736','2020-02-27 18:13:46.606409',11,357,1,82,0),(203,0,1,'2020-02-17 15:21:11.813793','2020-02-27 18:13:47.008507',11,358,17,7,0),(204,0,1,'2020-02-17 15:21:11.859168','2020-02-17 15:21:11.859202',11,359,25,1,0),(205,0,1,'2020-02-17 22:17:03.134787','2020-02-17 22:17:03.134857',4,364,25,1,0),(206,1,1,'2020-02-17 22:19:56.984032','2020-03-03 16:37:18.929110',4,NULL,8,4,4),(207,1,1,'2020-02-18 15:50:14.334592','2020-02-27 18:13:46.246206',11,NULL,8,4,4),(208,0,1,'2020-02-18 17:03:26.615763','2020-02-18 17:03:26.615822',6,368,25,1,0),(209,1,1,'2020-02-18 22:54:26.653820','2020-02-27 18:13:46.580431',11,NULL,13,4,2),(210,1,1,'2020-02-19 17:43:03.115558','2020-03-18 16:53:25.606668',10,NULL,23,2,2),(211,1,1,'2020-02-19 20:00:00.695246','2020-03-18 16:53:25.201520',10,NULL,4,3,3),(212,1,1,'2020-02-21 19:34:09.001561','2020-03-18 16:53:25.638473',10,NULL,26,1,1),(213,1,1,'2020-02-21 19:42:39.433332','2020-03-18 16:53:25.173373',10,NULL,3,2,2),(214,1,1,'2020-02-21 20:43:47.481890','2020-03-18 16:53:25.311560',10,NULL,9,6,6),(215,1,1,'2020-02-21 22:11:27.252526','2020-03-18 16:53:25.384964',10,NULL,13,4,4),(216,1,1,'2020-02-21 22:21:51.890398','2020-03-18 16:53:25.403081',10,NULL,14,1,1),(217,1,1,'2020-02-21 22:24:09.198801','2020-03-18 16:53:25.421468',10,NULL,15,4,4),(218,1,1,'2020-02-21 22:24:30.275237','2020-03-18 16:53:25.449785',10,NULL,16,4,4),(219,1,1,'2020-02-21 22:25:45.123178','2020-03-18 16:53:25.478674',10,NULL,18,3,3),(220,1,1,'2020-02-21 22:26:11.010007','2020-03-18 16:53:25.506330',10,NULL,19,3,3),(221,1,1,'2020-02-21 22:26:32.325198','2020-03-18 16:53:25.536943',10,NULL,20,2,2),(222,1,1,'2020-02-21 22:27:26.016527','2020-03-18 16:53:25.561941',10,NULL,21,3,3),(223,1,1,'2020-02-21 22:31:32.424761','2020-03-18 16:53:25.588789',10,NULL,22,3,3),(224,1,1,'2020-02-25 17:11:05.154982','2020-02-27 18:13:46.775781',11,NULL,21,3,3),(225,1,1,'2020-02-25 18:03:55.277556','2020-02-27 18:13:46.903562',11,NULL,22,3,3),(226,1,1,'2020-02-25 18:05:10.601037','2020-02-27 18:13:46.991060',11,NULL,23,2,2),(227,1,1,'2020-02-25 18:08:27.943939','2020-02-27 18:13:45.890189',11,NULL,3,2,2),(228,1,1,'2020-02-25 18:09:00.096087','2020-02-27 18:13:45.952255',11,NULL,4,3,3),(229,1,1,'2020-02-25 18:09:38.150648','2020-02-27 18:13:46.031130',11,NULL,5,3,2),(230,1,1,'2020-02-25 18:10:41.257546','2020-02-27 18:13:46.098328',11,NULL,6,3,3),(231,1,1,'2020-02-25 18:11:51.305527','2020-02-27 18:13:46.492443',11,NULL,11,3,3),(232,1,1,'2020-02-26 18:45:22.108750','2020-02-27 18:13:46.156214',11,NULL,7,4,4),(233,1,1,'2020-02-26 21:38:11.253264','2020-02-27 18:13:46.312468',11,NULL,9,6,6),(234,1,1,'2020-02-26 21:42:24.765863','2020-02-27 18:13:46.401489',11,NULL,10,5,5),(235,1,1,'2020-02-26 22:22:47.588613','2020-02-26 22:33:30.183686',1,NULL,18,3,3),(236,1,1,'2020-02-28 16:01:52.720103','2020-03-18 16:53:51.445602',12,NULL,2,4,4),(237,0,1,'2020-02-28 16:01:52.728322','2020-03-18 16:53:51.529839',12,417,1,55,15),(238,0,1,'2020-02-28 16:01:52.735259','2020-03-18 16:53:51.612763',12,418,17,18,5),(239,0,1,'2020-02-28 16:01:52.748169','2020-03-18 16:53:51.633531',12,419,25,1,1),(240,1,1,'2020-02-28 16:04:01.267480','2020-03-18 16:53:51.498119',12,NULL,8,4,4),(241,1,0,'2020-02-28 16:50:33.245410','2020-03-18 16:53:51.582213',12,NULL,21,3,3),(242,1,1,'2020-03-03 16:38:10.941664','2020-03-04 15:49:16.972685',13,NULL,2,4,4),(243,0,1,'2020-03-03 16:38:10.951068','2020-03-04 15:49:17.106590',13,423,1,55,0),(244,0,1,'2020-03-03 16:38:10.962518','2020-03-03 16:38:10.962547',13,424,17,18,0),(245,0,1,'2020-03-03 16:38:10.969591','2020-03-03 16:38:10.969618',13,425,25,1,0),(246,1,1,'2020-03-03 17:00:05.626491','2020-03-04 15:49:17.081267',13,NULL,14,1,1),(247,1,1,'2020-03-04 17:08:19.771764','2020-03-18 16:53:51.462290',12,NULL,3,2,2),(248,1,1,'2020-03-04 17:08:46.603276','2020-03-18 16:53:51.627606',12,NULL,26,1,1),(249,1,1,'2020-03-06 22:27:30.250042','2020-03-18 16:53:51.605666',12,NULL,23,2,2),(250,1,0,'2020-03-10 22:39:27.679219','2020-03-18 16:53:51.519871',12,NULL,10,5,5);
/*!40000 ALTER TABLE `declaracion_secciondeclaracion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_secciones`
--

DROP TABLE IF EXISTS `declaracion_secciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_secciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seccion` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `parametro` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_secciones_tree_id_bfed8a79` (`tree_id`),
  KEY `declaracion_secciones_parent_id_c75d16ef` (`parent_id`),
  CONSTRAINT `declaracion_seccione_parent_id_c75d16ef_fk_declaraci` FOREIGN KEY (`parent_id`) REFERENCES `declaracion_secciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_secciones`
--

LOCK TABLES `declaracion_secciones` WRITE;
/*!40000 ALTER TABLE `declaracion_secciones` DISABLE KEYS */;
INSERT INTO `declaracion_secciones` VALUES (1,'DECLARACIÓN PATRIMONIAL','','',1,1,14,1,0,NULL),(2,'DATOS GENERALES','informacion-general','',1,2,3,1,1,1),(3,'DOMICILIO DEL DECLARANTE','direccion','',2,4,5,1,1,1),(4,'DATOS CURRICULARES DEL DECLARANTE','datos-curriculares','',3,6,7,1,1,1),(5,'DATOS DEL EMPLEO, CARGO O COMISIÓN QUE INICIA','datos-del-encargo-actual','',4,8,9,1,1,1),(6,'EXPERIENCIA LABORAL(ÙLTIMO 5 EMPLEOS)','experiencia-laboral','',5,10,11,1,1,1),(7,'DATOS DE LA PAREJA','datos-pareja','',6,12,13,1,1,1),(8,'DATOS DEL DEPENDIENTE ECÓNOMICO','dependientes-economicos','',7,14,15,1,1,1),(9,'INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTES ECONÓMICOS (SITUACION ACTUAL','ingresos-netos','',8,16,17,1,1,1),(10,'TE DESEMPEÑASTE COMO SERVIDOR PÚBLICO EN EL AÑO ANTERIOR?','ingresos-servidor-publico','',9,18,19,1,1,1),(11,'BIENES INMUEBLES','bienes-inmuebles','',10,20,21,1,1,1),(12,'VEHÍCULOS','muebles-noregistrables','',11,22,23,1,1,1),(13,'BIENES MUEBLES','bienes-muebles','',12,24,25,1,1,1),(14,'INVERSIONES, CUENTAS BANCARIAS Y OTRO TIPO DE VALORES / ACTIVOS (SITUACION ACTUAL)','inversiones','',13,26,27,1,1,1),(15,'ADEUDOS/PASIVOS(SITUACION ACTUAL)','deudas','',14,28,29,1,1,1),(16,'PRESTAMO O COMODATO POR TERCEROS(SITUACIÓN ACTUAL)','prestamoComodato','',15,30,31,1,1,1),(17,'DECLARACION DE INTERESES','','',1,1,22,2,0,NULL),(18,'PARTICIPACION EN EMPRESAS, SOCIEDADES O ASOCIACIONES(HASTA LOS ULTIMOS 2 AÑOS)','socios-comerciales','',1,2,3,2,1,17),(19,'¿PARTICIPA EN LA TOMA DE DECISIONES DE ALGUNA DE ESTAS INSTITUCIONES ? (HASTA LOS 2 ÚLTIMOS AÑOS)','membresias','',2,4,5,2,1,17),(20,'APOYOS O BENEFICIOS PÚBLICOS (HASTA LOS 2 ÚLTIMOS AÑOS)','apoyos','',3,6,7,2,1,17),(21,'REPRESENTACIÓN (HASTA LOS 2 ÚLTIMOS AÑOS)','representacion-activa','',4,8,9,2,1,17),(22,'CLIENTES PRINCIPALES (HASTA LOS 2 ÚLTIMOS AÑOS)','clientes-principales','',5,10,11,2,1,17),(23,'BENEFICIOS PRIVADOS (HASTA LOS 2 ÚLTIMOS AÑOS)','beneficios-gratuitos','',6,12,13,2,1,17),(24,'FIDEICOMISOS (HASTA LOS 2 ÚLTIMOS AÑOS)','fideicomisos','',7,14,15,2,1,17),(25,'DECLARACION FISCAL','','',1,1,14,3,0,NULL),(26,'SUBIR ARCHIVOS','declaracion-fiscal','',1,2,3,3,1,25);
/*!40000 ALTER TABLE `declaracion_secciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_socioscomerciales`
--

DROP TABLE IF EXISTS `declaracion_socioscomerciales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_socioscomerciales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actividad_vinculante` varchar(255) NOT NULL,
  `tipo_vinculo` varchar(255) NOT NULL,
  `antiguedad_vinculo` varchar(255) NOT NULL,
  `rfc_entidad_vinculante` varchar(255) NOT NULL,
  `porcentaje_participacion` decimal(5,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `socio_infopersonalvar_id` int(11) NOT NULL,
  `tipoRelacion_id` int(11) DEFAULT NULL,
  `tipoParticipacion_id` int(11) DEFAULT NULL,
  `otraParticipacion` varchar(20) DEFAULT NULL,
  `recibeRemuneracion` tinyint(1) DEFAULT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `montoMensual` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_sociosco_declaraciones_id_10a954b5_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_sociosco_observaciones_id_35bc50c6_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_sociosco_socio_infopersonalva_d15f024c_fk_declaraci` (`socio_infopersonalvar_id`),
  KEY `declaracion_sociosco_tipoRelacion_id_f4e9806f_fk_declaraci` (`tipoRelacion_id`),
  KEY `declaracion_sociosco_tipoParticipacion_id_e90abc2d_fk_declaraci` (`tipoParticipacion_id`),
  KEY `declaracion_sociosco_moneda_id_6c366cd4_fk_declaraci` (`moneda_id`),
  KEY `declaracion_sociosco_cat_tipos_operacione_59bc084d_fk_declaraci` (`cat_tipos_operaciones_id`),
  CONSTRAINT `declaracion_sociosco_cat_tipos_operacione_59bc084d_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_sociosco_declaraciones_id_10a954b5_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_sociosco_moneda_id_6c366cd4_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_sociosco_observaciones_id_35bc50c6_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_sociosco_socio_infopersonalva_d15f024c_fk_declaraci` FOREIGN KEY (`socio_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_sociosco_tipoParticipacion_id_e90abc2d_fk_declaraci` FOREIGN KEY (`tipoParticipacion_id`) REFERENCES `declaracion_cattipopersona` (`id`),
  CONSTRAINT `declaracion_sociosco_tipoRelacion_id_f4e9806f_fk_declaraci` FOREIGN KEY (`tipoRelacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_socioscomerciales`
--

LOCK TABLES `declaracion_socioscomerciales` WRITE;
/*!40000 ALTER TABLE `declaracion_socioscomerciales` DISABLE KEYS */;
INSERT INTO `declaracion_socioscomerciales` VALUES (1,'Comerciante','','','JAER010101333',50.00,'2019-12-10 18:09:22.241869','2020-02-26 22:22:47.535241',1,28,13,NULL,17,NULL,0,NULL,NULL,1),(2,'Comerciante','Local','1 año','JAER010101333',50.00,'2019-12-24 19:24:37.286282','2019-12-24 19:24:37.286333',3,132,64,1,14,NULL,1,101,5200,1),(3,'Comerciante','','','JAER010101333',85.00,'2020-02-04 05:46:08.998403','2020-02-04 05:46:08.998477',9,288,110,16,16,NULL,1,101,5690,3),(4,'EMPRESA PRUEBA participacion','','','',NULL,'2020-02-10 19:44:45.798036','2020-02-10 19:51:02.901205',2,335,150,NULL,NULL,NULL,1,NULL,NULL,NULL),(5,'SYSTEM','','','SYYS010101256',45.00,'2020-02-21 22:25:45.032514','2020-02-21 22:25:45.032766',10,387,177,NULL,NULL,NULL,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `declaracion_socioscomerciales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaracion_sueldospublicos`
--

DROP TABLE IF EXISTS `declaracion_sueldospublicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_sueldospublicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfc` varchar(255) NOT NULL,
  `ingreso_bruto_anual` int(11) DEFAULT NULL,
  `duracion_dias` decimal(6,2) DEFAULT NULL,
  `duracion_meses` decimal(6,2) DEFAULT NULL,
  `duracion_anual` decimal(6,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `es_transaccion` tinyint(1) DEFAULT NULL,
  `fecha_transaccion` date DEFAULT NULL,
  `otro_ente` varchar(255) DEFAULT NULL,
  `cat_entes_publicos_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `ingreso_neto_mensual` int(11) DEFAULT NULL,
  `fecha_conclusion` date DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `ingreso_anio_anterior` tinyint(1) DEFAULT NULL,
  `nombre_ente_publico` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_sueldosp_cat_entes_publicos_i_d2529563_fk_declaraci` (`cat_entes_publicos_id`),
  KEY `declaracion_sueldosp_cat_monedas_id_048cef09_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_sueldosp_declaraciones_id_4e8596ed_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_sueldosp_observaciones_id_a09f8540_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_sueldosp_cat_entes_publicos_i_d2529563_fk_declaraci` FOREIGN KEY (`cat_entes_publicos_id`) REFERENCES `declaracion_catentespublicos` (`id`),
  CONSTRAINT `declaracion_sueldosp_cat_monedas_id_048cef09_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_sueldosp_declaraciones_id_4e8596ed_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_sueldosp_observaciones_id_a09f8540_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaracion_sueldospublicos`
--

LOCK TABLES `declaracion_sueldospublicos` WRITE;
/*!40000 ALTER TABLE `declaracion_sueldospublicos` DISABLE KEYS */;
INSERT INTO `declaracion_sueldospublicos` VALUES (6,'DAEF010101258',NULL,NULL,NULL,NULL,'2019-12-24 19:28:52.523009','2019-12-24 19:28:52.523119',NULL,NULL,NULL,3,101,3,136,455,NULL,NULL,0,NULL),(7,'SEAJ010101569',NULL,NULL,NULL,NULL,'2020-01-13 18:35:54.473388','2020-01-13 18:47:20.795673',NULL,NULL,NULL,3,101,1,157,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `declaracion_sueldospublicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2019-12-27 04:54:51.925308','12','InfoPersonalFija object (12)',1,'new through import_export',66,11),(2,'2019-12-27 04:55:42.858212','14','InfoPersonalFija object (14)',1,'new through import_export',66,11),(3,'2019-12-27 04:56:16.013242','16','InfoPersonalFija object (16)',1,'new through import_export',66,11),(4,'2019-12-27 04:56:53.634105','18','InfoPersonalFija object (18)',1,'new through import_export',66,11),(5,'2019-12-27 04:57:43.274459','18','InfoPersonalFija object (18)',3,'',66,11),(6,'2019-12-27 04:57:43.402401','16','InfoPersonalFija object (16)',3,'',66,11),(7,'2019-12-27 04:57:43.413455','14','InfoPersonalFija object (14)',3,'',66,11),(8,'2019-12-27 04:57:43.414976','12','InfoPersonalFija object (12)',3,'',66,11),(9,'2019-12-30 17:04:48.222558','13','ZAER010101MJCRSS56',1,'new through import_export',4,11),(10,'2020-01-29 17:06:27.499021','5','MOUR010101458',2,'[{\"changed\": {\"fields\": [\"password\"]}}]',4,11),(11,'2020-02-06 17:13:54.983090','20','PUER010101123',1,'new through import_export',4,11),(12,'2020-02-06 18:47:12.971878','22','FAEZ010101123',1,'new through import_export',4,11),(13,'2020-02-14 23:40:05.871987','24','PUER010101321',1,'new through import_export',4,11),(14,'2020-02-17 15:39:12.651237','26','PUER010101852',1,'new through import_export',4,11),(15,'2020-02-17 17:30:11.505020','28','PUER010101569',1,'new through import_export',4,11),(16,'2020-03-12 22:18:54.742163','2','DEAF010101256',2,'[{\"changed\": {\"fields\": [\"is_staff\"]}}]',4,11),(17,'2020-03-12 22:30:16.070034','2','DEAF010101256',2,'[{\"changed\": {\"fields\": [\"is_staff\"]}}]',4,11);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(7,'declaracion','activosbienes'),(83,'declaracion','apoyos'),(82,'declaracion','beneficiosespecie'),(81,'declaracion','beneficiosgratuitos'),(80,'declaracion','bienesinmuebles'),(79,'declaracion','bienesintangibles'),(78,'declaracion','bienesmuebles'),(77,'declaracion','bienespersonas'),(8,'declaracion','catactivobien'),(9,'declaracion','catambitoslaborales'),(84,'declaracion','catcamposobligatorios'),(10,'declaracion','catdocumentosobtenidos'),(11,'declaracion','catentespublicos'),(12,'declaracion','catentidadesfederativas'),(13,'declaracion','catestadosciviles'),(14,'declaracion','catestatusdeclaracion'),(15,'declaracion','catestatusestudios'),(16,'declaracion','catformasadquisiciones'),(17,'declaracion','catfuncionesprincipales'),(18,'declaracion','catgradosacademicos'),(19,'declaracion','catmonedas'),(90,'declaracion','catmotivobaja'),(85,'declaracion','catmunicipios'),(20,'declaracion','catnaturalezamembresia'),(21,'declaracion','catordenesgobierno'),(22,'declaracion','catpaises'),(23,'declaracion','catpoderes'),(92,'declaracion','catpuestos'),(24,'declaracion','catregimenesmatrimoniales'),(25,'declaracion','catsectoresindustria'),(26,'declaracion','cattipoparticipacion'),(27,'declaracion','cattipopersona'),(28,'declaracion','cattiposacreedores'),(29,'declaracion','cattiposactividad'),(30,'declaracion','cattiposadeudos'),(31,'declaracion','cattiposapoyos'),(32,'declaracion','cattiposbeneficios'),(33,'declaracion','cattiposbienes'),(34,'declaracion','cattiposdeclaracion'),(35,'declaracion','cattiposespecificosinversiones'),(36,'declaracion','cattiposfideicomisos'),(37,'declaracion','cattiposingresosvarios'),(38,'declaracion','cattiposinmuebles'),(39,'declaracion','cattiposinstituciones'),(88,'declaracion','cattiposinstrumentos'),(40,'declaracion','cattiposinversiones'),(41,'declaracion','cattiposmetales'),(42,'declaracion','cattiposmuebles'),(43,'declaracion','cattiposoperaciones'),(44,'declaracion','cattipospasivos'),(45,'declaracion','cattiposrelacionespersonales'),(46,'declaracion','cattiposrepresentaciones'),(47,'declaracion','cattipostitulares'),(48,'declaracion','cattipovia'),(49,'declaracion','cattitulartiposrelaciones'),(87,'declaracion','catunidadesmedida'),(50,'declaracion','catunidadestemporales'),(76,'declaracion','clientesprincipales'),(75,'declaracion','conyugedependientes'),(74,'declaracion','cuentasporcobrar'),(73,'declaracion','datoscurriculares'),(51,'declaracion','declaraciones'),(86,'declaracion','declaracionfiscal'),(72,'declaracion','deudasotros'),(52,'declaracion','domicilios'),(71,'declaracion','efectivometales'),(70,'declaracion','empresassociedades'),(69,'declaracion','encargos'),(68,'declaracion','experiencialaboral'),(67,'declaracion','fideicomisos'),(66,'declaracion','infopersonalfija'),(53,'declaracion','infopersonalvar'),(89,'declaracion','ingresosdeclaracion'),(65,'declaracion','ingresosvarios'),(64,'declaracion','inversiones'),(63,'declaracion','membresias'),(62,'declaracion','mueblesnoregistrables'),(61,'declaracion','nacionalidades'),(54,'declaracion','observaciones'),(60,'declaracion','otraspartes'),(91,'declaracion','prestamocomodato'),(59,'declaracion','representaciones'),(58,'declaracion','secciondeclaracion'),(57,'declaracion','secciones'),(56,'declaracion','socioscomerciales'),(55,'declaracion','sueldospublicos'),(6,'sessions','session'),(93,'sitio','declaracion_faqs'),(94,'sitio','sitio_personalizacion');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-12-06 18:24:40.900960'),(2,'auth','0001_initial','2019-12-06 18:24:42.789598'),(3,'admin','0001_initial','2019-12-06 18:24:42.907292'),(4,'admin','0002_logentry_remove_auto_add','2019-12-06 18:24:42.926233'),(5,'admin','0003_logentry_add_action_flag_choices','2019-12-06 18:24:42.947718'),(6,'contenttypes','0002_remove_content_type_name','2019-12-06 18:24:43.039908'),(7,'auth','0002_alter_permission_name_max_length','2019-12-06 18:24:43.052389'),(8,'auth','0003_alter_user_email_max_length','2019-12-06 18:24:43.075346'),(9,'auth','0004_alter_user_username_opts','2019-12-06 18:24:43.096054'),(10,'auth','0005_alter_user_last_login_null','2019-12-06 18:24:43.140556'),(11,'auth','0006_require_contenttypes_0002','2019-12-06 18:24:43.145917'),(12,'auth','0007_alter_validators_add_error_messages','2019-12-06 18:24:43.163622'),(13,'auth','0008_alter_user_username_max_length','2019-12-06 18:24:43.193574'),(14,'auth','0009_alter_user_last_name_max_length','2019-12-06 18:24:43.226352'),(15,'declaracion','0001_initial','2019-12-06 18:24:56.784853'),(16,'declaracion','0002_infopersonalfija_telefono','2019-12-06 18:24:56.895607'),(17,'declaracion','0003_infopersonalfija_puesto','2019-12-06 18:24:57.020183'),(18,'declaracion','0004_auto_20190403_1745','2019-12-06 18:24:57.278364'),(19,'declaracion','0005_auto_20190403_1846','2019-12-06 18:24:57.862070'),(20,'declaracion','0006_fideicomisos_nombre_fideicomiso','2019-12-06 18:24:58.023854'),(21,'declaracion','0007_fideicomisos_porcentaje','2019-12-06 18:24:58.324319'),(22,'declaracion','0008_auto_20190404_0735','2019-12-06 18:24:58.836488'),(23,'declaracion','0009_auto_20190404_0744','2019-12-06 18:24:59.358200'),(24,'declaracion','0010_auto_20190404_1900','2019-12-06 18:24:59.889733'),(25,'declaracion','0011_remove_bienesintangibles_propietario_registrado','2019-12-06 18:25:00.170139'),(26,'declaracion','0012_cuentasporcobrar_num_registro','2019-12-06 18:25:00.451694'),(27,'declaracion','0004_declaraciones_cat_estatus','2019-12-06 18:25:00.677618'),(28,'declaracion','0005_declaraciones_avance','2019-12-06 18:25:00.836633'),(29,'declaracion','0013_merge_20190404_2156','2019-12-06 18:25:00.840703'),(30,'declaracion','0014_auto_20190405_0013','2019-12-06 18:25:01.285912'),(31,'declaracion','0015_auto_20190405_0203','2019-12-06 18:25:01.417982'),(32,'declaracion','0016_auto_20190405_0208','2019-12-06 18:25:01.558030'),(33,'declaracion','0017_catcamposobligatorios','2019-12-06 18:25:01.857681'),(34,'declaracion','0018_catcamposobligatorios_es_principal','2019-12-06 18:25:01.905659'),(35,'declaracion','0019_auto_20190407_2203','2019-12-06 18:25:01.928352'),(36,'declaracion','0020_auto_20190408_1711','2019-12-06 18:25:02.292613'),(37,'declaracion','0021_remove_membresias_tiene_apoyos','2019-12-06 18:25:02.461438'),(38,'declaracion','0022_conyugedependientes_tiene_apoyos','2019-12-06 18:25:02.650884'),(39,'declaracion','0023_auto_20190408_2105','2019-12-06 18:25:03.232007'),(40,'declaracion','0024_membresias_monto','2019-12-06 18:25:03.581620'),(41,'declaracion','0025_auto_20190412_1732','2019-12-06 18:25:04.145962'),(42,'declaracion','0026_apoyos_declaraciones','2019-12-06 18:25:04.624490'),(43,'declaracion','0027_auto_20190418_0525','2019-12-06 18:25:05.165180'),(44,'declaracion','0028_auto_20190422_1901','2019-12-06 18:25:05.317596'),(45,'declaracion','0029_conyugedependientes_otra_relacion_familiar','2019-12-06 18:25:05.584763'),(46,'declaracion','0030_auto_20190423_0016','2019-12-06 18:25:05.924694'),(47,'declaracion','0031_auto_20190423_1902','2019-12-06 18:25:06.101670'),(48,'declaracion','0032_declaraciones_sello','2019-12-06 18:25:06.237304'),(49,'declaracion','0033_auto_20190424_0013','2019-12-06 18:25:06.375312'),(50,'sessions','0001_initial','2019-12-06 18:25:06.409603'),(51,'declaracion','0034_catmunicipios','2019-12-06 18:53:20.478693'),(52,'declaracion','0002_auto_20191206_1905','2019-12-06 19:05:49.019926'),(53,'declaracion','0003_auto_20191206_1908','2019-12-06 19:09:59.389922'),(54,'declaracion','0004_auto_20191206_1924','2019-12-06 19:25:06.081455'),(55,'declaracion','0005_auto_20191206_2243','2019-12-06 22:43:22.899342'),(56,'declaracion','0006_experiencialaboral_tipooperacion','2019-12-09 15:27:00.685097'),(57,'declaracion','0007_auto_20191209_1606','2019-12-09 16:06:51.812097'),(58,'declaracion','0008_auto_20191209_1619','2019-12-09 16:19:47.581301'),(59,'declaracion','0009_remove_experiencialaboral_nombreempresasociedadasociacion','2019-12-09 16:20:15.067345'),(60,'declaracion','0010_remove_conyugedependientes_tiene_apoyos','2019-12-09 18:14:28.107896'),(61,'declaracion','0011_remove_conyugedependientes_proveedor_contratista','2019-12-09 18:14:52.839047'),(62,'declaracion','0012_remove_conyugedependientes_intereses_comunes','2019-12-09 18:15:23.704672'),(63,'declaracion','0013_remove_conyugedependientes_cabildeo_sector','2019-12-09 18:15:43.596821'),(64,'declaracion','0014_auto_20191209_1825','2019-12-09 18:26:06.566460'),(65,'declaracion','0015_conyugedependientes_proveedor_contratista','2019-12-09 18:56:13.824179'),(66,'declaracion','0016_conyugedependientes_actividadlaboral','2019-12-09 19:03:36.360758'),(67,'declaracion','0017_conyugedependientes_actividadlaboralsector','2019-12-09 19:22:46.180405'),(68,'declaracion','0018_encargos_salariomensualneto','2019-12-09 23:40:16.442499'),(69,'declaracion','0019_auto_20191210_1545','2019-12-10 15:46:20.862770'),(70,'declaracion','0020_encargos_nombreempresasociedadasociacion','2019-12-10 15:48:27.426709'),(71,'declaracion','0021_encargos_moneda','2019-12-10 15:54:35.343617'),(72,'declaracion','0022_socioscomerciales_tiporelacion','2019-12-10 18:24:08.572417'),(73,'declaracion','0023_socioscomerciales_tipoparticipacion','2019-12-10 21:16:01.508236'),(74,'declaracion','0024_socioscomerciales_otraparticipacion','2019-12-10 21:56:55.945707'),(75,'declaracion','0025_socioscomerciales_reciberemuneracion','2019-12-10 22:19:12.469957'),(76,'declaracion','0026_auto_20191210_2237','2019-12-10 22:37:28.337797'),(77,'declaracion','0027_membresias_tiporelacion','2019-12-10 23:12:44.580448'),(78,'declaracion','0028_membresias_rfc','2019-12-10 23:52:11.528541'),(79,'declaracion','0029_membresias_moneda','2019-12-10 23:56:23.424886'),(80,'declaracion','0030_apoyos_forma_recepcion','2019-12-11 18:13:11.775088'),(81,'declaracion','0031_apoyos_monto_apoyo_mensual','2019-12-11 18:20:15.244945'),(82,'declaracion','0032_auto_20191211_1825','2019-12-11 18:25:33.493308'),(83,'declaracion','0033_representaciones_cat_tipos_relaciones_personales','2019-12-11 19:34:13.795408'),(84,'declaracion','0034_representaciones_cat_moneda','2019-12-11 19:59:28.898899'),(85,'declaracion','0035_infopersonalvar_rfc_negocio','2019-12-12 18:18:10.267811'),(86,'declaracion','0036_clientesprincipales_realizaactividadlucrativa','2019-12-12 18:22:26.764348'),(87,'declaracion','0037_clientesprincipales_cat_tipos_relaciones_personales','2019-12-12 18:32:01.263641'),(88,'declaracion','0038_auto_20191212_1839','2019-12-12 18:39:23.770594'),(89,'declaracion','0039_auto_20191212_1907','2019-12-12 19:07:13.319875'),(90,'declaracion','0040_beneficiosgratuitos_moneda','2019-12-12 19:21:15.447470'),(91,'declaracion','0041_beneficiosgratuitos_cat_tipos_relaciones_personales','2019-12-12 19:44:34.684584'),(92,'declaracion','0042_auto_20191212_2008','2019-12-12 20:08:59.671270'),(93,'declaracion','0043_remove_beneficiosgratuitos_es_fisica','2019-12-12 21:10:51.209070'),(94,'declaracion','0044_beneficiosgratuitos_tipo_persona','2019-12-12 21:14:28.429329'),(95,'declaracion','0045_auto_20191212_2123','2019-12-12 21:23:55.228920'),(96,'declaracion','0046_auto_20191212_2131','2019-12-12 21:32:02.969112'),(97,'declaracion','0047_auto_20191212_2148','2019-12-12 21:48:42.315950'),(98,'declaracion','0048_auto_20191213_1533','2019-12-13 15:34:37.758059'),(99,'declaracion','0049_sueldospublicos_ingreso_neto_mensual','2019-12-13 18:09:25.630214'),(100,'declaracion','0050_ingresosvarios_ingreso_neto_mensual','2019-12-13 19:40:51.889003'),(101,'declaracion','0051_auto_20191214_0412','2019-12-14 04:13:04.678521'),(102,'declaracion','0052_declaracionfiscal','2019-12-16 02:41:03.149955'),(103,'declaracion','0002_declaracionfiscal_declaraciones','2019-12-16 17:41:34.216289'),(104,'declaracion','0003_auto_20191216_1909','2019-12-16 19:10:04.968661'),(105,'declaracion','0002_catunidadesmedida','2019-12-16 22:55:21.950107'),(106,'declaracion','0003_auto_20191218_1955','2019-12-18 19:55:34.612012'),(107,'declaracion','0004_auto_20191218_1956','2019-12-18 19:56:43.638800'),(108,'declaracion','0005_datoscurriculares_cat_tipos_operaciones','2019-12-19 19:32:15.002088'),(109,'declaracion','0006_encargos_cat_tipos_operaciones','2019-12-19 19:36:34.011205'),(110,'declaracion','0007_auto_20191219_1940','2019-12-19 19:40:52.231892'),(111,'declaracion','0008_auto_20191219_1941','2019-12-19 19:41:48.097925'),(112,'declaracion','0009_conyugedependientes_cat_tipos_operaciones','2019-12-19 20:00:23.912295'),(113,'declaracion','0010_socioscomerciales_cat_tipos_operaciones','2019-12-19 20:17:02.854164'),(114,'declaracion','0011_membresias_cat_tipos_operaciones','2019-12-19 20:22:27.134119'),(115,'declaracion','0012_apoyos_cat_tipos_operaciones','2019-12-19 20:34:00.247378'),(116,'declaracion','0013_representaciones_cat_tipos_operaciones','2019-12-19 20:37:05.367151'),(117,'declaracion','0014_clientesprincipales_cat_tipos_operaciones','2019-12-19 20:42:38.263016'),(118,'declaracion','0015_beneficiosgratuitos_cat_tipos_operaciones','2019-12-19 20:49:44.191145'),(119,'declaracion','0016_ingresosvarios_tipo_instrumento','2019-12-20 16:23:05.146853'),(120,'declaracion','0017_auto_20191220_1635','2019-12-20 16:35:17.465271'),(121,'declaracion','0018_cattiposinstrumentos','2019-12-20 16:50:12.487849'),(122,'declaracion','0019_auto_20191220_1712','2019-12-20 17:14:24.197175'),(123,'declaracion','0020_auto_20191220_1904','2019-12-20 19:06:07.351106'),(124,'declaracion','0021_auto_20191220_2040','2019-12-20 20:41:03.844793'),(125,'declaracion','0022_remove_ingresosvarios_servidor_publico_anterior','2019-12-20 20:47:03.633709'),(126,'declaracion','0023_auto_20191220_2102','2019-12-20 22:27:00.278389'),(127,'declaracion','0024_auto_20191220_2246','2019-12-20 22:46:37.573228'),(128,'declaracion','0025_auto_20191220_2247','2019-12-20 22:47:34.309021'),(129,'declaracion','0026_auto_20191223_1538','2019-12-23 15:38:23.080462'),(130,'declaracion','0027_ingresosvarios_cat_tipos_bienes','2019-12-23 15:48:53.865995'),(131,'declaracion','0028_auto_20191223_1607','2019-12-23 16:07:31.347713'),(132,'declaracion','0029_auto_20191223_1608','2019-12-23 16:08:17.613174'),(133,'declaracion','0030_sueldospublicos_ingreso_anio_anterior','2019-12-23 16:09:37.539276'),(134,'declaracion','0031_fideicomisos_rfc','2019-12-23 16:20:37.531255'),(135,'declaracion','0032_auto_20191223_2000','2019-12-23 20:00:33.553578'),(136,'declaracion','0033_catmunicipios_cat_entidades_federativas','2019-12-23 23:14:21.518134'),(137,'declaracion','0002_auto_20191227_2042','2019-12-27 20:42:10.690043'),(138,'declaracion','0003_auto_20191230_1542','2019-12-30 15:43:07.102702'),(139,'declaracion','0004_auto_20191230_1546','2019-12-30 15:47:00.156808'),(140,'declaracion','0005_auto_20200109_1737','2020-01-09 17:37:57.872877'),(141,'declaracion','0006_auto_20200109_1801','2020-01-09 18:01:43.569238'),(142,'declaracion','0007_catcamposobligatorios_es_publico','2020-01-15 17:09:47.625848'),(143,'declaracion','0008_auto_20200115_2136','2020-01-15 21:37:14.897991'),(144,'auth','0010_alter_group_name_max_length','2020-01-21 16:50:36.213936'),(145,'auth','0011_update_proxy_permissions','2020-01-21 16:50:36.435094'),(146,'declaracion','0009_auto_20200122_1832','2020-01-22 18:32:14.724024'),(147,'declaracion','0010_auto_20200122_1847','2020-01-22 18:47:38.001805'),(148,'declaracion','0011_auto_20200122_1856','2020-01-22 18:56:20.239214'),(149,'declaracion','0012_auto_20200122_2239','2020-01-22 22:39:42.104526'),(150,'declaracion','0013_auto_20200122_2245','2020-01-22 22:45:54.720720'),(151,'declaracion','0014_auto_20200122_2248','2020-01-22 22:48:15.336213'),(152,'declaracion','0015_auto_20200122_2303','2020-01-22 23:03:13.470103'),(153,'declaracion','0016_auto_20200122_2308','2020-01-22 23:08:30.389903'),(154,'declaracion','0017_auto_20200124_0038','2020-01-24 00:38:45.061000'),(155,'declaracion','0018_auto_20200127_0445','2020-01-27 04:45:59.736988'),(156,'declaracion','0019_auto_20200127_1830','2020-01-27 18:31:21.663829'),(157,'declaracion','0020_auto_20200127_1837','2020-01-27 18:37:42.397343'),(158,'declaracion','0021_auto_20200127_1950','2020-01-27 19:50:56.291168'),(159,'declaracion','0022_auto_20200127_2018','2020-01-27 20:18:25.497679'),(160,'declaracion','0002_auto_20200129_2133','2020-01-29 21:48:14.208623'),(161,'declaracion','0003_ingresosdeclaracion','2020-01-30 15:32:16.675201'),(162,'declaracion','0004_ingresosdeclaracion_otro_tipo_instrumento','2020-01-30 17:42:51.964600'),(163,'declaracion','0005_auto_20200130_1823','2020-01-30 18:24:00.693217'),(164,'declaracion','0006_auto_20200130_2129','2020-01-30 21:29:36.409429'),(165,'declaracion','0007_ingresosdeclaracion_tipo_ingreso','2020-01-30 23:26:45.830564'),(166,'declaracion','0008_auto_20200130_2350','2020-01-30 23:50:31.180681'),(167,'declaracion','0009_catmotivobaja','2020-01-31 21:47:38.278105'),(168,'declaracion','0010_auto_20200131_2151','2020-01-31 21:51:36.157989'),(169,'declaracion','0011_auto_20200201_2036','2020-02-01 20:36:48.453843'),(170,'declaracion','0012_conyugedependientes_es_extranjero','2020-02-01 22:19:28.384059'),(171,'declaracion','0013_auto_20200203_2133','2020-02-03 21:34:04.077014'),(172,'declaracion','0014_fideicomisos_cat_tipo_participacion','2020-02-04 00:13:25.632208'),(173,'declaracion','0015_auto_20200204_0155','2020-02-04 01:55:30.862782'),(174,'declaracion','0016_mueblesnoregistrables_cat_tipos_relaciones_personales','2020-02-04 02:13:58.000886'),(175,'declaracion','0017_remove_mueblesnoregistrables_cat_tipos_relaciones_personales','2020-02-04 02:23:43.309892'),(176,'declaracion','0002_auto_20200204_2042','2020-02-04 20:43:58.324667'),(177,'declaracion','0003_auto_20200205_1633','2020-02-05 16:35:46.228974'),(178,'declaracion','0004_auto_20200205_1908','2020-02-05 19:08:58.666989'),(179,'declaracion','0002_remove_inversiones_cat_unidades_temporales','2020-02-05 22:14:11.471421'),(180,'declaracion','0003_catpuestos','2020-02-05 22:20:25.281077'),(181,'declaracion','0004_encargos_nombre_ente_publico','2020-02-05 22:47:05.526777'),(182,'declaracion','0005_auto_20200206_1539','2020-02-06 15:40:05.883372'),(183,'declaracion','0006_auto_20200206_1949','2020-02-06 19:50:11.552703'),(184,'declaracion','0007_bienesmuebles_forma_pago','2020-02-07 03:07:06.451853'),(185,'declaracion','0008_bienesmuebles_cat_motivo_baja','2020-02-07 03:09:53.109053'),(186,'declaracion','0009_inversiones_campo_prueba','2020-02-07 03:20:00.593691'),(187,'declaracion','0010_auto_20200207_0319','2020-02-07 03:20:01.911443'),(188,'declaracion','0011_auto_20200207_0320','2020-02-07 03:20:45.075759'),(189,'declaracion','0012_inversiones_cat_unidades_temporales','2020-02-07 03:31:17.960666'),(190,'declaracion','0013_auto_20200207_0442','2020-02-07 04:42:27.598685'),(191,'declaracion','0014_encargos_cat_puestos','2020-02-07 19:30:28.614906'),(192,'declaracion','0015_catpuestos_puesto','2020-02-07 19:35:37.728913'),(193,'declaracion','0016_auto_20200210_1930','2020-02-10 19:30:35.171441'),(194,'declaracion','0017_auto_20200210_1940','2020-02-10 19:40:09.699411'),(195,'declaracion','0018_apoyos_cat_tipos_relaciones_personales','2020-02-10 20:16:26.045244'),(196,'declaracion','0019_auto_20200210_2252','2020-02-10 22:52:08.302134'),(197,'sitio','0001_initial','2020-02-17 20:28:34.830078'),(198,'declaracion','0020_auto_20200218_2041','2020-02-18 20:42:01.458578'),(199,'declaracion','0021_auto_20200219_1636','2020-02-19 16:41:48.072474'),(200,'declaracion','0022_prestamocomodato_otro_tipo_relacion','2020-02-19 17:03:26.887074'),(201,'declaracion','0023_beneficiosgratuitos_otorgante_infopersonalvar','2020-02-19 17:27:51.276968'),(202,'declaracion','0024_auto_20200219_2017','2020-02-19 20:18:04.037117'),(203,'declaracion','0025_declaraciones_datos_publicos','2020-02-27 17:16:18.311718');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0dkd0oakwcm9kzyyq7j92ucddjpppjhv','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-02-25 16:37:43.386971'),('0o08jxur7zlg1lhmv9zusuo1inqa9v07','NTk0YzM5OWJlYjM2ZTFlZjQzZGRjYzhlNzMzYmVlMDZkZGZlYjU0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzgwNDIyODQxMmU1YmRiZjM1NGFjOWYzNGIzZTVkYjg4OTUzMmFmNSJ9','2020-03-04 17:41:27.905313'),('2997tzf29kztcayutosogku4t4ajo5pn','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-03-12 18:14:34.394862'),('2b56m68odvj0zdyoe3d8gwtfy9u4tn4k','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-03-31 18:32:11.570633'),('2btbkiqmci5ofh3rpsa4saexxj8st3b7','MmNmMTQxYjI1MWJjNGI1ODQwZTk3MjIxMzViYjI0M2IzNzY0ZTdjYzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiZTRhZWMxYzUzZTljOTk4YmMyNDIzZTMzMTFiYTNhYTJiZDRiOTk1In0=','2019-12-27 22:30:42.455169'),('5hfq3nknt2fhdob2tec1sez36mpuhgez','NmQ2M2M5NzIxYjJhNWNjNTJjZDA3OTQ2NzdjM2E3YTM2N2ZjZDBlMzp7Il9hdXRoX3VzZXJfaWQiOiIyNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjRiZjU3MTgwOGI2MmQwZjBkZWE4NmQzYTQ1OTJjYmU1ODlhNWFiMyJ9','2020-03-10 23:00:38.778736'),('6aa3eoaz0xxrqhwjgtbnqxh63yr5kpfk','NTk0YzM5OWJlYjM2ZTFlZjQzZGRjYzhlNzMzYmVlMDZkZGZlYjU0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzgwNDIyODQxMmU1YmRiZjM1NGFjOWYzNGIzZTVkYjg4OTUzMmFmNSJ9','2020-03-06 16:43:41.785209'),('7btejbl1h361xhq4w132x4k7m7qfqn43','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-03-23 19:12:17.609721'),('8hdp4emwkzfzy7ulqychrypb7515570r','YjBkNWVlYmU3ODBjZDQxMWM3Y2JhNWMxNGVhNzc1NDQ5NjcyZGY0Mjp7Il9hdXRoX3VzZXJfaWQiOiI5IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyMWQ2N2Y5NmQ0ZmQyNDRiYWExMGNhZmFhMzRkNTM0ZGQ1YjhiOTk1In0=','2020-01-29 16:05:54.916954'),('8ptjmz6kexme4xsqk5bfas8dcu0pg5u1','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-03-31 22:15:53.604350'),('8ql0vd9i6bc5ncdpwg6wzn11bp6aorul','NmQ2M2M5NzIxYjJhNWNjNTJjZDA3OTQ2NzdjM2E3YTM2N2ZjZDBlMzp7Il9hdXRoX3VzZXJfaWQiOiIyNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjRiZjU3MTgwOGI2MmQwZjBkZWE4NmQzYTQ1OTJjYmU1ODlhNWFiMyJ9','2020-03-03 17:04:01.632482'),('dh7x1kry0icdlsfvwxpyu5ytzelvf1u0','ZGZlZDM4ZjMzNzFjMDBjMzQxYzcxNjQ5MjMwNmRhOWJhMTdjMmQ0ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4ZjViZWRhY2YwNjcxZjQzMTIzYjg2NDU2MWQ3N2M4ZDY3ZjNhYTJiIn0=','2020-01-02 19:27:20.220081'),('dihxrvxndu2h7u022t2i2wib6mirn66d','YjRmM2ExZWJiZDIzZDI1Njk3OGViOTdhNmY4MGFhY2UwYTA3ZDBjNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OWEyY2RlYmUzMmQ2NmU1NDM0Zjc4OTVhZWY5ZTFkN2JkMDRlYjZlIn0=','2020-02-09 00:59:41.013961'),('im8lgwyqn6js10r9xz8gdwc8m2s0te7h','ZGZlZDM4ZjMzNzFjMDBjMzQxYzcxNjQ5MjMwNmRhOWJhMTdjMmQ0ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4ZjViZWRhY2YwNjcxZjQzMTIzYjg2NDU2MWQ3N2M4ZDY3ZjNhYTJiIn0=','2020-01-29 18:58:55.237979'),('iv7mvvwz6iax1080iemtxg8jypztd6xl','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-03-19 22:44:56.773094'),('izp530jrjbztyrs2bvgmsq2e508vjyrs','NmQ2M2M5NzIxYjJhNWNjNTJjZDA3OTQ2NzdjM2E3YTM2N2ZjZDBlMzp7Il9hdXRoX3VzZXJfaWQiOiIyNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjRiZjU3MTgwOGI2MmQwZjBkZWE4NmQzYTQ1OTJjYmU1ODlhNWFiMyJ9','2020-03-05 15:24:41.665815'),('jhnvlf1wme64jdv1e2uvnajuft0vij2o','NmQ2M2M5NzIxYjJhNWNjNTJjZDA3OTQ2NzdjM2E3YTM2N2ZjZDBlMzp7Il9hdXRoX3VzZXJfaWQiOiIyNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjRiZjU3MTgwOGI2MmQwZjBkZWE4NmQzYTQ1OTJjYmU1ODlhNWFiMyJ9','2020-03-03 17:16:50.609810'),('jubixj5ry7201u9rlym4opoxt888clts','NTk0YzM5OWJlYjM2ZTFlZjQzZGRjYzhlNzMzYmVlMDZkZGZlYjU0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzgwNDIyODQxMmU1YmRiZjM1NGFjOWYzNGIzZTVkYjg4OTUzMmFmNSJ9','2020-03-12 23:06:18.131265'),('kuazxuyf0msqd4s43gq887p8so6hnpbj','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-03-19 19:35:27.625343'),('lv8ew4w11i2atesve3w76yb0chu0srrg','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-03-13 15:58:44.644188'),('medcsn8d2c7yhsskn8bsoa2eq2juppso','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-02-20 21:30:46.537903'),('nejflclp395v5ragp1uvefbo5plcfuzl','NmQ2M2M5NzIxYjJhNWNjNTJjZDA3OTQ2NzdjM2E3YTM2N2ZjZDBlMzp7Il9hdXRoX3VzZXJfaWQiOiIyNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjRiZjU3MTgwOGI2MmQwZjBkZWE4NmQzYTQ1OTJjYmU1ODlhNWFiMyJ9','2020-03-11 18:00:33.916344'),('nogca2hxf3lgwo31b1xasj876rh744n7','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-04-01 16:53:51.404217'),('nsmm3ruoj85dye2wqp2qmuhwevwxykp0','ZGZlZDM4ZjMzNzFjMDBjMzQxYzcxNjQ5MjMwNmRhOWJhMTdjMmQ0ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4ZjViZWRhY2YwNjcxZjQzMTIzYjg2NDU2MWQ3N2M4ZDY3ZjNhYTJiIn0=','2020-01-23 16:54:49.357413'),('ryrtkkf3buyl4aw96bf0old79hmg7hlx','ZGZlZDM4ZjMzNzFjMDBjMzQxYzcxNjQ5MjMwNmRhOWJhMTdjMmQ0ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4ZjViZWRhY2YwNjcxZjQzMTIzYjg2NDU2MWQ3N2M4ZDY3ZjNhYTJiIn0=','2019-12-30 17:05:05.465942'),('umwlyg4xvssnb8x0nxa3t4s4rl26riuc','ZGZlZDM4ZjMzNzFjMDBjMzQxYzcxNjQ5MjMwNmRhOWJhMTdjMmQ0ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4ZjViZWRhY2YwNjcxZjQzMTIzYjg2NDU2MWQ3N2M4ZDY3ZjNhYTJiIn0=','2020-01-29 18:48:48.000233'),('vsdmca7iyzr1khah6aqopypj28ribdxg','YTc1NDQ0ODk5YzczYTkwODQ0M2E4OTNkZjBiMzE5M2RiMTRjN2RmNDp7Il9hdXRoX3VzZXJfaWQiOiI5IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJjYzU4NWEwZjYwNGZiMDgwNDExZDM1MGUyNjBmZTg5ZTFmMjk0Y2ZjIn0=','2020-03-17 22:47:04.021821'),('w9gosc1eaeu84if05hoss78pnikiscay','NTk0YzM5OWJlYjM2ZTFlZjQzZGRjYzhlNzMzYmVlMDZkZGZlYjU0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYzgwNDIyODQxMmU1YmRiZjM1NGFjOWYzNGIzZTVkYjg4OTUzMmFmNSJ9','2020-03-11 22:35:38.550050'),('z40u48sagk0jfo31463hlld5jcno8vhi','ZDNmODgyMWU5YzcxZjgyNTkzOTUwNjFiZTc4Y2YxMmRhZWEyODJjZTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkODRkZTBmM2EzNjYzNDc2MGE0OTk5MzFiYzk4MmM3MzJkZTJjNTU5In0=','2020-02-24 15:47:38.909266'),('z9jhrh8p16ui26go12sq6xob3ia58oi8','ZGZlZDM4ZjMzNzFjMDBjMzQxYzcxNjQ5MjMwNmRhOWJhMTdjMmQ0ODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4ZjViZWRhY2YwNjcxZjQzMTIzYjg2NDU2MWQ3N2M4ZDY3ZjNhYTJiIn0=','2020-01-06 17:33:03.158534');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitio_declaracion_faqs`
--

DROP TABLE IF EXISTS `sitio_declaracion_faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitio_declaracion_faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden` int(11) NOT NULL,
  `pregunta` varchar(3000) NOT NULL,
  `respuesta` varchar(3000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitio_declaracion_faqs`
--

LOCK TABLES `sitio_declaracion_faqs` WRITE;
/*!40000 ALTER TABLE `sitio_declaracion_faqs` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitio_declaracion_faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitio_sitio_personalizacion`
--

DROP TABLE IF EXISTS `sitio_sitio_personalizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitio_sitio_personalizacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_institucion` varchar(300) NOT NULL,
  `siglas_sistema` varchar(30) NOT NULL,
  `bg_color` varchar(50) NOT NULL,
  `font_color_primary` varchar(50) NOT NULL,
  `color_primary` varchar(50) NOT NULL,
  `color_secondary` varchar(50) NOT NULL,
  `color_success` varchar(50) NOT NULL,
  `color_info` varchar(50) NOT NULL,
  `color_warning` varchar(50) NOT NULL,
  `color_danger` varchar(50) NOT NULL,
  `color_light` varchar(50) NOT NULL,
  `color_dark` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitio_sitio_personalizacion`
--

LOCK TABLES `sitio_sitio_personalizacion` WRITE;
/*!40000 ALTER TABLE `sitio_sitio_personalizacion` DISABLE KEYS */;
INSERT INTO `sitio_sitio_personalizacion` VALUES (1,'Secretaría ejecutiva','SEAJAL','000','000','000','000','000','000','000','000','000','000');
/*!40000 ALTER TABLE `sitio_sitio_personalizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'declaracionesU'
--

--
-- Dumping routines for database 'declaracionesU'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-19  8:01:05
