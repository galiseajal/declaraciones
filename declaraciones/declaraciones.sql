-- MySQL dump 10.17  Distrib 10.3.17-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: declaraciones
-- ------------------------------------------------------
-- Server version	10.3.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=401 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_activosbienes`
--

DROP TABLE IF EXISTS `declaracion_activosbienes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_activosbienes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_activobien` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_activo_bien_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_activosb_cat_activo_bien_id_cff5986d_fk_declaraci` (`cat_activo_bien_id`),
  KEY `declaracion_activosb_declaraciones_id_a7d9ae97_fk_declaraci` (`declaraciones_id`),
  CONSTRAINT `declaracion_activosb_cat_activo_bien_id_cff5986d_fk_declaraci` FOREIGN KEY (`cat_activo_bien_id`) REFERENCES `declaracion_catactivobien` (`id`),
  CONSTRAINT `declaracion_activosb_declaraciones_id_a7d9ae97_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_apoyos`
--

DROP TABLE IF EXISTS `declaracion_apoyos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_apoyos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_programa` varchar(255) NOT NULL,
  `institucion_otorgante` varchar(255) NOT NULL,
  `otro_apoyo` varchar(255) NOT NULL,
  `valor_anual` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `forma_recepcion` varchar(20) DEFAULT NULL,
  `monto_apoyo_mensual` int(11) DEFAULT NULL,
  `especifiqueApoyo` varchar(255) DEFAULT NULL,
  `beneficiario_infopersonalvar_id` int(11) NOT NULL,
  `cat_ordenes_gobierno_id` int(11) DEFAULT NULL,
  `cat_tipos_apoyos_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_apoyos_beneficiario_infoper_732a138e_fk_declaraci` (`beneficiario_infopersonalvar_id`),
  KEY `declaracion_apoyos_cat_ordenes_gobierno_df1ffade_fk_declaraci` (`cat_ordenes_gobierno_id`),
  KEY `declaracion_apoyos_cat_tipos_apoyos_id_313b18d0_fk_declaraci` (`cat_tipos_apoyos_id`),
  KEY `declaracion_apoyos_cat_tipos_operacione_8cce0a3b_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_apoyos_declaraciones_id_e29aa2b1_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_apoyos_moneda_id_5033d60c_fk_declaraci` (`moneda_id`),
  KEY `declaracion_apoyos_observaciones_id_7ce16aaf_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_apoyos_cat_tipos_relaciones_512ddfd2_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  CONSTRAINT `declaracion_apoyos_beneficiario_infoper_732a138e_fk_declaraci` FOREIGN KEY (`beneficiario_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_apoyos_cat_ordenes_gobierno_df1ffade_fk_declaraci` FOREIGN KEY (`cat_ordenes_gobierno_id`) REFERENCES `declaracion_catordenesgobierno` (`id`),
  CONSTRAINT `declaracion_apoyos_cat_tipos_apoyos_id_313b18d0_fk_declaraci` FOREIGN KEY (`cat_tipos_apoyos_id`) REFERENCES `declaracion_cattiposapoyos` (`id`),
  CONSTRAINT `declaracion_apoyos_cat_tipos_operacione_8cce0a3b_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_apoyos_cat_tipos_relaciones_512ddfd2_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_apoyos_declaraciones_id_e29aa2b1_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_apoyos_moneda_id_5033d60c_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_apoyos_observaciones_id_7ce16aaf_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_beneficiosgratuitos`
--

DROP TABLE IF EXISTS `declaracion_beneficiosgratuitos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_beneficiosgratuitos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otros_beneficios` varchar(255) NOT NULL,
  `origen_beneficio` varchar(255) NOT NULL,
  `otro_sector` varchar(255) NOT NULL,
  `valor_beneficio` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `tipo_persona` varchar(255) DEFAULT NULL,
  `razon_social_otorgante` varchar(255) DEFAULT NULL,
  `rfc_otorgante` varchar(255) DEFAULT NULL,
  `forma_recepcion` varchar(20) DEFAULT NULL,
  `especifiqueBeneficio` varchar(255) DEFAULT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `cat_tipos_beneficios_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  `otorgante_infopersonalVar_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_benefici_cat_sectores_industr_cb9102ff_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_benefici_cat_tipos_beneficios_f60c0e0a_fk_declaraci` (`cat_tipos_beneficios_id`),
  KEY `declaracion_benefici_cat_tipos_operacione_03b8d265_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_benefici_cat_tipos_relaciones_ba3381be_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  KEY `declaracion_benefici_declaraciones_id_141bcf3e_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_benefici_moneda_id_16bdaed8_fk_declaraci` (`moneda_id`),
  KEY `declaracion_benefici_observaciones_id_8a095534_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_benefici_otorgante_infoperson_6da3d0c9_fk_declaraci` (`otorgante_infopersonalVar_id`),
  CONSTRAINT `declaracion_benefici_cat_sectores_industr_cb9102ff_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_benefici_cat_tipos_beneficios_f60c0e0a_fk_declaraci` FOREIGN KEY (`cat_tipos_beneficios_id`) REFERENCES `declaracion_cattiposbeneficios` (`id`),
  CONSTRAINT `declaracion_benefici_cat_tipos_operacione_03b8d265_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_benefici_cat_tipos_relaciones_ba3381be_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_benefici_declaraciones_id_141bcf3e_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_benefici_moneda_id_16bdaed8_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_benefici_observaciones_id_8a095534_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_benefici_otorgante_infoperson_6da3d0c9_fk_declaraci` FOREIGN KEY (`otorgante_infopersonalVar_id`) REFERENCES `declaracion_infopersonalvar` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_bienesinmuebles`
--

DROP TABLE IF EXISTS `declaracion_bienesinmuebles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_bienesinmuebles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `superficie_terreno` decimal(12,2) DEFAULT NULL,
  `superficie_construccion` decimal(12,2) DEFAULT NULL,
  `otro_titular` varchar(255) NOT NULL,
  `num_escritura_publica` varchar(255) NOT NULL,
  `num_registro_publico` varchar(255) NOT NULL,
  `folio_real` varchar(255) NOT NULL,
  `fecha_contrato_compra` date DEFAULT NULL,
  `otra_forma` varchar(255) NOT NULL,
  `fecha_adquisicion` date DEFAULT NULL,
  `precio_adquisicion` decimal(12,2) DEFAULT NULL,
  `valor_catastral` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otra_operacion` varchar(255) DEFAULT NULL,
  `otro_inmueble` varchar(255) DEFAULT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_formas_adquisiciones_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_tipos_inmuebles_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_titulares_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `domicilios_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `forma_pago` varchar(255) NOT NULL,
  `valor_conforme_a` varchar(255) DEFAULT NULL,
  `cat_motivo_baja_id` int(11) DEFAULT NULL,
  `otro_motivo` varchar(255) NOT NULL,
  `unidad_medida_construccion` varchar(255) DEFAULT NULL,
  `unidad_medida_terreno` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_bienesin_activos_bienes_id_4ce7b103_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_bienesin_cat_formas_adquisici_0f66ea20_fk_declaraci` (`cat_formas_adquisiciones_id`),
  KEY `declaracion_bienesin_cat_monedas_id_74348361_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_bienesin_cat_tipos_inmuebles__e7713b8e_fk_declaraci` (`cat_tipos_inmuebles_id`),
  KEY `declaracion_bienesin_cat_tipos_operacione_5251e561_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_bienesin_cat_tipos_titulares__000810d0_fk_declaraci` (`cat_tipos_titulares_id`),
  KEY `declaracion_bienesin_declaraciones_id_d3bd4a00_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_bienesin_domicilios_id_50855758_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_bienesin_observaciones_id_8deaca1c_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_bienesin_cat_motivo_baja_id_71062e70_fk_declaraci` (`cat_motivo_baja_id`),
  CONSTRAINT `declaracion_bienesin_activos_bienes_id_4ce7b103_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_formas_adquisici_0f66ea20_fk_declaraci` FOREIGN KEY (`cat_formas_adquisiciones_id`) REFERENCES `declaracion_catformasadquisiciones` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_monedas_id_74348361_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_motivo_baja_id_71062e70_fk_declaraci` FOREIGN KEY (`cat_motivo_baja_id`) REFERENCES `declaracion_catmotivobaja` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_tipos_inmuebles__e7713b8e_fk_declaraci` FOREIGN KEY (`cat_tipos_inmuebles_id`) REFERENCES `declaracion_cattiposinmuebles` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_tipos_operacione_5251e561_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_bienesin_cat_tipos_titulares__000810d0_fk_declaraci` FOREIGN KEY (`cat_tipos_titulares_id`) REFERENCES `declaracion_cattipostitulares` (`id`),
  CONSTRAINT `declaracion_bienesin_declaraciones_id_d3bd4a00_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_bienesin_domicilios_id_50855758_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_bienesin_observaciones_id_8deaca1c_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_bienesmuebles`
--

DROP TABLE IF EXISTS `declaracion_bienesmuebles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_bienesmuebles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otro_tipo_mueble` varchar(255) NOT NULL,
  `otro_titular` varchar(255) NOT NULL,
  `otra_forma` varchar(255) NOT NULL,
  `fecha_adquisicion` date DEFAULT NULL,
  `precio_adquisicion` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  `cat_formas_adquisiciones_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipos_muebles_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_titulares_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `cat_motivo_baja_id` int(11) DEFAULT NULL,
  `forma_pago` varchar(255) NOT NULL,
  `descripcion_bien` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_bienesmu_activos_bienes_id_d26b3ee2_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_bienesmu_cat_entidades_federa_eb2a8206_fk_declaraci` (`cat_entidades_federativas_id`),
  KEY `declaracion_bienesmu_cat_formas_adquisici_fcd98d03_fk_declaraci` (`cat_formas_adquisiciones_id`),
  KEY `declaracion_bienesmu_cat_monedas_id_139584b5_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_bienesmu_cat_paises_id_3a63262a_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_bienesmu_cat_tipos_muebles_id_80b2d144_fk_declaraci` (`cat_tipos_muebles_id`),
  KEY `declaracion_bienesmu_cat_tipos_operacione_716f92b4_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_bienesmu_cat_tipos_titulares__8d6bb54f_fk_declaraci` (`cat_tipos_titulares_id`),
  KEY `declaracion_bienesmu_declaraciones_id_4ca011d8_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_bienesmu_observaciones_id_be05200e_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_bienesmu_cat_motivo_baja_id_edfecb9a_fk_declaraci` (`cat_motivo_baja_id`),
  CONSTRAINT `declaracion_bienesmu_activos_bienes_id_d26b3ee2_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_entidades_federa_eb2a8206_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_formas_adquisici_fcd98d03_fk_declaraci` FOREIGN KEY (`cat_formas_adquisiciones_id`) REFERENCES `declaracion_catformasadquisiciones` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_monedas_id_139584b5_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_motivo_baja_id_edfecb9a_fk_declaraci` FOREIGN KEY (`cat_motivo_baja_id`) REFERENCES `declaracion_catmotivobaja` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_paises_id_3a63262a_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_tipos_muebles_id_80b2d144_fk_declaraci` FOREIGN KEY (`cat_tipos_muebles_id`) REFERENCES `declaracion_cattiposmuebles` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_tipos_operacione_716f92b4_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_bienesmu_cat_tipos_titulares__8d6bb54f_fk_declaraci` FOREIGN KEY (`cat_tipos_titulares_id`) REFERENCES `declaracion_cattipostitulares` (`id`),
  CONSTRAINT `declaracion_bienesmu_declaraciones_id_4ca011d8_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_bienesmu_observaciones_id_be05200e_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_bienespersonas`
--

DROP TABLE IF EXISTS `declaracion_bienespersonas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_bienespersonas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `porcentaje` decimal(5,2) DEFAULT NULL,
  `es_propietario` tinyint(1) DEFAULT NULL,
  `precio_adquision` decimal(13,2) DEFAULT NULL,
  `el_adquirio` tinyint(1) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otra_relacion` varchar(255) DEFAULT NULL,
  `otra_relacion_familiar` varchar(255) NOT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_tipo_participacion_id` int(11) NOT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  `otra_persona_id` int(11) DEFAULT NULL,
  `tipo_relacion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_bienespe_activos_bienes_id_50697569_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_bienespe_info_personal_var_id_5e966265_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_bienespe_otra_persona_id_6a48ced9_fk_declaraci` (`otra_persona_id`),
  KEY `declaracion_bienespe_tipo_relacion_id_5daea845_fk_declaraci` (`tipo_relacion_id`),
  KEY `declaracion_bienespe_cat_tipo_participaci_51722f94_fk_declaraci` (`cat_tipo_participacion_id`),
  CONSTRAINT `declaracion_bienespe_activos_bienes_id_50697569_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_bienespe_cat_tipo_participaci_51722f94_fk_declaraci` FOREIGN KEY (`cat_tipo_participacion_id`) REFERENCES `declaracion_cattipoparticipacion` (`id`),
  CONSTRAINT `declaracion_bienespe_info_personal_var_id_5e966265_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_bienespe_otra_persona_id_6a48ced9_fk_declaraci` FOREIGN KEY (`otra_persona_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_bienespe_tipo_relacion_id_5daea845_fk_declaraci` FOREIGN KEY (`tipo_relacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catactivobien`
--

DROP TABLE IF EXISTS `declaracion_catactivobien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catactivobien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activo_bien` varchar(45) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catambitoslaborales`
--

DROP TABLE IF EXISTS `declaracion_catambitoslaborales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catambitoslaborales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ambito_laboral` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catcamposobligatorios`
--

DROP TABLE IF EXISTS `declaracion_catcamposobligatorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catcamposobligatorios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_tabla` varchar(255) DEFAULT NULL,
  `nombre_columna` varchar(255) DEFAULT NULL,
  `es_obligatorio` int(11) DEFAULT NULL,
  `es_principal` tinyint(1) NOT NULL,
  `esta_pantalla` tinyint(1) NOT NULL,
  `tipo` int(11) NOT NULL,
  `es_privado` tinyint(1) NOT NULL,
  `seccion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_catcampo_seccion_id_2408ef6d_fk_declaraci` (`seccion_id`),
  CONSTRAINT `declaracion_catcampo_seccion_id_2408ef6d_fk_declaraci` FOREIGN KEY (`seccion_id`) REFERENCES `declaracion_secciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=896 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catdocumentosobtenidos`
--

DROP TABLE IF EXISTS `declaracion_catdocumentosobtenidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catdocumentosobtenidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documento_obtenido` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catentespublicos`
--

DROP TABLE IF EXISTS `declaracion_catentespublicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catentespublicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ente_publico` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=430 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catentidadesfederativas`
--

DROP TABLE IF EXISTS `declaracion_catentidadesfederativas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catentidadesfederativas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entidad_federativa` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catestadosciviles`
--

DROP TABLE IF EXISTS `declaracion_catestadosciviles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catestadosciviles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado_civil` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catestatusdeclaracion`
--

DROP TABLE IF EXISTS `declaracion_catestatusdeclaracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catestatusdeclaracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estatus_declaracion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catestatusestudios`
--

DROP TABLE IF EXISTS `declaracion_catestatusestudios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catestatusestudios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estatus_estudios` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catformasadquisiciones`
--

DROP TABLE IF EXISTS `declaracion_catformasadquisiciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catformasadquisiciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_adquisicion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catfuncionesprincipales`
--

DROP TABLE IF EXISTS `declaracion_catfuncionesprincipales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catfuncionesprincipales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `funcion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catgradosacademicos`
--

DROP TABLE IF EXISTS `declaracion_catgradosacademicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catgradosacademicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grado_academico` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catmonedas`
--

DROP TABLE IF EXISTS `declaracion_catmonedas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catmonedas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `moneda` varchar(255) NOT NULL,
  `moneda_abrev` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catmotivobaja`
--

DROP TABLE IF EXISTS `declaracion_catmotivobaja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catmotivobaja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forma_baja` varchar(45) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catmunicipios`
--

DROP TABLE IF EXISTS `declaracion_catmunicipios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catmunicipios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `clave` varchar(3) NOT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_catmunic_cat_entidades_federa_15697655_fk_declaraci` (`cat_entidades_federativas_id`),
  CONSTRAINT `declaracion_catmunic_cat_entidades_federa_15697655_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2467 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catnaturalezamembresia`
--

DROP TABLE IF EXISTS `declaracion_catnaturalezamembresia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catnaturalezamembresia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naturaleza_membresia` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catordenesgobierno`
--

DROP TABLE IF EXISTS `declaracion_catordenesgobierno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catordenesgobierno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden_gobierno` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catpaises`
--

DROP TABLE IF EXISTS `declaracion_catpaises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catpaises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catpoderes`
--

DROP TABLE IF EXISTS `declaracion_catpoderes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catpoderes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poder` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catpuestos`
--

DROP TABLE IF EXISTS `declaracion_catpuestos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catpuestos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `puesto` varchar(100) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catregimenesmatrimoniales`
--

DROP TABLE IF EXISTS `declaracion_catregimenesmatrimoniales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catregimenesmatrimoniales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regimen_matrimonial` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catsectoresindustria`
--

DROP TABLE IF EXISTS `declaracion_catsectoresindustria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catsectoresindustria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sector_industria` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattipoparticipacion`
--

DROP TABLE IF EXISTS `declaracion_cattipoparticipacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattipoparticipacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_participacion` varchar(45) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattipopersona`
--

DROP TABLE IF EXISTS `declaracion_cattipopersona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattipopersona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_persona` varchar(45) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposacreedores`
--

DROP TABLE IF EXISTS `declaracion_cattiposacreedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposacreedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_acreedor` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposactividad`
--

DROP TABLE IF EXISTS `declaracion_cattiposactividad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposactividad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_actividad` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposadeudos`
--

DROP TABLE IF EXISTS `declaracion_cattiposadeudos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposadeudos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_adeudo` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposapoyos`
--

DROP TABLE IF EXISTS `declaracion_cattiposapoyos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposapoyos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_apoyo` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposbeneficios`
--

DROP TABLE IF EXISTS `declaracion_cattiposbeneficios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposbeneficios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_beneficio` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposbienes`
--

DROP TABLE IF EXISTS `declaracion_cattiposbienes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposbienes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_bien` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposdeclaracion`
--

DROP TABLE IF EXISTS `declaracion_cattiposdeclaracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposdeclaracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_declaracion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposespecificosinversiones`
--

DROP TABLE IF EXISTS `declaracion_cattiposespecificosinversiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposespecificosinversiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_especifico_inversion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposfideicomisos`
--

DROP TABLE IF EXISTS `declaracion_cattiposfideicomisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposfideicomisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_fideicomiso` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposingresosvarios`
--

DROP TABLE IF EXISTS `declaracion_cattiposingresosvarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposingresosvarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_ingreso_varios` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposinmuebles`
--

DROP TABLE IF EXISTS `declaracion_cattiposinmuebles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposinmuebles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_inmueble` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposinstituciones`
--

DROP TABLE IF EXISTS `declaracion_cattiposinstituciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposinstituciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_institucion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposinstrumentos`
--

DROP TABLE IF EXISTS `declaracion_cattiposinstrumentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposinstrumentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `clave` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposinversiones`
--

DROP TABLE IF EXISTS `declaracion_cattiposinversiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposinversiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_inversion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposmetales`
--

DROP TABLE IF EXISTS `declaracion_cattiposmetales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposmetales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_metal` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposmuebles`
--

DROP TABLE IF EXISTS `declaracion_cattiposmuebles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposmuebles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_mueble` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposoperaciones`
--

DROP TABLE IF EXISTS `declaracion_cattiposoperaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposoperaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_operacion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattipospasivos`
--

DROP TABLE IF EXISTS `declaracion_cattipospasivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattipospasivos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_pasivo` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposrelacionespersonales`
--

DROP TABLE IF EXISTS `declaracion_cattiposrelacionespersonales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposrelacionespersonales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_relacion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `grupo_familia` int(11) DEFAULT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattiposrepresentaciones`
--

DROP TABLE IF EXISTS `declaracion_cattiposrepresentaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattiposrepresentaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_representacion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattipostitulares`
--

DROP TABLE IF EXISTS `declaracion_cattipostitulares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattipostitulares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_titular` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattipovia`
--

DROP TABLE IF EXISTS `declaracion_cattipovia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattipovia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_via` varchar(45) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_cattitulartiposrelaciones`
--

DROP TABLE IF EXISTS `declaracion_cattitulartiposrelaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_cattitulartiposrelaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_relacion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catunidadesmedida`
--

DROP TABLE IF EXISTS `declaracion_catunidadesmedida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catunidadesmedida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `valor` varchar(255) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_catunidadestemporales`
--

DROP TABLE IF EXISTS `declaracion_catunidadestemporales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_catunidadestemporales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidad_temporal` varchar(45) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `default` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_clientesprincipales`
--

DROP TABLE IF EXISTS `declaracion_clientesprincipales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_clientesprincipales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `porcentaje_facturacion_cliente` decimal(5,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `nombre_encargado` varchar(255) DEFAULT NULL,
  `realizaActividadLucrativa` tinyint(1) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_clientes_cat_tipos_operacione_8ab9ba69_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_clientes_cat_tipos_relaciones_7223a8f6_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  KEY `declaracion_clientes_declaraciones_id_147d7ff6_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_clientes_info_personal_var_id_6079b942_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_clientes_moneda_id_91342f75_fk_declaraci` (`moneda_id`),
  KEY `declaracion_clientes_observaciones_id_97d3ca01_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_clientes_cat_tipos_operacione_8ab9ba69_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_clientes_cat_tipos_relaciones_7223a8f6_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_clientes_declaraciones_id_147d7ff6_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_clientes_info_personal_var_id_6079b942_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_clientes_moneda_id_91342f75_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_clientes_observaciones_id_97d3ca01_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_conyugedependientes`
--

DROP TABLE IF EXISTS `declaracion_conyugedependientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_conyugedependientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `medio_contacto` varchar(255) NOT NULL,
  `habita_domicilio` tinyint(1) DEFAULT NULL,
  `ingresos_propios` tinyint(1) DEFAULT NULL,
  `ocupacion_profesion` varchar(255) NOT NULL,
  `proveedor_contratista` tinyint(1) DEFAULT NULL,
  `otro_sector` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otra_relacion` varchar(255) NOT NULL,
  `otra_relacion_familiar` varchar(255) NOT NULL,
  `es_pareja` tinyint(1) DEFAULT NULL,
  `es_extranjero` tinyint(1) DEFAULT NULL,
  `actividadLaboral_id` int(11) DEFAULT NULL,
  `actividadLaboralSector_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `declarante_infopersonalvar_id` int(11) NOT NULL,
  `dependiente_infopersonalvar_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_conyuged_actividadLaboral_id_450b8484_fk_declaraci` (`actividadLaboral_id`),
  KEY `declaracion_conyuged_actividadLaboralSect_dd29cd69_fk_declaraci` (`actividadLaboralSector_id`),
  KEY `declaracion_conyuged_cat_tipos_operacione_f431eb09_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_conyuged_cat_tipos_relaciones_efff6ef0_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  KEY `declaracion_conyuged_declaraciones_id_9c8c7844_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_conyuged_declarante_infoperso_2bee774e_fk_declaraci` (`declarante_infopersonalvar_id`),
  KEY `declaracion_conyuged_dependiente_infopers_dad7aa87_fk_declaraci` (`dependiente_infopersonalvar_id`),
  KEY `declaracion_conyuged_observaciones_id_b340bffa_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_conyuged_actividadLaboralSect_dd29cd69_fk_declaraci` FOREIGN KEY (`actividadLaboralSector_id`) REFERENCES `declaracion_encargos` (`id`),
  CONSTRAINT `declaracion_conyuged_actividadLaboral_id_450b8484_fk_declaraci` FOREIGN KEY (`actividadLaboral_id`) REFERENCES `declaracion_catambitoslaborales` (`id`),
  CONSTRAINT `declaracion_conyuged_cat_tipos_operacione_f431eb09_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_conyuged_cat_tipos_relaciones_efff6ef0_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_conyuged_declaraciones_id_9c8c7844_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_conyuged_declarante_infoperso_2bee774e_fk_declaraci` FOREIGN KEY (`declarante_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_conyuged_dependiente_infopers_dad7aa87_fk_declaraci` FOREIGN KEY (`dependiente_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_conyuged_observaciones_id_b340bffa_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_datoscurriculares`
--

DROP TABLE IF EXISTS `declaracion_datoscurriculares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_datoscurriculares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `institucion_educativa` varchar(255) NOT NULL,
  `carrera_o_area` varchar(255) NOT NULL,
  `conclusion` date DEFAULT NULL,
  `cedula_profesional` varchar(255) NOT NULL,
  `diploma` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_documentos_obtenidos_id` int(11) DEFAULT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  `cat_estatus_estudios_id` int(11) DEFAULT NULL,
  `cat_grados_academicos_id` int(11) DEFAULT NULL,
  `cat_pais_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `municipio_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_datoscur_cat_documentos_obten_50cd4b12_fk_declaraci` (`cat_documentos_obtenidos_id`),
  KEY `declaracion_datoscur_cat_entidades_federa_4c423d71_fk_declaraci` (`cat_entidades_federativas_id`),
  KEY `declaracion_datoscur_cat_estatus_estudios_66ccf80f_fk_declaraci` (`cat_estatus_estudios_id`),
  KEY `declaracion_datoscur_cat_grados_academico_f9f94605_fk_declaraci` (`cat_grados_academicos_id`),
  KEY `declaracion_datoscur_cat_pais_id_1974b772_fk_declaraci` (`cat_pais_id`),
  KEY `declaracion_datoscur_cat_tipos_operacione_e16bc423_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_datoscur_declaraciones_id_ce4fc49f_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_datoscur_municipio_id_8892a053_fk_declaraci` (`municipio_id`),
  KEY `declaracion_datoscur_observaciones_id_9b7a7cb2_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_datoscur_cat_documentos_obten_50cd4b12_fk_declaraci` FOREIGN KEY (`cat_documentos_obtenidos_id`) REFERENCES `declaracion_catdocumentosobtenidos` (`id`),
  CONSTRAINT `declaracion_datoscur_cat_entidades_federa_4c423d71_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`),
  CONSTRAINT `declaracion_datoscur_cat_estatus_estudios_66ccf80f_fk_declaraci` FOREIGN KEY (`cat_estatus_estudios_id`) REFERENCES `declaracion_catestatusestudios` (`id`),
  CONSTRAINT `declaracion_datoscur_cat_grados_academico_f9f94605_fk_declaraci` FOREIGN KEY (`cat_grados_academicos_id`) REFERENCES `declaracion_catgradosacademicos` (`id`),
  CONSTRAINT `declaracion_datoscur_cat_pais_id_1974b772_fk_declaraci` FOREIGN KEY (`cat_pais_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_datoscur_cat_tipos_operacione_e16bc423_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_datoscur_declaraciones_id_ce4fc49f_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_datoscur_municipio_id_8892a053_fk_declaraci` FOREIGN KEY (`municipio_id`) REFERENCES `declaracion_catmunicipios` (`id`),
  CONSTRAINT `declaracion_datoscur_observaciones_id_9b7a7cb2_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_declaraciones`
--

DROP TABLE IF EXISTS `declaracion_declaraciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_declaraciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folio` char(32) NOT NULL,
  `fecha_recepcion` datetime(6) DEFAULT NULL,
  `fecha_declaracion` date DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `avance` int(11) NOT NULL,
  `sello` longtext NOT NULL,
  `cat_estatus_id` int(11) NOT NULL,
  `cat_tipos_declaracion_id` int(11) DEFAULT NULL,
  `info_personal_fija_id` int(11) NOT NULL,
  `datos_publicos` tinyint(1),
  PRIMARY KEY (`id`),
  KEY `declaracion_declarac_cat_estatus_id_02b4ac18_fk_declaraci` (`cat_estatus_id`),
  KEY `declaracion_declarac_cat_tipos_declaracio_9358630c_fk_declaraci` (`cat_tipos_declaracion_id`),
  KEY `declaracion_declarac_info_personal_fija_i_df3b9808_fk_declaraci` (`info_personal_fija_id`),
  CONSTRAINT `declaracion_declarac_cat_estatus_id_02b4ac18_fk_declaraci` FOREIGN KEY (`cat_estatus_id`) REFERENCES `declaracion_catestatusdeclaracion` (`id`),
  CONSTRAINT `declaracion_declarac_cat_tipos_declaracio_9358630c_fk_declaraci` FOREIGN KEY (`cat_tipos_declaracion_id`) REFERENCES `declaracion_cattiposdeclaracion` (`id`),
  CONSTRAINT `declaracion_declarac_info_personal_fija_i_df3b9808_fk_declaraci` FOREIGN KEY (`info_personal_fija_id`) REFERENCES `declaracion_infopersonalfija` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_declaracionfiscal`
--

DROP TABLE IF EXISTS `declaracion_declaracionfiscal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_declaracionfiscal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `archivo_xml` varchar(100) DEFAULT NULL,
  `archivo_pdf` varchar(100) DEFAULT NULL,
  `declaraciones_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_declarac_declaraciones_id_dda251b7_fk_declaraci` (`declaraciones_id`),
  CONSTRAINT `declaracion_declarac_declaraciones_id_dda251b7_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_deudasotros`
--

DROP TABLE IF EXISTS `declaracion_deudasotros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_deudasotros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otro_tipo_acreedor` varchar(255) NOT NULL,
  `otro_tipo_adeudo` varchar(255) NOT NULL,
  `numero_cuenta` varchar(255) NOT NULL,
  `fecha_generacion` date DEFAULT NULL,
  `monto_original` decimal(12,2) DEFAULT NULL,
  `tasa_interes` decimal(5,2) DEFAULT NULL,
  `saldo_pendiente` decimal(12,2) DEFAULT NULL,
  `monto_abonado` decimal(12,2) DEFAULT NULL,
  `plazo` decimal(6,2) DEFAULT NULL,
  `otro_titular` varchar(255) NOT NULL,
  `porcentaje_adeudo` decimal(5,2) DEFAULT NULL,
  `garantia` tinyint(1) DEFAULT NULL,
  `nombre_garantes` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `acreedor_infopersonalvar_id` int(11) NOT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipos_acreedores_id` int(11) DEFAULT NULL,
  `cat_tipos_adeudos_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_pasivos_id` int(11) DEFAULT NULL,
  `cat_tipos_titulares_id` int(11) DEFAULT NULL,
  `cat_unidades_temporales_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `acreedor_nombre` varchar(255) DEFAULT NULL,
  `acreedor_rfc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_deudasot_acreedor_infopersona_2d15b68b_fk_declaraci` (`acreedor_infopersonalvar_id`),
  KEY `declaracion_deudasot_cat_monedas_id_cc17183f_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_deudasot_cat_paises_id_33c78190_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_deudasot_cat_tipos_acreedores_5287dc71_fk_declaraci` (`cat_tipos_acreedores_id`),
  KEY `declaracion_deudasot_cat_tipos_adeudos_id_e73d9123_fk_declaraci` (`cat_tipos_adeudos_id`),
  KEY `declaracion_deudasot_cat_tipos_operacione_6508c122_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_deudasot_cat_tipos_pasivos_id_2289f5d6_fk_declaraci` (`cat_tipos_pasivos_id`),
  KEY `declaracion_deudasot_cat_tipos_titulares__b12ef02a_fk_declaraci` (`cat_tipos_titulares_id`),
  KEY `declaracion_deudasot_cat_unidades_tempora_3a4d00cc_fk_declaraci` (`cat_unidades_temporales_id`),
  KEY `declaracion_deudasot_declaraciones_id_50e8cf5f_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_deudasot_observaciones_id_ad9eaf9a_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_deudasot_acreedor_infopersona_2d15b68b_fk_declaraci` FOREIGN KEY (`acreedor_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_monedas_id_cc17183f_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_paises_id_33c78190_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_tipos_acreedores_5287dc71_fk_declaraci` FOREIGN KEY (`cat_tipos_acreedores_id`) REFERENCES `declaracion_cattiposacreedores` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_tipos_adeudos_id_e73d9123_fk_declaraci` FOREIGN KEY (`cat_tipos_adeudos_id`) REFERENCES `declaracion_cattiposadeudos` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_tipos_operacione_6508c122_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_tipos_pasivos_id_2289f5d6_fk_declaraci` FOREIGN KEY (`cat_tipos_pasivos_id`) REFERENCES `declaracion_cattipospasivos` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_tipos_titulares__b12ef02a_fk_declaraci` FOREIGN KEY (`cat_tipos_titulares_id`) REFERENCES `declaracion_cattipostitulares` (`id`),
  CONSTRAINT `declaracion_deudasot_cat_unidades_tempora_3a4d00cc_fk_declaraci` FOREIGN KEY (`cat_unidades_temporales_id`) REFERENCES `declaracion_catunidadestemporales` (`id`),
  CONSTRAINT `declaracion_deudasot_declaraciones_id_50e8cf5f_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_deudasot_observaciones_id_ad9eaf9a_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_domicilios`
--

DROP TABLE IF EXISTS `declaracion_domicilios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_domicilios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cp` varchar(255) NOT NULL,
  `colonia` varchar(255) NOT NULL,
  `nombre_via` varchar(255) NOT NULL,
  `num_exterior` varchar(255) NOT NULL,
  `num_interior` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `ciudadLocalidad` varchar(100) DEFAULT NULL,
  `estadoProvincia` varchar(100) DEFAULT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  `cat_pais_id` int(11) DEFAULT NULL,
  `cat_tipo_via_id` int(11) DEFAULT NULL,
  `municipio_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_domicili_cat_entidades_federa_456c9653_fk_declaraci` (`cat_entidades_federativas_id`),
  KEY `declaracion_domicili_cat_pais_id_930ca211_fk_declaraci` (`cat_pais_id`),
  KEY `declaracion_domicili_cat_tipo_via_id_bc93edfc_fk_declaraci` (`cat_tipo_via_id`),
  KEY `declaracion_domicili_municipio_id_7055d0b1_fk_declaraci` (`municipio_id`),
  CONSTRAINT `declaracion_domicili_cat_entidades_federa_456c9653_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`),
  CONSTRAINT `declaracion_domicili_cat_pais_id_930ca211_fk_declaraci` FOREIGN KEY (`cat_pais_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_domicili_cat_tipo_via_id_bc93edfc_fk_declaraci` FOREIGN KEY (`cat_tipo_via_id`) REFERENCES `declaracion_cattipovia` (`id`),
  CONSTRAINT `declaracion_domicili_municipio_id_7055d0b1_fk_declaraci` FOREIGN KEY (`municipio_id`) REFERENCES `declaracion_catmunicipios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_empresassociedades`
--

DROP TABLE IF EXISTS `declaracion_empresassociedades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_empresassociedades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rol_empresa` varchar(255) NOT NULL,
  `actividad_economica` tinyint(1) DEFAULT NULL,
  `porcentaje_participacion` decimal(5,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `declarante_infopersonalvar_id` int(11) NOT NULL,
  `empresa_infopersonalvar_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_empresas_declaraciones_id_5c363649_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_empresas_declarante_infoperso_c632e522_fk_declaraci` (`declarante_infopersonalvar_id`),
  KEY `declaracion_empresas_empresa_infopersonal_33224da1_fk_declaraci` (`empresa_infopersonalvar_id`),
  KEY `declaracion_empresas_observaciones_id_fb8ea203_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_empresas_declaraciones_id_5c363649_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_empresas_declarante_infoperso_c632e522_fk_declaraci` FOREIGN KEY (`declarante_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_empresas_empresa_infopersonal_33224da1_fk_declaraci` FOREIGN KEY (`empresa_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_empresas_observaciones_id_fb8ea203_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_encargos`
--

DROP TABLE IF EXISTS `declaracion_encargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_encargos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empleo_cargo_comision` varchar(255) DEFAULT NULL,
  `honorarios` tinyint(1) DEFAULT NULL,
  `nivel_encargo` varchar(255) DEFAULT NULL,
  `area_adscripcion` varchar(255) DEFAULT NULL,
  `posesion_conclusion` date DEFAULT NULL,
  `posesion_inicio` date DEFAULT NULL,
  `telefono_laboral` varchar(255) DEFAULT NULL,
  `email_laboral` varchar(255) DEFAULT NULL,
  `otro_sector` varchar(255) DEFAULT NULL,
  `otra_funcion` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `telefono_extension` varchar(255) DEFAULT NULL,
  `otro_naturalezas_juridicas` varchar(255) DEFAULT NULL,
  `otro_ente` varchar(255) DEFAULT NULL,
  `salarioMensualNeto` int(11) DEFAULT NULL,
  `rfc` varchar(255) DEFAULT NULL,
  `nombreEmpresaSociedadAsociacion` varchar(255) DEFAULT NULL,
  `cat_entes_publicos_id` int(11) DEFAULT NULL,
  `cat_funciones_principales_id` int(11) DEFAULT NULL,
  `cat_ordenes_gobierno_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_poderes_id` int(11) DEFAULT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) DEFAULT NULL,
  `domicilios_id` int(11) DEFAULT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) DEFAULT NULL,
  `nombre_ente_publico` varchar(300) DEFAULT NULL,
  `cat_puestos_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_encargos_cat_entes_publicos_i_c694e7a5_fk_declaraci` (`cat_entes_publicos_id`),
  KEY `declaracion_encargos_cat_funciones_princi_7856c833_fk_declaraci` (`cat_funciones_principales_id`),
  KEY `declaracion_encargos_cat_ordenes_gobierno_a8b7d240_fk_declaraci` (`cat_ordenes_gobierno_id`),
  KEY `declaracion_encargos_cat_paises_id_1274280c_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_encargos_cat_poderes_id_f5b206b8_fk_declaraci` (`cat_poderes_id`),
  KEY `declaracion_encargos_cat_sectores_industr_3165053e_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_encargos_cat_tipos_operacione_9230d23b_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_encargos_declaraciones_id_2316fade_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_encargos_domicilios_id_c5d14b3c_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_encargos_moneda_id_9c3a3546_fk_declaraci` (`moneda_id`),
  KEY `declaracion_encargos_observaciones_id_8abf3420_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_encargos_cat_puestos_id_cf665b8e_fk_declaraci` (`cat_puestos_id`),
  CONSTRAINT `declaracion_encargos_cat_entes_publicos_i_c694e7a5_fk_declaraci` FOREIGN KEY (`cat_entes_publicos_id`) REFERENCES `declaracion_catentespublicos` (`id`),
  CONSTRAINT `declaracion_encargos_cat_funciones_princi_7856c833_fk_declaraci` FOREIGN KEY (`cat_funciones_principales_id`) REFERENCES `declaracion_catfuncionesprincipales` (`id`),
  CONSTRAINT `declaracion_encargos_cat_ordenes_gobierno_a8b7d240_fk_declaraci` FOREIGN KEY (`cat_ordenes_gobierno_id`) REFERENCES `declaracion_catordenesgobierno` (`id`),
  CONSTRAINT `declaracion_encargos_cat_paises_id_1274280c_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_encargos_cat_poderes_id_f5b206b8_fk_declaraci` FOREIGN KEY (`cat_poderes_id`) REFERENCES `declaracion_catpoderes` (`id`),
  CONSTRAINT `declaracion_encargos_cat_puestos_id_cf665b8e_fk_declaraci` FOREIGN KEY (`cat_puestos_id`) REFERENCES `declaracion_catpuestos` (`id`),
  CONSTRAINT `declaracion_encargos_cat_sectores_industr_3165053e_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_encargos_cat_tipos_operacione_9230d23b_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_encargos_declaraciones_id_2316fade_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_encargos_domicilios_id_c5d14b3c_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_encargos_moneda_id_9c3a3546_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_encargos_observaciones_id_8abf3420_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_experiencialaboral`
--

DROP TABLE IF EXISTS `declaracion_experiencialaboral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_experiencialaboral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otro_poder` varchar(255) NOT NULL,
  `nombre_institucion` varchar(255) NOT NULL,
  `unidad_area_administrativa` varchar(255) NOT NULL,
  `otro_sector` varchar(255) NOT NULL,
  `jerarquia_rango` varchar(255) NOT NULL,
  `cargo_puesto` varchar(255) NOT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `fecha_salida` date DEFAULT NULL,
  `otra_funcion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otro_ambitos_laborales` varchar(255) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `cat_ambitos_laborales_id` int(11) DEFAULT NULL,
  `cat_funciones_principales_id` int(11) DEFAULT NULL,
  `cat_ordenes_gobierno_id` int(11) DEFAULT NULL,
  `cat_poderes_id` int(11) DEFAULT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `domicilios_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_experien_cat_ambitos_laborale_79a45acd_fk_declaraci` (`cat_ambitos_laborales_id`),
  KEY `declaracion_experien_cat_funciones_princi_88f728f5_fk_declaraci` (`cat_funciones_principales_id`),
  KEY `declaracion_experien_cat_ordenes_gobierno_7b2c7dfe_fk_declaraci` (`cat_ordenes_gobierno_id`),
  KEY `declaracion_experien_cat_poderes_id_400030b2_fk_declaraci` (`cat_poderes_id`),
  KEY `declaracion_experien_cat_sectores_industr_d5ad472b_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_experien_cat_tipos_operacione_f363162d_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_experien_declaraciones_id_f1ba0aad_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_experien_domicilios_id_a7959cd2_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_experien_observaciones_id_36520823_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_experien_cat_ambitos_laborale_79a45acd_fk_declaraci` FOREIGN KEY (`cat_ambitos_laborales_id`) REFERENCES `declaracion_catambitoslaborales` (`id`),
  CONSTRAINT `declaracion_experien_cat_funciones_princi_88f728f5_fk_declaraci` FOREIGN KEY (`cat_funciones_principales_id`) REFERENCES `declaracion_catfuncionesprincipales` (`id`),
  CONSTRAINT `declaracion_experien_cat_ordenes_gobierno_7b2c7dfe_fk_declaraci` FOREIGN KEY (`cat_ordenes_gobierno_id`) REFERENCES `declaracion_catordenesgobierno` (`id`),
  CONSTRAINT `declaracion_experien_cat_poderes_id_400030b2_fk_declaraci` FOREIGN KEY (`cat_poderes_id`) REFERENCES `declaracion_catpoderes` (`id`),
  CONSTRAINT `declaracion_experien_cat_sectores_industr_d5ad472b_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_experien_cat_tipos_operacione_f363162d_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_experien_declaraciones_id_f1ba0aad_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_experien_domicilios_id_a7959cd2_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_experien_observaciones_id_36520823_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_fideicomisos`
--

DROP TABLE IF EXISTS `declaracion_fideicomisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_fideicomisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_fideicomiso` varchar(255) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `rfc_fideicomiso` varchar(255) NOT NULL,
  `otra_operacion` varchar(255) NOT NULL,
  `otro_fideicomiso` varchar(255) NOT NULL,
  `objetivo_fideicomiso` varchar(255) NOT NULL,
  `num_registro` varchar(255) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `plazo_vigencia` varchar(255) NOT NULL,
  `valor_fideicomiso` decimal(12,2) DEFAULT NULL,
  `ingreso_monetario` decimal(12,2) DEFAULT NULL,
  `porcentaje` decimal(5,2) DEFAULT NULL,
  `institucion_fiduciaria` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipo_participacion_id` int(11) DEFAULT NULL,
  `cat_tipos_fideicomisos_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `tipo_persona_id` int(11) DEFAULT NULL,
  `tipo_relacion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_fideicom_activos_bienes_id_df3314c3_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_fideicom_cat_monedas_id_cebff38e_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_fideicom_cat_paises_id_d151330c_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_fideicom_cat_tipo_participaci_9616776a_fk_declaraci` (`cat_tipo_participacion_id`),
  KEY `declaracion_fideicom_cat_tipos_fideicomis_f258a97c_fk_declaraci` (`cat_tipos_fideicomisos_id`),
  KEY `declaracion_fideicom_cat_tipos_operacione_ff4aec47_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_fideicom_declaraciones_id_9ddd40ea_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_fideicom_observaciones_id_f261c138_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_fideicom_tipo_persona_id_43c4405d_fk_declaraci` (`tipo_persona_id`),
  KEY `declaracion_fideicom_tipo_relacion_id_9e45cbce_fk_declaraci` (`tipo_relacion_id`),
  KEY `declaracion_fideicom_cat_sectores_industr_bb5fb50f_fk_declaraci` (`cat_sectores_industria_id`),
  CONSTRAINT `declaracion_fideicom_activos_bienes_id_df3314c3_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_monedas_id_cebff38e_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_paises_id_d151330c_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_sectores_industr_bb5fb50f_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_tipo_participaci_9616776a_fk_declaraci` FOREIGN KEY (`cat_tipo_participacion_id`) REFERENCES `declaracion_cattipoparticipacion` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_tipos_fideicomis_f258a97c_fk_declaraci` FOREIGN KEY (`cat_tipos_fideicomisos_id`) REFERENCES `declaracion_cattiposfideicomisos` (`id`),
  CONSTRAINT `declaracion_fideicom_cat_tipos_operacione_ff4aec47_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_fideicom_declaraciones_id_9ddd40ea_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_fideicom_observaciones_id_f261c138_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_fideicom_tipo_persona_id_43c4405d_fk_declaraci` FOREIGN KEY (`tipo_persona_id`) REFERENCES `declaracion_cattipopersona` (`id`),
  CONSTRAINT `declaracion_fideicom_tipo_relacion_id_9e45cbce_fk_declaraci` FOREIGN KEY (`tipo_relacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_infopersonalfija`
--

DROP TABLE IF EXISTS `declaracion_infopersonalfija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_infopersonalfija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) NOT NULL,
  `apellido1` varchar(255) NOT NULL,
  `apellido2` varchar(255) NOT NULL,
  `curp` varchar(255) NOT NULL,
  `puesto` varchar(255) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `otro_ente` varchar(255) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `cat_ente_publico_id` int(11) DEFAULT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  `cat_pais_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL,
  `nombre_ente_publico` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_infopers_cat_ente_publico_id_a3b30fba_fk_declaraci` (`cat_ente_publico_id`),
  KEY `declaracion_infopers_cat_entidades_federa_e29e8024_fk_declaraci` (`cat_entidades_federativas_id`),
  KEY `declaracion_infopers_cat_pais_id_484e2f32_fk_declaraci` (`cat_pais_id`),
  KEY `declaracion_infopersonalfija_usuario_id_2e744aa8_fk_auth_user_id` (`usuario_id`),
  CONSTRAINT `declaracion_infopers_cat_ente_publico_id_a3b30fba_fk_declaraci` FOREIGN KEY (`cat_ente_publico_id`) REFERENCES `declaracion_catentespublicos` (`id`),
  CONSTRAINT `declaracion_infopers_cat_entidades_federa_e29e8024_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`),
  CONSTRAINT `declaracion_infopers_cat_pais_id_484e2f32_fk_declaraci` FOREIGN KEY (`cat_pais_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_infopersonalfija_usuario_id_2e744aa8_fk_auth_user_id` FOREIGN KEY (`usuario_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_infopersonalvar`
--

DROP TABLE IF EXISTS `declaracion_infopersonalvar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_infopersonalvar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `es_fisica` tinyint(1) DEFAULT NULL,
  `razon_social` varchar(255) DEFAULT NULL,
  `nombres` varchar(255) NOT NULL,
  `apellido1` varchar(255) NOT NULL,
  `apellido2` varchar(255) NOT NULL,
  `curp` varchar(255) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `homoclave` varchar(3) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `num_id_identificacion` varchar(255) NOT NULL,
  `email_personal` varchar(255) NOT NULL,
  `tel_particular` varchar(255) NOT NULL,
  `tel_movil` varchar(255) NOT NULL,
  `otro_sector` varchar(255) DEFAULT NULL,
  `otro_estado_civil` varchar(255) DEFAULT NULL,
  `actividad_economica` varchar(255) DEFAULT NULL,
  `ocupacion_girocomercial` varchar(255) DEFAULT NULL,
  `nombre_negocio` varchar(255) DEFAULT NULL,
  `rfc_negocio` varchar(50) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_entidades_federativas_id` int(11) DEFAULT NULL,
  `cat_estados_civiles_id` int(11) DEFAULT NULL,
  `cat_pais_id` int(11) DEFAULT NULL,
  `cat_regimenes_matrimoniales_id` int(11) DEFAULT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `cat_tipo_persona_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `domicilios_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_infopers_cat_entidades_federa_aca67810_fk_declaraci` (`cat_entidades_federativas_id`),
  KEY `declaracion_infopers_cat_estados_civiles__ea86eb1a_fk_declaraci` (`cat_estados_civiles_id`),
  KEY `declaracion_infopers_cat_pais_id_3c350fea_fk_declaraci` (`cat_pais_id`),
  KEY `declaracion_infopers_cat_regimenes_matrim_b8803425_fk_declaraci` (`cat_regimenes_matrimoniales_id`),
  KEY `declaracion_infopers_cat_sectores_industr_c9c540d2_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_infopers_cat_tipo_persona_id_b7db6218_fk_declaraci` (`cat_tipo_persona_id`),
  KEY `declaracion_infopers_declaraciones_id_75d0c55e_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_infopers_domicilios_id_f9298f9b_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_infopers_observaciones_id_641a285c_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_infopers_cat_entidades_federa_aca67810_fk_declaraci` FOREIGN KEY (`cat_entidades_federativas_id`) REFERENCES `declaracion_catentidadesfederativas` (`id`),
  CONSTRAINT `declaracion_infopers_cat_estados_civiles__ea86eb1a_fk_declaraci` FOREIGN KEY (`cat_estados_civiles_id`) REFERENCES `declaracion_catestadosciviles` (`id`),
  CONSTRAINT `declaracion_infopers_cat_pais_id_3c350fea_fk_declaraci` FOREIGN KEY (`cat_pais_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_infopers_cat_regimenes_matrim_b8803425_fk_declaraci` FOREIGN KEY (`cat_regimenes_matrimoniales_id`) REFERENCES `declaracion_catregimenesmatrimoniales` (`id`),
  CONSTRAINT `declaracion_infopers_cat_sectores_industr_c9c540d2_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_infopers_cat_tipo_persona_id_b7db6218_fk_declaraci` FOREIGN KEY (`cat_tipo_persona_id`) REFERENCES `declaracion_cattipopersona` (`id`),
  CONSTRAINT `declaracion_infopers_declaraciones_id_75d0c55e_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_infopers_domicilios_id_f9298f9b_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_infopers_observaciones_id_641a285c_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_ingresosdeclaracion`
--

DROP TABLE IF EXISTS `declaracion_ingresosdeclaracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_ingresosdeclaracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `ingreso_mensual_cargo` int(11) DEFAULT NULL,
  `ingreso_mensual_otros_ingresos` int(11) DEFAULT NULL,
  `ingreso_mensual_actividad` int(11) DEFAULT NULL,
  `razon_social_negocio` varchar(100) DEFAULT NULL,
  `tipo_negocio` varchar(300) DEFAULT NULL,
  `ingreso_mensual_financiera` int(11) DEFAULT NULL,
  `otro_tipo_instrumento` varchar(255) NOT NULL,
  `ingreso_mensual_servicios` int(11) DEFAULT NULL,
  `tipo_servicio` varchar(100) DEFAULT NULL,
  `ingreso_otros_ingresos` int(11) DEFAULT NULL,
  `ingreso_mensual_neto` int(11) DEFAULT NULL,
  `ingreso_mensual_pareja_dependientes` int(11) DEFAULT NULL,
  `ingreso_mensual_total` int(11) DEFAULT NULL,
  `ingreso_anio_anterior` tinyint(1) DEFAULT NULL,
  `ingreso_enajenacion_bienes` int(11) DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `fecha_conclusion` date DEFAULT NULL,
  `tipo_ingreso` tinyint(1) DEFAULT NULL,
  `cat_moneda_actividad_id` int(11) NOT NULL,
  `cat_moneda_cargo_id` int(11) NOT NULL,
  `cat_moneda_enajenacion_bienes_id` int(11) NOT NULL,
  `cat_moneda_financiera_id` int(11) NOT NULL,
  `cat_moneda_neto_id` int(11) NOT NULL,
  `cat_moneda_otro_ingresos_mensual_id` int(11) NOT NULL,
  `cat_moneda_otros_ingresos_id` int(11) NOT NULL,
  `cat_moneda_pareja_dependientes_id` int(11) NOT NULL,
  `cat_moneda_servicios_id` int(11) NOT NULL,
  `cat_moneda_total_id` int(11) NOT NULL,
  `cat_tipo_instrumento_id` int(11) DEFAULT NULL,
  `cat_tipos_actividad_id` int(11) DEFAULT NULL,
  `cat_tipos_bienes_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_ingresos_cat_moneda_actividad_d7bdf545_fk_declaraci` (`cat_moneda_actividad_id`),
  KEY `declaracion_ingresos_cat_moneda_cargo_id_23912393_fk_declaraci` (`cat_moneda_cargo_id`),
  KEY `declaracion_ingresos_cat_moneda_enajenaci_4b8f2266_fk_declaraci` (`cat_moneda_enajenacion_bienes_id`),
  KEY `declaracion_ingresos_cat_moneda_financier_314c5e99_fk_declaraci` (`cat_moneda_financiera_id`),
  KEY `declaracion_ingresos_cat_moneda_neto_id_139501ad_fk_declaraci` (`cat_moneda_neto_id`),
  KEY `declaracion_ingresos_cat_moneda_otro_ingr_3880dff4_fk_declaraci` (`cat_moneda_otro_ingresos_mensual_id`),
  KEY `declaracion_ingresos_cat_moneda_otros_ing_3e834e0c_fk_declaraci` (`cat_moneda_otros_ingresos_id`),
  KEY `declaracion_ingresos_cat_moneda_pareja_de_fbd119b9_fk_declaraci` (`cat_moneda_pareja_dependientes_id`),
  KEY `declaracion_ingresos_cat_moneda_servicios_0df72867_fk_declaraci` (`cat_moneda_servicios_id`),
  KEY `declaracion_ingresos_cat_moneda_total_id_dda49ed1_fk_declaraci` (`cat_moneda_total_id`),
  KEY `declaracion_ingresos_cat_tipo_instrumento_9e66ee82_fk_declaraci` (`cat_tipo_instrumento_id`),
  KEY `declaracion_ingresos_cat_tipos_actividad__73e74b0c_fk_declaraci` (`cat_tipos_actividad_id`),
  KEY `declaracion_ingresos_cat_tipos_bienes_id_1abb50ce_fk_declaraci` (`cat_tipos_bienes_id`),
  KEY `declaracion_ingresos_declaraciones_id_d4ef074c_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_ingresos_observaciones_id_dcc54453_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_actividad_d7bdf545_fk_declaraci` FOREIGN KEY (`cat_moneda_actividad_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_cargo_id_23912393_fk_declaraci` FOREIGN KEY (`cat_moneda_cargo_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_enajenaci_4b8f2266_fk_declaraci` FOREIGN KEY (`cat_moneda_enajenacion_bienes_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_financier_314c5e99_fk_declaraci` FOREIGN KEY (`cat_moneda_financiera_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_neto_id_139501ad_fk_declaraci` FOREIGN KEY (`cat_moneda_neto_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_otro_ingr_3880dff4_fk_declaraci` FOREIGN KEY (`cat_moneda_otro_ingresos_mensual_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_otros_ing_3e834e0c_fk_declaraci` FOREIGN KEY (`cat_moneda_otros_ingresos_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_pareja_de_fbd119b9_fk_declaraci` FOREIGN KEY (`cat_moneda_pareja_dependientes_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_servicios_0df72867_fk_declaraci` FOREIGN KEY (`cat_moneda_servicios_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_moneda_total_id_dda49ed1_fk_declaraci` FOREIGN KEY (`cat_moneda_total_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipo_instrumento_9e66ee82_fk_declaraci` FOREIGN KEY (`cat_tipo_instrumento_id`) REFERENCES `declaracion_cattiposinstrumentos` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipos_actividad__73e74b0c_fk_declaraci` FOREIGN KEY (`cat_tipos_actividad_id`) REFERENCES `declaracion_cattiposactividad` (`id`),
  CONSTRAINT `declaracion_ingresos_cat_tipos_bienes_id_1abb50ce_fk_declaraci` FOREIGN KEY (`cat_tipos_bienes_id`) REFERENCES `declaracion_cattiposbienes` (`id`),
  CONSTRAINT `declaracion_ingresos_declaraciones_id_d4ef074c_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_ingresos_observaciones_id_dcc54453_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_inversiones`
--

DROP TABLE IF EXISTS `declaracion_inversiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_inversiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otra_inversion` varchar(255) NOT NULL,
  `otro_tipo_especifico` varchar(255) NOT NULL,
  `num_cuenta` varchar(255) NOT NULL,
  `otra_forma` varchar(255) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `monto_original` decimal(12,2) DEFAULT NULL,
  `tasa_interes` decimal(5,2) DEFAULT NULL,
  `saldo_actual` decimal(10,2) DEFAULT NULL,
  `plazo` decimal(6,2) DEFAULT NULL,
  `otro_tipo_titular` varchar(255) NOT NULL,
  `porcentaje_inversion` decimal(5,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_formas_adquisiciones_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipo_persona_id` int(11) DEFAULT NULL,
  `cat_tipos_especificos_inversiones_id` int(11) DEFAULT NULL,
  `cat_tipos_inversiones_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_titulares_id` int(11) DEFAULT NULL,
  `cat_unidades_temporales_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  `afores` varchar(255) DEFAULT NULL,
  `fondo_inversion` varchar(255) DEFAULT NULL,
  `organizaciones` varchar(255) DEFAULT NULL,
  `posesiones` varchar(255) DEFAULT NULL,
  `seguros` varchar(255) DEFAULT NULL,
  `valores_bursatiles` varchar(255) DEFAULT NULL,
  `institucion` varchar(255) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `activos_bienes_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_inversio_cat_formas_adquisici_7a34b96c_fk_declaraci` (`cat_formas_adquisiciones_id`),
  KEY `declaracion_inversio_cat_monedas_id_f4c04ea9_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_inversio_cat_paises_id_adf125bf_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_inversio_cat_tipo_persona_id_868c64e2_fk_declaraci` (`cat_tipo_persona_id`),
  KEY `declaracion_inversio_cat_tipos_especifico_f4b4003b_fk_declaraci` (`cat_tipos_especificos_inversiones_id`),
  KEY `declaracion_inversio_cat_tipos_inversione_9d3d2b86_fk_declaraci` (`cat_tipos_inversiones_id`),
  KEY `declaracion_inversio_cat_tipos_operacione_eab54072_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_inversio_cat_tipos_titulares__52574655_fk_declaraci` (`cat_tipos_titulares_id`),
  KEY `declaracion_inversio_cat_unidades_tempora_e115056e_fk_declaraci` (`cat_unidades_temporales_id`),
  KEY `declaracion_inversio_declaraciones_id_2688f2af_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_inversio_info_personal_var_id_65a231a6_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_inversio_observaciones_id_37615d8b_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_inversio_activos_bienes_id_4bfea7c3_fk_declaraci` (`activos_bienes_id`),
  CONSTRAINT `declaracion_inversio_activos_bienes_id_4bfea7c3_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_inversio_cat_formas_adquisici_7a34b96c_fk_declaraci` FOREIGN KEY (`cat_formas_adquisiciones_id`) REFERENCES `declaracion_catformasadquisiciones` (`id`),
  CONSTRAINT `declaracion_inversio_cat_monedas_id_f4c04ea9_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_inversio_cat_paises_id_adf125bf_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_inversio_cat_tipo_persona_id_868c64e2_fk_declaraci` FOREIGN KEY (`cat_tipo_persona_id`) REFERENCES `declaracion_cattipopersona` (`id`),
  CONSTRAINT `declaracion_inversio_cat_tipos_especifico_f4b4003b_fk_declaraci` FOREIGN KEY (`cat_tipos_especificos_inversiones_id`) REFERENCES `declaracion_cattiposespecificosinversiones` (`id`),
  CONSTRAINT `declaracion_inversio_cat_tipos_inversione_9d3d2b86_fk_declaraci` FOREIGN KEY (`cat_tipos_inversiones_id`) REFERENCES `declaracion_cattiposinversiones` (`id`),
  CONSTRAINT `declaracion_inversio_cat_tipos_operacione_eab54072_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_inversio_cat_tipos_titulares__52574655_fk_declaraci` FOREIGN KEY (`cat_tipos_titulares_id`) REFERENCES `declaracion_cattipostitulares` (`id`),
  CONSTRAINT `declaracion_inversio_cat_unidades_tempora_e115056e_fk_declaraci` FOREIGN KEY (`cat_unidades_temporales_id`) REFERENCES `declaracion_catunidadestemporales` (`id`),
  CONSTRAINT `declaracion_inversio_declaraciones_id_2688f2af_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_inversio_info_personal_var_id_65a231a6_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_inversio_observaciones_id_37615d8b_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_membresias`
--

DROP TABLE IF EXISTS `declaracion_membresias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_membresias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otras_instituciones` varchar(255) NOT NULL,
  `nombre_institucion` varchar(255) NOT NULL,
  `rfc` varchar(255) NOT NULL,
  `otro_sector` varchar(255) NOT NULL,
  `puesto_rol` varchar(255) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `pagado` tinyint(1) DEFAULT NULL,
  `monto` decimal(13,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otra_naturaleza` varchar(255) DEFAULT NULL,
  `cat_naturaleza_membresia_id` int(11) DEFAULT NULL,
  `cat_sectores_industria_id` int(11) DEFAULT NULL,
  `cat_tipos_instituciones_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `domicilios_id` int(11) NOT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  `tipoRelacion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_membresi_cat_naturaleza_membr_e3502c57_fk_declaraci` (`cat_naturaleza_membresia_id`),
  KEY `declaracion_membresi_cat_sectores_industr_f72dfe5d_fk_declaraci` (`cat_sectores_industria_id`),
  KEY `declaracion_membresi_cat_tipos_institucio_aede5c47_fk_declaraci` (`cat_tipos_instituciones_id`),
  KEY `declaracion_membresi_cat_tipos_operacione_f5d60af4_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_membresi_declaraciones_id_45741598_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_membresi_domicilios_id_f8d850a4_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_membresi_moneda_id_afa0614e_fk_declaraci` (`moneda_id`),
  KEY `declaracion_membresi_observaciones_id_d0f2470e_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_membresi_tipoRelacion_id_1d92a718_fk_declaraci` (`tipoRelacion_id`),
  CONSTRAINT `declaracion_membresi_cat_naturaleza_membr_e3502c57_fk_declaraci` FOREIGN KEY (`cat_naturaleza_membresia_id`) REFERENCES `declaracion_catnaturalezamembresia` (`id`),
  CONSTRAINT `declaracion_membresi_cat_sectores_industr_f72dfe5d_fk_declaraci` FOREIGN KEY (`cat_sectores_industria_id`) REFERENCES `declaracion_catsectoresindustria` (`id`),
  CONSTRAINT `declaracion_membresi_cat_tipos_institucio_aede5c47_fk_declaraci` FOREIGN KEY (`cat_tipos_instituciones_id`) REFERENCES `declaracion_cattiposinstituciones` (`id`),
  CONSTRAINT `declaracion_membresi_cat_tipos_operacione_f5d60af4_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_membresi_declaraciones_id_45741598_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_membresi_domicilios_id_f8d850a4_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_membresi_moneda_id_afa0614e_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_membresi_observaciones_id_d0f2470e_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_membresi_tipoRelacion_id_1d92a718_fk_declaraci` FOREIGN KEY (`tipoRelacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_mueblesnoregistrables`
--

DROP TABLE IF EXISTS `declaracion_mueblesnoregistrables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_mueblesnoregistrables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otro_bien_mueble` varchar(255) NOT NULL,
  `marca` varchar(255) NOT NULL,
  `modelo` varchar(255) NOT NULL,
  `forma_pago` varchar(255) NOT NULL,
  `anio` int(11) DEFAULT NULL,
  `num_serie` varchar(255) NOT NULL,
  `descripcion_bien` varchar(255) NOT NULL,
  `otro_titular` varchar(255) NOT NULL,
  `otra_forma` varchar(255) NOT NULL,
  `fecha_adquisicion` date DEFAULT NULL,
  `precio_adquisicion` decimal(12,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `activos_bienes_id` int(11) NOT NULL,
  `cat_formas_adquisiciones_id` int(11) DEFAULT NULL,
  `cat_monedas_id` int(11) DEFAULT NULL,
  `cat_motivo_baja_id` int(11) DEFAULT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipos_muebles_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_titulares_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `domicilios_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  `tipo_relacion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_mueblesn_activos_bienes_id_024dcf19_fk_declaraci` (`activos_bienes_id`),
  KEY `declaracion_mueblesn_cat_formas_adquisici_faa85e62_fk_declaraci` (`cat_formas_adquisiciones_id`),
  KEY `declaracion_mueblesn_cat_monedas_id_e6c992b9_fk_declaraci` (`cat_monedas_id`),
  KEY `declaracion_mueblesn_cat_motivo_baja_id_9dc2f165_fk_declaraci` (`cat_motivo_baja_id`),
  KEY `declaracion_mueblesn_cat_paises_id_3ca85c81_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_mueblesn_cat_tipos_muebles_id_47f1bccc_fk_declaraci` (`cat_tipos_muebles_id`),
  KEY `declaracion_mueblesn_cat_tipos_operacione_b7fe9022_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_mueblesn_cat_tipos_titulares__f8d5dc97_fk_declaraci` (`cat_tipos_titulares_id`),
  KEY `declaracion_mueblesn_declaraciones_id_c619a54e_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_mueblesn_domicilios_id_67d417c4_fk_declaraci` (`domicilios_id`),
  KEY `declaracion_mueblesn_observaciones_id_cc910b1b_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_mueblesn_tipo_relacion_id_ddb07b73_fk_declaraci` (`tipo_relacion_id`),
  CONSTRAINT `declaracion_mueblesn_activos_bienes_id_024dcf19_fk_declaraci` FOREIGN KEY (`activos_bienes_id`) REFERENCES `declaracion_activosbienes` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_formas_adquisici_faa85e62_fk_declaraci` FOREIGN KEY (`cat_formas_adquisiciones_id`) REFERENCES `declaracion_catformasadquisiciones` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_monedas_id_e6c992b9_fk_declaraci` FOREIGN KEY (`cat_monedas_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_motivo_baja_id_9dc2f165_fk_declaraci` FOREIGN KEY (`cat_motivo_baja_id`) REFERENCES `declaracion_catmotivobaja` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_paises_id_3ca85c81_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_tipos_muebles_id_47f1bccc_fk_declaraci` FOREIGN KEY (`cat_tipos_muebles_id`) REFERENCES `declaracion_cattiposmuebles` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_tipos_operacione_b7fe9022_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_mueblesn_cat_tipos_titulares__f8d5dc97_fk_declaraci` FOREIGN KEY (`cat_tipos_titulares_id`) REFERENCES `declaracion_cattipostitulares` (`id`),
  CONSTRAINT `declaracion_mueblesn_declaraciones_id_c619a54e_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_mueblesn_domicilios_id_67d417c4_fk_declaraci` FOREIGN KEY (`domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_mueblesn_observaciones_id_cc910b1b_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_mueblesn_tipo_relacion_id_ddb07b73_fk_declaraci` FOREIGN KEY (`tipo_relacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_nacionalidades`
--

DROP TABLE IF EXISTS `declaracion_nacionalidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_nacionalidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_nacional_cat_paises_id_33c12751_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_nacional_info_personal_var_id_69f68616_fk_declaraci` (`info_personal_var_id`),
  CONSTRAINT `declaracion_nacional_cat_paises_id_33c12751_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_nacional_info_personal_var_id_69f68616_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_observaciones`
--

DROP TABLE IF EXISTS `declaracion_observaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_observaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `observacion` varchar(255) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=293 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_prestamocomodato`
--

DROP TABLE IF EXISTS `declaracion_prestamocomodato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_prestamocomodato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_operacion` varchar(255) NOT NULL,
  `otro_tipo_inmueble` varchar(100) DEFAULT NULL,
  `otro_tipo_mueble` varchar(255) NOT NULL,
  `mueble_marca` varchar(255) NOT NULL,
  `mueble_submarca` varchar(255) NOT NULL,
  `mueble_modelo` int(11) DEFAULT NULL,
  `mueble_num_serie` varchar(255) NOT NULL,
  `mueble_num_registro` varchar(255) NOT NULL,
  `otro_tipo_adeudo` varchar(255) NOT NULL,
  `cat_paises_id` int(11) DEFAULT NULL,
  `cat_tipos_inmueble_id` int(11) DEFAULT NULL,
  `cat_tipos_muebles_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `inmueble_domicilios_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  `titular_relacion_id` int(11) DEFAULT NULL,
  `tipo_obj_comodato` varchar(100) DEFAULT NULL,
  `titular_infopersonalVar_id` int(11) DEFAULT NULL,
  `otro_tipo_relacion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_prestamo_cat_paises_id_4ab8eda8_fk_declaraci` (`cat_paises_id`),
  KEY `declaracion_prestamo_cat_tipos_inmueble_i_831c45ff_fk_declaraci` (`cat_tipos_inmueble_id`),
  KEY `declaracion_prestamo_cat_tipos_operacione_3fab732c_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_prestamo_observaciones_id_252fa429_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_prestamo_declaraciones_id_b63d3649_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_prestamocomodato_cat_tipos_muebles_id_1b69f005` (`cat_tipos_muebles_id`),
  KEY `declaracion_prestamo_inmueble_domicilios__1f840946_fk_declaraci` (`inmueble_domicilios_id`),
  KEY `declaracion_prestamo_titular_infopersonal_cb34e238_fk_declaraci` (`titular_infopersonalVar_id`),
  KEY `declaracion_prestamo_titular_relacion_id_a35cb496_fk_declaraci` (`titular_relacion_id`),
  CONSTRAINT `declaracion_prestamo_cat_paises_id_4ab8eda8_fk_declaraci` FOREIGN KEY (`cat_paises_id`) REFERENCES `declaracion_catpaises` (`id`),
  CONSTRAINT `declaracion_prestamo_cat_tipos_inmueble_i_831c45ff_fk_declaraci` FOREIGN KEY (`cat_tipos_inmueble_id`) REFERENCES `declaracion_cattiposinmuebles` (`id`),
  CONSTRAINT `declaracion_prestamo_cat_tipos_muebles_id_1b69f005_fk_declaraci` FOREIGN KEY (`cat_tipos_muebles_id`) REFERENCES `declaracion_cattiposmuebles` (`id`),
  CONSTRAINT `declaracion_prestamo_cat_tipos_operacione_3fab732c_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_prestamo_declaraciones_id_b63d3649_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_prestamo_inmueble_domicilios__1f840946_fk_declaraci` FOREIGN KEY (`inmueble_domicilios_id`) REFERENCES `declaracion_domicilios` (`id`),
  CONSTRAINT `declaracion_prestamo_observaciones_id_252fa429_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_prestamo_titular_infopersonal_cb34e238_fk_declaraci` FOREIGN KEY (`titular_infopersonalVar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_prestamo_titular_relacion_id_a35cb496_fk_declaraci` FOREIGN KEY (`titular_relacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_representaciones`
--

DROP TABLE IF EXISTS `declaracion_representaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_representaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `otra_representacion` varchar(255) NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `pagado` tinyint(1) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `es_representacion_activa` tinyint(1) DEFAULT NULL,
  `monto` decimal(13,2) DEFAULT NULL,
  `es_mueble` tinyint(1) DEFAULT NULL,
  `otro_mueble` varchar(255) DEFAULT NULL,
  `cat_moneda_id` int(11) DEFAULT NULL,
  `cat_tipos_muebles_id` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `cat_tipos_relaciones_personales_id` int(11) DEFAULT NULL,
  `cat_tipos_representaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `info_personal_var_id` int(11) NOT NULL,
  `observaciones_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_represen_cat_moneda_id_ede36c3f_fk_declaraci` (`cat_moneda_id`),
  KEY `declaracion_represen_cat_tipos_muebles_id_3d480067_fk_declaraci` (`cat_tipos_muebles_id`),
  KEY `declaracion_represen_cat_tipos_operacione_4fcd608c_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_represen_cat_tipos_relaciones_93e0bbf5_fk_declaraci` (`cat_tipos_relaciones_personales_id`),
  KEY `declaracion_represen_cat_tipos_representa_15700f00_fk_declaraci` (`cat_tipos_representaciones_id`),
  KEY `declaracion_represen_declaraciones_id_9fc9f340_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_represen_info_personal_var_id_c945bcb6_fk_declaraci` (`info_personal_var_id`),
  KEY `declaracion_represen_observaciones_id_acef1dc3_fk_declaraci` (`observaciones_id`),
  CONSTRAINT `declaracion_represen_cat_moneda_id_ede36c3f_fk_declaraci` FOREIGN KEY (`cat_moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_represen_cat_tipos_muebles_id_3d480067_fk_declaraci` FOREIGN KEY (`cat_tipos_muebles_id`) REFERENCES `declaracion_cattiposmuebles` (`id`),
  CONSTRAINT `declaracion_represen_cat_tipos_operacione_4fcd608c_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_represen_cat_tipos_relaciones_93e0bbf5_fk_declaraci` FOREIGN KEY (`cat_tipos_relaciones_personales_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`),
  CONSTRAINT `declaracion_represen_cat_tipos_representa_15700f00_fk_declaraci` FOREIGN KEY (`cat_tipos_representaciones_id`) REFERENCES `declaracion_cattiposrepresentaciones` (`id`),
  CONSTRAINT `declaracion_represen_declaraciones_id_9fc9f340_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_represen_info_personal_var_id_c945bcb6_fk_declaraci` FOREIGN KEY (`info_personal_var_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_represen_observaciones_id_acef1dc3_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_secciondeclaracion`
--

DROP TABLE IF EXISTS `declaracion_secciondeclaracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_secciondeclaracion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estatus` int(11) NOT NULL,
  `aplica` tinyint(1) NOT NULL,
  `max` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `observaciones_id` int(11) DEFAULT NULL,
  `seccion_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_secciond_declaraciones_id_d2a22dad_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_secciond_observaciones_id_0ac9ba06_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_secciond_seccion_id_809df765_fk_declaraci` (`seccion_id`),
  CONSTRAINT `declaracion_secciond_declaraciones_id_d2a22dad_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_secciond_observaciones_id_0ac9ba06_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_secciond_seccion_id_809df765_fk_declaraci` FOREIGN KEY (`seccion_id`) REFERENCES `declaracion_secciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=266 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_secciones`
--

DROP TABLE IF EXISTS `declaracion_secciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_secciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seccion` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `parametro` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_secciones_tree_id_bfed8a79` (`tree_id`),
  KEY `declaracion_secciones_parent_id_c75d16ef` (`parent_id`),
  CONSTRAINT `declaracion_seccione_parent_id_c75d16ef_fk_declaraci` FOREIGN KEY (`parent_id`) REFERENCES `declaracion_secciones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `declaracion_socioscomerciales`
--

DROP TABLE IF EXISTS `declaracion_socioscomerciales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaracion_socioscomerciales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actividad_vinculante` varchar(255) NOT NULL,
  `tipo_vinculo` varchar(255) NOT NULL,
  `antiguedad_vinculo` varchar(255) NOT NULL,
  `rfc_entidad_vinculante` varchar(255) NOT NULL,
  `porcentaje_participacion` decimal(5,2) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `otraParticipacion` varchar(20) DEFAULT NULL,
  `recibeRemuneracion` tinyint(1) DEFAULT NULL,
  `montoMensual` int(11) DEFAULT NULL,
  `cat_tipos_operaciones_id` int(11) DEFAULT NULL,
  `declaraciones_id` int(11) NOT NULL,
  `moneda_id` int(11) DEFAULT NULL,
  `observaciones_id` int(11) NOT NULL,
  `socio_infopersonalvar_id` int(11) NOT NULL,
  `tipoParticipacion_id` int(11) DEFAULT NULL,
  `tipoRelacion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `declaracion_sociosco_cat_tipos_operacione_59bc084d_fk_declaraci` (`cat_tipos_operaciones_id`),
  KEY `declaracion_sociosco_declaraciones_id_10a954b5_fk_declaraci` (`declaraciones_id`),
  KEY `declaracion_sociosco_moneda_id_6c366cd4_fk_declaraci` (`moneda_id`),
  KEY `declaracion_sociosco_observaciones_id_35bc50c6_fk_declaraci` (`observaciones_id`),
  KEY `declaracion_sociosco_socio_infopersonalva_d15f024c_fk_declaraci` (`socio_infopersonalvar_id`),
  KEY `declaracion_sociosco_tipoParticipacion_id_e90abc2d_fk_declaraci` (`tipoParticipacion_id`),
  KEY `declaracion_sociosco_tipoRelacion_id_f4e9806f_fk_declaraci` (`tipoRelacion_id`),
  CONSTRAINT `declaracion_sociosco_cat_tipos_operacione_59bc084d_fk_declaraci` FOREIGN KEY (`cat_tipos_operaciones_id`) REFERENCES `declaracion_cattiposoperaciones` (`id`),
  CONSTRAINT `declaracion_sociosco_declaraciones_id_10a954b5_fk_declaraci` FOREIGN KEY (`declaraciones_id`) REFERENCES `declaracion_declaraciones` (`id`),
  CONSTRAINT `declaracion_sociosco_moneda_id_6c366cd4_fk_declaraci` FOREIGN KEY (`moneda_id`) REFERENCES `declaracion_catmonedas` (`id`),
  CONSTRAINT `declaracion_sociosco_observaciones_id_35bc50c6_fk_declaraci` FOREIGN KEY (`observaciones_id`) REFERENCES `declaracion_observaciones` (`id`),
  CONSTRAINT `declaracion_sociosco_socio_infopersonalva_d15f024c_fk_declaraci` FOREIGN KEY (`socio_infopersonalvar_id`) REFERENCES `declaracion_infopersonalvar` (`id`),
  CONSTRAINT `declaracion_sociosco_tipoParticipacion_id_e90abc2d_fk_declaraci` FOREIGN KEY (`tipoParticipacion_id`) REFERENCES `declaracion_cattipopersona` (`id`),
  CONSTRAINT `declaracion_sociosco_tipoRelacion_id_f4e9806f_fk_declaraci` FOREIGN KEY (`tipoRelacion_id`) REFERENCES `declaracion_cattiposrelacionespersonales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `front_placeholder`
--

DROP TABLE IF EXISTS `front_placeholder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_placeholder` (
  `key` varchar(40) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `front_placeholderhistory`
--

DROP TABLE IF EXISTS `front_placeholderhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_placeholderhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` longtext NOT NULL,
  `saved` datetime(6) NOT NULL,
  `placeholder_id` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `front_placeholderhis_placeholder_id_4e606a11_fk_front_pla` (`placeholder_id`),
  CONSTRAINT `front_placeholderhis_placeholder_id_4e606a11_fk_front_pla` FOREIGN KEY (`placeholder_id`) REFERENCES `front_placeholder` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth2_provider_accesstoken`
--

DROP TABLE IF EXISTS `oauth2_provider_accesstoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_provider_accesstoken` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `expires` datetime(6) NOT NULL,
  `scope` longtext NOT NULL,
  `application_id` bigint(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `source_refresh_token_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth2_provider_accesstoken_token_8af090f8_uniq` (`token`),
  UNIQUE KEY `source_refresh_token_id` (`source_refresh_token_id`),
  KEY `oauth2_provider_accesstoken_user_id_6e4c9a65_fk_auth_user_id` (`user_id`),
  KEY `oauth2_provider_accesstoken_application_id_b22886e1_fk` (`application_id`),
  CONSTRAINT `oauth2_provider_acce_source_refresh_token_e66fbc72_fk_oauth2_pr` FOREIGN KEY (`source_refresh_token_id`) REFERENCES `oauth2_provider_refreshtoken` (`id`),
  CONSTRAINT `oauth2_provider_accesstoken_application_id_b22886e1_fk` FOREIGN KEY (`application_id`) REFERENCES `oauth2_provider_application` (`id`),
  CONSTRAINT `oauth2_provider_accesstoken_user_id_6e4c9a65_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth2_provider_application`
--

DROP TABLE IF EXISTS `oauth2_provider_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_provider_application` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(100) NOT NULL,
  `redirect_uris` longtext NOT NULL,
  `client_type` varchar(32) NOT NULL,
  `authorization_grant_type` varchar(32) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `skip_authorization` tinyint(1) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`),
  KEY `oauth2_provider_application_client_secret_53133678` (`client_secret`),
  KEY `oauth2_provider_application_user_id_79829054_fk_auth_user_id` (`user_id`),
  CONSTRAINT `oauth2_provider_application_user_id_79829054_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth2_provider_grant`
--

DROP TABLE IF EXISTS `oauth2_provider_grant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_provider_grant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `expires` datetime(6) NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  `scope` longtext NOT NULL,
  `application_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth2_provider_grant_code_49ab4ddf_uniq` (`code`),
  KEY `oauth2_provider_grant_application_id_81923564_fk` (`application_id`),
  KEY `oauth2_provider_grant_user_id_e8f62af8_fk_auth_user_id` (`user_id`),
  CONSTRAINT `oauth2_provider_grant_application_id_81923564_fk` FOREIGN KEY (`application_id`) REFERENCES `oauth2_provider_application` (`id`),
  CONSTRAINT `oauth2_provider_grant_user_id_e8f62af8_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth2_provider_refreshtoken`
--

DROP TABLE IF EXISTS `oauth2_provider_refreshtoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_provider_refreshtoken` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `access_token_id` bigint(20) DEFAULT NULL,
  `application_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `revoked` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `access_token_id` (`access_token_id`),
  UNIQUE KEY `oauth2_provider_refreshtoken_token_revoked_af8a5134_uniq` (`token`,`revoked`),
  KEY `oauth2_provider_refreshtoken_application_id_2d1c311b_fk` (`application_id`),
  KEY `oauth2_provider_refreshtoken_user_id_da837fce_fk_auth_user_id` (`user_id`),
  CONSTRAINT `oauth2_provider_refr_access_token_id_775e84e8_fk_oauth2_pr` FOREIGN KEY (`access_token_id`) REFERENCES `oauth2_provider_accesstoken` (`id`),
  CONSTRAINT `oauth2_provider_refreshtoken_application_id_2d1c311b_fk` FOREIGN KEY (`application_id`) REFERENCES `oauth2_provider_application` (`id`),
  CONSTRAINT `oauth2_provider_refreshtoken_user_id_da837fce_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sitio_declaracion_faqs`
--

DROP TABLE IF EXISTS `sitio_declaracion_faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitio_declaracion_faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden` int(11) NOT NULL,
  `pregunta` varchar(3000) NOT NULL,
  `respuesta` varchar(3000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sitio_sitio_personalizacion`
--

DROP TABLE IF EXISTS `sitio_sitio_personalizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitio_sitio_personalizacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_institucion` varchar(300) NOT NULL,
  `siglas_sistema` varchar(30) NOT NULL,
  `bg_color` varchar(50) NOT NULL,
  `color_danger` varchar(50) NOT NULL,
  `color_dark` varchar(50) NOT NULL,
  `color_info` varchar(50) NOT NULL,
  `color_light` varchar(50) NOT NULL,
  `color_primary` varchar(50) NOT NULL,
  `color_secondary` varchar(50) NOT NULL,
  `color_success` varchar(50) NOT NULL,
  `color_warning` varchar(50) NOT NULL,
  `font_color_primary` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-01 11:16:17
