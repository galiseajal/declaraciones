# -- coding: utf-8 --
import requests
import time
import pytest
import random
import string
import pprint
import json

pytest.host = "http://189.213.134.151:8080/api/"
#pytest.host = "http://54.173.200.251/api/"
#pytest.host = "http://189.213.134.151:8080/api/"
pytest.tokenbearer = "BUP4MkZLQoDQKMzJvNigSK1zMAvf9S"
pytest.TIME_TO_PASS = 5
@pytest.fixture
def gettoken():
    data = {
        #"grant_type": "authorization_code",
        #"code": "1xWpREheoai3frZDE6vaWHNp7FYkLu",
        "grant_type": "password",
        "client_id":"client_id",
        "client_secret":"client_secret",
        "scope":"read write",
        "username":"plataforma",
        "password":"digitalnacional"
    }
    response = requests.post(
            url= "http://189.213.134.151:8080/o/token/",
        data=data
    ).json()
    
    tokenbearer = response["access_token"]
    pytest.tokenbearer = tokenbearer

@pytest.mark.test_1
def test01(gettoken):
    data = {
        #"grant_type": "authorization_code",
        #"code": "1xWpREheoai3frZDE6vaWHNp7FYkLu",
        "grant_type": "password",
        "client_id":"client_id",
        "client_secret":"client_secret",
        "scope":"read write",
        "username":"plataforma",
        "password":"digitalnacional"
    }
    response = requests.post(
        url= "http://189.213.134.151:8080/o/token/",
        data=data
    ).json()

    
    assert "refresh_token" in response
    assert "access_token" in response
    assert "token_type" in response
    assert "expires_in" in response


@pytest.mark.test_3
def test_03():
    expires_in=pytest.TIME_TO_PASS
    time.sleep(expires_in)
    data = {
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    }
    
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    )
    print(str(response))
    try:
        print("Prueba 3")
        response=response.json()
        app_json = json.dumps(response, sort_keys=True)
    except (TypeError, OverflowError):
      print('ERROR!!!!'+str(response))
    f= open("guru03.json","w+")
    f.write(str(app_json))
    f.close()
    '''assert "pagination" in response
    pagination = response["pagination"]
    assert "pageSize" in pagination
    assert "page" in pagination
    assert "totalRows" in pagination
    assert "hasNextPage" in pagination
    assert "results" in response
    results = response["results"]
    assert type(results) == list
    assert len(response["results"]) > 0
    result = response["results"][0]
    assert "id" in result
    assert "metadata" in result
    assert "declaracion" in result'''


@pytest.mark.test_4
def test_04():
    """ 
    1 .- This depends on the loaded data...
    2 .- size of the page... if only the first
    10th elemnts are not of that type this 
    would fail...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    # Declaraciones.objects.filter(cat_tipos_declaracion__codigo="MODIFICACIÓN")
    Declaraciones.objects.filter(cat_tipos_declaracion__codigo="Modificada")
    # Crear una...
    CatTiposDeclaracion.objects.get(codigo="Modificada") 
    """
    expires_in=pytest.TIME_TO_PASS
    time.sleep(expires_in)
    data = {}
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    )

    print('444444 '+str(response))
    try:
        response=response.json()
        app_json = json.dumps(response, sort_keys=True)
    except (TypeError, OverflowError):
      print('ERROR!!!!'+str(response))

    f= open("guru04.json","w+")
    f.write(str(app_json))
    f.close()
    for result in response["results"]:
        if "tipo" in result["metadata"]:
            pass
        pass
    pass


@pytest.mark.test_5
def test_05():
    """ 
    1 .- This depends on the loaded data...
    2 .- size of the page... if only the first
    10th elemnts are not of that type this 
    would fail...
    from declaracion.models.informacion_personal import Declaraciones
    from declaracion.models.catalogos import CatTiposDeclaracion    
    # Declaraciones.objects.filter(cat_tipos_declaracion__codigo="MODIFICACIÓN")
    Declaraciones.objects.filter(cat_tipos_declaracion__codigo="Modificada")
    # Crear una...
    CatTiposDeclaracion.objects.get(codigo="Conclusión") 
    """
    expires_in=pytest.TIME_TO_PASS
    time.sleep(expires_in)
    data = {}
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    )
    print('05>>>'+str(response))
    try:
        response= response.json()
        app_json = json.dumps(response, sort_keys=True)
    except (TypeError, OverflowError):
      print('ERROR!!!!'+str(response))
    f= open("guru05.json","w+")
    f.write(str(app_json))
    f.close()
    for result in response["results"]:
        if "tipo" in result["metadata"]:
            pass
        pass
    pass

@pytest.mark.test_6
def test_06():
    """ 
    send "a next page"

    """
    expires_in=pytest.TIME_TO_PASS
    time.sleep(expires_in)

    data = {}
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    )
    try:
        print('06'+str(response))
        response=response.json()
        app_json = json.dumps(response, sort_keys=True)
    except (TypeError, OverflowError):
      print('ERROR!!!!'+str(response))
    f= open("guru06.json","w+")
    f.write(str(app_json))
    f.close()
    assert "pagination" in response
    pagination = response["pagination"]

    assert "totalRows" in pagination
    total_rows = int(pagination["totalRows"])

    if total_rows > 10:
        page_calculado = (total_rows/10) + 1
    else:
        page_calculado = 1

    data = {
        "query":{
            "page": page_calculado
        }
    }
    print(data)

    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    pagination2 = response2["pagination"]
    print(pagination2)
    assert(pagination2["page"] == page_calculado)



@pytest.mark.test_7
def test_07():
    """ 
    send "diff page sizes"

    """

    expires_in=pytest.TIME_TO_PASS
    time.sleep(expires_in)

    data = {
        "pageSize": 1
    }
    headers = {
        "Authorization": "Bearer {}".format(pytest.tokenbearer)
    } 
    response = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    )

    try:
        print('no se cual'+str(response))
        response=response.json()
        app_json = json.dumps(response, sort_keys=True)
    except (TypeError, OverflowError):
      print('ERROR!!!!'+str(response))
    f= open("guru07.json","w+")
    f.write(str(app_json))
    f.close()
    data = {
        "pageSize": 15
    }
    response2 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    data = {
        "pageSize": 30
    }
    response3 = requests.post(
        url= pytest.host + "v2/declaraciones/",
        json=data,
        headers=headers
    ).json()

    assert "pagination" in response
    pagination =  response["pagination"]
    assert "pagination" in response2
    pagination2 = response2["pagination"]
    assert "pagination" in response3
    pagination3 = response3["pagination"]

    assert pagination["pageSize"] == 1
    assert pagination2["pageSize"] == 15
    assert pagination3["pageSize"] == 30

